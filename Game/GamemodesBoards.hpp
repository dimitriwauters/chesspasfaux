// INFO-F209 - Groupe 5 - GamemodesBoards.hpp

#ifndef PARTIE2_GAMEMODESBOARDS_HPP
#define PARTIE2_GAMEMODESBOARDS_HPP

#include "Board.hpp"

class BoardDark : public Board {
public:
    BoardDark();
    ~BoardDark();

};

class BoardDice : public Board {
public:
    BoardDice();
    ~BoardDice();
};

class BoardHorde : public Board {
public:
    BoardHorde();
    void placePieces(bool) override;
    ~BoardHorde();
};

#endif //PARTIE2_GAMEMODESBOARDS_HPP
