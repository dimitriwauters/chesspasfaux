// INFO-F209 - Groupe 5 - Board.hpp

#ifndef BOARD_HPP
#define BOARD_HPP

#include <cstring>
#include <ncurses.h>
#include <tgmath.h>
#include "Network.hpp"
#include "Pieces.hpp"

class Board
{
  protected:
    AbstractPiece* board_mat[MAX_ROWS][MAX_COLS];
    bool virtual_move = false;
    WINDOW* cases[MAX_ROWS][MAX_COLS];
  public:
    Board();
    virtual ~Board();
    // Add an remove pieces
    virtual void placePieces(bool);
    virtual bool addPiece(int, std::pair<int, int>, bool);
    virtual void removeAllPieces();
    virtual bool removePiece(std::pair<int, int>);
    // Information about pieces (getters)
    std::string getTypePiece(std::pair<int, int>) const;
    unsigned getTypePieceNbr(std::pair<int, int>) const;
    bool getColorPiece(std::pair<int, int>) const;
    bool hasMoved(std::pair<int, int>) const;
    std::vector<std::pair<int, int>> getAllMoves(std::pair<int, int> coord);
    // Moves
    void setFirstMovePiece(std::pair<int, int>);
    virtual void setNewPosition(std::pair<int, int>, std::pair<int, int>);
    unsigned whichMove(std::pair<int, int>, std::pair<int, int>);
    void doVirtualMove(bool);
    bool isVirtualMove();
    bool isInRange(std::pair<int, int>);
    bool isLegalMovePiece(std::pair<int, int>, std::pair<int, int>);
    bool noPiecesBetween(std::pair<int, int>, std::pair<int, int>);
    bool isEmpty(std::pair<int, int>);
    bool rangeEmpty(int, int, int, int, int, int, bool);
    bool noDiagonalJump(std::pair<int, int>, int, int, int);
    virtual bool isSpecialMove(std::pair<int, int>);
};

#endif
