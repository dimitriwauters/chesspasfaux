// INFO-F209 - Groupe 5 - network.hpp

#ifndef PARTIE2_NETWORK_HPP
#define PARTIE2_NETWORK_HPP

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <pthread.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>
#include "const.hpp"

typedef struct sockaddr_in SockaddrIn;
typedef struct sockaddr Sockaddr;

	//Server
void initialiseSocket(SockaddrIn*, uint16_t);
void setServer(SockaddrIn*, uint16_t);
void setHost(char*, SockaddrIn*, uint16_t);
void binding(int*, SockaddrIn*);
void listening(int*);
void accepting(int*, SockaddrIn*, int*);
void connecting(int*, SockaddrIn*);
int i_number(int, char*);
void closeSocket(int);
	//Sending
bool sending_T(int, char*);
bool sending(int, std::string);
bool sending(int, int);
char* prepForSending(int, std::string);
	//Receiving
bool receiving_T(int, char[]);
bool receiving(int, char[]);
bool receiving(int, std::string*);
bool receiving(int, int*);

#endif //PARTIE2_NETWORK_HPP
