// INFO-F209 - Groupe 5 - Game.cpp

#include "Game.hpp"

template <typename B>
AbstractGame<B>::AbstractGame(Player* player1, Player* player2, int gamemode, \
    int type): turn(WHITE), gamemode(gamemode), type(type)
{
    player[WHITE] = player1;
    player[BLACK] = player2;
}

template <typename B>
AbstractGame<B>::~AbstractGame() = default;

template <typename B>
void AbstractGame<B>::setPiecesOnBoard()
// Set pieces on the chessboard.
{
    if (pieces_beginning.size() == 0) // New game
    {
        board->placePieces(BLACK);
        board->placePieces(WHITE);
        setKingPosition(WHITE, std::make_pair(MAX_ROWS-1, 4));
        setKingPosition(BLACK, std::make_pair(0, 4));
    }
    else // Paused game
    {
        for (int i = 0; i < static_cast<int>(pieces_beginning.size()); ++i)
        {
            std::string piece = pieces_beginning.at(i);
            int x_pos = std::stoi(piece.substr(0, 1));
            int y_pos = std::stoi(piece.substr(1, 1));
            int piece_type = std::stoi(piece.substr(2, 1));
            bool piece_color = static_cast<bool>(std::stoi(piece.substr(3, 1)));
            board->addPiece(piece_type, std::make_pair(x_pos, y_pos), piece_color);
            if (piece_type == KING_NBR) setKingPosition(piece_color, std::make_pair(x_pos, y_pos));
        }
    }
}

template <typename B>
std::pair<Player*, Player*> AbstractGame<B>::getPlayers()
// Return a tuple of players.
{
    return std::make_pair(player[WHITE], player[BLACK]);
}

////////////////////////////////////////////////////////////////////////////////
// RULES & WIN CONDITIONS
////////////////////////////////////////////////////////////////////////////////

template <typename B>
void AbstractGame<B>::swapTurn()
// Change the turn.
{
    turn = (turn == WHITE) ? BLACK : WHITE;
}

template <typename B>
bool AbstractGame<B>::notOver()
// Check if the game is over (following the chess rules).
{
    return (running_game && (!(player[WHITE]->isWinner() || player[BLACK]->isWinner())));
}

template <typename B>
bool AbstractGame<B>::canDeleteGame()
// Check if the game could be deleted.
{
    return finished;
}

template <typename B>
void AbstractGame<B>::gameFinished()
// Mark the game as totally ended (it means that it could be deleted).
{
    finished = true;
}

template <typename B>
void AbstractGame<B>::setWinner(bool color)
// Set a winner.
{
  player[color]->hasWon();
}

template <typename B>
Player* AbstractGame<B>::getWinner()
// Return the player who is winner.
{
    if (player[WHITE]->isWinner()) return player[WHITE];
    else if (player[BLACK]->isWinner()) return player[BLACK];
    else return nullptr;
}

template <typename B>
void AbstractGame<B>::checkWinConditions(bool player_turn)
// Check if a win condition is occurring (after a move).
{
    bool opponent_player = !player_turn;
    // Has the player whose turn it is moved out of check?
    if (!this->isUnderAttack(this->getKingPosition(player_turn), opponent_player))
    {
        this->setCheck(player_turn, false);
    }
    // Is the opponent player's king now in check?
    if (this->isUnderAttack(this->getKingPosition(opponent_player), player_turn))
    {
        this->setCheck(opponent_player, true);
    }
    // Is the opponent player's now in stalemate?
    if (!this->getCheck(opponent_player))
    {
        if (this->isInStalemate(opponent_player)) running_game = false;
    }
    // Is the opponent player's now in checkmate?
    if (this->getCheck(opponent_player))
    {
        if(this->isInCheckMate(opponent_player)) this->setWinner(player_turn);
    }
}

////////////////////////////////////////////////////////////////////////////////
// MOVES
////////////////////////////////////////////////////////////////////////////////

template <typename B>
bool AbstractGame<B>::movePiece(std::pair<int, int> src_pos, std::pair<int, int> dest_pos, bool player_color)
// Move a piece if it is legal. Return the type of the move processed.
{
    unsigned move_type = this->checkMove(src_pos, dest_pos, player_color);
    return movePiece(src_pos, dest_pos, move_type);
}

template <typename B>
unsigned AbstractGame<B>::checkMove(std::pair<int, int> src_pos, std::pair<int, int> dest_pos, bool player_turn)
// Check if the move conditions are satisfied (i.e. if the move from src_pos to
// dest_pos is legal). Return the type of the move involved.
{
    unsigned move = MOVE_NOT_ALLOWED;
    if (!board->isEmpty(src_pos) && board->isInRange(dest_pos))
    {
        if (board->isLegalMovePiece(src_pos, dest_pos))
        // The move has to correspond to the particular moves of the selected piece
        {
            if (board->noPiecesBetween(src_pos, dest_pos))
            {
                if (board->isEmpty(dest_pos)) // Empty square
                {
                    // En passant's move
                    if ((board->getTypePiece(src_pos) == PAWN) &&
                    (abs(src_pos.first-dest_pos.first) == \
                    abs(src_pos.second-dest_pos.second))) return enPassant(src_pos, dest_pos);
                    move = isLegalMoveAllowed(src_pos, dest_pos, player_turn);
                }
                else // Occupied square
                {
                    if (board->getColorPiece(src_pos) != board->getColorPiece(dest_pos))
                    {
                        // Restrict the pawn's move
                        if ((board->getTypePiece(src_pos) == PAWN) && \
                        (abs(src_pos.first - dest_pos.first) == 1) \
                        && (src_pos.second == dest_pos.second)) return move = MOVE_NOT_ALLOWED;
                        move = isLegalMoveAllowed(src_pos, dest_pos, player_turn);
                    }
                }
            }
        }
        else if (board->isSpecialMove(src_pos)) // Is the move a special move?
        {
            // Castling
            if ((board->getTypePiece(src_pos) == KING) && (!isUnderAttack(dest_pos, !player_turn)))
            {
                unsigned which_castling = this->canCastle(src_pos, dest_pos, this->turn);
                if (which_castling == 1) move = MOVE_CASTLING_LONG;
                else if (which_castling == 2) move = MOVE_CASTLING_SHORT;
            }
        }
    }
    return move;
}

template <typename B>
unsigned AbstractGame<B>::isLegalMoveAllowed(std::pair<int, int> src_pos, std::pair<int, int> dest_pos, bool player_turn)
// Check if a legal move could be allowed (i.e. if the king is not in check, if we do not have a promotion, etc.).
{
    unsigned move = MOVE_ALLOWED;
    if (willBeInCheck(src_pos, dest_pos, player_turn)) move = MOVE_NOT_ALLOWED;
    else if ((board->getTypePiece(src_pos) == PAWN) && (dest_pos.first == 7 || \
        dest_pos.first == 0)) move = MOVE_PROMOTION;
    return move;
}

template <typename B>
std::vector<std::pair<int, int>> AbstractGame<B>::getAllMoves(std::pair<int, int> coord, bool playerTurn)
// Get all legal moves of a piece (whose position is given by coord).
{
    std::vector<std::pair<int, int>> moves = this->board->getAllMoves(coord);
    std::vector<std::pair<int, int>> confirmed_moves;
    if(!moves.empty()) {
        for(unsigned i = 0; i < moves.size(); ++i) {
            if(this->board->isInRange(moves[i])) {
                if(this->checkMove(coord, moves[i], playerTurn)) {
                    confirmed_moves.emplace_back(moves[i]);
                }
            }
        }
    }
    return confirmed_moves;
}

template <typename B>
std::vector<std::pair<int, int>> AbstractGame<B>::getAllMovesAllPieces(bool color)
{
    std::vector<std::pair<int, int>> res;
    std::pair<int, int> square;
    for (int i = 0; i < MAX_COLS; i++) {
        for (int j = 0; j < MAX_ROWS; j++) {
            square = std::make_pair(i, j);
            if (!this->board->isEmpty(square)) {
                if (this->board->getColorPiece(square) == color) {
                    std::vector<std::pair<int, int>> moves = this->getAllMoves(square, color);
                    res.insert(res.end(), moves.begin(), moves.end());
                    res.emplace_back(std::make_pair(i, j));
                }
            }
        }
    }
    return res;
}

template <typename B>
bool AbstractGame<B>::movePiece(std::pair<int, int> src_pos, std::pair<int, int> dest_pos, unsigned canMove)
// Move a piece by updating the board.
{
    bool res = false;
    if(canMove != MOVE_NOT_ALLOWED) {
        switch(canMove) {
            case MOVE_ALLOWED: {
                if(!board->isEmpty(dest_pos)) {
                    capturePiece(dest_pos);
                    no_captured_pieces = -1;
                }
                break;
            }
            case MOVE_EN_PASSANT: {
                std::pair<int, int> cell = std::make_pair(src_pos.first, dest_pos.second);
                board->removePiece(cell);
                no_captured_pieces = -1;
                break;
            }
            case MOVE_CASTLING_LONG: {
                setCastlingForRook(std::make_pair(src_pos.first, 0), std::make_pair(src_pos.first, 3));
                break;
            }
            case MOVE_CASTLING_SHORT: {
                setCastlingForRook(std::make_pair(src_pos.first, 7), std::make_pair(src_pos.first, 5));
				break;
			}
        }
        board->setFirstMovePiece(src_pos);
        board->setNewPosition(src_pos, dest_pos);
        setLastMove(dest_pos);
        no_captured_pieces++;
        // If the king has moved, we update its position
        if (board->getTypePiece(dest_pos) == KING) setKingPosition(board->getColorPiece(dest_pos), dest_pos);
        res = true;
    }
    return res;
}

////////////////////////////////////////////////////////////////////////////////
// SPECIAL MOVES
////////////////////////////////////////////////////////////////////////////////

// En passant

template <typename B>
unsigned AbstractGame<B>::enPassant(std::pair<int, int> src_pos, std::pair<int, int> dest_pos)
// Check if a "prise en passant" can occur.
{
    std::pair<int, int> cell;
    cell = std::make_pair(src_pos.first, dest_pos.second);
    if (compareLastMove(cell) && (board->getTypePiece(cell) == PAWN) && ((board->getTypePiece(src_pos)) == PAWN))
    {
        if (((board->getColorPiece(src_pos) == WHITE) && dest_pos.first == 2) || \
         ((board->getColorPiece(src_pos) == BLACK) && dest_pos.first == 5)) \
         return MOVE_EN_PASSANT;
    }
    return MOVE_NOT_ALLOWED;
}

template <typename B>
void AbstractGame<B>::setLastMove(std::pair<int, int> move)
{
	last_move.first = move.first;
	last_move.second = move.second;
}

template <typename B>
bool AbstractGame<B>::compareLastMove(std::pair<int, int> move)
{
	return last_move.first == move.first && last_move.second == move.second;
}

// Castling

template <typename B>
unsigned AbstractGame<B>::canCastle(std::pair<int, int> src_pos, std::pair<int, int>\
     dest_pos, bool player_color)
// Check if a castling can occur (i.e.: if the king is not in check; the squares
// between the pieces available and not threatened).
{
    unsigned castling_type = 0, x = 0, y = 0;
    if (!(src_pos.first-dest_pos.first) && !getCheck(player_color))
    // Pieces on the same rank and the king not in check
    {
        for (int i = src_pos.second+1; i < dest_pos.second; i++)
        {
            x++;
            // Squares between src_pos and dest_pos cannot be under attack
            if (!isUnderAttack(std::make_pair(src_pos.first, i), !player_color)) y++;
        }
        if (x == y) castling_type = whichCastling(src_pos, dest_pos);
    }
    return castling_type;
}

template <typename B>
unsigned AbstractGame<B>::whichCastling(std::pair<int, int> src_pos, std::pair<int, int> dest_pos)
// Identify the type of castling involved (castling long or castling short).
{
    unsigned res = MOVE_NOT_ALLOWED;
    if (dest_pos.second == 2) // Castling long (queenside)
    {
        // Are the squares between the rook and the king free?
        if (this->board->rangeEmpty(NO_DEFINED, 1, 4, src_pos.first, src_pos.second, -1, true))
        {
            std::pair<int, int> rook_pos = std::make_pair(src_pos.first, 0);
            if ((!this->board->isEmpty(rook_pos)) && (!this->board->hasMoved(rook_pos)))
            {
                res = 1;
            }
        }
    }
    else if (dest_pos.second == 6) // Castling short (kingside)
    {
        if (this->board->rangeEmpty(NO_DEFINED, 1, 3, src_pos.first, src_pos.second, 1, true))
        {
            std::pair<int, int> rook_pos = std::make_pair(src_pos.first, 7);
            if ((!this->board->isEmpty(rook_pos)) && (!this->board->hasMoved(rook_pos)))
            {
                res = 2;
            }
        }
    }
    return res;
}

template <typename B>
void AbstractGame<B>::setCastlingForRook(std::pair<int, int> first_rook_pos, \
    std::pair<int, int> new_rook_pos)
// Update the rook status and position on the board (in the case of a castling).
{
    board->setFirstMovePiece(first_rook_pos);
    board->setNewPosition(first_rook_pos, new_rook_pos);
}

// Promotion

template <typename B>
void AbstractGame<B>::promote(std::pair<int, int> coord, bool color, int user_choice)
// Do the promotion of the pawn: remove it and replace it by the piece which was
// chosen by the user.
{
  board->removePiece(coord);
  switch (user_choice)
  {
    case QUEEN_NBR:
        board->addPiece(QUEEN_NBR, coord, color);
        break;
    case ROOK_NBR:
        board->addPiece(ROOK_NBR, coord, color);
        break;
    case BISHOP_NBR:
        board->addPiece(BISHOP_NBR, coord, color);
        break;
    case KNIGHT_NBR:
        board->addPiece(KNIGHT_NBR, coord, color);
        break;
  }
}

////////////////////////////////////////////////////////////////////////////////
// CAPTURE
////////////////////////////////////////////////////////////////////////////////

template <typename B>
void AbstractGame<B>::capturePiece(std::pair<int, int> dest_pos)
// Remove a piece at position coord by calling the appropriate board's function.
{
    if (board->getTypePiece(dest_pos) == KING)
    {
        setKingPosition(board->getColorPiece(dest_pos), std::make_pair(-1, -1));
    }
    board->removePiece(dest_pos);
}

template <typename B>
bool AbstractGame<B>::canCapture(std::pair<int, int> src_pos, std::pair<int, int> dest_pos)
// Check if a move from src_pos to dest_pos allows an attack (i.e. the move is
// legal and there are no pieces in the range).
{
    if(board->getTypePiece(src_pos) == PAWN)
    {
        // If the piece is a pawn and its move is diagonal, it can capture a piece
        return abs(src_pos.first-dest_pos.first) == 1 && abs(src_pos.second-dest_pos.second) == 1;
    }
    return ((board->isLegalMovePiece(src_pos, dest_pos)) && (board->noPiecesBetween(src_pos, dest_pos)));
}

template <typename B>
bool AbstractGame<B>::isUnderAttack(std::pair<int, int> current_pos, bool opponent_player)
// Check if a piece (at the position current_pos) is attacked by any opponent's pieces.
{
    std::pair<int, int> piece_pos = std::make_pair(current_pos.first, current_pos.second);
    int i, j;
    for (i = 0; i < MAX_ROWS; i++) {
        for (j = 0; j < MAX_COLS; j++) {
            std::pair<int, int> square = std::make_pair(i, j);
            if (!board->isEmpty(square)) {
                if (board->getColorPiece(square) == opponent_player) {
                    // Can the opponent's piece attack from square the piece located
                    // at piece_pos?
                    if (canCapture(square, piece_pos)) return true;
                }
            }
        }
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////
// STALEMATE, CHECK & CHECKMATE
////////////////////////////////////////////////////////////////////////////////

// Stalemate

template <typename B>
bool AbstractGame<B>::isInStalemate(bool player_color)
// Check if there is a stalemate situation for the given player color (black/white).
{
    if (getCheck(player_color)) return false;
    int i, j, nb_left_pieces = 0, nb_blocked_pieces = 0;
    std::pair<int, int> my_piece;
    for (i = 0; i < MAX_COLS; i++) {
        for (j = 0; j < MAX_ROWS; j++) {
            my_piece = std::make_pair(i, j);
            if (!board->isEmpty(my_piece)) { // If there is a piece on this square
                if (board->getColorPiece(my_piece) == player_color) {
                    // ...and if the piece belongs to the player whose color is given
                    nb_left_pieces++;
                    std::vector<std::pair<int, int>> moves = \
                    this->getAllMoves(my_piece, player_color);
                    if (moves.empty()) nb_blocked_pieces++;
                }
            }
        }
    }
    // Has the player no legal move at all?
    return (nb_blocked_pieces == nb_left_pieces);
}

// Check

template <typename B>
std::pair<int, int> AbstractGame<B>::getKingPosition(bool color) const
// Return the king coord of the given player color (black/white).
{
    return king_position[color];
}

template <typename B>
void AbstractGame<B>::setKingPosition(bool color, std::pair<int, int> new_pos)
// Set and update the king coord of the given player color (black/white).
{
    king_position[color] = new_pos;
}

template <typename B>
void AbstractGame<B>::setCheck(bool color, bool checked)
// Mark a check of the given player color (black/white).
{
	check[color] = checked;
}

template <typename B>
bool AbstractGame<B>::getCheck(bool color) const
// Return the check "state" of the given player color (black/white).
{
	return check[color];
}

template <typename B>
bool AbstractGame<B>::willBeInCheck(std::pair<int, int> src_pos, \
    std::pair<int, int> dest_pos, bool player_turn)
// Check if a move results in a position where the king is in check. For that,
// we do a virtual move in order to protect the king and check if this one is
// still in check.
{
    unsigned type_piece_removed;
    bool color_piece_removed, check, readd_piece = false;
    board->doVirtualMove(true);
    if (!board->isEmpty(dest_pos)) // Move to an occupied square
    {
		if (board->getTypePiece(src_pos) == PAWN && (abs(src_pos.first-dest_pos.first) == 1 \
        || abs(src_pos.first-dest_pos.first) == 2) && (src_pos.second == dest_pos.second))
			return true;
        // Saving information about a piece that we are removing (virtually)
        type_piece_removed = board->getTypePieceNbr(dest_pos);
        color_piece_removed = board->getColorPiece(dest_pos);
        board->removePiece(dest_pos);
        readd_piece = true;
    }
    else // Move to an empty square
    {
		if (board->getTypePiece(src_pos) == PAWN && \
        ((abs(src_pos.first-dest_pos.first) == 1) && \
        (abs(src_pos.second-dest_pos.second) == 1)))
			return true;
	}
    board->setNewPosition(src_pos, dest_pos);
    if (board->getTypePiece(dest_pos) == KING) this->setKingPosition(player_turn, dest_pos);
    // Is the king still in check after this move?
    check = isUnderAttack(this->getKingPosition(player_turn), !player_turn);
    // Undo the move
    board->setNewPosition(dest_pos, src_pos);
    if (board->getTypePiece(src_pos) == KING) this->setKingPosition(player_turn, src_pos);
    // Undo the remove
    if (readd_piece) board->addPiece(type_piece_removed, dest_pos, color_piece_removed);
    board->doVirtualMove(false);
    return check;
}

// Checkmate

template <typename B>
bool AbstractGame<B>::isInCheckMate(bool player_color)
// Check if there is a checkmate (i.e. if there is no legal move possible to save the king).
{
    bool res = true;
    for (int i = 0; i < MAX_ROWS; i++)
    {
        for (int j = 0; j < MAX_COLS; j++)
        {
            std::pair<int, int> square = std::make_pair(i, j);
            if (!board->isEmpty(square))
            {
                if (board->getColorPiece(square) == player_color)
                {
                    for (int k = 0; k < MAX_ROWS; k++)
                    {
                        for (int l = 0; l < MAX_COLS; l++)
                        {
                            // A move is still available: we do not have a checkmate
                            if (this->checkMove(square, std::make_pair(k, l), \
                            player_color) != MOVE_NOT_ALLOWED) res = false;
                        }
                    }
                }
            }
        }
    }
    return res;
}


////////////////////////////////////////////////////////////////////////////////
// CONVERSIONS: TUPLE <-> STR, INT <-> STR
////////////////////////////////////////////////////////////////////////////////

template <typename B>
std::string AbstractGame<B>::getChessColumn(int col)
{
    switch (col)
    {
        case 0: return COL_A;
        case 1: return COL_B;
        case 2: return COL_C;
        case 3: return COL_D;
        case 4: return COL_E;
        case 5: return COL_F;
        case 6: return COL_G;
        case 7: return COL_H;
    }
    return nullptr;
}

template <typename B>
std::string AbstractGame<B>::getChessRank(int rank)
{
    int res = MAX_ROWS-rank;
    return std::to_string(res);
}

template <typename B>
std::string AbstractGame<B>::pairToString(std::pair<int, int> src, \
    std::pair<int, int> dest)
{
    return std::to_string(src.first) + std::to_string(src.second) + \
    std::to_string(dest.first) + std::to_string(dest.second);
}

template <typename B>
std::pair<std::pair<int, int>, std::pair<int, int>> AbstractGame<B>::stringToPair\
(char* buffer)
{
	std::string string(buffer);
    std::pair<int, int> src;
    std::pair<int, int> dest;
    src.first = std::stoi(string.substr(0, 1));
    src.second = std::stoi(string.substr(1, 1));
    dest.first = std::stoi(string.substr(2, 1));
    dest.second = std::stoi(string.substr(3, 1));
    return {src, dest};
}

////////////////////////////////////////////////////////////////////////////////
// OVERRIDE
////////////////////////////////////////////////////////////////////////////////

template <typename B>
void AbstractGame<B>::rearrangeBoard() {}

template <typename B>
void AbstractGame<B>::rollTheDices() {}

template <typename B>
std::pair<int, int> AbstractGame<B>::getDices()
{
	return std::make_pair(0, 0);
}

template <typename B>
void AbstractGame<B>::setDices(std::pair<int, int> new_dices)
{
	if (new_dices.first == -1 && new_dices.second == -1)
    {
		new_dices.first = 0; new_dices.second = 0;
	}
}
