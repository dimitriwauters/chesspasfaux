// INFO-F209 - Groupe 5 - db.hpp

#include <iostream>
#include <sqlite3.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <ctime>
#include "const.hpp"

	//Data Base
int initializeDB();
void selectDB(std::string* data, std::string champ, std::string table, std::string where, std::string where_res);
void selectDBLimit(std::vector<std::pair<std::string, int>>* data, std::string champ, std::string table, std::string order, int limit);
void countDB(std::string* data, std::string table, std::string where, std::string where_res);
void updateDB(std::string champ, std::string champ_res, std::string table, std::string where, std::string where_res);
void updateDBInc(std::string champ, std::string table, std::string where, std::string where_res, int n);
	//Players & Friends
void insertPlayerDB(std::string pseudo, std::string password);
void insertFriendsDB(std::string friend1, std::string friend2);
void acceptFriendsDB(std::string friend1, std::string friend2);
void deleteFriendsDB(std::string friend1, std::string friend2);
void getFriendsDB(std::string* data, std::string player_id, std::string status);
	//Histo
unsigned insertMatchDB(std::string player1, std::string player2, std::string winner, bool ended, int game_mode);
unsigned insertMatchDB(std::string player1, std::string player2, std::string winner, bool ended, int game_mode, time_t date);
void getMatchDB(std::vector<std::vector<std::string>>* data, std::string pseudo);
void getMatchDB(std::pair<std::string, std::string>* data, std::string game_id, int* game_mode);
void deleteMatchDB(std::string);
	//Saved Game
void insertSavedGameDB(unsigned game_id, int x_pos, int y_pos, int piece_type, bool color_piece);
void getSavedGameDB(std::vector<std::string>* data, std::string game_id);
void deleteSavedGameDB(std::string);
