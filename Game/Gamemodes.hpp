// INFO-F209 - Groupe 5 - Gamemodes.hpp

#ifndef PARTIE2_GAMEMODES_HPP
#define PARTIE2_GAMEMODES_HPP

#include <ctime>
#include <algorithm>
#include "GamemodesBoards.hpp"
#include "Game.hpp"

template <typename B>
class GameDefault : public AbstractGame<B> {
public:
    GameDefault(Player*, Player*, int);
    ~GameDefault();
};



template <typename B>
class GameDark : public AbstractGame<B> {
public:
    GameDark(Player*, Player*, int);
    void rearrangeBoard() override;
    void checkWinConditions(bool) override;
    void capturePiece(std::pair<int, int>) override;
    bool willBeInCheck(std::pair<int, int>, std::pair<int, int>, bool) override;
    unsigned canCastle(std::pair<int, int>, std::pair<int, int>, bool) override;
    ~GameDark();
};



template <typename B>
class GameDice : public AbstractGame<B> {
public:
    GameDice(Player*, Player*, int);
    ~GameDice();
    void rollTheDices() override;
    std::pair<int, int> getDices() override;
    void setDices(std::pair<int, int>) override;
    unsigned checkMove(std::pair<int, int>, std::pair<int, int>, bool) override;
    void checkWinConditions(bool) override;
    void capturePiece(std::pair<int, int>) override;
private:
	std::pair<int, int> dices = std::make_pair(0,0);
};



template <typename B>
class GameHorde : public AbstractGame<B> {
private:
    int nb_black_pawns = HORDE_NB_PAWNS;
public:
    GameHorde(Player*, Player*, int);
    void setPiecesOnBoard() override;
    void capturePiece(std::pair<int, int>) override;
    void checkWinConditions(bool) override;
    bool isInStalemate(bool) override;
    ~GameHorde();
};

#include "Gamemodes.cpp"

#endif //PARTIE2_GAMEMODES_HPP
