// INFO-F209 - Groupe 5 - Menu.cpp

#include "menu.hpp"

GameMenu::GameMenu(int fd, char* hostname) : _socketfd(fd), hostname(hostname) {}

long int GameMenu::charToInt(char* str) {
    long int intStr;
    char* end;
    intStr = strtol(str, &end, 10);
    return intStr;
}

//////////////////////////////////////////////////////////////////
// AUTHENTIFICATION
//////////////////////////////////////////////////////////////////

bool GameMenu::startAuthMenu() {
    // First Menu, which allows the user to sign in or sign up
    int choice;
    bool _isAth = false, _hasQuit = false;
    do {
        system("clear");
        std::cout << MENU_GAME_NAME "\n\n";
        std::cout << FIRST_DESCRIPTION;
        std::cout << MENU_SELECT_NUMBER << std::endl << "1. " << FIRST_SIGN_UP << std::endl << "2. " << FIRST_SIGN_IN << std::endl << "3. " << QUIT << std::endl;
        std::cin >> choice;
        while (!std::cin.good()){
			std::cin.clear();
			std::cin.ignore(1000,'\n');
			sleep(2);
			system("clear");
			std::cout << MENU_GAME_NAME << std::endl;
            std::cout << FIRST_DESCRIPTION;
			std::cout << MENU_SELECT_NUMBER << std::endl << "1. " << FIRST_SIGN_UP << std::endl << "2. " << FIRST_SIGN_IN << std::endl << "3. " << QUIT << std::endl;
			std::cin >> choice;
        }
        switch(choice) {
            case 1: { this->signUp(); break; }
            case 2: { _isAth = this->signIn(); break; }
            case 3: { std::cout << QUIT << std::endl; _hasQuit = true; return _isAth; break; }
            default : std::cout << ERROR_INPUT << std::endl;  //Deconnexion.
        }
    } while (!_hasQuit and !_isAth);
    return _isAth;
}

//////////////////////////////////////////////////////////////////
// SIGN UP
//////////////////////////////////////////////////////////////////

bool GameMenu::signUp() {
	char pseudo[32], passwd[32], passwd2[32];
	char buffer[MAXBUFSIZE];
	bool ok;
	system("clear");
	do {
		std::cout << MENU_GAME_NAME << std::endl << MENU_WRITE_QUIT << std::endl;
		std::cout << SIGN_UP_PSEUDO " :" << std::endl;
        strcpy(pseudo, "");
		std::cin >> pseudo;
		if(!strcmp(pseudo, QUIT_LOWERCASE))
			return true;

		if(signUpPasswordCheck(passwd, passwd2)) return true;

		std::string sender = pseudo;
		sender += MSG_SEPARATOR;
		sender += passwd;
		sending_T(_socketfd, prepForSending(210, sender));
        memset(buffer, '\0', MAXBUFSIZE);

        ok = signUpConfirmation(buffer, ok);

	}while(!ok);
    return true;
}

bool GameMenu::signUpPasswordCheck(char passwd[32], char passwd2[32]) {
	bool redo;
	do { // as long as the password are different
		redo = false;
		std::cout << SIGN_UP_PASSWORD " :" << std::endl;
		strcpy(passwd, "");
		std::cin >> passwd;
		if(!strcmp(passwd, QUIT_LOWERCASE))
			return true;
		std::cout << SIGN_UP_PASSWORD2 " :" << std::endl;
		strcpy(passwd2, "");
		std::cin >> passwd2;
		if(!strcmp(passwd2, QUIT_LOWERCASE))
			return true;
		if(strcmp(passwd,passwd2)) {
			redo = true;
			std::cout << SIGN_UP_PASSWORD_ERROR << std::endl;
			sleep(2);
			system("clear");
		}
	}while(redo); //message try again?
	return false;
}

bool GameMenu::signUpConfirmation(char buffer[MAXBUFSIZE], bool ok) {
    // confirm the sign up if pseudo is free
	if(receiving_T(_socketfd, buffer)) {
		std::string message = buffer;
		message = message.substr(3, message.size());
		if(message == "true") {
			ok = true;
			std::cout << SIGN_UP_SUCCESS << std::endl;
			sleep(2);
		}
		else {
			std::cout << SIGN_UP_PSEUDO_ERROR << std::endl;
			sleep(2);
			system("clear");
		}
	}
	return ok;
}

//////////////////////////////////////////////////////////////////
// SIGN IN
//////////////////////////////////////////////////////////////////

bool GameMenu::signIn() {
    // sign in menu, sedding the information to the server until break is called or successful signing in
    char buffer[MAXBUFSIZE];
    char pseudo[32], passwd[32];
	bool ok = false;
	do{
		system("clear");
		std::cout << MENU_GAME_NAME << std::endl << MENU_WRITE_QUIT << std::endl;
		std::cout<< SIGN_IN_PSEUDO " :" << std::endl;
        strcpy(pseudo, "");
        std::cin.clear();
        std::cin.ignore(1000,'\n');
		std::cin >> pseudo;
		if(!strcmp(pseudo, QUIT_LOWERCASE))
			return false;
		std::cout<< SIGN_IN_PASSWORD " :" << std::endl;
        strcpy(passwd, "");
        std::cin.clear();
        std::cin.ignore(1000,'\n');
		std::cin >> passwd;
		if(!strcmp(passwd, QUIT_LOWERCASE))
			return false;
		std::string sender = pseudo;
        sender += MSG_SEPARATOR;
        sender += passwd;
        sending_T(_socketfd, prepForSending(220, sender));
        memset(buffer, '\0', MAXBUFSIZE);
        ok = signInConfirmation(buffer, ok);
	} while(!ok);
    clientName = pseudo;
    return true;
}

bool GameMenu::signInConfirmation(char buffer[MAXBUFSIZE], bool ok) {
    // if connexion is accepted, handle it
	if(receiving_T(_socketfd, buffer)) {
		std::string message = buffer;
		message = message.substr(3, message.size());
		if(message == "true") {
			ok = true;
			std::cout << SIGN_IN_SUCCESS << std::endl;
			sleep(2);
			system("clear");
		}
		else if (message == "connected") {
			std::cout << SIGN_IN_ALREADY_CONNECTED << std::endl;
			sleep(2);
			system("clear");
		}
		else {
			std::cout << SIGN_IN_ERROR << std::endl;
			sleep(2);
			system("clear");
		}
	}
	return ok;
}

//////////////////////////////////////////////////////////////////
// GENERAL MENU
//////////////////////////////////////////////////////////////////

unsigned GameMenu::startGameMenu() {
    // main menu, with all the different actions
    unsigned choice;
    unsigned game = 0;
    bool hasQuit = false;
    do {
        system("clear");
        std::cin.clear();
        std::cin.ignore(10000, '\n');
        std::cout << MENU_GAME_NAME << std::endl;
        std::cout << MENU_SELECT_NUMBER << std::endl << "1. " << MAIN_NEW_GAME << std::endl << "2. " << MAIN_SAVED_GAME << std::endl \
        << "3. " << MAIN_STATS << std::endl << "4. " << MAIN_FRIENDS << std::endl << "5. " << MAIN_CHAT << std::endl << \
        "6. " << MAIN_HELP << std::endl << "7. " << QUIT << std::endl;
        std::cin >> choice;
        switch(choice){
            case 1: { game = startGameChoice(); break; }
            case 2: { sending_T(_socketfd, prepForSending(270, "")); game = continueGame(); break; }
            case 3: { getStats(); break; }
            case 4: { manageFriendsList(); break; }
            case 5: { openChat(); break; }
            case 6: { help(); break; }
            case 7: { std::cout << QUIT << std::endl; hasQuit = true; game = 0; break; }
            default: { std::cout << ERROR_INPUT << std::endl; sleep(2); break; }
        }
        if(game) hasQuit = true;
    } while(!hasQuit);
    return game;
}

unsigned GameMenu::startGameChoice(){
    // what game mode
	unsigned choice;
	bool hasQuit = false;
    do{
		system("clear");
        std::cout << MENU_GAME_NAME << std::endl;
        std::cout << MENU_SELECT_NUMBER << std::endl << "1. " << DEFAULT_GAMEMODE_STR << std::endl << "2. " << HORDE_GAMEMODE_STR << \
        std::endl << "3. " << DARK_GAMEMODE_STR << std::endl << "4. " << DICE_GAMEMODE_STR << std::endl << \
        "5. " << QUIT << std::endl;
        std::cin.clear();
        std::cin.ignore(1000,'\n');
		std::cin >> choice;
		if(choice < 1 || choice > 5)
			std::cout << ERROR_INPUT << std::endl;
		else
			hasQuit = true;
	}while(!hasQuit);
	return choice;
}

unsigned GameMenu::startGameTypeChoice() {
    // classic or real time
    unsigned choice;
    bool hasQuit = false;
    do{
        system("clear");
        std::cout << MENU_GAME_NAME << std::endl;
        std::cout << MENU_SELECT_NUMBER << std::endl << "1. " << REALTIME_OFF_STR << std::endl << "2. " << REALTIME_ON_STR << \
        std::endl << "3. " << QUIT << std::endl;
        std::cin.clear();
        std::cin.ignore(1000,'\n');
        std::cin >> choice;
        if(choice < 1 || choice > 3)
            std::cout << ERROR_INPUT << std::endl;
        else
            hasQuit = true;
    }while(!hasQuit);
    return choice;
}

//////////////////////////////////////////////////////////////////
// CONTINUE GAME
//////////////////////////////////////////////////////////////////

unsigned GameMenu::continueGame() {
    // to relauch an existing game, if the other user is online
    unsigned choice;
    bool hasQuit = false;
    std::string result;
    std::vector<std::vector<std::string>> data;
    if(receiving(_socketfd, &result))
    {
        unsigned long pos;
        while(!result.empty()) {
            pos = result.find(MSG_SEPARATOR);
            std::string temp = result.substr(0, pos);
            result = result.substr(pos + 1, result.size());
            pos = temp.find('+');
            std::string other_player = temp.substr(0, pos);
            temp = temp.substr(pos + 1, temp.size());
            pos = temp.find('+');
            std::string date = temp.substr(0, pos);
            std::string game_id = temp.substr(pos + 1, temp.size());

            std::vector<std::string> vector = {other_player, date, game_id};
            data.emplace_back(vector);
        }
    }
    do { // shows all the saved games
        system("clear");
        std::cin.clear();
        std::cin.ignore(10000, '\n');
        std::cout << MENU_GAME_NAME << std::endl;
        std::cout << YOU_HAVE + std::to_string(data.size()) + SAVED_GAMES_NB << std::endl;
        for(int i = 0; i < static_cast<int>(data.size()); ++i) {
            std::cout << "          " << std::to_string(i+1) + ". " + clientName + " VS " + data.at(i).at(0) + " ( " + data.at(i).at(1) + " )" << std::endl;
        }
        std::cout << std::endl << SAVED_GAMES_LAUNCH << std::endl;
        std::cin >> choice;
        switch(choice) {
            case 0: {
                hasQuit = true;
                break;
            }
            default: {
                if(choice <= data.size()) {
                    return 999 + static_cast<unsigned>(std::stoi(data.at(choice-1).at(2)));
                }
                else {
                    std::cout << ERROR_INPUT << std::endl;
                    sleep(2);
                }
                break;
            }
        }
    } while(!hasQuit);
    return 0;
}

//////////////////////////////////////////////////////////////////
// STATS
//////////////////////////////////////////////////////////////////

unsigned GameMenu::getStats(){
    // main menu of stat branch
    unsigned choice;
    bool hasQuit = false;
    char buffer[MAXBUFSIZE];

    do{
    system("clear");
    std::cout << MENU_GAME_NAME << std::endl;
    std::cout << MENU_SELECT_NUMBER << std::endl << "1. " << STATS_PERSO << std::endl << "2. " << STATS_FRIENDS << \
    std::endl << "3. " << STATS_GENERAL << std::endl << "4. " << QUIT << std::endl;
    std::cin >> choice;
    while (!std::cin.good()){
            std::cin.clear();
            std::cin.ignore(1000,'\n');
            std::cout << ERROR_INPUT << std::endl;
            std::cin >> choice;
    }
    switch(choice) {
        case 1: {
			statsUsernameDisplay();
            break;
        }
        case 2: {	//Username
			statsFriends();
            break;
        }
        case 3: {	//General Stats
			statsGeneral(buffer);
            break;
        }
        case 4: { hasQuit = true; break;}
        default : { std::cout << ERROR_INPUT << std::endl; }
    }
    } while(!hasQuit);
    return choice;
}

void GameMenu::statsUsernameDisplay() {
    // shows the stat of a selected player
	char buffer[MAXBUFSIZE];
	sending_T(_socketfd, prepForSending(240, ""));
	if(receiving_T(_socketfd, buffer)) {
		std::string message = buffer;
		message = message.substr(3, message.size());
		if(message == "not_found") {
			std::cout << PLAYER_NOT_FOUND << std::endl;
		}
		else {
			std::string result = STATS_ELO_OF + clientName + IS;
			unsigned long pos;
			pos = message.find(MSG_SEPARATOR);
			result += message.substr(pos + 1, message.size());
			std::cout << result << std::endl;
		}
		sleep(1);
		std::cout << "\n" << MENU_PRESS_ENTER << std::endl;
		std::cin.ignore();
		std::cin.ignore();
	}
}

void GameMenu::statsFriends(){
    // shows the friend ranking
	char buffer[MAXBUFSIZE];
	sending_T(_socketfd, prepForSending(280, ""));
	if(receiving_T(_socketfd, buffer)) {
		std::string message = buffer;
		message = message.substr(3, message.size());
		unsigned long pos;
		int count = 1;
		while(!message.empty() && count <= 10) {
			std::string result;
			pos = message.find(MSG_SEPARATOR);
			result = std::to_string(count) + ". " + message.substr(0, pos) + " | ";
			message = message.substr(pos + 1, message.size());
			pos = message.find(MSG_SEPARATOR);
			result += message.substr(0, pos);
			message = message.substr(pos+ 1, message.size());
			count++;
			std::cout << result << std::endl;
		}
	std::cout << "\n" << MENU_PRESS_ENTER << std::endl;
	std::cin.ignore();
	std::cin.ignore();
	}
}

void GameMenu::statsGeneral(char buffer[MAXBUFSIZE]) {
    // shows the general ranking
	std::cout << STATS_GENERAL << std::endl;
	sending_T(_socketfd, prepForSending(250, "10"));
	if(receiving_T(_socketfd, buffer)) {
		std::string message = buffer;
		message = message.substr(3, message.size());
		unsigned long pos;
		int count = 1;
		while(!message.empty() && count <= 10) {
			std::string result;
			pos = message.find(MSG_SEPARATOR);
			result = std::to_string(count) + ". " + message.substr(0, pos) + " | ";
			message = message.substr(pos + 1, message.size());
			pos = message.find(MSG_SEPARATOR);
			result += message.substr(0, pos);
			message = message.substr(pos+ 1, message.size());
			count++;
			std::cout << result << std::endl;
		}
		std::cout << "\n\n" << MENU_PRESS_ENTER << std::endl;
		std::cin.ignore();
		std::cin.ignore();
	}
}

//////////////////////////////////////////////////////////////////
// FRIENDS
//////////////////////////////////////////////////////////////////

void GameMenu::manageFriendsList(){
    // main menu of friendship thing
    char buffer[MAXBUFSIZE], input[MAXBUFSIZE];
    unsigned choice;
    bool _hasQuit = false;
    std::string message;
    do {
		sleep(1);
		system("clear");
		std::cout << MENU_GAME_NAME << std::endl;
		std::cout << MENU_SELECT_NUMBER << std::endl << "1. " << FRIENDS_ADD << std::endl << "2. " << FRIENDS_REMOVE << \
		std::endl << "3. " << FRIENDS_SHOW_REQUESTS << std::endl << "4. " << FRIENDS_SHOW_FRIENDS_LIST << std::endl << "5. " << \
		QUIT << std::endl;
		std::cin >> choice;
		while (!std::cin.good()) {
			std::cin.clear();
			std::cin.ignore(1000,'\n');
			std::cout << ERROR_INPUT << std::endl;
			std::cin >> choice;
		}
		switch(choice) {
			case 1: { //Add
				friendsAdd(input, buffer);
				break;
			}
			case 2: { //Remove
				friendsRemove(input, buffer);
				break;
			}
			case 3: { //Request
				friendsRequest(input, buffer);
				break;
			}
			case 4: { //Show
				friendsShow(buffer);
				break;
			}
			case 5: { _hasQuit = true; break; }
			default : { std::cout << ERROR_INPUT << std::endl; }
		}
    } while(!_hasQuit);
}

void GameMenu::friendsAdd(char input[MAXBUFSIZE], char buffer[MAXBUFSIZE]) {
    // about adding a friend
	std::string message;
	std::cout << FRIENDS_ADD_USERNAME << std::endl;
	memset(input, '\0', MAXBUFSIZE);
	std::cin>>input;
	sending_T(_socketfd, prepForSending(261, input));
	memset(buffer, '\0', MAXBUFSIZE);
	if(receiving_T(_socketfd, buffer)) {
		message = buffer;
		message = message.substr(3, message.size());
		std::cout << message << std::endl;
		sleep(1);
	}
}

void GameMenu::friendsRemove(char input[MAXBUFSIZE], char buffer[MAXBUFSIZE]) {
    // about removig a friend, if exists
	std::cout << DASHES " " FRIENDS_FRIENDS_LIST_TITLE " " DASHES << std::endl;
	sending_T(_socketfd, prepForSending(264, ""));
	memset(buffer, '\0', MAXBUFSIZE);
	if(receiving_T(_socketfd, buffer)) {
		std::string message = buffer;
		message = message.substr(3, message.size());
		unsigned long pos;
		std::string names;
		while(!message.empty()) {
			pos = message.find(MSG_SEPARATOR);
			names += message.substr(0, pos)+", ";
			message = message.substr(pos+ 1, message.size());
		}
		if(!names.empty()) {
			names[names.size()-2]='.';
			std::cout << names<< std::endl;
			std::cout<<FRIENDS_REMOVE_USERNAME<<std::endl;
			memset(input, '\0', MAXBUFSIZE);
			std::cin>>input;
			sending_T(_socketfd, prepForSending(262, input));
			memset(buffer, '\0', MAXBUFSIZE);
			if(receiving_T(_socketfd, buffer)) {
				message = buffer;
				message = message.substr(3, message.size());
				std::cout << message << std::endl;
				sleep(1);
			}
		}
		else { std::cout<<FRIENDS_EMPTY_FRIENDS_LIST<<std::endl; }
	}
	std::cout << "\n\n" << MENU_PRESS_ENTER << std::endl;
	std::cin.ignore();
	std::cin.ignore();
}

void GameMenu::friendsRequest(char input[MAXBUFSIZE], char buffer[MAXBUFSIZE]) {
    // asking for a new friend
	std::cout << FRIENDS_REQUESTS_LIST_TITLE << std::endl;
	sending_T(_socketfd, prepForSending(265, ""));
	memset(buffer, '\0', MAXBUFSIZE);
	if(receiving_T(_socketfd, buffer)) {
		std::string message = buffer;
		message = message.substr(3, message.size());
		unsigned long pos;
		std::string names;
		while(!message.empty()) {
			pos = message.find(MSG_SEPARATOR);
			names += message.substr(0, pos)+", ";
			message = message.substr(pos+ 1, message.size());
		}
		if(!names.empty()) {
			names[names.size()-2]='.';
			std::cout << names<< std::endl;
			std::cout<<FRIENDS_ADD_USERNAME<<std::endl;
			memset(input, '\0', MAXBUFSIZE);
			std::cin>>input;
			sending_T(_socketfd, prepForSending(263, input));
			memset(buffer, '\0', MAXBUFSIZE);
			if(receiving_T(_socketfd, buffer)) {
				message = buffer;
				message = message.substr(3, message.size());
				std::cout << message << std::endl;
				sleep(1);
			}
		}
		else { std::cout<<FRIENDS_EMPTY_REQUESTS_LIST<<std::endl; }
	}
	std::cout << "\n\n" << MENU_PRESS_ENTER << std::endl;
	std::cin.ignore();
	std::cin.ignore();
}

void GameMenu::friendsShow(char buffer[MAXBUFSIZE]) {
    // shows all friends
	std::cout << FRIENDS_FRIENDS_LIST_TITLE << std::endl;
	sending_T(_socketfd, prepForSending(264, ""));
	memset(buffer, '\0', MAXBUFSIZE);
	if(receiving_T(_socketfd, buffer))
	{
		std::string message = buffer;
		message = message.substr(3, message.size());
		unsigned long pos;
		std::string names;
		while(!message.empty()) {
			pos = message.find(MSG_SEPARATOR);
			names += message.substr(0, pos)+", ";
			message = message.substr(pos+ 1, message.size());
		}
		if(!names.empty()){
			names[names.size()-2]='.';
			std::cout << names<< std::endl;
		}
		else { std::cout<<FRIENDS_EMPTY_FRIENDS_LIST<<std::endl; }
	}
	std::cout << "\n\n" << MENU_PRESS_ENTER << std::endl;
	std::cin.ignore();
	std::cin.ignore();
}

//////////////////////////////////////////////////////////////////
// CHAT
//////////////////////////////////////////////////////////////////

void GameMenu::openChat() {
	int pid = fork();
	if (pid == 0) {
		execlp("xterm", "xterm", "-e", "./chat", hostname, clientName.c_str(), NULL);
	}
}

//////////////////////////////////////////////////////////////////
// HELP
//////////////////////////////////////////////////////////////////

void GameMenu::help() {
    int choice;
    bool _hasQuit = false;
    do {
		system("clear");
        std::cout << MENU_GAME_NAME << std::endl;
        std::cout << MENU_SELECT_NUMBER << std::endl << "1. " << HELP_GAMEMODES << std::endl << "2. " << HELP_CHESS_RULES << \
        std::endl << "3. " << QUIT << std::endl;
		//std::cout << "Select number:\n1. Game modes.\n2. Game rules.\n3. Quit.\n" <<std::endl;
		std::cin >> choice;
        while (!std::cin.good()) {
            std::cin.clear();
            std::cin.ignore(1000,'\n');
            std::cout << ERROR_INPUT << std::endl;
            std::cin >> choice;
        }
		switch(choice) {
			case 1: { helpGamemodes(); break; }
			case 2: { helpChessRules(); break; }
			case 3: { _hasQuit = true; break; }
			default : { std::cout << ERROR_INPUT << std::endl; }
		}
    }while(!_hasQuit);
}

void GameMenu::helpGamemodes() {
	bool finished = false;
	std::string quit = "";
	unsigned part = 1;
	do {
		system("clear");
		switch(part) {
			case 1: { std::cout << HELP_GAMEMODES_1 << std::endl; break; }
			case 2: { std::cout << HELP_GAMEMODES_2 << std::endl; break; }
			case 3: { std::cout << HELP_GAMEMODES_3 << std::endl; break; }
			case 4: { std::cout << HELP_GAMEMODES_4 << std::endl; break; }
			case 5: { std::cout << HELP_GAMEMODES_5 << std::endl; finished = true; break; }
		}
		part++;
		std::cout << HELP_CONTINUE << std::endl;
		std::cin >> quit;
	}while(!finished);
}

void GameMenu::helpChessRules() {
	bool finished = false;
	std::string quit = "";
	unsigned part = 1;
	do {
		system("clear");
		switch(part) {
			case 1: { std::cout << HELP_CHESS_RULES_1 << std::endl; break; }
			case 2: { std::cout << HELP_CHESS_RULES_2 << std::endl; break; }
			case 3: { std::cout << HELP_CHESS_RULES_3 << std::endl; break; }
			case 4: { std::cout << HELP_CHESS_RULES_4 << std::endl; break; }
			case 5: { std::cout << HELP_CHESS_RULES_5 << std::endl; break; }
			case 6: { std::cout << HELP_CHESS_RULES_6 << std::endl; break; }
			case 7: { std::cout << HELP_CHESS_RULES_7 << std::endl; finished = true; break; }
		}
		part++;
		std::cout << HELP_CONTINUE << std::endl;
		std::cin >> quit;
	}while(!finished);
}
