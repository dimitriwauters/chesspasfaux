// INFO-F209 - Groupe 5 - chat.hpp

#ifndef CHAT_HPP
#define CHAT_HPP

#include <iostream>
#include <thread>
#include <cstdio>
#include <cstdlib>
#include "../Network.hpp"
#include "../const.hpp"

int chatfd;
bool connected = false;
bool can_send = false;
char* username;
std::string receiver_name;
std::string send_message;

// Main actions
void run();
void displayChatOptions();
void chooseOption();
void showConversation();
// Interpreting server's information
void getServerMessage();
void decodeServerMessage(int, std::string);
void connecting();
void getConnectedUsers(std::string);
void isReceiverAvailable(std::string);
void isReceiverOnline(std::string);
void getReceiverMessage(std::string);

#endif
