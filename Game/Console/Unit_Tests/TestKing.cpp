// g++ -o testKing ../Pieces.cpp TestKing.cpp -lcppunit

#include "Test.hpp"
#include "../const.hpp"
#include "../Pieces.hpp"

class TestKing : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestKing);
    CPPUNIT_TEST(testCheckMove);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testCheckMove(void);

private:
    King *TestObj1;
    King *TestObj2;
    King *TestObj3;
};

///////////////////////////////////////////////////////////////////////////////

void TestKing::testCheckMove(void)
{
    CPPUNIT_ASSERT(0 == TestObj1->checkMove(6,0));
    CPPUNIT_ASSERT(0 == TestObj1->checkMove(6,4));
    CPPUNIT_ASSERT(0 == TestObj1->checkMove(0,1));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(4,6));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(5,6));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(3,7));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(5,3));
    CPPUNIT_ASSERT(0 == TestObj3->checkMove(6,1));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(4,2));
}

void TestKing::setUp(void)
{
    TestObj1 = new King(4, 0, BLACK);
    TestObj2 = new King(4, 7, WHITE);
    TestObj3 = new King(4, 3, BLACK);
}

void TestKing::tearDown(void)
{
    delete TestObj1;
    delete TestObj2;
    delete TestObj3;
}

///////////////////////////////////////////////////////////////////////////////

CPPUNIT_TEST_SUITE_REGISTRATION( TestKing );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;
    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);
    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);
    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);
    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();
    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
