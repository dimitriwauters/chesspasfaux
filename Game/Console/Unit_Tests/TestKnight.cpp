// g++ -o testKnight ../Pieces.cpp TestKnight.cpp -lcppunit

#include "Test.hpp"
#include "../const.hpp"
#include "../Pieces.hpp"

class TestKnight : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestKnight);
    CPPUNIT_TEST(testCheckMove);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testCheckMove(void);

private:
    Knight *TestObj1;
    Knight *TestObj2;
    Knight *TestObj3;
};

///////////////////////////////////////////////////////////////////////////////

void TestKnight::testCheckMove(void)
{
    CPPUNIT_ASSERT(1 == TestObj1->checkMove(0,2));
    CPPUNIT_ASSERT(1 == TestObj1->checkMove(3,1));
    CPPUNIT_ASSERT(0 == TestObj1->checkMove(1,3));
    CPPUNIT_ASSERT(0 == TestObj1->checkMove(2,0));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(7,5));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(5,5));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(4,6));
    CPPUNIT_ASSERT(0 == TestObj2->checkMove(2,2));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(4,5));
    CPPUNIT_ASSERT(0 == TestObj3->checkMove(5,3));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(2,5));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(1,2));
    CPPUNIT_ASSERT(0 == TestObj3->checkMove(5,1));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(2,1));
}

void TestKnight::setUp(void)
{
    TestObj1 = new Knight(1, 0, BLACK);
    TestObj2 = new Knight(6, 7, WHITE);
    TestObj3 = new Knight(3, 3, WHITE);
}

void TestKnight::tearDown(void)
{
    delete TestObj1;
    delete TestObj2;
    delete TestObj3;
}

///////////////////////////////////////////////////////////////////////////////

CPPUNIT_TEST_SUITE_REGISTRATION( TestKnight );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;
    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);
    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);
    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);
    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();
    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
