// g++ -o testQueen ../Pieces.cpp TestQueen.cpp -lcppunit

#include "Test.hpp"
#include "../const.hpp"
#include "../Pieces.hpp"

class TestQueen : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestQueen);
    CPPUNIT_TEST(testCheckMove);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testCheckMove(void);

private:
    Queen *TestObj1;
    Queen *TestObj2;
    Queen *TestObj3;
};

///////////////////////////////////////////////////////////////////////////////

void TestQueen::testCheckMove(void)
{
    CPPUNIT_ASSERT(1 == TestObj1->checkMove(5,3));
    CPPUNIT_ASSERT(0 == TestObj1->checkMove(1,5));
    CPPUNIT_ASSERT(0 == TestObj1->checkMove(3,1));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(0,3));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(5,1));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(7,7));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(2,3));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(0,0));
    CPPUNIT_ASSERT(0 == TestObj3->checkMove(4,5));
}

void TestQueen::setUp(void)
{
    TestObj1 = new Queen(0, 3, WHITE);
    TestObj2 = new Queen(7, 3, BLACK);
    TestObj3 = new Queen(3, 3, WHITE);
}

void TestQueen::tearDown(void)
{
    delete TestObj1;
    delete TestObj2;
    delete TestObj3;
}

///////////////////////////////////////////////////////////////////////////////

CPPUNIT_TEST_SUITE_REGISTRATION( TestQueen );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;
    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);
    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);
    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);
    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();
    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
