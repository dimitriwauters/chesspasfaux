// g++ -o testBishop ../Pieces.cpp TestBishop.cpp -lcppunit

#include "Test.hpp"
#include "../const.hpp"
#include "../Pieces.hpp"

class TestBishop : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestBishop);
    CPPUNIT_TEST(testCheckMove);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testCheckMove(void);

private:
    Bishop *TestObj1;
    Bishop *TestObj2;
    Bishop *TestObj3;
};

///////////////////////////////////////////////////////////////////////////////

void TestBishop::testCheckMove(void)
{
    CPPUNIT_ASSERT(0 == TestObj1->checkMove(2,3));
    CPPUNIT_ASSERT(0 == TestObj1->checkMove(7,7));
    CPPUNIT_ASSERT(1 == TestObj1->checkMove(3,1));
    CPPUNIT_ASSERT(0 == TestObj2->checkMove(5,3));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(5,4));
    CPPUNIT_ASSERT(0 == TestObj2->checkMove(5,0));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(5,1));
    CPPUNIT_ASSERT(0 == TestObj3->checkMove(6,2));
    CPPUNIT_ASSERT(0 == TestObj3->checkMove(1,4));
}

void TestBishop::setUp(void)
{
    TestObj1 = new Bishop(2, 0, BLACK);
    TestObj2 = new Bishop(2, 7, WHITE);
    TestObj3 = new Bishop(2, 4, WHITE);
}

void TestBishop::tearDown(void)
{
    delete TestObj1;
    delete TestObj2;
    delete TestObj3;
}

///////////////////////////////////////////////////////////////////////////////

CPPUNIT_TEST_SUITE_REGISTRATION( TestBishop );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;
    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);
    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);
    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);
    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();
    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
