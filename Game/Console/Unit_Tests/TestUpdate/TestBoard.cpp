// g++ -o testBoard ../Board.cpp ../Pieces.cpp TestBoard.cpp -lcppunit

#include "Test.hpp"
#include "../const.hpp"
#include "../Board.hpp"

class TestBoard : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestBoard);
    CPPUNIT_TEST(testIsInRange);
    CPPUNIT_TEST(testAddPieceAndRemovePiece);
    CPPUNIT_TEST(testGetTypeTypeNbrAndColor);
    CPPUNIT_TEST(testSetFirstMoveAndHasMoved);
    CPPUNIT_TEST(testMoveTest);
    CPPUNIT_TEST(testSetNewPos);
    CPPUNIT_TEST(testWhichLegalNoPieces);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testIsInRange(void);
    void testAddPieceAndRemovePiece(void);
    void testGetTypeTypeNbrAndColor(void);
    void testSetFirstMoveAndHasMoved(void);
    void testMoveTest(void);
    void testSetNewPos(void);
    void testWhichLegalNoPieces(void);

private:
    Board *TestObj1;
};

///////////////////////////////////////////////////////////////////////////////

void TestBoard::testIsInRange(void)
{
    CPPUNIT_ASSERT(1 == TestObj1->isInRange(std::make_pair(0, 0)));
    CPPUNIT_ASSERT(1 == TestObj1->isInRange(std::make_pair(5, 5)));
    CPPUNIT_ASSERT(1 == TestObj1->isInRange(std::make_pair(7, 7)));
    CPPUNIT_ASSERT(0 == TestObj1->isInRange(std::make_pair(8, 8)));
    CPPUNIT_ASSERT(0 == TestObj1->isInRange(std::make_pair(0, 9)));
    CPPUNIT_ASSERT(0 == TestObj1->isInRange(std::make_pair(9, 0)));
    CPPUNIT_ASSERT(0 == TestObj1->isInRange(std::make_pair(-15, 19)));
    CPPUNIT_ASSERT(1 == TestObj1->isInRange(std::make_pair(7, 2)));
}

void TestBoard::testAddPieceAndRemovePiece(void)
{
    CPPUNIT_ASSERT(1 == TestObj1->addPiece(PAWN_NBR, std::make_pair(0, 0), 1));
    CPPUNIT_ASSERT(0 == TestObj1->addPiece(ROOK_NBR, std::make_pair(0, 0), 0));
    CPPUNIT_ASSERT(1 == TestObj1->addPiece(QUEEN_NBR, std::make_pair(5, 5), 0));
    CPPUNIT_ASSERT(1 == TestObj1->addPiece(KING_NBR, std::make_pair(7, 7), 1));
    CPPUNIT_ASSERT(0 == TestObj1->addPiece(KING_NBR, std::make_pair(7, 7), 1));
    CPPUNIT_ASSERT(1 == TestObj1->addPiece(KING_NBR, std::make_pair(8, 7), 1));

    CPPUNIT_ASSERT(1 == TestObj1->addPiece(KING_NBR, std::make_pair(7, 8), 1));
    CPPUNIT_ASSERT(1 == TestObj1->addPiece(KING_NBR, std::make_pair(9, 9), 1));
    CPPUNIT_ASSERT(0 == TestObj1->isEmpty(std::make_pair(9, 9))); //TODO

    CPPUNIT_ASSERT(1 == TestObj1->removePiece(std::make_pair(7, 7)));
    CPPUNIT_ASSERT(0 == TestObj1->removePiece(std::make_pair(7, 7)));
    CPPUNIT_ASSERT(1 == TestObj1->removePiece(std::make_pair(5, 5)));
    CPPUNIT_ASSERT(1 == TestObj1->removePiece(std::make_pair(0, 0)));
    CPPUNIT_ASSERT(0 == TestObj1->removePiece(std::make_pair(3, 3)));

}

void TestBoard::testGetTypeTypeNbrAndColor(void)
{
    TestObj1->addPiece(ROOK_NBR, std::make_pair(4, 4), 1);
    TestObj1->addPiece(PAWN_NBR, std::make_pair(5, 5), 0);
    TestObj1->addPiece(KNIGHT_NBR, std::make_pair(7, 7), 0);
    TestObj1->addPiece(QUEEN_NBR, std::make_pair(6, 6), 1);
    TestObj1->addPiece(KING_NBR, std::make_pair(2, 2), 0);
    TestObj1->addPiece(BISHOP_NBR, std::make_pair(1, 1), 1);

    CPPUNIT_ASSERT(PAWN == TestObj1->getTypePiece(std::make_pair(5, 5)));
    CPPUNIT_ASSERT(QUEEN == TestObj1->getTypePiece(std::make_pair(6, 6)));
    CPPUNIT_ASSERT(BISHOP == TestObj1->getTypePiece(std::make_pair(1, 1)));
    CPPUNIT_ASSERT(KING == TestObj1->getTypePiece(std::make_pair(2, 2)));
    CPPUNIT_ASSERT(ROOK == TestObj1->getTypePiece(std::make_pair(4, 4)));
    CPPUNIT_ASSERT(KNIGHT == TestObj1->getTypePiece(std::make_pair(7, 7)));

    CPPUNIT_ASSERT(PAWN_NBR == TestObj1->getTypePieceNbr(std::make_pair(5, 5)));
    CPPUNIT_ASSERT(QUEEN_NBR == TestObj1->getTypePieceNbr(std::make_pair(6, 6)));
    CPPUNIT_ASSERT(BISHOP_NBR == TestObj1->getTypePieceNbr(std::make_pair(1, 1)));
    CPPUNIT_ASSERT(KING_NBR == TestObj1->getTypePieceNbr(std::make_pair(2, 2)));
    CPPUNIT_ASSERT(ROOK_NBR == TestObj1->getTypePieceNbr(std::make_pair(4, 4)));
    CPPUNIT_ASSERT(KNIGHT_NBR == TestObj1->getTypePieceNbr(std::make_pair(7, 7)));

    CPPUNIT_ASSERT(0 == TestObj1->getColorPiece(std::make_pair(5, 5)));
    CPPUNIT_ASSERT(1 == TestObj1->getColorPiece(std::make_pair(6, 6)));
    CPPUNIT_ASSERT(1 == TestObj1->getColorPiece(std::make_pair(1, 1)));
    CPPUNIT_ASSERT(0 == TestObj1->getColorPiece(std::make_pair(2, 2)));
    CPPUNIT_ASSERT(1 == TestObj1->getColorPiece(std::make_pair(4, 4)));
    CPPUNIT_ASSERT(0 == TestObj1->getColorPiece(std::make_pair(7, 7)));
}

void TestBoard::testSetFirstMoveAndHasMoved(void)
{
    TestObj1->addPiece(QUEEN_NBR, std::make_pair(5, 5), 1);
    CPPUNIT_ASSERT(0 == TestObj1->hasMoved(std::make_pair(5, 5)));
    TestObj1->setFirstMovePiece(std::make_pair(5, 5));
    CPPUNIT_ASSERT(1 == TestObj1->hasMoved(std::make_pair(5, 5)));
}

void TestBoard::testMoveTest(void)
{
    TestObj1->doVirtualMove(1);
    CPPUNIT_ASSERT(1 == TestObj1->isVirtualMove());  // xNumberOfTestYouWant
    TestObj1->doVirtualMove(0);
    CPPUNIT_ASSERT(0 == TestObj1->isVirtualMove());
}

void TestBoard::testSetNewPos(void)
{
    TestObj1->addPiece(PAWN_NBR, std::make_pair(5, 5), 1);
    CPPUNIT_ASSERT(0 == TestObj1->isEmpty(std::make_pair(5, 5)));
    TestObj1->setNewPosition(std::make_pair(5, 5), std::make_pair(1, 1));
    CPPUNIT_ASSERT(1 == TestObj1->isEmpty(std::make_pair(5, 5)));
    CPPUNIT_ASSERT(0 == TestObj1->isEmpty(std::make_pair(1, 1)));
}

void TestBoard::testWhichLegalNoPieces(void)
{
    CPPUNIT_ASSERT(DIAGONAL == TestObj1->whichMove(std::make_pair(0, 0), std::make_pair(1, 1)));
    CPPUNIT_ASSERT(DIAGONAL == TestObj1->whichMove(std::make_pair(0, 0), std::make_pair(7, 7)));
    CPPUNIT_ASSERT(DIAGONAL == TestObj1->whichMove(std::make_pair(4, 5), std::make_pair(6, 7)));
    CPPUNIT_ASSERT(DIAGONAL == TestObj1->whichMove(std::make_pair(7, 1), std::make_pair(7, 1)));

    CPPUNIT_ASSERT(HORIZONTAL == TestObj1->whichMove(std::make_pair(4, 1), std::make_pair(4, 5)));
    CPPUNIT_ASSERT(HORIZONTAL == TestObj1->whichMove(std::make_pair(3, 7), std::make_pair(3, 0)));
    CPPUNIT_ASSERT(HORIZONTAL == TestObj1->whichMove(std::make_pair(7, 2), std::make_pair(7, 1)));
    CPPUNIT_ASSERT(HORIZONTAL == TestObj1->whichMove(std::make_pair(0, 1), std::make_pair(0, 5)));

    CPPUNIT_ASSERT(VERTICAL == TestObj1->whichMove(std::make_pair(7, 1), std::make_pair(5, 1)));
    CPPUNIT_ASSERT(VERTICAL == TestObj1->whichMove(std::make_pair(7, 1), std::make_pair(5, 1)));
    CPPUNIT_ASSERT(VERTICAL == TestObj1->whichMove(std::make_pair(7, 1), std::make_pair(5, 1)));
    CPPUNIT_ASSERT(VERTICAL == TestObj1->whichMove(std::make_pair(7, 1), std::make_pair(5, 1)));

    ///////////////////////////////////////////////////////////////////////////////////////////////

    TestObj1->addPiece(ROOK_NBR, std::make_pair(5, 5), 0);
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(3, 5)));
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(5, 7)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(3, 3)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(4, 6)));
    TestObj1->removePiece(std::make_pair(5, 5));

    TestObj1->addPiece(BISHOP_NBR, std::make_pair(5, 5), 0);
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(3, 5)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(5, 7)));
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(3, 3)));
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(7, 3)));
    TestObj1->removePiece(std::make_pair(5, 5));

    TestObj1->addPiece(KNIGHT_NBR, std::make_pair(5, 5), 0);
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(4, 3)));
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(6, 7)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(1, 1)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(4, 4)));
    TestObj1->removePiece(std::make_pair(5, 5));

    TestObj1->addPiece(KING_NBR, std::make_pair(5, 5), 0);
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(4, 4)));
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(6, 5)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(1, 1)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(2, 4)));
    TestObj1->removePiece(std::make_pair(5, 5));

    TestObj1->addPiece(QUEEN_NBR, std::make_pair(5, 5), 0);
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(0, 0)));
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(5, 1)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(4, 3)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(2, 4)));
    TestObj1->removePiece(std::make_pair(5, 5));

    TestObj1->addPiece(PAWN_NBR, std::make_pair(5, 5), 0);
    CPPUNIT_ASSERT(1 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(6, 5)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(6, 7)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(1, 1)));
    CPPUNIT_ASSERT(0 == TestObj1->isLegalMovePiece(std::make_pair(5, 5), std::make_pair(4, 4)));
    TestObj1->removePiece(std::make_pair(5, 5));

    ////////////////////////////////////////////////////////////////////////////////////////////

    TestObj1->addPiece(PAWN_NBR, std::make_pair(3, 2), 0);
    TestObj1->addPiece(PAWN_NBR, std::make_pair(5, 5), 0);
    CPPUNIT_ASSERT(0 == TestObj1->noPiecesBetween(std::make_pair(5, 3), std::make_pair(5, 7)));
    CPPUNIT_ASSERT(0 == TestObj1->noPiecesBetween(std::make_pair(2, 1), std::make_pair(5, 4)));
    CPPUNIT_ASSERT(0 == TestObj1->noPiecesBetween(std::make_pair(2, 2), std::make_pair(7, 2)));
    CPPUNIT_ASSERT(1 == TestObj1->noPiecesBetween(std::make_pair(2, 0), std::make_pair(2, 7)));
    CPPUNIT_ASSERT(1 == TestObj1->noPiecesBetween(std::make_pair(4, 1), std::make_pair(6, 3)));
    CPPUNIT_ASSERT(1 == TestObj1->noPiecesBetween(std::make_pair(7, 0), std::make_pair(0, 7)));
}

void TestBoard::setUp(void)
{
    TestObj1 = new Board();
}

void TestBoard::tearDown(void)
{
    delete TestObj1;
}

///////////////////////////////////////////////////////////////////////////////

CPPUNIT_TEST_SUITE_REGISTRATION( TestBoard );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;
    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);
    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);
    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);
    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();
    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
