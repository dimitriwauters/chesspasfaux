// g++ -o testPlayer ../Player.cpp TestPlayer.cpp -lcppunit

#include "Test.hpp"
#include "../const.hpp"
#include "../Player.hpp"

class TestPlayer : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestPlayer);
    CPPUNIT_TEST(testGetColor);
    CPPUNIT_TEST(testIsWinner);
    CPPUNIT_TEST(testHasWon);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testGetColor(void);
    void testIsWinner(void);
    void testHasWon(void);

private:
    Player *TestObj1;
    Player *TestObj2;
};

///////////////////////////////////////////////////////////////////////////////

void TestPlayer::testGetColor(void)
{
    CPPUNIT_ASSERT(WHITE == TestObj1->getColor());
    CPPUNIT_ASSERT(BLACK == TestObj2->getColor());
}

void TestPlayer::testIsWinner(void)
{
    CPPUNIT_ASSERT(0 == TestObj1->isWinner());
    CPPUNIT_ASSERT(0 == TestObj2->isWinner());
}

void TestPlayer::testHasWon(void)
{
    TestObj1->hasWon();
    CPPUNIT_ASSERT(1 == TestObj1->isWinner());
    CPPUNIT_ASSERT(0 == TestObj2->isWinner());
    TestObj2->hasWon();
    CPPUNIT_ASSERT(1 == TestObj2->isWinner());
}

void TestPlayer::setUp(void)
{
    TestObj1 = new Player("Keno", WHITE);
    TestObj2 = new Player("François", BLACK);
}

void TestPlayer::tearDown(void)
{
    delete TestObj1;
    delete TestObj2;
}

///////////////////////////////////////////////////////////////////////////////

CPPUNIT_TEST_SUITE_REGISTRATION( TestPlayer );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;
    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);
    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);
    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);
    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();
    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
