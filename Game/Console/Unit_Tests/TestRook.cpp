// g++ -o testRook ../Pieces.cpp TestRook.cpp -lcppunit

#include "Test.hpp"
#include "../const.hpp"
#include "../Pieces.hpp"

class TestRook : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRook);
    CPPUNIT_TEST(testCheckMove);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testCheckMove(void);

private:
    Rook *TestObj1;
    Rook *TestObj2;
    Rook *TestObj3;
};

///////////////////////////////////////////////////////////////////////////////

void TestRook::testCheckMove(void)
{
    CPPUNIT_ASSERT(1 == TestObj1->checkMove(3,0));
    CPPUNIT_ASSERT(0 == TestObj1->checkMove(1,1));
    CPPUNIT_ASSERT(1 == TestObj1->checkMove(0,5));
    CPPUNIT_ASSERT(0 == TestObj2->checkMove(3,5));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(7,4));
    CPPUNIT_ASSERT(1 == TestObj2->checkMove(5,0));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(1,3));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(7,3));
    CPPUNIT_ASSERT(1 == TestObj3->checkMove(3,0));
    CPPUNIT_ASSERT(0 == TestObj3->checkMove(5,5));
    CPPUNIT_ASSERT(0 == TestObj3->checkMove(2,4));
}

void TestRook::setUp(void)
{
    TestObj1 = new Rook(0, 0, WHITE);
    TestObj2 = new Rook(7, 0, BLACK);
    TestObj3 = new Rook(3, 3, WHITE);
}

void TestRook::tearDown(void)
{
    delete TestObj1;
    delete TestObj2;
    delete TestObj3;
}

///////////////////////////////////////////////////////////////////////////////

CPPUNIT_TEST_SUITE_REGISTRATION( TestRook );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;
    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);
    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);
    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);
    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();
    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
