// INFO-F209 - Groupe 5 - chat.cpp
// Create a chat and manage its data.

#include "chat.hpp"

////////////////////////////////////////////////////////////////////////////////
// CONNECTION
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    // Establish the connection client-server
    char* hostname = argv[argc-2];
    username = argv[argc-1];
    SockaddrIn chat_addr;
    setHost(hostname, &chat_addr, PORT_CHAT);
    if ((chatfd = socket(PF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("Client: socket");
        exit(1);
    }
    connecting(&chatfd, &chat_addr);
    std::cout << CHAT_LOGIN << std::endl;
    sending_T(chatfd, username);
    std::thread handle_thread(getServerMessage); // Create a dedicated thread for the chat
    handle_thread.detach();
	run();
	return EXIT_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////
// MAIN ACTIONS
////////////////////////////////////////////////////////////////////////////////

void run()
// Run the window with the chat commands and do the main actions.
{
	while (true)
    {
        if (connected && !can_send) { displayChatOptions(); chooseOption(); }
        else if (can_send) showConversation();
    }
}

void displayChatOptions()
// Print the options that the user can choose.
{
    std::cout << CHAT_COMMANDS << std::endl;
}

void chooseOption()
// Send to the server the option chosen by the user (if it is valid).
{
    int user_choice;
    if (std::cin >> user_choice)
    {
        switch (user_choice)
        {
            case WHO_IS_CONNECTED: // Display who is connected
            {
                sending_T(chatfd, prepForSending(320, ""));
                break;
            }
            case SEND_MSG: // Send a message
            {
                std::cout << CHAT_RECEIVER_USERNAME << std::endl; // Ask the receiver name
                std::cin >> receiver_name;
                sending_T(chatfd, prepForSending(330, receiver_name));
                break;
            }
            default :
            {
                std::cout << ERROR_INPUT << std::endl;
                break;
            }
        }
    }
}

void showConversation()
{
    std::cin.clear();
    if (std::getline(std::cin, send_message))
    {
        std::cout << "[" << username << "] : " << send_message << std::endl;
        send_message = receiver_name + "$" + send_message;
        sending_T(chatfd, prepForSending(350, send_message));
    }
}

////////////////////////////////////////////////////////////////////////////////
// INTERPRETING SERVER'S INFORMATION
////////////////////////////////////////////////////////////////////////////////

void getServerMessage()
// Get information sent by the server.
{
    std::string message;
    char buffer[MAXBUFSIZE];
    int opcode_first, opcode_second;
    while (true)
    {
        memset(buffer, '\0', MAXBUFSIZE);
        if (receiving_T(chatfd, buffer))
        {
            message = buffer;
            opcode_first = std::stoi(message.substr(0, 1)); // Get the message by cutting its opcode
            opcode_second = std::stoi(message.substr(1, 1));
            message = message.substr(3, message.size());
            // opcode_first == 3 <=> a message for the chat
            if (opcode_first == 3) decodeServerMessage(opcode_second, message);
        }
        else break;
    }
}

void decodeServerMessage(int opcode, std::string message)
// Decode information sent by the server.
{
	switch (opcode)
    {
		case 1: { connecting(); break; }
		case 2: { getConnectedUsers(message); break; }
		case 3: { isReceiverAvailable(message); break; }
		case 4: { isReceiverOnline(message); break; }
		case 5: { getReceiverMessage(message); break; }
	}
}

void connecting()
// Link the user to the chat.
{
    std::cout << DASHES " " CHAT_CONNECTED_AS "'" << username << "'" DASHES "\n\n";
    connected = true;
}

void getConnectedUsers(std::string message)
// Get a list of users who are currently connected to the program.
{
	if (!message.empty())
	{
		std::string usernames;
		unsigned long pos;
		while (!message.empty())
        {
			pos = message.find(MSG_SEPARATOR);
			usernames += message.substr(0, pos) + ", ";
			message = message.substr(pos + 1, message.size());
			std::cout << message << std::endl;
		}
		usernames = usernames.substr(0, usernames.size()-2);
		std::cout << CHAT_CONNECTED_USERS " : " << usernames << std::endl;
	}
	else std::cout << CHAT_NOBODY_ONLINE << std::endl;
}

void isReceiverAvailable(std::string message)
// Check if the user to whom the player wants to talk is available.
{
	if (message == CHAT_RECEIVER_AVAILABLE)
	{
		std::cout << CHAT_START_CHAT << receiver_name << std::endl;
		can_send = true;
	}
	else if (message == CHAT_RECEIVER_BUSY)
	{
		receiver_name = "";
		std::cout << CHAT_ALREADY_IN_CHAT << std::endl;
	}
	else
    {
		receiver_name = "";
		std::cout << CHAT_ERROR << std::endl;
	}
}

void isReceiverOnline(std::string message)
// Check if the receiver is always online.
{
	if (message == CHAT_RECEIVER_DISCONNECTED)
    {
		can_send = false;
		std::cout << receiver_name << CHAT_DISCONNECTED << std::endl;
		receiver_name = "";
	}
	else {
		receiver_name = message;
		std::cout << CHAT_START_CHAT << receiver_name << std::endl;
		can_send = true;
	}
}

void getReceiverMessage(std::string message)
// Get and print the message sent by the receiver.
{
    unsigned long pos = message.find(MSG_SEPARATOR);
    if (message.substr(0, pos) == receiver_name)
    {
        std::cout << "[" << receiver_name << "] : " << message.substr(pos + 1, \
            message.size()) << std::endl;
    }
}
