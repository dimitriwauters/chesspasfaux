// INFO-F209 - Groupe 5 - client.cpp

#include "client.hpp"

////////////////////////////////////////////////////////////////////////////////
// CONNECTION
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    // Establishing client-server connection
    char* hostname;
    SockaddrIn server_addr;
    hostname = argv[argc-1];
    bool auth;
    std::signal(SIGINT, signalHandler);
    setHost(hostname, &server_addr, PORT_GAME);
    if ((socketfd = socket(PF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("Client: socket");
        exit(1);
    }
    connecting(&socketfd, &server_addr);
    GameMenu menu(socketfd, hostname);
    auth = menu.startAuthMenu();
    if (!auth) { close(socketfd); exit(0); }
    run(auth, menu);
}

void signalHandler(int signal)
// Shut down the client if a signal is catched.
{
    endwin();
    system("clear");
    std::cout << CLIENT_SHUTDOWN << std::endl;
    close(socketfd);
    exit(signal);
}

////////////////////////////////////////////////////////////////////////////////
// STARTING A GAME: CLIENT SIDE
////////////////////////////////////////////////////////////////////////////////

void run(bool auth, GameMenu menu)
// Run the process of starting a game and launch it when it is done.
{
	unsigned user_choice;
	std::string mode = "unknown";
	std::vector<std::string> pieces;
    std::vector<Player *> players;
    while (true)
    {
        if (auth)
        {
            // Select options in menu
            bool has_chosen = false;
            while (!has_chosen)
            {
                system("clear");
                user_choice = menu.startGameMenu();
                if (user_choice == 0) { close(socketfd); exit(0); } // Disconnect
                system("clear");
                if (user_choice < 999) // Launch a new game (user_choice == chosen gamemode)
                {
					mode = chooseModeGame(mode, menu); // Default/Real time choice
                    has_chosen = sendToServer(user_choice, has_chosen, mode, menu);
                }
                else // Launch saved game
                {
					user_choice = continueSavedGame(user_choice, &pieces)+1;
					sending(socketfd, prepForSending(999, std::to_string(pieces.size())));
                    has_chosen = true;
                }
            }
            launchMatchmaking(menu, user_choice, mode, pieces);
        }
    }
}

std::string chooseModeGame(std::string mode, GameMenu menu)
// Return the mode (default/real time) selected by the user.
{
	unsigned user_choice = menu.startGameTypeChoice();
	if (user_choice == 0) { close(socketfd); exit(0); } // Disconnect
	system("clear");
	switch (user_choice)
    {
		case 1: { mode = SERVER_DEFAULT_MODE; break; }
		case 2: { mode = SERVER_REALTIME_MODE; break; }
		case 3: { menu.startGameTypeChoice(); break; }
	}
	return mode;
}

unsigned continueSavedGame(unsigned choice, std::vector<std::string>* pieces)
// Start a matchmaking with the player with whom the user has saved a game.
{
	std::string buffer;
	int gamemode;
	sending_T(socketfd, prepForSending(271, std::to_string(choice - 999)));
	waiting();
	if (receiving(socketfd, &buffer))
    {
		unsigned long pos;
		while (!buffer.empty())
        {
			pos = buffer.find(MSG_SEPARATOR);
			pieces->emplace_back(buffer.substr(0, pos)); // Get the pieces positions
			buffer = buffer.substr(pos + 1, buffer.size());
		}
	}
    receiving(socketfd, &gamemode);
	return static_cast<unsigned>(gamemode);
}

void launchMatchmaking(GameMenu menu, unsigned gamemode, std::string mode, std::vector<std::string> pieces)
{
    waiting();
    int team = 0;
    receiving(socketfd, &team);
    char player_name[256] = "Unknown";
    receiving(socketfd, player_name);
    int ELO = 0;
    receiving(socketfd, &ELO);
    Player* white_player;
    Player* black_player;
    if (team)
    {
        white_player = new Player(WHITE, menu.getClientName());
        black_player = new Player(BLACK, player_name);
        black_player->setELO(ELO);
    }
    else
    {
        white_player = new Player(WHITE, player_name);
        black_player = new Player(BLACK, menu.getClientName());
        white_player->setELO(ELO);
    }
    launchGame(gamemode, mode, white_player, black_player, team, pieces);
}

void waiting()
// When waiting the opponent...
{
    std::cout << MATCHMAKING_WAITING << std::endl;
    printQuotes(); // Kaamelott easter egg
}

void printQuotes()
// Print Kaamelott quotes.
{
	std::ifstream src_file("resources/text/quotes.txt");
	if (src_file)
    {
		srand((int)time(0));
		std::string quote;
		int lineCounter = 0, lineNumber = rand() % QUOTES_NBR;
		while (lineCounter != lineNumber)
        {
			getline(src_file, quote);
			lineCounter++;
		}
		std::cout << std::endl << quote << std::endl;
		src_file.close();
	}
	else std::cout << FIRST_DESCRIPTION;
}

void launchGame(unsigned gamemode, std::string mode, Player* white_player, \
    Player* black_player, int team, std::vector<std::string> pieces)
// Start a game (client side) following the user's choices. The game is deleted when it is finished.
{
    gamemode--; // User inputs start at 1 <-> 0
	switch (gamemode)
    {
		case DEFAULT_GAMEMODE:
        {
			if (mode == SERVER_REALTIME_MODE)
            {
				auto client = new gameDefaultRealtime(white_player, black_player, socketfd, team, REALTIME_ON);
				std::thread server(getServerMessage<gameDefaultRealtime>, client); // Creating a thread to catch server's information
				server.join();
				while (!client->canDeleteGame()) {}
				delete client;
			}
			else
            {
				auto client = new gameDefault(white_player, black_player, socketfd, team, REALTIME_OFF, pieces);
				std::thread server(getServerMessage<gameDefault>, client);
				server.join();
				while (!client->canDeleteGame()) {}
				delete client;
			}
			break;
		}
		case HORDE_GAMEMODE:
        {
			if (mode == SERVER_REALTIME_MODE)
            {
				auto client = new gameHordeRealtime(white_player, black_player, socketfd, team, REALTIME_ON);
				std::thread server(getServerMessage<gameHordeRealtime>, client);
				server.join();
				while(!client->canDeleteGame()) {}
				delete client;
			}
			else
            {
				auto client = new gameHorde(white_player, black_player, socketfd, team, REALTIME_OFF, pieces);
				std::thread server(getServerMessage<gameHorde>, client);
				server.join();
				while (!client->canDeleteGame()) {}
				delete client;
			}
			break;
		}
		case DARK_GAMEMODE:
        {
			if(mode == SERVER_REALTIME_MODE)
            {
				auto client = new gameDarkRealtime(white_player, black_player, socketfd, team, REALTIME_ON);
				std::thread server(getServerMessage<gameDarkRealtime>, client);
				server.join();
				while (!client->canDeleteGame()) {}
				delete client;
			}
			else
            {
				auto client = new gameDark(white_player, black_player, socketfd, team, REALTIME_OFF, pieces);
				std::thread server(getServerMessage<gameDark>, client);
				server.join();
				while (!client->canDeleteGame()) {}
				delete client;
			}
			break;
		}
		case DICE_GAMEMODE:
        {
			if (mode == SERVER_REALTIME_MODE)
            {
				auto client = new gameDiceRealtime(white_player, black_player, socketfd, team, REALTIME_ON);
				std::thread server(getServerMessage<gameDiceRealtime>, client);
				server.join();
				while (!client->canDeleteGame()) {}
				delete client;
			}
			else
            {
				auto client = new gameDice(white_player, black_player, socketfd, team, REALTIME_OFF, pieces);
				std::thread server(getServerMessage<gameDice>, client);
				server.join();
				while (!client->canDeleteGame()) {}
				delete client;
			}
			break;
		}
	}
    sleep(6);
    delete black_player;
    delete white_player;
}

bool sendToServer(unsigned choice, bool has_chosen, std::string mode, GameMenu menu)
// Send to the server the client's information relating to the start of a game.
{
    choice--;
	switch (choice)
    {
		case DEFAULT_GAMEMODE: // Starting a default gamemode with mode (default/real time) mode
        {
			sending_T(socketfd, prepForSending(231, mode));
			has_chosen = true;
			break;
		}
		case HORDE_GAMEMODE:
        {
			sending_T(socketfd, prepForSending(232, mode));
			has_chosen = true;
			break;
		}
		case DARK_GAMEMODE:
        {
			sending_T(socketfd, prepForSending(233, mode));
			has_chosen = true;
			break;
		}
		case DICE_GAMEMODE:
        {
			sending_T(socketfd, prepForSending(234, mode));
			has_chosen = true;
			break;
		}
		default:
        {
			menu.startGameMenu();
			break;
		}
	}
	return has_chosen;
}

////////////////////////////////////////////////////////////////////////////////
// INTERPRETING SERVER'S INFORMATION
////////////////////////////////////////////////////////////////////////////////

template <typename G>
void getServerMessage(G* game)
// Get information sent by the server.
{
    std::string message;
    char buffer[MAXBUFSIZE];
    int opcode_first, opcode_second;
    bool running = true;
    while (running)
    {
        memset(buffer, '\0', MAXBUFSIZE);
        if(receiving_T(socketfd, buffer))
        {
            message = buffer;
            opcode_first = std::stoi(message.substr(0, 1)); // Get the message by cutting its opcode
            opcode_second = std::stoi(message.substr(1, 1));
            message = message.substr(3, message.size());
			running = decodeServerMessage(opcode_first, opcode_second, message, game, running);
        }
        else { sleep(2); endwin(); close(socketfd); exit(1); }
    }
}

template <typename G>
bool decodeServerMessage(int opcode_first, int opcode_second, std::string message, G* game, bool running)
// Decode information sent by the server.
{
	switch (opcode_first)
    {
		case 1: // IN GAME'S ACTIONS
        {
			switch (opcode_second)
            {
				case 1: { game->messageFromServer(1, message); break; } // Move
				case 2: { game->messageFromServer(2, message); break; } // Resign
				case 3: { game->messageFromServer(3, message); break; } // Win
				case 4: { game->messageFromServer(4, message); break; } // Promotion
				case 5: { game->messageFromServer(5, message); break; } // Swap turn
				case 6: { running = false; break; } // Stop thread
				case 7: { game->messageFromServer(7, message); break; } // Roll the dices (Dice Chess)
				case 8: { game->messageFromServer(8, message); break; } // Save a game
			}
			break;
		}
		case 3: // CHAT
        {
			switch (opcode_second)
            {
				case 3: { game->messageFromServer(33, message); break; }  // Message
			}
			break;
		}
	}
	return running;
}
