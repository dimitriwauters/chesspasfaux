// INFO-F209 - Groupe 5 - client.hpp

#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <csignal>
#include <iostream>
#include <fstream>
#include <ctime>
#include "../Network.hpp"
#include "../Player.hpp"
#include "GameClient.hpp"
#include "GameClientRealtime.hpp"
#include "BoardClient.hpp"
#include "../Gamemodes.hpp"
#include "menu.hpp"

typedef GameClient<GameDefault<BoardClient<Board>>, BoardClient<Board>> gameDefault;
typedef GameClient<GameDark<BoardClient<BoardDark>>, BoardClient<BoardDark>> gameDark;
typedef GameClient<GameDice<BoardClient<BoardDice>>, BoardClient<BoardDice>> gameDice;
typedef GameClient<GameHorde<BoardClient<BoardHorde>>, BoardClient<BoardHorde>> gameHorde;
typedef GameClientRealtime<GameClient<GameDefault<BoardClient<Board>>, BoardClient<Board>>> gameDefaultRealtime;
typedef GameClientRealtime<GameClient<GameDark<BoardClient<BoardDark>>, BoardClient<BoardDark>>> gameDarkRealtime;
typedef GameClientRealtime<GameClient<GameDice<BoardClient<BoardDice>>, BoardClient<BoardDice>>> gameDiceRealtime;
typedef GameClientRealtime<GameClient<GameHorde<BoardClient<BoardHorde>>, BoardClient<BoardHorde>>> gameHordeRealtime;

int socketfd;

// Connection
void signalHandler(int);
// Starting a game
void run(bool, GameMenu);
std::string chooseModeGame(std::string, GameMenu);
unsigned continueSavedGame(unsigned, std::vector<std::string>*);
void launchMatchmaking(GameMenu, unsigned, std::string, std::vector<std::string>);
void waiting();
void printQuotes();
void launchGame(unsigned, std::string, Player*, Player*, int, std::vector<std::string>);
bool sendToServer(unsigned, bool, std::string, GameMenu);
// Interpreting's server information
template <typename G> void getServerMessage(G*);
template <typename G> bool decodeServerMessage(int, int, std::string, G*, bool);

#endif
