// INFO-F209 - Groupe 5 - Terminal.cpp

#include "Terminal.hpp"

Terminal::Terminal(std::pair<int, int> size, std::pair<int, int> coord, std::vector<std::string> text): size(std::move(size)), coord(std::move(coord)), terminal_text(std::move(text)) {
    terminal_text.reserve(terminal_text.size()+1);
    terminal_changed = TRUE;
    terminal = nullptr;
}

Terminal::Terminal(Player* me, Player* other, int server): me(me), other(other), serverfd(server) {
    terminal_text.reserve(MAX_ROWS);
    terminal_changed = TRUE;
    terminal = nullptr;
}

Terminal::~Terminal() = default;

void Terminal::setPrefixCommand(std::string command) {
    prefix_command = std::move(command);
}

void Terminal::refreshTerminal(int length, int width, int coord_length, int coord_width, bool force_refresh) {
    // refresh the whole terminal
    if(size.first != length || size.second != width || coord.first != coord_length || coord.second != coord_width || terminal_changed || force_refresh) {
        terminal_changed = FALSE;
        size.first = length;
        size.second = width;
        coord.first = coord_length;
        coord.second = coord_width;
        max_message_length = size.first-15;
        if(static_cast<int>(terminal_command.length()) > max_message_length) {
            terminal_command = terminal_command.substr(0, static_cast<unsigned>(max_message_length));
        }
        if(terminal) {
            wclear(terminal);
            delwin(terminal);
        }
        terminal = subwin(stdscr, width, length, coord_width, coord_length);
        box(terminal, '*', '*');
        mvwhline(terminal, width-3, 2, 0, length-4);
        offset = 0;
        unsigned i = 0;
        while((i+offset) < getMaxLines() && i < terminal_text.size())
        {
            addText(i+1, terminal_text[terminal_text.size()-1-i]);
            i++;
        }
        std::string text = TERMINAL_WRITE + terminal_command;
        mvwprintw(terminal, width-2, 3, text.c_str());
    }
}

unsigned Terminal::getMaxLines() {
    // Return as unisgned the number of lines contained by the terminal
    unsigned result;
    if(terminal_text.size() > static_cast<unsigned>(size.second-4)) result = static_cast<unsigned>(size.second-4);
    else result = static_cast<unsigned>(terminal_text.size());
    return result;
}

void Terminal::addText(unsigned line, std::string text) {
    unsigned long max_size = (size.first-2) - me->getUsername().length();
    while(text.length() >= 1) {
        if(text.length() > max_size) {
            mvwprintw(terminal, line+offset, 2, text.substr(0, max_size).c_str());
            text = text.substr(max_size+1, text.size());
            if(text.length() >= 1) offset++;
        }
        else {
            mvwprintw(terminal, line+offset, 2, text.c_str());
            text = "";
        }
    }
}

void Terminal::write(std::string text) {
    terminal_text.emplace_back(text);
    terminal_changed = TRUE;
}

/////////////////////////////////////////////////////////////////
// COMMAND
/////////////////////////////////////////////////////////////////

void Terminal::addLetterCommand(char letter) {
    if(terminal_command.length() < static_cast<unsigned>(max_message_length)) {
        terminal_command += letter;
        terminal_changed = TRUE;
    }
}

void Terminal::removeLetterCommand() {
    // clean the command
    terminal_command = terminal_command.substr(0, terminal_command.length()-1);
    terminal_changed = TRUE;
}

bool Terminal::interpretCommand() {
    bool result = FALSE;
    terminal_changed = TRUE;
    if(terminal_command.length() > 0) {
        if (prefix_command.length() > 0) {
			doCommand();
			prefix_command = "";
		}
        else getCommand();
    }
    terminal_command = "";
    return result;
}

void Terminal::doCommand() {
    // executation of the command entered by the user
	if (prefix_command == TERMINAL_QUIT) {
		if (terminal_command == YES) signal = 9;
		else { write(TERMINAL_CONTINUE); signal = -1; }
	}
	else if (prefix_command == TERMINAL_QUIT_NULL) {
		if (terminal_command == YES) signal = 8;
		else { write(TERMINAL_CONTINUE); signal = -2; }
	}
	else if (prefix_command == TERMINAL_SAVE) {
		if (terminal_command == YES) signal = 14;
		else { write(TERMINAL_CONTINUE); signal = -1; }
	}
	else if (prefix_command == TERMINAL_SAVE_OTHER) {
		if (terminal_command == YES) signal = 15;
		else { write(TERMINAL_CONTINUE); signal = 16; }
	}
	else if (prefix_command == QUEEN) {
		if (terminal_command == YES) {signal = 10;}
		else { write(TERMINAL_CHOOSE_PIECE); signal = -3; }
	}
	else if (prefix_command == KNIGHT) {
		if (terminal_command == YES) signal = 11;
		else { write(TERMINAL_CHOOSE_PIECE); signal = -3; }
	}
	else if (prefix_command == ROOK) {
		if (terminal_command == YES) signal = 12;
		else { write(TERMINAL_CHOOSE_PIECE); signal = -3; }
	}
	else if (prefix_command == BISHOP) {
		if (terminal_command == YES) signal = 13;
		else { write(TERMINAL_CHOOSE_PIECE); signal = -3; }
	}
}

void Terminal::getCommand() {
    // Interpretation of the command entered by the user
   if (terminal_command == TERMINAL_QUIT) {
		write(TERMINAL_ASK_QUIT);
		prefix_command = TERMINAL_QUIT;
	}
	else if (terminal_command == TERMINAL_SAVE) {
		write(TERMINAL_ASK_SAVE);
		prefix_command = TERMINAL_SAVE;
	}
	else if(terminal_command == QUEEN) {
		write(TERMINAL_PROMOTION_ASK_QUEEN);
		prefix_command = QUEEN;
	}
	else if(terminal_command == KNIGHT) {
		write(TERMINAL_PROMOTION_ASK_KNIGHT);
		prefix_command = KNIGHT;
	}
	else if(terminal_command == ROOK) {
		write(TERMINAL_PROMOTION_ASK_ROOK);
		prefix_command = ROOK;
	}
	else if(terminal_command == BISHOP) {
		write(TERMINAL_PROMOTION_ASK_BISHOP);
		prefix_command = BISHOP;
	}
	else {
		std::string message = "[" + me->getUsername() + "] : " + terminal_command;
		write(message);
		sending_T(serverfd, prepForSending(330, message));
	}
}

int Terminal::getSignal() {
    int status = signal;
    signal = 0;
    return status;
}
