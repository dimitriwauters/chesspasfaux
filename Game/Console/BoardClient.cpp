// INFO-F209 - Groupe 5 - BoardClient.cpp
// Display a graphic representation in the console of the board (client side only)
// using ncurses.

#include "BoardClient.hpp"

template <typename B>
BoardClient<B>::BoardClient() : B()
{
    last_turn = false;
    initializeConsole();
}

template <typename B>
BoardClient<B>::~BoardClient() = default;

////////////////////////////////////////////////////////////////////////////////
// GRAPHICAL REPRESENTATION WITH NCURSES (CONSOLE MODE)
////////////////////////////////////////////////////////////////////////////////

// Console & Windows

template <typename B>
void BoardClient<B>::initializeConsole()
// Initialize and prepare (graphically) the console mode.
{
    setlocale(LC_ALL, "");
    initscr();
    noecho();
    cbreak();
    mousemask(ALL_MOUSE_EVENTS, nullptr);
    keypad(stdscr, TRUE);
    start_color();
    curs_set(FALSE);
    // Set of colors
    init_color(COLOR_MAGENTA, 500, 500, 500); // Gray
    init_pair(1, COLOR_BLACK, COLOR_MAGENTA); // Black on gray
    init_pair(2, COLOR_BLACK, COLOR_YELLOW); // Black on orange
    init_pair(3, COLOR_BLACK, COLOR_WHITE); // Black on white
    init_pair(4, COLOR_BLACK, COLOR_BLUE); // Black on blue
    init_pair(5, COLOR_WHITE, COLOR_BLACK); // White on black
    init_pair(6, COLOR_BLACK, COLOR_RED); // Black on red
    length_window = 0;
    width_window = 0;
}

template <typename B>
bool BoardClient<B>::refreshDisplay(int length, int width, unsigned length_oversize,\
    unsigned width_oversize, bool player_turn)
// Refresh the display and update the chessboard (by creating it every time the
// function is called). Return if it was done or not.
{
    if (length != length_window || width != width_window || board_changed || \
        last_turn != player_turn)
    {
        last_turn = player_turn;
        board_changed = false;
        clear();
        length_window = length;
        width_window = width;
        length_window_oversize = length_oversize;
        width_window_oversize = width_oversize;
        createBoard();
        createWindows();
        for (int i = 0; i < MAX_COLS; ++i)
        {
            for(int j = 0; j < MAX_ROWS; ++j)
            {
                if (!this->isEmpty(std::pair<int, int>(i, j))) // Pieces
                {
                    addTextWindow(this->cases[i][j], this->board_mat[i][j]->getType().c_str(), \
                        this->board_mat[i][j]->getColor());
                }
            }
        }
        // If a user selects a square
        if (getSelectedWindow()) wbkgd(getSelectedWindow(), COLOR_PAIR(2));
        return TRUE;
    }
    return FALSE;
}

template <typename B>
void BoardClient<B>::drawBox(short pair, bool turn, bool color)
// Draw the box around the chessboard.
{
    std::string color_str, res;
    if (turn) wattron(stdscr, pair);
    box(stdscr, '*', '*');
    if (color) color_str = WHITE_P;
    else color_str = BLACK_P;
    res = YOU_PLAY + color_str;
    mvwprintw(stdscr, 0, static_cast<int>(COLS/2-res.length()/2), res.c_str());
    wattroff(stdscr, pair);
}

template <typename B>
void BoardClient<B>::createBoard()
// Draw the board lines (depending on the size of the window).
{
    for (int i = 1; i < MAX_COLS; ++i)
    {
        mvwvline(stdscr, 1+width_window_oversize/2, (i*length_window)+ \
        length_window_oversize/2, 0, width_window*MAX_ROWS);
    }
    for (int i = 1; i < MAX_ROWS; ++i)
    {
        mvwhline(stdscr, (i*width_window)+width_window_oversize/2, 1+ \
        length_window_oversize/2, 0, length_window*MAX_ROWS);
    }
}

template <typename B>
void BoardClient<B>::createWindows()
// Draw the board (depending on the size of the window).
{
    bool switcher = true;
    WINDOW* new_win;
    for (int i = 0; i < MAX_ROWS; ++i)
    {
        for (int j = 0; j < MAX_COLS; ++j)
        {
            new_win = subwin(stdscr, width_window, length_window, \
              i*width_window+1+width_window_oversize/2, j*length_window+1+ \
              length_window_oversize/2);
            if (this->cases[i][j])
            {
                if (this->cases[i][j] == getSelectedWindow()) selectedWindow = new_win;
                delwin(this->cases[i][j]);
            }
            this->cases[i][j] = new_win;
            if (switcher)
            {
                wbkgd(this->cases[i][j], COLOR_PAIR(1));
                switcher = false;
            }
            else switcher = true;
        }
        switcher = (switcher == true) ? false : true;
    }
}

template <typename B>
void BoardClient<B>::selectWindow(std::pair<int, int> coord)
// Define the selected window and its coord.
{
    selectedWindow = this->cases[coord.first][coord.second];
    selectedWindowCoord = coord;
    board_changed = true;
}

template <typename B>
WINDOW* BoardClient<B>::getSelectedWindow()
// Return the selected window.
{
    return selectedWindow;
}

template <typename B>
std::pair<int, int> BoardClient<B>::getSelectedWindowCoord()
// Return the coord of the selected window.
{
    return selectedWindowCoord;
}

template <typename B>
void BoardClient<B>::selectWindow()
{
    selectedWindow = nullptr;
    selectedWindowCoord = std::pair<int, int>(-1, -1);
    board_changed = true;
}

template <typename B>
void BoardClient<B>::addTextWindow(WINDOW* win, const char* string, bool white)
// Print the text string on the window win (given by parameter).
{
    int max_x, max_y;
    getmaxyx(win, max_x, max_y);
    max_y -= static_cast<int>(strlen(string));
    if (white) wattron(win, COLOR_PAIR(3));
    mvwprintw(win, max_x/2-1, max_y/2, string);
    wattroff(win, COLOR_PAIR(3));
    wrefresh(win);
}

template <typename B>
std::pair<int, int> BoardClient<B>::coordToWindow(std::pair<int, int> coord)
// Convert coord to a window.
{
    int length = static_cast<int>(std::floor((coord.first-length_window_oversize/2)/length_window));
    int width = static_cast<int>(std::floor((coord.second-2)/width_window));
    return {width, length};
}

template <typename B>
WINDOW* BoardClient<B>::getWindow(std::pair<int, int> coord)
// Give the window corresponding to the position coord given by parameter.
{
    return this->cases[coord.first][coord.second];
}

template <typename B>
std::pair<int, int> BoardClient<B>::mouseClicked(int ch)
// Give the window on which the player has clicked.
{
    std::pair<int, int> res = {NO_DEFINED, NO_DEFINED};
    MEVENT event;
    if (ch == KEY_MOUSE)
    {
        if (getmouse(&event) == OK) res = coordToWindow(std::pair<int, int>(event.x, event.y));
    }
    return res;
}

template <typename B>
int BoardClient<B>::getKeyboardKey()
// Return the key pressed by the player.
{
    int ch = getch();
    return ch;
}

////////////////////////////////////////////////////////////////////////////////
// PIECES (functions overriden, cf. Board.cpp)
////////////////////////////////////////////////////////////////////////////////

template <typename B>
void BoardClient<B>::placePieces(bool color)
{
    B::placePieces(color);
    board_changed = true;
}

template <typename B>
bool BoardClient<B>::addPiece(int piece, std::pair<int, int> coord, bool color)
{
    bool res = false;
    if (this->isEmpty(coord))
    {
        B::addPiece(piece, coord, color);
        board_changed = true;
        res = true;
    }
    return res;
}

template <typename B>
void BoardClient<B>::removeAllPieces()
{
    refresh();
    for (int i = 0; i < MAX_ROWS; ++i)
    {
        for (int j = 0; j < MAX_COLS; ++j)
        {
            removePiece(std::pair<int, int>(i, j));
            delwin(this->cases[i][j]);
        }
    }
    sleep(5);
    endwin();
}

template <typename B>
bool BoardClient<B>::removePiece(std::pair<int, int> coord)
{
    bool res = false;
    if (!this->isEmpty(coord))
    {
        B::removePiece(coord);
        board_changed = true;
        res = true;
    }
    return res;
}

////////////////////////////////////////////////////////////////////////////////
// MOVES
////////////////////////////////////////////////////////////////////////////////

template <typename B>
void BoardClient<B>::showMovesSelectedPiece(std::vector<std::pair<int, int>> moves)
// Display the valid moves for the piece selected by the player.
{
    if (getSelectedWindow())
    {
        if (!moves.empty())
        {
            for(unsigned i = 0; i < moves.size(); ++i) wbkgd(getWindow(moves[i]), COLOR_PAIR(4));
        }
        wbkgd(getSelectedWindow(), COLOR_PAIR(2));
    }
}

// Functions overriden (cf. Board.cpp)

template <typename B>
void BoardClient<B>::setNewPosition(std::pair<int, int> src_pos, std::pair<int, int> dest_pos)
{
    B::setNewPosition(src_pos, dest_pos);
    if (!this->isVirtualMove()) board_changed = true;
}

template <typename B>
bool BoardClient<B>::isSpecialMove(std::pair<int, int> src_pos)
{
	B::isSpecialMove(src_pos);
	return true;
}
