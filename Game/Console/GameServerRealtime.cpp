// INFO-F209 - Groupe 5 - GameServerRealtime.cpp

#include "GameServerRealtime.hpp"

template <typename B>
GameServerRealtime<B>::GameServerRealtime(Player* white_player, Player* black_player, int type) : B(white_player, black_player, type) {
    this->turn = WHITE;
    std::thread timer_thread(&GameServerRealtime<B>::timer, this);
    timer_thread.detach();
}

template <typename B>
GameServerRealtime<B>::~GameServerRealtime() = default;

template <typename B>
void GameServerRealtime<B>::messageFromServer(Player* player, int opcode, std::string buffer) {
// Receive message from player.
    switch (opcode) {
        case 1: { // Check move the player send
            bool found = false;
            std::cout << "--- --- ---" << std::endl;
            std::cout << buffer << std::endl;
            std::pair<std::pair<int, int>, std::pair<int, int>> fromBuffer = this->stringToPair(const_cast<char*>(buffer.c_str()));
            for(unsigned i = 0; i < blocked_pieces.size(); ++i)
            {
                if(blocked_pieces.at(i).first == fromBuffer.first) { found = true; break; }
            }
            unsigned result = this->checkMove(fromBuffer.first, fromBuffer.second, this->turn);     
            if(result && !found) {
                std::cout << buffer << std::endl;
                movePiece(fromBuffer.first, fromBuffer.second, result);
                if (result == MOVE_ALLOWED || result == MOVE_EN_PASSANT)
                    this->move(const_cast<char*>(buffer.c_str()));
                else if((result == MOVE_CASTLING_LONG) || (result == MOVE_CASTLING_SHORT))
                    this->roque(const_cast<char*>(buffer.c_str()), result, fromBuffer);
                else if(result == MOVE_PROMOTION)
                {
                    sending_T(this->player[WHITE]->getSocket(), prepForSending(110, buffer));
                    sending_T(this->player[BLACK]->getSocket(), prepForSending(110, buffer));
                }
            }
            else {
                buffer = "9999"; // Valeur sentinelle
                std::cout << buffer << std::endl;
                sending_T(player->getSocket(), prepForSending(110, buffer));
            }
            std::cout << "--- --- ---" << std::endl; // TEMP
            if(!this->notOver()) this->finishGame();
            break;
        }
        default: {
            B::messageFromServer(player, opcode, buffer);
            break;
        }
    }
}

template <typename B>
bool GameServerRealtime<B>::willBeInCheck(std::pair<int, int> src __attribute__((unused)), std::pair<int, int> dest __attribute__((unused)), bool playerTurn __attribute__((unused)))
{
    return false;
}

template <typename B>
bool GameServerRealtime<B>::isUnderAttack(std::pair<int, int> current_pos __attribute__((unused)), bool color __attribute__((unused)))
{
    return false;
}

template <typename B>
void GameServerRealtime<B>::checkWinConditions(bool playerTurn)
{
    if(this->isInStalemate(!playerTurn)) this->running_game = false;
    if(this->isInStalemate(playerTurn)) this->running_game = false;
    if(this->getKingPosition(!playerTurn).first == -1) this->setWinner(playerTurn);
    if(this->getKingPosition(playerTurn).first == -1) this->setWinner(!playerTurn);
}

template <typename B>
bool GameServerRealtime<B>::movePiece(std::pair<int, int> src_pos, std::pair<int, int> dest_pos, unsigned canMove)
{
    if(canMove != MOVE_NOT_ALLOWED) {
        blocked_pieces.emplace_back(std::make_pair(dest_pos, current_tick));
        return B::movePiece(src_pos, dest_pos, canMove);
    }
    return false;
}

template <typename B>
unsigned GameServerRealtime<B>::enPassant(std::pair<int, int> src_pos, std::pair<int, int> dest_pos)
{
    bool found = false;
    unsigned passant = B::enPassant(src_pos, dest_pos);
    if(passant) {
        std::pair<int, int> cell;
        cell = std::make_pair(src_pos.first, dest_pos.second);
        for(unsigned i = 0; i < blocked_pieces.size(); ++i)
        {
            if(blocked_pieces.at(i).first == cell) { found = true; break; }
        }
        if(!found) { passant = 0; }
    }
    return passant;
}

template <typename B>
void GameServerRealtime<B>::timer() {
// Realtime server timer.
    while(this->notOver())
    {
        current_tick++;
        for(int i = static_cast<int>(blocked_pieces.size())-1; i >= 0; --i)
        {
            if(blocked_pieces.at(i).second + (REALTIME_BLOCKEDTIME / (REALTIME_DELAYTIME/1000)) <= current_tick) {
                blocked_pieces.erase(blocked_pieces.begin() + i);
            }
        }
        usleep(REALTIME_DELAYTIME*1000);
    }
}
