// INFO-F209 - Groupe 5 - server.cpp

#include "Server.hpp"

int main() {
    SockaddrIn server_addr;
    SockaddrIn chat_addr;
    SockaddrIn client_addr;
    int yes=1;

    if ((socketfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }
    if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
        perror("setsockopt");
        exit(1);
    }

    if ((chatfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("chatfd");
        exit(1);
    }
    if (setsockopt(chatfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
        perror("setsockopt");
        exit(1);
    }

    setServer(&server_addr, PORT_GAME);
    setServer(&chat_addr, PORT_CHAT);

    binding(&socketfd, &server_addr);
    binding(&chatfd, &chat_addr);

    listening(&socketfd);
    listening(&chatfd);

    std::signal(SIGINT, signalHandler);

    std::thread player_connect(playerConnection, socketfd, client_addr, &players_connected);
    player_connect.detach();

    std::thread player_chat(playerChatConnection, chatfd, chat_addr, &players_connected);
    player_chat.detach();

    serverRun();
}

template <typename B>
void endGame(std::vector<B>* games, bool shutdown) {
// End the games passed by argument.
    if(!games->empty()) {
        for (long int i = games->size()-1; i >= 0; i--) {
            if(!shutdown) {	// Game End
                if(games->at(i)->canDeleteGame()) {
                    std::pair<Player*, Player*> players = games->at(i)->getPlayers();
                    players.first->resetGamePlayer();
                    players.second->resetGamePlayer();
                    delete games->at(i);
                    games->erase(games->begin() + i);
                    break;
                }
            }
            else {	// Server shutdown
                std::pair<Player*, Player*> players = games->at(i)->getPlayers();
                sending_T(players.first->getSocket(), prepForSending(120, "drawn"));
                sending_T(players.second->getSocket(), prepForSending(120, "drawn"));
                disconnectPlayer(players.first);
                disconnectPlayer(players.second);
                delete games->at(i);
            }
        }
    }
}

void signalHandler(int signal)
{
// Server shutdown, stop all games and disconnect all players.
    std::cout << SERVER_SHUTDOWN << std::endl;
    if(!default_games.empty()) { std::cout << "--- Ending default games" << std::endl; endGame(&default_games, true); }
    if(!horde_games.empty()) { std::cout << "--- Ending horde games" << std::endl; endGame(&horde_games, true); }
    if(!dice_games.empty()) { std::cout << "--- Ending dice games" << std::endl; endGame(&dice_games, true); }
    if(!dark_games.empty()) { std::cout << "--- Ending dark games" << std::endl; endGame(&dark_games, true); }
    if(!players_connected.empty())
    {
        std::cout << "--- Disconnecting players" << std::endl;
        for (unsigned i = 0; i < players_connected.size(); ++i) {
            delete players_connected[i];
        }
    }
    std::cout << "Bye !" << std::endl;
    close(socketfd);
    exit(signal);
}

//////////////////////////////////////////////////////////////
// CLIENT
//////////////////////////////////////////////////////////////

void serverRun() {
// Principal loop of the server.
    while(TRUE) {
		connectAllWaitingPlayers();
        if(!players_waiting_continue_saved.empty()) {
            if(players_waiting_continue_saved.size() >= 2) {
                int found = -1;
                for(int i = static_cast<int>(players_waiting_continue_saved.size())-1; i >= 0; --i) {
                    for(int j = static_cast<int>(players_waiting_continue_saved.size())-1; j >= 0; --j) {
                        if(i != j) {
                            if(players_waiting_continue_saved.at(j).second == players_waiting_continue_saved.at(i).second) {
                                found = i;
                                players_waiting_continue_saved.erase(players_waiting_continue_saved.begin() + i);
                                players_waiting_continue_saved.erase(players_waiting_continue_saved.begin() + j);
                                break;
                            }
                        }
                    }
                    if(found >= 0) break;
                }
                if(found >= 0) {
                    Player *white_player, *black_player;
                    std::pair<std::string, std::string> data;
                    int gamemode;
                    getMatchDB(&data, players_waiting_continue_saved[found].second, &gamemode);
                    for (int i = 0; i < static_cast<int>(players_connected.size()); ++i) {
                        if(players_connected.at(i)->getUsername() == data.first) {
                            white_player = players_connected.at(i);
                        }
                        else if (players_connected.at(i)->getUsername() == data.second) {
                            black_player = players_connected.at(i);
                        }
                    }
                    std::cout << gamemode << std::endl;
                    if(white_player && black_player) //if 2 players found, launch game.
						serverGameLaunch(found, white_player, black_player, gamemode);
                }
            }
        }
        endGame(&default_games, false);
        endGame(&horde_games, false);
        endGame(&dice_games, false);
        endGame(&dark_games, false);
    }
}

void connectAllWaitingPlayers() {
	connectWaitingPlayers(&players_waiting_default_normal, SERVER_DEFAULT_GAMEMODE, "normal");
	connectWaitingPlayers(&players_waiting_horde_normal, SERVER_HORDE_GAMEMODE, "normal");
	connectWaitingPlayers(&players_waiting_dice_normal, SERVER_DICE_GAMEMODE, "normal");
	connectWaitingPlayers(&players_waiting_dark_normal, SERVER_DARK_GAMEMODE, "normal");
	connectWaitingPlayers(&players_waiting_default_realtime, SERVER_DEFAULT_GAMEMODE, "realtime");
	connectWaitingPlayers(&players_waiting_horde_realtime, SERVER_HORDE_GAMEMODE, "realtime");
	connectWaitingPlayers(&players_waiting_dice_realtime, SERVER_DICE_GAMEMODE, "realtime");
	connectWaitingPlayers(&players_waiting_dark_realtime, SERVER_DARK_GAMEMODE, "realtime");
}

void serverGameLaunch(int found, Player* white_player, Player* black_player, int gamemode) {
//Launch a saved game.
	std::vector<std::string> data;
	std::string message;
	getSavedGameDB(&data, players_waiting_continue_saved[found].second);
	if(!data.empty()) {
		for(int i = 0; i < static_cast<int>(data.size()); ++i) {
			message += data.at(i) + MSG_SEPARATOR;
		}
		//Send all the coordinates of the pieces to the players.
		std::cout << "Saved pieces : " << message << std::endl;
		sending(white_player->getSocket(), message);
		sending(black_player->getSocket(), message);

		sending(white_player->getSocket(), gamemode);
		sending(black_player->getSocket(), gamemode);

		sending(white_player->getSocket(), 1);
		white_player->setColor(WHITE);
		sending(black_player->getSocket(), 0);
		black_player->setColor(BLACK);

		sending(white_player->getSocket(), black_player->getUsername());
		sending(black_player->getSocket(), white_player->getUsername());

		sending(white_player->getSocket(), black_player->getELO());
		sending(black_player->getSocket(), white_player->getELO());

		serverGamemode(gamemode, white_player, black_player, data);

		deleteMatchDB(players_waiting_continue_saved[found].second);
		deleteSavedGameDB(players_waiting_continue_saved[found].second);

		std::string gamemode_str = SERVER_DEFAULT_GAMEMODE;
		if(gamemode == 1) gamemode_str = SERVER_HORDE_GAMEMODE;
		else if(gamemode == 2) gamemode_str = SERVER_DARK_GAMEMODE;
		else if(gamemode == 3) gamemode_str = SERVER_DICE_GAMEMODE;
		std::cout << SERVER_LAUNCH_GAME + white_player->getUsername() + " and " + black_player->getUsername() + " on " + gamemode_str + " (resumed game) mode !\n";
	}
}

void serverGamemode(int gamemode, Player* white_player, Player* black_player, std::vector<std::string> data) {
// Create the game with the correct gamemode.
	if(gamemode == DEFAULT_GAMEMODE) {
		auto server = new gameDefault(white_player, black_player, REALTIME_OFF, data);
		default_games.emplace_back(server);
	}
	else if (gamemode == HORDE_GAMEMODE) {
		auto server = new gameHorde(white_player, black_player, REALTIME_OFF, data);
		horde_games.emplace_back(server);
	}
	else if (gamemode == DICE_GAMEMODE) {
		auto server = new gameDice(white_player, black_player, REALTIME_OFF, data);
		dice_games.emplace_back(server);
	}
	else if (gamemode == DARK_GAMEMODE) {
		auto server = new gameDark(white_player, black_player, REALTIME_OFF, data);
		dark_games.emplace_back(server);
	}
	else std::cout << FATAL_ERROR << std::endl;
}

//////////////////////////////////////////////////////////////
// CLIENT
//////////////////////////////////////////////////////////////

void clientInfos(Player* player)
{
// Loop for receiving the infos from the player.
    std::string message;
    char buffer[MAXBUFSIZE];
    int opcode_first, opcode_second, opcode_third;
    while(TRUE) { //While the player is connected
        memset(buffer, '\0', MAXBUFSIZE);
        if(receiving_T(player->getSocket(), buffer)) {
            std::cout << SERVER_RECEIVED_INFO << buffer << std::endl;
            message = buffer;
            opcode_first = std::stoi(message.substr(0, 1));
            opcode_second = std::stoi(message.substr(1, 1));
            opcode_third = std::stoi(message.substr(2, 1));
            message = message.substr(3, message.size());
            switch (opcode_first) {
                case 1: { // Game
					gameSwitch(opcode_second, message, player);
                    break;
                }
                case 2: { // Menu
					menuSwitch(opcode_second, opcode_third, message, player);
                    break;
                }
                case 3: { // Chat
					chatSwitch(opcode_second, message, player);
                    break;
                }
            }
        }
        else break; // Player disconnect
    }
    if(!player->getUsername().empty()) std::cout << player->getUsername() + SERVER_DECONNECTION << std::endl;
    disconnectPlayer(player);
    auto game = static_cast<gameDefault *>(getGameFromPlayer(player));
    if(game) {
        std::pair<Player*, Player*> players = game->getPlayers();
        if(players.first == player) { game->messageFromServer(players.first, 2, ""); }
        else { game->messageFromServer(players.second, 2, ""); }
    }
}

void gameSwitch(int opcode_second, std::string message, Player* player) {
//Switch for game informations.
	switch (opcode_second) { 
		case 1: { // Move
			auto game = static_cast<gameDefault *>(getGameFromPlayer(player));
			game->messageFromServer(player, 1, message);
			break;
		}
		case 2: { // Give Up
			auto game = static_cast<gameDefault *>(getGameFromPlayer(player));
			game->messageFromServer(player, 2, "");
			break;
		}
		case 3: { // Winner
			auto game = static_cast<gameDefault *>(getGameFromPlayer(player));
			game->messageFromServer(player, 3, message);
			break;
		}
		case 4: { // Change piece (Promotion)
			auto game = static_cast<gameDefault *>(getGameFromPlayer(player));
			game->messageFromServer(player, 4, message);
			break;
		}
		case 5: { // Swap turn
			auto game = static_cast<gameDefault *>(getGameFromPlayer(player));
			game->messageFromServer(player, 5, message);
			break;
		}
		case 7: { // Roll the dices
			auto game = static_cast<gameDefault *>(getGameFromPlayer(player));
			game->messageFromServer(player, 7, message);
			break;
		}
		case 8: { // Save game
			auto game = static_cast<gameDefault *>(getGameFromPlayer(player));
			game->messageFromServer(player, 8, message);
			break;
		}
	}
}

void menuSwitch(int opcode_second, int opcode_third, std::string message, Player* player) {
// Switch for menu informations.
	char* messageToChar = const_cast<char *>(message.c_str());
	switch (opcode_second) {
		case 1: { // Sign up
			menuSignUp(message, player);
			break;
		}
		case 2: { // Sign in
			menuSignIn(message, player);
			break;
		}
		case 3: { // Launch game
			menuGameLaunch(opcode_third, message, player);
			break;
		}
		case 4: { // Stats
			menuStats(player);
			break;
		}
		case 5: { // General stats
			menuGeneralStats(message, player);
			break;
		}
		case 6: { // Friends
			menuFriends(opcode_third, messageToChar, player);
			break;
		}
		case 7: { // Continue game
			menuContinueGame(opcode_third, message, player);
			break;
		}
		case 8: { // Friends stats
			menuFriendsStats(player);
			break;
		}
	}
}

void menuSignUp(std::string message, Player* player) {
// Sign up confirmation.
	std::string username;
	std::string password;
	std::string result;
	long unsigned int pos = message.find(MSG_SEPARATOR);
	username = message.substr(0, pos);
	password = message.substr(pos + 1, message.size());
	countDB(&result, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, username);
	if(std::stoi(result) == 0) { // if no same pseudo, player is signed up
		insertPlayerDB(username, password);
		sending_T(player->getSocket(), prepForSending(220, "true"));
		std::cout << username + SERVER_SUBSCRIPTION << std::endl;
	}
	else sending_T(player->getSocket(), prepForSending(220, "false"));
}

void menuSignIn(std::string message, Player* player) {
// Sign in confirmation.
	std::string username;
	std::string password;
	std::string result;
	bool is_connected = false;
	long unsigned int pos = message.find(MSG_SEPARATOR);
	username = message.substr(0, pos);
	password = message.substr(pos + 1, message.size());
	for(unsigned i = 0; i < players_connected.size(); ++i) {
		if(players_connected.at(i)->getUsername() == username) {
			is_connected = true; // Set the player has connected.
		}
	}
	if(!is_connected) {	// Player not conected
		selectDB(&result, DB_PASSWORD_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, username);
		if(password == result) { // Check password
			sending_T(player->getSocket(), prepForSending(220, "true"));
			player->setUsername(username);
			selectDB(&result, DB_ELO_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, username);
			player->setELO(static_cast<unsigned>(std::stoi(result)));
			setFriendsList(player);
			setFriendsRequestsList(player);
			std::cout << player->getUsername() + SERVER_CONNECTION << std::endl;
		}
		else sending_T(player->getSocket(), prepForSending(220, "false"));
	} // Player already connected
	else sending_T(player->getSocket(), prepForSending(220, "connected"));
}

void menuGameLaunch(int opcode_third, std::string message, Player* player) {
// Set the player in the correct gamemode waiting list.
	switch (opcode_third) {
		case 1: { // Launch Game -> Default
			if(message == "realtime") players_waiting_default_realtime.emplace_back(player);
			else players_waiting_default_normal.emplace_back(player);
			break;
		}
		case 2: { // Launch Game -> Horde
			if(message == "realtime") players_waiting_horde_realtime.emplace_back(player);
			else players_waiting_horde_normal.emplace_back(player);
			break;
		}
		case 3: { // Launch Game -> Dark
			if(message == "realtime") players_waiting_dark_realtime.emplace_back(player);
			else players_waiting_dark_normal.emplace_back(player);
			break;
		}
		case 4: { // Launch Game -> Dice
			if(message == "realtime") players_waiting_dice_realtime.emplace_back(player);
			else players_waiting_dice_normal.emplace_back(player);
			break;
		}
	}
}

void menuStats(Player* player) {
// Send the stats of a player.
	std::string result;
	selectDB(&result, DB_ELO_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, player->getUsername());
	std::string sender = player->getUsername();
	sender += MSG_SEPARATOR;
	sender += result;
	sending_T(player->getSocket(), prepForSending(240, sender));
}

void menuFriendsStats(Player* player){
// Send the stats of a player's friends.
	std::string response;
	std::string result;
	std::vector<std::string> friendsUsernames = player->getFriendsUsernames();
	std::vector<std::pair<std::string, int>> points_elo;
    for (auto i = friendsUsernames.begin(); i != friendsUsernames.end(); ++i){
		// Get all friends and get their ELO
		selectDB(&result, DB_ELO_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, *i);
		points_elo.push_back(std::pair<std::string, int>({*i, std::atoi(result.c_str())}));
	}
	std::sort (points_elo.begin(), points_elo.end(), compareStats);
	for(unsigned i=0; i<points_elo.size(); ++i){
		// Sort friends by ELO points
		response += points_elo[i].first + MSG_SEPARATOR;
		response += std::to_string(points_elo[i].second) + MSG_SEPARATOR;
	}
	sending_T(player->getSocket(), prepForSending(280, response));
}

void menuGeneralStats(std::string message, Player* player) {
// Send stats of all players.
	std::string response;
	std::vector<std::pair<std::string, int>> points_elo;
	selectDBLimit(&points_elo, "pseudo, elo", "Players", "elo", std::stoi(message));
	for (unsigned i = 0; i < points_elo.size(); ++i) {
		response += points_elo[i].first + MSG_SEPARATOR;
		response += std::to_string(points_elo[i].second) + MSG_SEPARATOR;
	}
	sending_T(player->getSocket(), prepForSending(250, response));
}

bool compareStats(std::pair<std::string, int> a, std::pair<std::string, int> b){
// Compare 2 ELO.
    return (a.second>b.second);
}

void menuFriends(int opcode_third, char* messageToChar, Player* player) {
// Switch for friend informations.
	switch (opcode_third) {
		case 1: { // Add friend
			addFriendRequest(messageToChar, player->getSocket());
			break;
		}
		case 2: { // Delete friend
			removeFriend(messageToChar, player->getSocket());
			break;
		}
		case 3: { // Request friend
			addFriend(messageToChar,player->getSocket());
			break;
		}
		case 4: { // Get friends names.
			sendFriendsUsernames(player->getSocket());
			break;
		}
		case 5:{ // Get friends' resquests
			sendFriendsRequestsUsernames(player->getSocket());
			break;
		}
		case 6:{ // Delete friend resquest
			removeFriendRequest(messageToChar, player->getSocket());
			break;
		}
	}
}

void menuContinueGame(int opcode_third, std::string message, Player* player) {
// Menu for continue game.
	switch (opcode_third) {
		case 0: { // Saved game info
			std::string result;
			std::vector<std::vector<std::string>> data;
			getMatchDB(&data, player->getUsername());
			for(int i = 0; i < static_cast<int>(data.size()); ++i) {
				result += data.at(i).at(0) + "+" + data.at(i).at(1).substr(4, data.at(i).at(1).size()-5) + "+" + data.at(i).at(2) + MSG_SEPARATOR;
			}
			sending(player->getSocket(), result);
			break;
		}
		case 1: { // Launch a saved game
			std::pair<std::string, std::string> data;
			int gamemode;
			getMatchDB(&data, message, &gamemode);
			if(!data.first.empty()) players_waiting_continue_saved.emplace_back(std::make_pair(player, message));
			break;
		}
	}
}

void chatSwitch(int opcode_second, std::string message, Player* player) {
// Switch for chat informations.
	switch (opcode_second) {
		case 3: { // Message
			auto game = static_cast<gameDefault *>(getGameFromPlayer(player));
			if(game) {
				std::pair<Player*, Player*> players = game->getPlayers();
				if(players.first == player) { sending_T(players.second->getSocket(), prepForSending(330, message)); }
				else { sending_T(players.first->getSocket(), prepForSending(330, message)); }
			}
			break;
		}
	}
}

/////////////////////////////////////////////////////////////
// CHAT
/////////////////////////////////////////////////////////////

void chatInfos(Player* player)
{
// Loop for receiving the chat informations.
    std::string message;
    char buffer[MAXBUFSIZE];
    int opcode_first, opcode_second;
    while(TRUE) {
        memset(buffer, '\0', MAXBUFSIZE);
        if (receiving_T(player->getChatSocket(), buffer)) {
            std::cout << "CHAT " << SERVER_RECEIVED_INFO << buffer << std::endl;
            message = buffer;
            opcode_first = std::stoi(message.substr(0, 1));
            opcode_second = std::stoi(message.substr(1, 1));
            message = message.substr(3, message.size());
            switch (opcode_first) {
                case 3: { // CHAT
                    switch (opcode_second) {
                        case 2: { // Get usernames
							chatGetUsernames(player);
                            break;
                        }
                        case 3: { // Check username
							chatCheckUsernames(message, player);
                            break;
                        }
                        case 5: { // Message
							chatMessage(player->getUsername(), message);
                            break;
                        }
                    }
                    break;
                }
            }
        }
        else break; // Player disconnected from chat
    }
    std::cout << player->getUsername() + SERVER_LEAVE_CHAT << std::endl;
    if(player->getInChat()) {
        sending_T(player->getInChat()->getChatSocket(), prepForSending(340, "disconnected"));
        player->getInChat()->setInChat(nullptr);
        player->setInChat(nullptr);
    }
    close(player->getChatSocket());
    player->setChatSocket(0);
}

void chatGetUsernames(Player* player) {
// Get friends' usernames.
    std::string usernames;
    std::vector<std::string> friendsUsernames = player->getFriendsUsernames();
	if(!friendsUsernames.empty())
	{
		for (unsigned i = 0; i < players_connected.size(); ++i)
    	{
        	auto search = std::find(friendsUsernames.begin(), friendsUsernames.end(), players_connected.at(i)->getUsername());
        	if(search != friendsUsernames.end() && players_connected.at(i)->getUsername() != player->getUsername()) \
        	usernames += players_connected.at(i)->getUsername() + MSG_SEPARATOR;
    	}
    	sending_T(player->getChatSocket(), prepForSending(320, usernames));
	}
}

void chatCheckUsernames(std::string message, Player* player) {
// Check if player is connected to the chat.
	bool found = false;
	for (unsigned i = 0; i < players_connected.size(); ++i)
	{
		if (players_connected.at(i)->getUsername() == message && players_connected.at(i)->getUsername() \
		!= player->getUsername() && players_connected.at(i)->getChatSocket() != 0)
		{	// Player in chat
			found = true;
			if(players_connected.at(i)->getInChat()) {
				sending_T(player->getChatSocket(), prepForSending(330, "in_chat"));
			}
			else {
				sending_T(player->getChatSocket(), prepForSending(330, "true"));
				sending_T(players_connected.at(i)->getChatSocket(), prepForSending(340, player->getUsername()));
				players_connected.at(i)->setInChat(player);
				player->setInChat(players_connected.at(i));
			}
		}
	}
	if(!found) sending_T(player->getChatSocket(), prepForSending(330, "false"));
}

void chatMessage(std::string from, std::string message) {
// Send message by chat.
	std::string username;
	long unsigned int pos = message.find(MSG_SEPARATOR);
	username = message.substr(0, pos);
	message = message.substr(pos + 1, message.size());
	for (unsigned i = 0; i < players_connected.size(); ++i)
	{
		if (players_connected.at(i)->getUsername() == username)
		{
			sending_T(players_connected.at(i)->getChatSocket(), prepForSending(350, from + MSG_SEPARATOR + message));
		}
	}
}

void playerChatConnection(int chatfd, SockaddrIn chat_addr, std::vector<Player*>* players_connected) {
// Loop for connecting players to chat.
    while(TRUE) {
        int new_fd;
        accepting(&chatfd, &chat_addr, &new_fd);
        std::thread waiting(playerChatConnectionWaiting, new_fd, players_connected);
        waiting.detach();
    }
}

void playerChatConnectionWaiting(int new_fd, std::vector<Player*>* players_connected) {
// Connect the players.
	char buffer[MAXBUFSIZE];
	bool found = false;
	if(receiving_T(new_fd, buffer))
	{
		std::cout << buffer << std::endl;
		std::string username = buffer;
		for (unsigned i = 0; i < players_connected->size(); ++i) {
			if(players_connected->at(i)->getUsername() == username)
			{
				std::cout << players_connected->at(i)->getUsername() + SERVER_JOIN_CHAT << std::endl;
				players_connected->at(i)->setChatSocket(new_fd);
				found = true;
				std::thread client_chat(chatInfos, players_connected->at(i));
				client_chat.detach();
				sending_T(new_fd, prepForSending(310, ""));
				break;
			}
		}
	}
	if(!found) close(new_fd);
}

////////////////////////////////////////////////////////////////
// PLAYERS
////////////////////////////////////////////////////////////////

void connectWaitingPlayers(std::vector<Player*>* waiting, std::string gamemode, std::string mode) {
// When a least 2 players are connected to the same gamemode, start trying to connect players.
    if(!waiting->empty()) {
        if(waiting->size() >= 2) {
            unsigned range = 50;
            while(waiting->size() >= 2) {
                std::cout << range << std::endl;
                for(unsigned i = 0; i < waiting->size(); ++i) { // for every players.
                    Player* white_player = waiting->at(i);
                    for(unsigned j = 0; j < waiting->size(); ++j) {
                        if(i != j) {
                            Player* black_player = waiting->at(j);
                            unsigned black_elo = black_player->getELO();
                            unsigned white_elo = white_player->getELO();
                            unsigned min = std::min(black_elo, white_elo);
                            unsigned max = std::max(black_elo, white_elo);
                            if(((max - range) <= min) && (min <= (max + range))) {
								// Check the players with the minimum difference between their ELO.
                                if ((std::find(waiting->begin(), waiting->end(), white_player) != waiting->end()) && \
                                (std::find(waiting->begin(), waiting->end(), black_player) != waiting->end()))
                                {
                                    waiting->erase(waiting->begin() + i);
                                    waiting->erase(waiting->begin() + j - 1);
                                    sending(white_player->getSocket(), 1);
                                    white_player->setColor(WHITE);
                                    sending(black_player->getSocket(), 0);
                                    black_player->setColor(BLACK);
                                    sending(white_player->getSocket(), black_player->getUsername());
                                    sending(black_player->getSocket(), white_player->getUsername());
                                    sending(white_player->getSocket(), black_player->getELO());
                                    sending(black_player->getSocket(), white_player->getELO());
                                    playerGamemode(gamemode, mode, white_player, black_player);
                                    std::cout << SERVER_LAUNCH_GAME + white_player->getUsername() + " and " + black_player->getUsername() + " on " + gamemode + " (" + mode + ") mode !\n";
                                }
                                return;
                            }
                        }
                    }
                }
                sleep(1);
                range += 50;
                //Increase ELO research range by 50 points every second.
            }
        }
    }
}

void playerGamemode(std::string gamemode, std::string mode, Player* white_player, Player* black_player) {
// Launch the server game for the players to play.
	if(gamemode == SERVER_DEFAULT_GAMEMODE) {
		if(mode == "realtime") {
			auto server = new gameDefaultRealtime(white_player, black_player, REALTIME_ON);
			default_games.emplace_back(server);
		}
		else {
			auto server = new gameDefault(white_player, black_player, REALTIME_OFF);
			default_games.emplace_back(server);
		}
	}
	else if (gamemode == SERVER_HORDE_GAMEMODE) {
		if(mode == "realtime") {
			auto server = new gameHordeRealtime(white_player, black_player, REALTIME_ON);
			horde_games.emplace_back(server);
		}
		else {
			auto server = new gameHorde(white_player, black_player, REALTIME_OFF);
			horde_games.emplace_back(server);
		}
	}
	else if (gamemode == SERVER_DICE_GAMEMODE) {
		if(mode == "realtime") {
			auto server = new gameDiceRealtime(white_player, black_player, REALTIME_ON);
			dice_games.emplace_back(server);
		}
		else {
			auto server = new gameDice(white_player, black_player, REALTIME_OFF);
			dice_games.emplace_back(server);
		}

	}
	else if (gamemode == SERVER_DARK_GAMEMODE) {
		if(mode == "realtime") {
			auto server = new gameDarkRealtime(white_player, black_player, REALTIME_ON);
			dark_games.emplace_back(server);
		}
		else {
			auto server = new gameDark(white_player, black_player, REALTIME_OFF);
			dark_games.emplace_back(server);
		}
	}
	else std::cout << FATAL_ERROR << std::endl;
}

void playerConnection(int socketfd, SockaddrIn client_addr, std::vector<Player*>* players_connected) {
// Connect waiting players.
    while(TRUE) {
        int new_fd;
        accepting(&socketfd, &client_addr, &new_fd);
        Player* player = new Player(new_fd);
        std::thread client(clientInfos, player);
        client.detach();
        players_connected->emplace_back(player);
    }
}

void disconnectPlayer(Player* player) {
// Disconnect player.
    removePlayerFromQueue(player, &players_connected);
    removePlayerFromQueue(player, &players_waiting_default_normal);
    removePlayerFromQueue(player, &players_waiting_default_realtime);
    removePlayerFromQueue(player, &players_waiting_horde_normal);
    removePlayerFromQueue(player, &players_waiting_horde_realtime);
    removePlayerFromQueue(player, &players_waiting_dice_normal);
    removePlayerFromQueue(player, &players_waiting_dice_realtime);
    removePlayerFromQueue(player, &players_waiting_dark_normal);
    removePlayerFromQueue(player, &players_waiting_dark_realtime);
    close(player->getSocket());
    player->setSocket(0);
    if(player->getChatSocket()) close(player->getChatSocket());
    player->setChatSocket(0);
}

void* getGameFromPlayer(Player* player) {
// Get the game in which a player is connected.
    for (unsigned i = 0; i < default_games.size(); ++i) {
        std::pair<Player*, Player*> players = default_games[i]->getPlayers();
        if(players.first->getUsername() == player->getUsername() || players.second->getUsername() == player->getUsername()) return default_games[i];
    }
    for (unsigned i = 0; i < horde_games.size(); ++i) {
        std::pair<Player*, Player*> players = horde_games[i]->getPlayers();
        if(players.first->getUsername() == player->getUsername() || players.second->getUsername() == player->getUsername()) return horde_games[i];
    }
    for (unsigned i = 0; i < dice_games.size(); ++i) {
        std::pair<Player*, Player*> players = dice_games[i]->getPlayers();
        if(players.first->getUsername() == player->getUsername() || players.second->getUsername() == player->getUsername()) return dice_games[i];
    }
    for (unsigned i = 0; i < dark_games.size(); ++i) {
        std::pair<Player*, Player*> players = dark_games[i]->getPlayers();
        if(players.first->getUsername() == player->getUsername() || players.second->getUsername() == player->getUsername()) return dark_games[i];
    }
    return nullptr;
}

void removePlayerFromQueue(Player* player, std::vector<Player*>* waiting) {
// Remove player from waiting list when they are connected.
    auto search = std::find(waiting->begin(), waiting->end(), player);
    if(search != waiting->end()) {
        waiting->erase(search);
    }
}

Player* getPlayerPtr(int socketfd) {
// Get a player from his socket.
    Player* player;
    for (auto i = players_connected.begin(); i != players_connected.end(); ++i){
		player = *i;
		if (player->getSocket() == socketfd || player->getChatSocket() == socketfd){
			break;
		}
    }
    return player;
}

Player* getPlayerPtr(char* name) {
// Get a player from is pseudo.
    Player* player;
    bool found = false;
    for (auto i = players_connected.begin(); i != players_connected.end(); ++i){
		player = *i;
		if(player->getUsername()==name){
			found = true;
			break;
		}
    }
    if(found == 0){
        player=nullptr;
    }
    return player;
}
