// INFO-F209 - Groupe 5 - GameClient.hpp

#ifndef GAMECLIENT_HPP
#define GAMECLIENT_HPP

#include <unistd.h>
#include <thread>
#include "../Network.hpp"
#include "Terminal.hpp"
#include "Timer.hpp"

template <typename B, typename C>
class GameClient : public B {
protected:
    Terminal* terminal;
    int serverfd;
    int length_window;
    int width_window;
    int length_terminal;
    int width_terminal;
    unsigned length_window_oversize;
    unsigned width_window_oversize;
    //timer
    Timer* timer;
    int width_timer;
    int length_timer;

    bool game_freeze = FALSE;
    int promotionPiece = 4;
    bool canPromote = false;
    std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> preMoves;
    bool diceTurn[2] = {true, true};

			//Methods
    virtual bool itsMyTurn();
    void interpretSignalTerminal();
		//Server interaction
	void messageMove(std::string);
	void messagePromotion(std::string);
	void messageSave(std::string);
public:
    GameClient(Player*, Player*, int, int, int);
	GameClient(Player*, Player*, int, int, int, std::vector<std::string>);
    ~GameClient();
    void createBoard() override;
    void finishGame() override;
		//Run
    void run() override;
    bool terminalSize(bool);
    void boardRefreshing(bool);
    std::pair<int, int> cellSelection(std::pair<int, int>);
    virtual void showBlockedPieces();
		//Server Interactions
	void messageFromServer(int, std::string);
	virtual void moveSelectedPiece(std::pair<int, int>);
		//Terminal Interactions
    //virtual void drawBox(short, bool);
    virtual void askEndGame();
    void showKingInCheck();
		//Promotion
    void doPromotion(std::pair<int, int>);
    virtual void checkPromotion(std::pair<int, int>);
    void setPromotionPiece(int);
    int getPromotionPiece();
    void setCanPromote(bool);
    bool getCanPromote();
		//Dice
    void diceChessCheck(std::pair<int, int>);
    void displayDicePieces(int, int);
		//Premoves
    void addPreMove(std::pair<std::pair<int, int>, std::pair<int, int>>);
    void cancelPreMoves();
    std::pair<std::pair<int, int>, std::pair<int, int>> getPreMove();
    bool isPreMovesEmpty();
    void doPreMove();
};

#include "GameClient.cpp"

#endif
