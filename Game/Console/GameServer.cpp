// INFO-F209 - Groupe 5 - GameServer.cpp

#include "GameServer.hpp"

////////////////////////////////////////////////////////////////////////////////
// INITIALIZATION AND END
////////////////////////////////////////////////////////////////////////////////

template <typename B, typename C>
GameServer<B, C>::GameServer(Player* white_player, Player* black_player, int type) : B(white_player, black_player, type) {
    createBoard();
    this->turn = WHITE;
}

template <typename B, typename C>
GameServer<B, C>::GameServer(Player* white_player, Player* black_player, int type, std::vector<std::string> pieces) : B(white_player, black_player, type) {
    if(!pieces.empty()) {
        this->pieces_beginning = pieces;
    }
    createBoard();
    this->turn = WHITE;
}

template <typename B, typename C>
GameServer<B, C>::~GameServer() = default;

template <typename B, typename C>
void GameServer<B, C>::createBoard()
{
    this->board = new C();
    B::setPiecesOnBoard();
}

template <typename B, typename C>
void GameServer<B, C>::run() {
    // the game never end until it really ends
    while (this->notOver()) {}
}

template <typename B, typename C>
void GameServer<B, C>::finishGame() {
    // when game is over, finish it properly
    if(!game_saved) {
        calculELO(this->player[BLACK]);
        calculELO(this->player[WHITE]);
        for(int i = 0; i < 2; ++i) {
            if(this->player[i]->isWinner()) {
                updateDBInc("games_won", "Players", "pseudo", this->player[i]->getUsername(), 1);
                updateDBInc("games_lost", "Players", "pseudo", this->player[!i]->getUsername(), 1);
            }
            else if (!this->running_game) updateDBInc("games_drawn", "Players", "pseudo", this->player[i]->getUsername(), 1);
        }
        if (!this->running_game) insertMatchDB(this->player[WHITE]->getUsername(), this->player[BLACK]->getUsername(), "", true, this->gamemode);
        else insertMatchDB(this->player[WHITE]->getUsername(), this->player[BLACK]->getUsername(), this->getWinner()->getUsername(), true, this->gamemode);
    }
    std::cout << "End !" << std::endl;
    if(this->player[BLACK]->getSocket()) sending_T(this->player[BLACK]->getSocket(), prepForSending(160, ""));
    if(this->player[WHITE]->getSocket()) sending_T(this->player[WHITE]->getSocket(), prepForSending(160, ""));
    this->board->removeAllPieces();
    delete this->board;
    this->gameFinished();
}

////////////////////////////////////////////////////////////////////////////////
// SERVER INTERACTION
////////////////////////////////////////////////////////////////////////////////

template <typename B, typename C>
void GameServer<B, C>::messageFromServer(Player* player, int opcode, std::string buffer)
{
    // the different possible cases that can be done
    switch (opcode) {
        case 1: {	//Move
			messageMove(player, buffer);
            break;
        }
        case 2: {	//Drawn
			messageDrawn(player, buffer);
            break;
        }
        case 3: {	//Set Winner
			sending_T(this->player[!this->turn]->getSocket(), prepForSending(130, buffer));
			this->setWinner(this->turn);
            break;
        }
        case 4: {	//Promotion
			promotion(const_cast<char*>(buffer.c_str()));
			break;
		}
		case 5: {	//Swap Turn
			this->swapTurn();
			std::cout << "Passe son tour" << std::endl;
			sending_T(this->player[this->turn]->getSocket(), prepForSending(150, ""));
			break;
		}
		case 7: {	//Roll the dices
			messageDice(buffer);
			break;
		}
		case 8: {	//Save
			messageSave(player, buffer);
		    break;
		}
    }
    if(!this->notOver()) finishGame();
}

template <typename B, typename C>
void GameServer<B, C>::messageMove(Player* player, std::string buffer)
{
	if(this->player[this->turn] == player) {
		std::cout << "--- --- ---" << std::endl; // TEMP
		std::cout << buffer << std::endl; // TEMP
		std::pair<std::pair<int, int>, std::pair<int, int>> fromBuffer = this->stringToPair(const_cast<char*>(buffer.c_str()));
		unsigned result = this->checkMove(fromBuffer.first, fromBuffer.second, this->turn);
		if(result) {
			std::cout << buffer << std::endl;
			this->movePiece(fromBuffer.first, fromBuffer.second, result);
			if (result == MOVE_ALLOWED || result == MOVE_EN_PASSANT)
				move(const_cast<char*>(buffer.c_str()));
			else if((result == MOVE_CASTLING_LONG) || (result == MOVE_CASTLING_SHORT))
				roque(const_cast<char*>(buffer.c_str()), result, fromBuffer);
			else if(result == MOVE_PROMOTION) {
				sending_T(this->player[this->turn]->getSocket(), prepForSending(110, buffer));
				sending_T(this->player[!this->turn]->getSocket(), prepForSending(110, buffer));
			}
		}
		else {
			buffer = "9999"; // Valeur sentinelle
			std::cout << buffer << std::endl;
			sending_T(player->getSocket(), prepForSending(110, buffer));
		}
		std::cout << "--- --- ---" << std::endl; // TEMP
	}
	else {
		std::cout << "CHEAT ! (Ce n'est pas le tour de ce joueur)" << std::endl;
		sending_T(player->getSocket(), prepForSending(110, "9999"));
	}
}

template <typename B, typename C>
void GameServer<B, C>::messageDrawn(Player* player, std::string buffer)
{
    // warn both players of a drawn match
	if(buffer == "drawn") this->running_game = false;
	else {
		if(this->player[this->turn] == player) {
			if(this->player[!this->turn]->getSocket()) sending_T(this->player[!this->turn]->getSocket(), prepForSending(120, buffer));
			this->player[!this->turn]->hasWon();
		}
		else if(this->player[!this->turn] == player) {
			if(this->player[this->turn]->getSocket()) sending_T(this->player[this->turn]->getSocket(), prepForSending(120, buffer));
			this->player[this->turn]->hasWon();
		}
	}
}

template <typename B, typename C>
void GameServer<B, C>::messageDice(std::string buffer)
{
    // give the information to both players about the dice result
	std::cout << "Server roll the dices" << std::endl;
	this->rollTheDices();
	std::pair<int, int> new_dices = this->getDices();
	buffer = std::to_string(new_dices.first) + std::to_string(new_dices.second);
	std::cout << "dice one : " << std::to_string(new_dices.first) << "; dice two : " << std::to_string(new_dices.second) << std::endl;
	sending_T(this->player[this->turn]->getSocket(), prepForSending(170, buffer));
	sending_T(this->player[!this->turn]->getSocket(), prepForSending(170, buffer));
}

template <typename B, typename C>
void GameServer<B, C>::messageSave(Player* player, std::string buffer)
{
    // for saving games
	if(this->type == REALTIME_OFF) {
		if(buffer == "ask") {
			sending_T(this->player[!player->getColor()]->getSocket(), prepForSending(180, ""));
		}
		else if (buffer == "true") {
			sending_T(this->player[!player->getColor()]->getSocket(), prepForSending(180, "true"));
			unsigned ID = insertMatchDB(this->player[WHITE]->getUsername(), this->player[BLACK]->getUsername(), "", false, this->gamemode);
			for (int i = 0; i < MAX_ROWS; i++)
			{
				for (int j = 0; j < MAX_COLS; j++)
				{
					if(this->board->getTypePiece(std::make_pair(i, j)) != NO_PIECE) {
						insertSavedGameDB(ID, i, j, this->board->getTypePieceNbr(std::make_pair(i, j)), this->board->getColorPiece(std::make_pair(i, j)));
					}
				}
			}
			this->running_game = false;
			game_saved = true;
		}
		else if (buffer == "false") {
			sending_T(this->player[!player->getColor()]->getSocket(), prepForSending(180, "false"));
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// MOVES
////////////////////////////////////////////////////////////////////////////////

template <typename B, typename C>
void GameServer<B, C>::move(char buffer[MAXBUFSIZE])
{
    // send to both players the move if accepted
	sending_T(this->player[this->turn]->getSocket(), prepForSending(110, buffer));
	sending_T(this->player[!this->turn]->getSocket(), prepForSending(110, buffer));
	this->checkWinConditions(this->turn);
	this->swapTurn();
}

template <typename B, typename C>
void GameServer<B, C>::roque(char buffer[MAXBUFSIZE], int result, std::pair<std::pair<int, int>, std::pair<int, int>> fromBuffer)
{
    // roque execute
	sending_T(this->player[this->turn]->getSocket(), prepForSending(110, buffer));
	sending_T(this->player[!this->turn]->getSocket(), prepForSending(110, buffer));
	std::pair<std::pair<int, int>, std::pair<int, int>> cells_rook;
	std::pair<int, int> cell, cell_rook;
	if (result == MOVE_CASTLING_LONG) {
		cell_rook = std::make_pair(fromBuffer.first.first, 0);
		cell = std::make_pair(fromBuffer.first.first, 3);
	} else {
		cell_rook = std::make_pair(fromBuffer.first.first, 7);
		cell = std::make_pair(fromBuffer.first.first, 5);
	}
	std::string rook = this->pairToString(cell_rook, cell);
	sending_T(this->player[!this->turn]->getSocket(), prepForSending(110, rook));
	this->checkWinConditions(this->turn);
	this->swapTurn();
}

template <typename B, typename C>
void GameServer<B, C>::promotion(char buffer[MAXBUFSIZE])
{
    // handle the promotion of a piece when called
	std::string string = buffer;
	std::string coord = string.substr(0, 4);
	int piece = std::stoi(string.substr(4, 1));

	std::pair<std::pair<int, int>, std::pair<int, int>> fromBuffer = this->stringToPair(const_cast<char*>(coord.c_str()));
	std::pair<int, int> dest_pos = fromBuffer.first;

	if (this->board->getColorPiece(dest_pos) == WHITE && dest_pos.first == 0) this->promote(dest_pos, this->board->getColorPiece(dest_pos), piece);
	else if (this->board->getColorPiece(dest_pos) == BLACK && dest_pos.first == MAX_ROWS-1) this->promote(dest_pos, this->board->getColorPiece(dest_pos), piece);

	string = "140" + string;
	sending_T(this->player[this->turn]->getSocket(), const_cast<char*>(string.c_str()));
	sending_T(this->player[!this->turn]->getSocket(), const_cast<char*>(string.c_str()));
	this->checkWinConditions(this->turn);
	this->swapTurn();
}

////////////////////////////////////////////////////////////////////////////////
// ELO
////////////////////////////////////////////////////////////////////////////////

template <typename B, typename C>
void GameServer<B, C>::calculELO(Player* player) {
    // elo update
    std::string nb_games_won_str, nb_games_lost_str, nb_games_drawn_str;
    selectDB(&nb_games_won_str, DB_GAMES_WON_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, player->getUsername());
    selectDB(&nb_games_lost_str, DB_GAMES_WON_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, player->getUsername());
    selectDB(&nb_games_drawn_str, DB_GAMES_WON_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, player->getUsername());
    unsigned my_points, opponent_points, nb_games;
    my_points = player->getELO();
    opponent_points = this->player[!player->getColor()]->getELO();
    nb_games = std::stoi(nb_games_won_str)+std::stoi(nb_games_lost_str)+std::stoi(nb_games_drawn_str);
    std::cout << " *** ELO *** " << std::endl;
    if(nb_games > 10) { // not updated if less than 10 games played
        unsigned K, E;
        double pD, W, D;
        if(this->no_captured_pieces >= MAX_TURNS) W = 0.5;
        else if (player->isWinner()) W = 1.0;
        else W = 0.0;
        if(nb_games <= 30) K = 40;
        if(my_points <= 2400) K = 20;
        else K = 10;
        D = opponent_points - my_points;
        pD = 1 / (1 + pow((-D)/400, 10));
        E = static_cast<unsigned>(my_points + K * (W - pD));
        std::string to_bdd = std::to_string(E);
        std::cout << E << std::endl;
        updateDB("elo", std::to_string(E), DB_TABLE_PLAYER, DB_PSEUDO_FIELD, player->getUsername());
    }
}
