// INFO-F209 - Groupe 5 - GameClient.cpp

#include "GameClient.hpp"

////////////////////////////////////////////////////////////////////////////////
// INITIALIZATION AND END
////////////////////////////////////////////////////////////////////////////////

template <typename B, typename C>
GameClient<B, C>::GameClient(Player* white_player, Player* black_player, int socket, int start, int type) : B(white_player, black_player, type), serverfd(socket) {
    this->my_player = this->player[start];
    this->other_player = this->player[!start];
    createBoard();
    usleep(500);	//Wait for gameServer to launch (dice mode) / microseconds
    std::thread run_game(&GameClient<B, C>::run, this);
    run_game.detach();
    sleep(1);
}

template <typename B, typename C>
GameClient<B, C>::GameClient(Player* white_player, Player* black_player, int socket, int start, int type, std::vector<std::string> pieces) : B(white_player, black_player, type), serverfd(socket) {
    if(!pieces.empty()) {
        this->pieces_beginning = pieces;
    }
    this->my_player = this->player[start];
	this->other_player = this->player[!start];
	createBoard();
	usleep(500);	//Wait for gameServer to launch (dice mode) / microseconds
	std::thread run_game(&GameClient<B, C>::run, this);
	run_game.detach();
	sleep(1);
}

template <typename B, typename C>
GameClient<B, C>::~GameClient() = default;

template <typename B, typename C>
void GameClient<B, C>::createBoard()
{
    this->board = new C();
    B::setPiecesOnBoard();
    timer = new Timer(this->player[WHITE], this->player[BLACK], TIMER_BLITZ, &this->turn);
    terminal = new Terminal(this->my_player, this->other_player, serverfd);
}

template <typename B, typename C>
void GameClient<B, C>::finishGame() {
    terminal->write(DASHES);
    terminal->write(TERMINAL_EXIT);
    if(this->no_captured_pieces >= MAX_TURNS || (this->isInStalemate(this->turn) && \
    (!this->getCheck(this->turn) || !this->getCheck(!this->turn))) || !this->running_game) {
        terminal->write(TERMINAL_DRAWN_MATCH);
    }
    else terminal->write(TERMINAL_WINNER + this->getWinner()->getUsername() + TERMINAL_CONGRATULATIONS);
    terminal->write(TERMINAL_GAME_OVER);
    this->board->refreshDisplay(length_window, width_window, length_window_oversize, 2, itsMyTurn());
    terminal->refreshTerminal(length_terminal, width_terminal, length_window_oversize/2, (width_window*MAX_ROWS)+width_window_oversize/2, TRUE);
    timer->refreshTimer(length_timer, width_timer, length_terminal+4, (width_window*MAX_ROWS)+width_window_oversize/2);
    this->board->drawBox(COLOR_PAIR(4), true, this->my_player->getColor());
    this->board->removeAllPieces();
    delete this->board;
    delete timer;
    delete terminal;
    this->gameFinished();
}

template <typename B, typename C>
bool GameClient<B, C>::itsMyTurn() {
    if(this->my_player->getColor() == this->turn) return TRUE;
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////
// RUN
////////////////////////////////////////////////////////////////////////////////

template <typename B, typename C>
void GameClient<B, C>::run() {
    bool boardRefreshed = true;
    std::pair<int, int> dice;
    std::pair<int, int> coord = std::make_pair(9,9);
    timeout(100);
    //terminal->write(TERMINAL_WELCOME + this->other_player->getUsername());
    while (this->notOver()) {
			//Pre
		boardRefreshed = terminalSize(boardRefreshed);
		if(this->gamemode == DICE_GAMEMODE) diceChessCheck(dice);
        this->rearrangeBoard();
        showKingInCheck();
        showBlockedPieces();
		boardRefreshing(boardRefreshed);

			//Move
        if(itsMyTurn() && !isPreMovesEmpty()) doPreMove();	//PreMoves
		coord = cellSelection(coord);

			//Post
        askEndGame();
        if(coord.first != 9) doPromotion(coord);
        interpretSignalTerminal();
    }
    finishGame();
}

template <typename B, typename C>
bool GameClient<B, C>::terminalSize(bool boardRefreshed) {
    length_window = (COLS - 4)/MAX_COLS;
    width_window = ((LINES - 4)/MAX_ROWS)-1;
    width_terminal = MAX_ROWS;
    length_terminal = length_window*MAX_COLS-16;
    length_window_oversize = static_cast<unsigned>(COLS - length_window * MAX_COLS);
    width_window_oversize = static_cast<unsigned>(LINES - width_terminal - width_window * MAX_ROWS);
    width_terminal += (width_window_oversize/2)-2;
    width_timer = width_terminal;
    length_timer = 16;
    boardRefreshed = this->board->refreshDisplay(length_window, width_window, length_window_oversize, 2, itsMyTurn());
    terminal->refreshTerminal(length_terminal, width_terminal, length_window_oversize/2, (width_window*MAX_ROWS)+width_window_oversize/2, boardRefreshed);
    timer->refreshTimer(length_timer, width_timer, length_terminal+4, (width_window*MAX_ROWS)+width_window_oversize/2);
    this->board->drawBox(COLOR_PAIR(2), itsMyTurn(), this->my_player->getColor());
    return boardRefreshed;
}

template <typename B, typename C>
void GameClient<B, C>::boardRefreshing(bool boardRefreshed)
{
	if(boardRefreshed && this->board->getSelectedWindow()) {
        std::pair<int, int> coord_select = this->board->getSelectedWindowCoord();
        std::vector<std::pair<int, int>> moves = this->getAllMoves(coord_select, this->turn);
        this->board->showMovesSelectedPiece(moves);
	}
}

template <typename B, typename C>
std::pair<int, int> GameClient<B, C>::cellSelection(std::pair<int, int> movement)
{
    int ch = this->board->getKeyboardKey();
    std::pair<int, int> coord = this->board->mouseClicked(ch);
    if (coord.first != 9) {
        if((0 <= coord.first) && (coord.first < MAX_ROWS) && (0 <= coord.second) && (coord.second < MAX_COLS)) {
            if (this->board->getSelectedWindow()) {
                if (coord != this->board->getSelectedWindowCoord()) {
                    moveSelectedPiece(coord);
                    this->board->selectWindow();
                    movement.first = coord.first;
                    movement.second = coord.second;
                }
            } else {
                if (!this->board->isEmpty(coord)) {
                    if (this->board->getColorPiece(coord) == this->my_player->getColor()) this->board->selectWindow(coord);
                }
            }
        }
    }

    if (ch == 410) {} // Redimensionnement fenêtre
	else if (ch == 10) { // ENTER
		terminal->interpretCommand();
	}
	else if (ch == KEY_BACKSPACE) {
		terminal->removeLetterCommand();
	}
	else if (isascii(ch)) {
		terminal->addLetterCommand(static_cast<char>(ch));
	}
	return movement;
}


////////////////////////////////////////////////////////////////////////////////
// SERVER INTERACTIONS
////////////////////////////////////////////////////////////////////////////////

template <typename B, typename C>
void GameClient<B, C>::messageFromServer(int opcode, std::string buffer)
{
    switch(opcode) {
        case 1: { // Move
			messageMove(buffer);
            break;
        }
        case 2: { // Give up
            if(buffer == "drawn") { this->running_game = FALSE; }
            else { this->my_player->hasWon(); }
            break;
        }
        case 3: { // Set winner
			int winner = std::stoi(buffer.substr(0, 1));
			this->setWinner(winner);
            break;
        }
        case 4: { // Promotion
			messagePromotion(buffer);
			break;
		}
		case 5: { // SwapTurn
			this->swapTurn();
			if(this->gamemode == DICE_GAMEMODE) diceTurn[this->turn] = true;
			break;
		}
		case 7: { // Set the dices
			std::pair<int, int> new_dices;
			new_dices.first = std::stoi(buffer.substr(0, 1));
			new_dices.second = std::stoi(buffer.substr(1, 1));
			this->setDices(new_dices);
			break;
		}
		case 8: { // Save game
			messageSave(buffer);
		    break;
		}
		case 33: {
		    terminal->write(buffer);
		    break;
		}
    }
}

template <typename B, typename C>
void GameClient<B, C>::messageMove(std::string buffer)
{
	std::pair<std::pair<int, int>, std::pair<int, int>> fromBuffer = this->stringToPair(const_cast<char*>(buffer.c_str()));
	std::pair<int, int> src_pos = fromBuffer.first;
	std::pair<int, int> dest_pos = fromBuffer.second;
	unsigned canMove = this->checkMove(src_pos, dest_pos, this->turn);
	if(canMove){
	    bool colorPiece = this->board->getColorPiece(src_pos);
		this->movePiece(src_pos, dest_pos, canMove);
			//Terminal
		if(this->gamemode != DARK_GAMEMODE){
			terminal->write("* " + this->board->getTypePiece(dest_pos) + TERMINAL_MOVE_FROM + this->getChessColumn(src_pos.second) + this->getChessRank(src_pos.first) + TERMINAL_MOVE_TO +\
			this->getChessColumn(dest_pos.second) + this->getChessRank(dest_pos.first) + BY + this->player[colorPiece]->getUsername());
		}

		else terminal->write("* " DARK_SQUARE TERMINAL_MOVE_FROM DARK_SQUARE TERMINAL_MOVE_TO DARK_SQUARE BY +this->player[colorPiece]->getUsername());
			//Special moves
		if (canMove == MOVE_ALLOWED || canMove == MOVE_EN_PASSANT) {
			std::pair<int, int> cell = std::make_pair(src_pos.first, dest_pos.second);
			if (this->compareLastMove(cell) && (this->board->getTypePiece(cell) == PAWN) &&
				((this->board->getTypePiece(dest_pos)) == PAWN) && (dest_pos.first == 6 || dest_pos.first == 1))
				this->board->removePiece(cell);	//En passant
			this->setLastMove(dest_pos);
		}
		else if(canMove == MOVE_PROMOTION) checkPromotion(dest_pos);
		if(!game_freeze){
			this->checkWinConditions(this->turn);
			this->swapTurn();
		}
		if(this->gamemode == DICE_GAMEMODE) diceTurn[this->turn] = true;
	}
}

template <typename B, typename C>
void GameClient<B, C>::messagePromotion(std::string buffer)
{
	std::string coord = buffer.substr(0, 4);
	int piece = std::stoi(buffer.substr(4, 1));
	std::pair<std::pair<int, int>, std::pair<int, int>> fromBuffer = this->stringToPair(const_cast<char*>(coord.c_str()));
	std::pair<int, int> dest_pos = fromBuffer.second;
	if (this->board->getColorPiece(dest_pos) == WHITE && dest_pos.first == 0) this->promote(dest_pos, this->board->getColorPiece(dest_pos), piece);
	else if (this->board->getColorPiece(dest_pos) == BLACK && dest_pos.first == MAX_ROWS-1) this->promote(dest_pos, this->board->getColorPiece(dest_pos), piece);
	B::checkWinConditions(this->turn);
	game_freeze = FALSE;
	this->swapTurn();
	if(this->gamemode == DICE_GAMEMODE) diceTurn[this->turn] = true;
}

template <typename B, typename C>
void GameClient<B, C>::messageSave(std::string buffer)
{
	if(buffer.empty()) {
		game_freeze = true;
		terminal->write(TERMINAL_ASK_SAVE);
		terminal->setPrefixCommand(TERMINAL_SAVE_OTHER);
	}
	else if (buffer == "true") {
		terminal->write(TERMINAL_SAVE_SAVED);
		this->running_game = false;
	}
	else if (buffer == "false") {
		terminal->write(TERMINAL_SAVE_NOT_SAVED);
		game_freeze = false;
	}
}

template <typename B, typename C>
void GameClient<B, C>::moveSelectedPiece(std::pair<int, int> coord)
{
    unsigned canMove = this->checkMove(this->board->getSelectedWindowCoord(), coord, this->turn);
    if(canMove)
    {
        if(itsMyTurn())	//Normal Move
            sending_T(serverfd, prepForSending(110, this->pairToString(this->board->getSelectedWindowCoord(), coord)));
        else //Add preMove
            addPreMove(std::make_pair(this->board->getSelectedWindowCoord(), coord));
    }
}

////////////////////////////////////////////////////////////////////////////////
// TERMINAL INTERACTIONS
////////////////////////////////////////////////////////////////////////////////

template <typename B, typename C>
void GameClient<B, C>::interpretSignalTerminal() {
    int signal = terminal->getSignal();
    if(signal != 0) {
        if (signal == -2) { game_freeze = FALSE; this->no_captured_pieces = 0; }
        else if (signal == -1) game_freeze = FALSE;
        else if (signal == 8) { this->running_game = FALSE; sending_T(serverfd, prepForSending(120, "drawn")); }// Match nul
        else if (signal == 9) { this->other_player->hasWon(); sending_T(serverfd, prepForSending(120, "")); }  // Abandon
        else if (signal == 10) { setPromotionPiece(QUEEN_NBR); game_freeze = FALSE;}
        else if (signal == 11) { setPromotionPiece(KNIGHT_NBR); game_freeze = FALSE;}
        else if (signal == 12) { setPromotionPiece(ROOK_NBR); game_freeze = FALSE;}
        else if (signal == 13) { setPromotionPiece(BISHOP_NBR); game_freeze = FALSE;}
        else if (signal == 14) {
            if(this->type == REALTIME_OFF) {
                terminal->write(REALTIME_WAITING);
                game_freeze = true;
                sending_T(serverfd, prepForSending(180, "ask"));
            }
            else {
                terminal->write(REALTIME_CANT_SAVE);
            }
        }
        else if (signal == 15) { sending_T(serverfd, prepForSending(180, "true")); this->running_game = FALSE; }
        else if (signal == 16) { sending_T(serverfd, prepForSending(180, "false")); game_freeze = false; }
    }
}

template <typename B, typename C>
void GameClient<B, C>::askEndGame()
{
    if (this->no_captured_pieces >= MAX_TURNS && !game_freeze)
    {
        terminal->write(TERMINAL_ASK_ENDGAME);
        terminal->setPrefixCommand("quit_null");
        game_freeze = TRUE;
    }
}

template <typename B, typename C>
void GameClient<B, C>::showKingInCheck() {
    if(this->getCheck(this->turn)) {
        wbkgd(this->board->getWindow(this->getKingPosition(this->turn)), COLOR_PAIR(6));
    }
}

//////////////////////////////////////////
// PROMOTION
//////////////////////////////////////////

template <typename B, typename C>
void GameClient<B, C>::doPromotion(std::pair<int, int> coord)
{
	if((0 <= coord.first) && (coord.first < MAX_ROWS) && (0 <= coord.second) && (coord.second < MAX_COLS)) {
		if((this->board->getTypePiece(coord) == PAWN) && (coord.first == 0 || coord.first == 7) && !game_freeze && getCanPromote())
		{
			int piece = getPromotionPiece();
			sending_T(serverfd, prepForSending(140, this->pairToString(coord, coord) + std::to_string(piece)));
			setCanPromote(false);
		}
	}
}

template <typename B, typename C>
void GameClient<B, C>::checkPromotion(std::pair<int, int> coord)
{
	game_freeze = TRUE;
	if((this->board->getTypePiece(coord) == PAWN) && (coord.first == 0 || coord.first == 7) && itsMyTurn()) {
		std::pair<int, int> king = this->getKingPosition(!this->turn);
		if((king.first == coord.first && king.second == coord.second) && this->gamemode == DARK_GAMEMODE) {
			sending_T(serverfd, prepForSending(130, std::to_string(this->turn)));
			this->setWinner(this->turn);
		}
		else {
			terminal->write(TERMINAL_PROMOTION_WRITE_CHOICE);
			terminal->write(TERMINAL_PROMOTION_CHOICE);
			terminal->write(TERMINAL_PROMOTION);
			setCanPromote(true);
		}
	}
}

template <typename B, typename C>
void GameClient<B, C>::setPromotionPiece(int piece)
{
	promotionPiece = piece;
}

template <typename B, typename C>
int GameClient<B, C>::getPromotionPiece()
{
	return promotionPiece;
}

template <typename B, typename C>
void GameClient<B, C>::setCanPromote(bool can)
{
	canPromote = can;
}

template <typename B, typename C>
bool GameClient<B, C>::getCanPromote()
{
	return canPromote;
}

//////////////////////////////////////////
// DICE CHESS
//////////////////////////////////////////

template <typename B, typename C>
void GameClient<B, C>::diceChessCheck(std::pair<int, int> dice)
{
	if(itsMyTurn() && diceTurn[this->turn]) {
		terminal->write(TERMINAL_DICECHESS_ROLL);
		sending_T(serverfd, prepForSending(170, "")); //Server RollTheDices
		sleep(1);	//Wait for server to set dices.
		dice = this->getDices();
		terminal->write(TERMINAL_DICECHESS_RESULT + std::to_string(dice.first) + AND + std::to_string(dice.second));
        displayDicePieces(dice.first, dice.second);
		diceTurn[this->turn] = false;
		if(this->isInStalemate(this->turn)) { //No moves allowed
			terminal->write(TERMINAL_DICECHESS_PASSTURN);
			diceTurn[this->turn] = true;
			sending_T(serverfd, prepForSending(150, "")); //Server SwapTurn
			this->swapTurn();
		}
	}
}

template <typename B, typename C>
void GameClient<B, C>::displayDicePieces(int first, int second)
{
	std::string diceOne, diceTwo;
	if(first == second)
		terminal->write(TERMINAL_DICECHESS_DOUBLES);
	else {
		switch(first) {
			case 1: {diceOne = A_PAWN; break;}
			case 2: {diceOne = A_KNIGHT; break;}
			case 3: {diceOne = A_BISHOP; break;}
			case 4: {diceOne = A_ROOK; break;}
			case 5: {diceOne = YOUR_QUEEN; break;}
			case 6: {diceOne = YOUR_KING; break;}
		}
		switch(second) {
			case 1: {diceTwo = A_PAWN; break;}
			case 2: {diceTwo = A_KNIGHT; break;}
			case 3: {diceTwo = A_BISHOP; break;}
			case 4: {diceTwo = A_ROOK; break;}
			case 5: {diceTwo = YOUR_QUEEN; break;}
			case 6: {diceTwo = YOUR_KING; break;}
		}
		terminal->write(TERMINAL_DICECHESS_ALLOWED_MOVES + diceOne + " or " + diceTwo);
	}
}

//////////////////////////////////////////
// PREMOVES
//////////////////////////////////////////

template <typename B, typename C>
void GameClient<B, C>::addPreMove(std::pair<std::pair<int, int>, std::pair<int, int>> newMove)
{
	if(preMoves.size() < 5) {
		preMoves.insert(preMoves.begin(), newMove);
		terminal->write(TERMINAL_PREMOVE_ADDED + std::to_string(5-preMoves.size()) + TERMINAL_PREMOVE_MORE);
	}
	else
		terminal->write(TERMINAL_PREMOVE_NO_MORE);
}

template <typename B, typename C>
void GameClient<B, C>::cancelPreMoves()
{
	preMoves.clear();
}

template <typename B, typename C>
std::pair<std::pair<int, int>, std::pair<int, int>> GameClient<B, C>::getPreMove()
{
	std::pair<std::pair<int, int>, std::pair<int, int>> move = preMoves.back();
	preMoves.pop_back();
	return move;
}

template <typename B, typename C>
bool GameClient<B, C>::isPreMovesEmpty()
{
	return preMoves.empty();
}

template <typename B, typename C>
void GameClient<B, C>::doPreMove()
{
	std::pair<std::pair<int, int>, std::pair<int, int>> move = getPreMove();
	unsigned canMove = this->checkMove(move.first, move.second, this->turn);
	if(canMove) {
		terminal->write(TERMINAL_PREMOVE_DO);
		sending_T(serverfd, prepForSending(110, this->pairToString(move.first, move.second)));
	}
	else {
		terminal->write(TERMINAL_PREMOVE_NOT_ALLOWED);
		cancelPreMoves();
	}
}

template <typename B, typename C>
void GameClient<B, C>::showBlockedPieces() {}

// End game conditions
