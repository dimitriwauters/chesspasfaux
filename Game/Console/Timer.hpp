#ifndef TIMER_H
#define TIMER_H

#include "../const.hpp"
#include "ncurses.h"
//#include <ncursesw/cursesw.h>
#include "../Player.hpp"
#include <iostream>
#include <ctime>
#include <thread>
#include <cmath>
#include <zconf.h>
#include <sstream>
#include <iomanip>

class Timer {
private:
    WINDOW* timer = nullptr;
    double time[2];
    Player* players[2];
    bool color = WHITE;
    int* game_turn;
    bool over = false;
public:
    Timer(Player*, Player*, float, int*);
    ~Timer();
    double getTime(bool);
    void timeOut();
    void refreshTimer(int, int, int, int);
};


#endif // TIMER_H
