// INFO-F209 - Groupe 5 - friends.hpp

#ifndef FRIENDS_HPP
#define FRIENDS_HPP

#include <string>
#include "../Network.hpp"
#include "../db.hpp"
#include "../const.hpp"
#include "../Player.hpp"

bool inFriendsList(char*, Player*);
bool inFriendsRequestsList(char*, Player*);
bool inDBFriendsRequestsList(std::string, char*);
void setFriendsList(Player*);
void setFriendsRequestsList(Player*);
// Add a friend
void addFriend(char*, int);
void addFriendRequest(char*, int);
// Remove a friend
void removeFriend(char*, int );
void removeFriendRequest(char*, int);
// Sending information
void sendFriendsUsernames(int);
void sendFriendsRequestsUsernames(int);

extern Player* getPlayerPtr(int);	//Function Server.cpp
extern Player* getPlayerPtr(char*); //Function Server.cpp

#endif
