// INFO-F209 - Groupe 5 - GameClientRealtime.hpp

#ifndef GAMECLIENTREALTIME_HPP
#define GAMECLIENTREALTIME_HPP

#include "GameClient.hpp"

template <typename B>
class GameClientRealtime : public B {
private:
    unsigned current_tick = 0;
    unsigned last_moved_piece = 0;
    std::vector<std::pair<std::pair<int, int>, unsigned>> blocked_pieces;
public:
    GameClientRealtime(Player*, Player*, int, int, int);
    ~GameClientRealtime();
    void moveSelectedPiece(std::pair<int, int>) override;
    void checkPromotion(std::pair<int, int>) override;
    bool willBeInCheck(std::pair<int, int>, std::pair<int, int>, bool) override;
    bool isUnderAttack(std::pair<int, int>, bool) override;
    void checkWinConditions(bool) override;
    bool movePiece(std::pair<int, int>, std::pair<int, int>, unsigned) override;
    void showBlockedPieces() override;
    unsigned enPassant(std::pair<int, int> src_pos, std::pair<int, int> dest_pos) override;
    bool itsMyTurn() override;
    void askEndGame() override;
    void timer();
};

#include "GameClientRealtime.cpp"

#endif
