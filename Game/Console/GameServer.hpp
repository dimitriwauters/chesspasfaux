// INFO-F209 - Groupe 5 - GameServer.hpp

#ifndef GAMESERVER_HPP
#define GAMESERVER_HPP

#include <cmath>
#include "../Network.hpp"
#include "../db.hpp"
#include "../Player.hpp"

template <typename B, typename C>
class GameServer : public B {
private:
    bool game_saved = false;
		//Server interaction
    void messageMove(Player*, std::string);
    void messageDrawn(Player*, std::string);
    void messageDice(std::string);
    void messageSave(Player*, std::string);
public:
    GameServer(Player*, Player*, int);
    GameServer(Player*, Player*, int, std::vector<std::string>);
    ~GameServer();
    void createBoard() override;
    void run() override;
    void finishGame() override;
		//Server interaction
    virtual void messageFromServer(Player*, int, std::string);
		//Moves
    void move(char[]);
    void roque(char[], int, std::pair<std::pair<int, int>, std::pair<int, int>>);
    void promotion(char[]);
		//Elo
    void calculELO(Player*);
};

#include "GameServer.cpp"

#endif
