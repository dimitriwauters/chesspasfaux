// INFO-F209 - Groupe 5 - Terminal.hpp

#ifndef TERMINAL_HPP
#define TERMINAL_HPP

#include <cstring>
#include "ncurses.h"
//#include <ncursesw/cursesw.h>
#include "../Player.hpp"
#include "../const.hpp"
#include "../Network.hpp"

class Terminal {
private:
    WINDOW* terminal;
    Player* me;
    Player* other;
    std::string prefix_command = "";
    std::string terminal_command = "";
    bool terminal_changed;
    std::pair<int, int> size = {0, 0};
    std::pair<int, int> coord = {0, 0};
    std::vector<std::string> terminal_text;
    int signal = 0;
    int max_message_length = 100;
    unsigned offset = 0;
    unsigned getMaxLines();
    int serverfd;
public:
    Terminal(std::pair<int, int>, std::pair<int, int>, std::vector<std::string>);
    Terminal(Player*, Player*, int);
    ~Terminal();
    void refreshTerminal(int, int, int, int, bool);
    void addText(unsigned, std::string);
    void write(std::string);
		//Command
    void addLetterCommand(char);
    void removeLetterCommand();
    bool interpretCommand();
    void doCommand();
    void getCommand();
    void setPrefixCommand(std::string);
    int getSignal();
};

#endif
