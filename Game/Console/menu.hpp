// INFO-F209 - Groupe 5 - Menu.hpp

#ifndef _MENU_CPP
#define _MENU_CPP

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>
#include <string.h>
#include <vector>
#include "../const.hpp"
#include "../Network.hpp"

class GameMenu{
private:
    int _socketfd;
    int _chatSocket;
    char* hostname;
    std::string clientName;

    long int charToInt(char* str);
		//SignUp
    bool signUp();
    bool signUpPasswordCheck(char[], char[]);
    bool signUpConfirmation(char[], bool);
		//SignIn
    bool signIn();
    bool signInConfirmation(char[], bool);
		//GeneralMenu
    unsigned startGameChoice();
		//ContinueGame
    unsigned continueGame();
		//Stats
    unsigned getStats();
    void statsUsernameDisplay();
    void statsFriends();
    void statsGeneral(char[]);
		//Friends;
    void manageFriendsList();
    void friendsAdd(char[], char[]);
    void friendsRemove(char[], char[]);
    void friendsRequest(char[], char[]);
    void friendsShow(char[]);
		//Chat
    void openChat();
		//Help
    void help();
    void helpGamemodes();
    void helpChessRules();
public:
    GameMenu() = default;
    GameMenu(int fd, char* hostname);
    ~GameMenu() = default;
		//Auth
    bool startAuthMenu();
    unsigned startGameMenu();
    unsigned startGameTypeChoice();
    std::string getClientName(){return clientName;}
    void setChatSocket(int socket){_chatSocket=socket;}
};

#endif
