// INFO-F209 - Groupe 5 - GameServerRealtime.hpp

#ifndef GAMESERVERREALTIME_HPP
#define GAMESERVERREALTIME_HPP

#include "GameServer.hpp"
#include <thread>

template <typename B>
class GameServerRealtime : public B {
private:
    unsigned current_tick = 0;
    std::vector<std::pair<std::pair<int, int>, unsigned>> blocked_pieces;
public:
    GameServerRealtime(Player*, Player*, int);
    ~GameServerRealtime();
    void messageFromServer(Player*, int, std::string) override;
    bool willBeInCheck(std::pair<int, int>, std::pair<int, int>, bool) override;
    bool isUnderAttack(std::pair<int, int>, bool) override;
    void checkWinConditions(bool) override;
    bool movePiece(std::pair<int, int>, std::pair<int, int>, unsigned) override;
    unsigned enPassant(std::pair<int, int> src_pos, std::pair<int, int> dest_pos) override;
    void timer();
};

#include "GameServerRealtime.cpp"

#endif
