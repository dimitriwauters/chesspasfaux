// INFO-F209 - Groupe 5 - GameClientRealtime.cpp

#include "GameClientRealtime.hpp"

template <typename B>
GameClientRealtime<B>::GameClientRealtime(Player* white_player, Player* black_player, int socket, int start, int type) : B(white_player, black_player, socket, start, type) {
    this->board->refreshDisplay(this->length_window, this->width_window, this->length_window_oversize, 2, true);
    std::thread timer(&GameClientRealtime<B>::timer, this);
    timer.detach();
}

template <typename B>
GameClientRealtime<B>::~GameClientRealtime() = default;

template <typename B>
void GameClientRealtime<B>::moveSelectedPiece(std::pair<int, int> coord) {
    bool found = false;
    for(unsigned i = 0; i < blocked_pieces.size(); ++i) {
        if(blocked_pieces.at(i).first == this->board->getSelectedWindowCoord()) { found = true; break; }
    }
    if(!found) {
        unsigned canMove = this->checkMove(this->board->getSelectedWindowCoord(), coord, this->turn);
        if(canMove) { sending_T(this->serverfd, prepForSending(110, this->pairToString(this->board->getSelectedWindowCoord(), coord))); }
    }
}

template <typename B>
bool GameClientRealtime<B>::itsMyTurn() {
    return true;
}

template <typename B>
void GameClientRealtime<B>::checkPromotion(std::pair<int, int> coord __attribute__((unused))) {}

template <typename B>
bool GameClientRealtime<B>::willBeInCheck(std::pair<int, int> src __attribute__((unused)), std::pair<int, int> dest __attribute__((unused)), bool playerTurn __attribute__((unused)))
{
    return false;
}

template <typename B>
bool GameClientRealtime<B>::isUnderAttack(std::pair<int, int> current_pos __attribute__((unused)), bool color __attribute__((unused)))
{
    return false;
}

template <typename B>
void GameClientRealtime<B>::checkWinConditions(bool playerTurn) {
    if(this->isInStalemate(!playerTurn)) this->running_game = false;
    if(this->isInStalemate(playerTurn)) this->running_game = false;
    if(this->getKingPosition(!playerTurn).first == -1) this->setWinner(playerTurn);
    if(this->getKingPosition(playerTurn).first == -1) this->setWinner(!playerTurn);
}

template <typename B>
bool GameClientRealtime<B>::movePiece(std::pair<int, int> src_pos, std::pair<int, int> dest_pos, unsigned canMove) {
    if(canMove != MOVE_NOT_ALLOWED) {
        if(this->board->getColorPiece(src_pos) == this->my_player->getColor()) blocked_pieces.emplace_back(std::make_pair(dest_pos, current_tick));
        last_moved_piece = current_tick;
        return B::movePiece(src_pos, dest_pos, canMove);
    }
    return false;
}

template <typename B>
void GameClientRealtime<B>::showBlockedPieces() {
    for(unsigned i = 0; i < blocked_pieces.size(); ++i) {
        wbkgd(this->board->getWindow(blocked_pieces.at(i).first), COLOR_PAIR(6));
    }
}

template <typename B>
unsigned GameClientRealtime<B>::enPassant(std::pair<int, int> src_pos, std::pair<int, int> dest_pos) {
    bool found = false;
    unsigned passant = B::enPassant(src_pos, dest_pos);
    if(passant) {
        std::pair<int, int> cell;
        cell = std::make_pair(src_pos.first, dest_pos.second);
        for(unsigned i = 0; i < blocked_pieces.size(); ++i) {
            if(blocked_pieces.at(i).first == cell) { found = true; break; }
        }
        if(!found) { passant = 0; }
    }
    return passant;
}

template <typename B>
void GameClientRealtime<B>::askEndGame() {}

template <typename B>
void GameClientRealtime<B>::timer() {
    while(this->notOver()) {
        current_tick++;
        if(last_moved_piece + (300 / (REALTIME_DELAYTIME/1000)) <= current_tick) {
            if(this->my_player->getColor() == WHITE) {
                this->running_game = FALSE;
                sending_T(this->serverfd, prepForSending(120, "drawn"));
            }
        }
        for(int i = static_cast<int>(blocked_pieces.size())-1; i >= 0; --i) {
            if(blocked_pieces.at(i).second + (REALTIME_BLOCKEDTIME / (REALTIME_DELAYTIME/1000)) <= current_tick) {
                unsigned nb_case = 7 * blocked_pieces.at(i).first.first + blocked_pieces.at(i).first.second;
                if(nb_case % 2 == 0 || nb_case == 0) wbkgd(this->board->getWindow(blocked_pieces.at(i).first), COLOR_PAIR(1));
                else wbkgd(this->board->getWindow(blocked_pieces.at(i).first), COLOR_PAIR(0));
                wrefresh(this->board->getWindow(blocked_pieces.at(i).first));
                blocked_pieces.erase(blocked_pieces.begin() + i);
            }
        }
        usleep(REALTIME_DELAYTIME*1000);
    }
}
