#include "Timer.hpp"

Timer::Timer(Player* white, Player* black, float mode, int* turn) {
    time[WHITE] = mode;
    time[BLACK] = mode;
    players[WHITE] = white;
    players[BLACK] = black;
    game_turn = turn;
    std::thread t1(&Timer::timeOut, this);
    t1.detach();
}

Timer::~Timer() {
    over = true;
}

double Timer::getTime(bool color) {
// Return the time of one player.
    return time[color];
}

void Timer::timeOut() {
// Substract 0.1 second to the time while the player hasn't played.
    while(!over) {
        while(*game_turn == color && !over) { // While same player
            usleep(100000); // 100 milliseconds
            time[color] -= 0.1;
            if(time[color] <= 0) players[!color]->hasWon();
        }
        time[color] += 5.0;
        color = (color == TRUE) ? FALSE : TRUE;
    }
}

void Timer::refreshTimer(int length, int width, int coord_length, int coord_width) {
// Refresh the timers.
    std::stringstream second;
    std::stringstream minute;
    if(timer) {
        wclear(timer);
        delwin(timer);
    }
    timer = subwin(stdscr, width, length, coord_width, coord_length);
    box(timer, '*', '*');
    minute << std::setw(2) << std::setfill('0') << std::to_string(static_cast<int>(std::floor(time[WHITE]/60)));
    second << std::setw(2) << std::setfill('0') << std::to_string(static_cast<int>(time[WHITE]) % 60);
    std::string time_white = "B - " + minute.str() + ":" + second.str();
    second.str(std::string());
    minute.str(std::string());
    minute << std::setw(2) << std::setfill('0') << std::to_string(static_cast<int>(std::floor(time[BLACK]/60)));
    second << std::setw(2) << std::setfill('0') << std::to_string(static_cast<int>(time[BLACK]) % 60);
    std::string time_black = "N - " + minute.str() + ":" + second.str();
    mvwprintw(timer, width/2-2, length/2-4, time_white.c_str());
    mvwprintw(timer, width/2+2, length/2-4, time_black.c_str());
}
