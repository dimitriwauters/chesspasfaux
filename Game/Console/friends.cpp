// INFO-F209 - Groupe 5 - friends.cpp
// This file is about the management of a friends list.

#include "friends.hpp"

bool inFriendsList(char* pseudo, Player* player)
// Check if pseudo is in player's friends list.
{
    bool found = false;
    std::vector<std::string> friends_usernames = player->getFriendsUsernames();
    for (auto i = friends_usernames.begin(); i != friends_usernames.end(); ++i)
    {
        if (*i==pseudo) { found = true; break; }
	}
	return found;
}

bool inFriendsRequestsList(char* pseudo, Player* player)
// Check if pseudo is in player's friends list.
{
    bool found = false;
    std::vector<std::string> friends_requests_list = player->getRequestFriend();
    for(auto i = friends_requests_list.begin(); i != friends_requests_list.end(); ++i)
    {
        if (*i==pseudo) { found = true; break; }
	}
	return found;
}

bool inDBFriendsRequestsList(std::string player, char* other_player)
// Check (within the database) if other_player is in friends requests list of player (i.e. if their
// friendship status == FRIENDS_STATUS_REQUEST in database).
{
    std::string other_player_id;
    selectDB(&other_player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, other_player);
    std::string list;
    // Get the friends requests list of other_player
    getFriendsDB(&list, other_player_id, FRIENDS_STATUS_REQUEST);
    bool found = false;
    if (!list.empty())
    {
		list += MSG_SEPARATOR;
		std::string username;
		std::string current_request;
		long unsigned int pos = 0;
		while (!list.empty()) // Browsing the friends requests list
        {
			pos = list.find(MSG_SEPARATOR);
			username = list.substr(0, pos);
			selectDB(&current_request, DB_PSEUDO_FIELD, DB_TABLE_PLAYER, DB_ID_FIELD, username);
			if (current_request == player) found = true;
			list = list.substr(pos + 1, list.size());
        }
    }
    return found;
}

void setFriendsList(Player* player)
// Get and set (with the database) the friends list of player (i.e. friendship status ==
// FRIENDS_STATUS_FRIENDS in database).
{
    std::string player_id;
    std::string list;
    selectDB(&player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, player->getUsername());
    // Get the friends list of player
    getFriendsDB(&list, player_id, FRIENDS_STATUS_FRIENDS);
    if (!list.empty())
    {
		list += MSG_SEPARATOR;
		std::string username;
		std::string current_friend;
		long unsigned int pos = 0;
		while (!list.empty())
        {
			pos = list.find(MSG_SEPARATOR);
			username = list.substr(0, pos);
			selectDB(&current_friend, DB_PSEUDO_FIELD, DB_TABLE_PLAYER, DB_ID_FIELD, username);
			char* name = const_cast<char *>(current_friend.c_str());
			player->pushInFriendsUsernames(name); // Set the friends list
			list = list.substr(pos + 1, list.size());
		}
	}
}

void setFriendsRequestsList(Player* player)
// Get and set the requests friends list of player (i.e friendship status == FRIENDS_STATUS_REQUEST).
{
    std::string player_id;
    selectDB(&player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, player->getUsername());
    std::string list;
    getFriendsDB(&list, player_id, FRIENDS_STATUS_REQUEST);
    if (!list.empty())
    {
		list += MSG_SEPARATOR;
		std::string username;
		std::string current_request;
		long unsigned int pos = 0;
		while (!list.empty())
        {
			pos = list.find(MSG_SEPARATOR);
			username = list.substr(0, pos);
			selectDB(&current_request, DB_PSEUDO_FIELD, DB_TABLE_PLAYER, DB_ID_FIELD, username);
			char* name = const_cast<char *>(current_request.c_str());
			player->pushRequestFriend(name); // Set the requests friends list
			list = list.substr(pos + 1, list.size());
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// ADD A FRIEND
////////////////////////////////////////////////////////////////////////////////

void addFriend(char* pseudo, int socketfd)
// Add a friend whose name is pseudo.
{
    Player* from_player;
    Player* to_player;
    bool is_made = false;
    from_player = getPlayerPtr(socketfd);
    std::vector<std::string> requestFriend = from_player->getRequestFriend();
    for (auto i = requestFriend.begin(); i != requestFriend.end(); ++i)
    {
        if (*i == pseudo) // If pseudo is in friends requests list
        {
            from_player->delRequestFriend(pseudo); // Delete the request
            from_player->pushInFriendsUsernames(pseudo); // Add pseudo in friends list (for from_player)
            to_player = getPlayerPtr(pseudo);
            // Add from_player in friends list (for to_player)
            if (to_player != nullptr) to_player->pushInFriendsUsernames(const_cast<char *>\
                ((from_player->getUsername()).c_str()));
            // Updating the database
            std::string from_player_id;
            std::string to_player_id;
            selectDB(&from_player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, from_player->getUsername());
            selectDB(&to_player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, pseudo);
            acceptFriendsDB(to_player_id, from_player_id);
            is_made = true;
            break;
        }
    }
    if (is_made) sending_T(socketfd, prepForSending(261, FRIENDS_ADDED));
    else sending_T(socketfd, prepForSending(261, PLAYER_NOT_FOUND));
}

void addFriendRequest(char* pseudo, int socketfd)
// Add a friend request to someone (pseudo) and update the database.
{
    Player* from_player; // Sender
    Player* to_player; // Receiver
    std::string result;
    bool is_made = false;
    countDB(&result, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, pseudo);
    if (std::stoi(result) != 0)
    {
        from_player = getPlayerPtr(socketfd);
        to_player = getPlayerPtr(pseudo);
        if (from_player->getUsername()!= pseudo && !inFriendsRequestsList(pseudo, from_player) \
        && !inFriendsList(pseudo, from_player) && !inDBFriendsRequestsList(from_player->getUsername(), pseudo))
        {
            // Send a request to the receiver to_player
            if (to_player != nullptr) to_player->pushRequestFriend(from_player->getUsername());
            // Updating the database
            std::string to_player_id;
            std::string from_player_id;
            selectDB(&to_player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, pseudo);
            selectDB(&from_player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, from_player->getUsername());
            insertFriendsDB(from_player_id, to_player_id);
            is_made = true;
        }
    }
    if (is_made) sending_T(socketfd, prepForSending(261, FRIENDS_SENT_REQUEST_TITLE));
    else sending_T(socketfd, prepForSending(261, PLAYER_NOT_FOUND));
}

////////////////////////////////////////////////////////////////////////////////
// REMOVE A FRIEND
////////////////////////////////////////////////////////////////////////////////

void removeFriend(char* pseudo, int socketfd)
// Delete a friend whose username is pseudo.
{
    Player* from_player;
    Player* to_player;
    bool is_made = false;
    from_player = getPlayerPtr(socketfd);
    // Get the friends list of the player who wants to undo a friendship
    std::vector<std::string> friends_usernames = from_player->getFriendsUsernames();
    auto search = std::find(friends_usernames.begin(), friends_usernames.end(), pseudo);
    if (search != friends_usernames.end()) // If found, we delete (for each side)
    {
            from_player->delFriend(pseudo);
            to_player = getPlayerPtr(pseudo);
            // If to_player is connected, we update its friends list
            if (to_player != nullptr) to_player->delFriend(const_cast<char *>((from_player->getUsername()).c_str()));
            // Updating the database
            std::string from_player_id;
            std::string to_player_id;
            selectDB(&from_player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, from_player->getUsername());
            selectDB(&to_player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, pseudo);
            deleteFriendsDB(from_player_id, to_player_id);
            is_made = true;
        }
    if (is_made) sending_T(socketfd, prepForSending(261, FRIENDS_REMOVED));
    else sending_T(socketfd, prepForSending(261, PLAYER_NOT_FOUND));
}

void removeFriendRequest(char* pseudo, int socketfd)
// Delete the request sent by pseudo for the player whose socket is socketfd.
{
    Player* from_player = getPlayerPtr(socketfd);
    // Get the friends requests list of from_player
    std::vector<std::string> friends_requests_list = from_player->getRequestFriend();
    for (auto i = friends_requests_list.begin(); i != friends_requests_list.end(); ++i)
    {
        if (*i == pseudo) // If found
        {
            from_player->delRequestFriend(pseudo);
            // Updating the database
            std::string from_player_id;
            std::string to_player_id;
            selectDB(&from_player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, from_player->getUsername());
            selectDB(&to_player_id, DB_ID_FIELD, DB_TABLE_PLAYER, DB_PSEUDO_FIELD, pseudo);
            deleteFriendsDB(from_player_id, to_player_id);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// SENDING INFORMATION
////////////////////////////////////////////////////////////////////////////////

void sendFriendsUsernames(int socketfd)
// Get the friends usernames of the player whose socket is socketfd and send it
// (separated by MSG_SEPARATOR).
{
    std::string names;
    Player* player = getPlayerPtr(socketfd);
    std::vector<std::string> friends_usernames = player->getFriendsUsernames();
    for (auto i = friends_usernames.begin(); i != friends_usernames.end(); ++i)
    {
        names += *i + MSG_SEPARATOR;
    }
   sending_T(socketfd, prepForSending(260, names));
}

void sendFriendsRequestsUsernames(int socketfd)
// Get the usernames of the people who are in friends requests list of the player whose socket
// is socketfd and send it (separated by MSG_SEPARATOR).
{
    std::string names;
    Player* player = getPlayerPtr(socketfd);
    std::vector<std::string> friends_requests_list = player->getRequestFriend();
    for (auto i = friends_requests_list.begin(); i != friends_requests_list.end(); ++i)
    {
        names += *i + MSG_SEPARATOR;
    }
   sending_T(socketfd, prepForSending(260, names));
}
