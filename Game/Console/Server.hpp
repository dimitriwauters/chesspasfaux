// INFO-F209 - Groupe 5 - server.hpp

#ifndef SERVER_HPP
#define SERVER_HPP

#include <vector>
#include <csignal>
#include <asm/socket.h>
#include <sys/socket.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <thread>
#include <algorithm>
#include "../Network.hpp"
#include "../Player.hpp"
#include "GameServer.hpp"
#include "GameServerRealtime.hpp"
#include "../Gamemodes.hpp"
#include "../db.hpp"
#include "friends.hpp"

typedef GameServer<GameDefault<Board>, Board> gameDefault;
typedef GameServer<GameHorde<BoardHorde>, BoardHorde> gameHorde;
typedef GameServer<GameDice<BoardDice>, BoardDice> gameDice;
typedef GameServer<GameDark<BoardDark>, BoardDark> gameDark;
typedef GameServerRealtime<GameServer<GameDefault<Board>, Board>> gameDefaultRealtime;
typedef GameServerRealtime<GameServer<GameHorde<BoardHorde>, BoardHorde>> gameHordeRealtime;
typedef GameServerRealtime<GameServer<GameDice<BoardDice>, BoardDice>> gameDiceRealtime;
typedef GameServerRealtime<GameServer<GameDark<BoardDark>, BoardDark>> gameDarkRealtime;

std::vector<Player*> players_connected;
std::vector<Player*> players_waiting_default_normal;
std::vector<Player*> players_waiting_default_realtime;
std::vector<Player*> players_waiting_horde_normal;
std::vector<Player*> players_waiting_horde_realtime;
std::vector<Player*> players_waiting_dice_normal;
std::vector<Player*> players_waiting_dice_realtime;
std::vector<Player*> players_waiting_dark_normal;
std::vector<Player*> players_waiting_dark_realtime;
std::vector<std::pair<Player*, std::string>> players_waiting_continue_saved;
std::vector<gameDefault*> default_games;
std::vector<gameHorde*> horde_games;
std::vector<gameDice*> dice_games;
std::vector<gameDark*> dark_games;
int socketfd, chatfd;

	//Server
template <typename B>
void endGame(std::vector<B>*, bool);
void signalHandler(int);
	//ServerRun
void serverRun();
void connectAllWaitingPlayers();
void serverGameLaunch(int, Player*, Player*, int);
void serverGamemode(int, Player*, Player*, std::vector<std::string>);
	//Client
void clientInfos(Player*);
void gameSwitch(int, std::string, Player*);
void menuSwitch(int, int, std::string, Player*);
void menuSignUp(std::string, Player*);
void menuSignIn(std::string, Player*);
void menuGameLaunch(int, std::string, Player*);
void menuStats(Player*);
void menuGeneralStats(std::string, Player*);
void menuFriendsStats(Player*);
void menuFriends(int, char*, Player*);
static bool compareStats(std::pair<std::string, int>, std::pair<std::string, int>);
void menuContinueGame(int, std::string, Player*);
void chatSwitch(int, std::string, Player*);
	//Chat
void chatInfos(Player*);
void chatGetUsernames(Player*);
void chatCheckUsernames(std::string, Player*);
void chatMessage(std::string, std::string);
void playerChatConnection(int, SockaddrIn, std::vector<Player*>*);
void playerChatConnectionWaiting(int, std::vector<Player*>*);

	//Players
void connectWaitingPlayers(std::vector<Player*>*, std::string, std::string);
void playerGamemode(std::string, std::string, Player*, Player*);
void playerConnection(int, SockaddrIn, std::vector<Player*>*);
void disconnectPlayer(Player*);
void* getGameFromPlayer(Player*);
void removePlayerFromQueue(Player*, std::vector<Player*>*);
Player* getPlayerPtr(int);
Player* getPlayerPtr(char*);

#endif
