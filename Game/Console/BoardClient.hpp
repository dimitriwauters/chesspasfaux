// INFO-F209 - Groupe 5 - BoardClient.hpp

#ifndef BOARDCLIENT_HPP
#define BOARDCLIENT_HPP

#include "../Board.hpp"

template <typename B>
class BoardClient : public B {
private:
    WINDOW* selectedWindow = nullptr;
    WINDOW* cases[MAX_ROWS][MAX_COLS] = {{ nullptr }};
    std::pair<int, int> selectedWindowCoord;
    int length_window;
    int width_window;
    unsigned length_window_oversize;
    unsigned width_window_oversize;
    bool board_changed;
    bool last_turn;

    void initializeConsole();
    void createBoard();
    void createWindows();
public:
    BoardClient();
    ~BoardClient();
    // Console & Windows
    bool refreshDisplay(int, int, unsigned, unsigned, bool);
    virtual void drawBox(short, bool, bool);
    void selectWindow(std::pair<int, int>);
    void selectWindow();
    WINDOW* getSelectedWindow();
    std::pair<int, int> getSelectedWindowCoord();
    void addTextWindow(WINDOW*, const char*, bool);
    std::pair<int, int> coordToWindow(std::pair<int, int>);
    WINDOW* getWindow(std::pair<int, int>);
    std::pair<int, int> mouseClicked(int);
    int getKeyboardKey();
	// Pieces
    void placePieces(bool) override;
    bool addPiece(int, std::pair<int, int>, bool) override;
    void removeAllPieces() override;
    bool removePiece(std::pair<int, int>) override;
    // Moves
    void showMovesSelectedPiece(std::vector<std::pair<int, int>>);
    void setNewPosition(std::pair<int, int> src_pos, std::pair<int, int> dest_pos) override;
    bool isSpecialMove(std::pair<int, int>) override;
};

#include "BoardClient.cpp"

#endif
