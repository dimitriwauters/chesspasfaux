// INFO-F209 - Groupe 5 - Pieces.cpp

#include "Pieces.hpp"

AbstractPiece::AbstractPiece() = default;

AbstractPiece::AbstractPiece(int t_x, int t_y, bool t_color) : m_position(t_x, t_y), m_color(t_color), m_moved(false)
{}

AbstractPiece::AbstractPiece(int t_type) : m_type(t_type)
{}

AbstractPiece::AbstractPiece(int t_x, int t_y, bool t_color, int t_type) : m_position(t_x, t_y), m_color(t_color), m_type(t_type), m_moved(false)
{}

AbstractPiece::~AbstractPiece() = default;

void AbstractPiece::setColor(bool t_yes) {
    // used to set the color of a piece
    if (t_yes) m_color = WHITE;
    else m_color = BLACK;
}

bool AbstractPiece::getColor() const {
    return m_color;
}

void AbstractPiece::move(int t_x, int t_y) {
    m_position.first = t_x;
    m_position.second = t_y;
}

std::pair <int, int> AbstractPiece::getPosition() const {
    return m_position;
}

void AbstractPiece::firstMove() {
	m_moved = true;
}

bool AbstractPiece::hasMoved() {
	return m_moved;
}

int AbstractPiece::getTypeNbr() const {
    return m_type;
}

void AbstractPiece::printPosition() const {
    std::cout << "x : " << m_position.first;
    std::cout << ", y : " << m_position.second << std::endl;
}

//////////////////////////////////////////////////////////////////
// ROOK
//////////////////////////////////////////////////////////////////

Rook::Rook() : AbstractPiece(ROOK_NBR)
{}

Rook::Rook(int t_x, int t_y, bool t_color) : AbstractPiece(t_x, t_y, t_color, ROOK_NBR)
{}

Rook::~Rook() = default;

bool Rook::checkMove(int t_x, int t_y) {
	return (t_y == m_position.second) || (t_x == m_position.first);
}

bool Rook::checkSpecialMove() {
	return false;
}

std::string Rook::getType() const {
    return ROOK;
}

std::vector<std::pair<int, int>> Rook::getAllMoves() {
    // Return as list of tuple all the positions that can be reached
    std::vector<std::pair<int, int>> moves;
    for (int i = 0; i < MAX_COLS; ++i) {
        if(checkMove(i, m_position.second)) moves.emplace_back(std::make_pair(i, m_position.second));
    }
    for (int i = 0; i < MAX_ROWS; ++i) {
        if(checkMove(m_position.first, i)) moves.emplace_back(std::make_pair(m_position.first, i));
    }
    return moves;
}

//////////////////////////////////////////////////////////////////
// KNIGHT
//////////////////////////////////////////////////////////////////

Knight::Knight() : AbstractPiece(KNIGHT_NBR)
{}

Knight::Knight(int t_x, int t_y, bool t_color) : AbstractPiece(t_x, t_y, t_color, KNIGHT_NBR)
{}

Knight::~Knight() = default;

bool Knight::checkMove(int t_x, int t_y) {
	return ((abs(m_position.first-t_x) == 1 && abs(m_position.second-t_y) == 2) || (abs(m_position.first-t_x) == 2 && abs(m_position.second-t_y) == 1));
}

bool Knight::checkSpecialMove() {
	return false;
}

std::string Knight::getType() const {
    return KNIGHT;
}

std::vector<std::pair<int, int>> Knight::getAllMoves() {
    // Return as list of tuple all the positions that can be reached
    std::vector<std::pair<int, int>> moves;
    for (int i = 0; i < 4; ++i) {
        if(i == 0) if(checkMove(m_position.first-2, m_position.second-1)) moves.emplace_back(std::make_pair(m_position.first-2, m_position.second-1));
        if(i == 1) if(checkMove(m_position.first-2, m_position.second+1)) moves.emplace_back(std::make_pair(m_position.first-2, m_position.second+1));
        if(i == 2) if(checkMove(m_position.first+2, m_position.second-1)) moves.emplace_back(std::make_pair(m_position.first+2, m_position.second-1));
        if(i == 3) if(checkMove(m_position.first+2, m_position.second+1)) moves.emplace_back(std::make_pair(m_position.first+2, m_position.second+1));
    }
    for (int i = 0; i < 4; ++i) {
        if(i == 0) if(checkMove(m_position.first-1, m_position.second-2)) moves.emplace_back(std::make_pair(m_position.first-1, m_position.second-2));
        if(i == 1) if(checkMove(m_position.first-1, m_position.second+2)) moves.emplace_back(std::make_pair(m_position.first-1, m_position.second+2));
        if(i == 2) if(checkMove(m_position.first+1, m_position.second-2)) moves.emplace_back(std::make_pair(m_position.first+1, m_position.second-2));
        if(i == 3) if(checkMove(m_position.first+1, m_position.second+2)) moves.emplace_back(std::make_pair(m_position.first+1, m_position.second+2));
    }
    return moves;
}

//////////////////////////////////////////////////////////////////
// BISHOP
//////////////////////////////////////////////////////////////////

Bishop::Bishop() : AbstractPiece(BISHOP_NBR)
{}

Bishop::Bishop(int t_x, int t_y, bool t_color) : AbstractPiece(t_x, t_y, t_color, BISHOP_NBR)
{}

Bishop::~Bishop() = default;

bool Bishop::checkMove(int t_x, int t_y) {
	return (abs(m_position.first-t_x) == abs(m_position.second-t_y));
}

bool Bishop::checkSpecialMove() {
	return false;
}

std::string Bishop::getType() const {
    return BISHOP;
}

std::vector<std::pair<int, int>> Bishop::getAllMoves() {
    // Return as list of tuple all the positions that can be reached
    std::vector<std::pair<int, int>> moves;
    for (int i = MAX_ROWS; i > -MAX_ROWS; --i) {
        if(checkMove(m_position.first-i, m_position.second+i)) moves.emplace_back(std::make_pair(m_position.first-i, m_position.second+i));
        if(checkMove(m_position.first-i, m_position.second-i)) moves.emplace_back(std::make_pair(m_position.first-i, m_position.second-i));
    }
    return moves;
}

//////////////////////////////////////////////////////////////////
// KING
//////////////////////////////////////////////////////////////////

King::King() : AbstractPiece(KING_NBR)
{}

King::King(int t_x, int t_y, bool t_color) : AbstractPiece(t_x, t_y, t_color, KING_NBR)
{
}

King::~King() = default;

bool King::checkMove(int t_x, int t_y) {
  int x = abs(m_position.first-t_x);
  int y = abs(m_position.second-t_y);
  return ((x == 0 || x == 1) && (y == 0 || y == 1));
}

bool King::checkSpecialMove() {
	return !m_moved;
}

std::string King::getType() const {
    return KING;
}

std::vector<std::pair<int, int>> King::getAllMoves() {
    // Return as list of tuple all the positions that can be reached with or without special move
    std::vector<std::pair<int, int>> moves;
    for (int i = -1; i < 3; ++i) {
        for (int j = -1; j < 3; ++j) {
            if(checkMove(m_position.first+i, m_position.second+j)) moves.emplace_back(std::make_pair(m_position.first+i, m_position.second+j));
        }
    }
    if (checkSpecialMove()) { //Castling
        moves.emplace_back(std::make_pair(0, 2));
        moves.emplace_back(std::make_pair(0, 6));
        moves.emplace_back(std::make_pair(7, 2));
        moves.emplace_back(std::make_pair(7, 6));
    }
    return moves;
}

//////////////////////////////////////////////////////////////////
// QUEEN
//////////////////////////////////////////////////////////////////

Queen::Queen() : AbstractPiece(QUEEN_NBR)
{}

Queen::Queen(int t_x, int t_y, bool t_color) : AbstractPiece(t_x, t_y, t_color, QUEEN_NBR)
{}

Queen::~Queen() = default;

bool Queen::checkMove(int t_x, int t_y) {
	return (t_y == m_position.second) || (t_x == m_position.first) || (abs(m_position.first-t_x) == abs(m_position.second-t_y));
}

bool Queen::checkSpecialMove() {
	return false;
}

std::string Queen::getType() const {
    return QUEEN;
}

std::vector<std::pair<int, int>> Queen::getAllMoves() {
    // Return as list of tuple all the positions that can be reached
    std::vector<std::pair<int, int>> moves;
    for (int i = 0; i < MAX_COLS; ++i) {
        if(checkMove(i, m_position.second)) moves.emplace_back(std::make_pair(i, m_position.second));
    }
    for (int i = 0; i < MAX_ROWS; ++i) {
        if(checkMove(m_position.first, i)) moves.emplace_back(std::make_pair(m_position.first, i));
    }
    for (int i = MAX_ROWS; i > -MAX_ROWS; --i) {
        if(checkMove(m_position.first-i, m_position.second+i)) moves.emplace_back(std::make_pair(m_position.first-i, m_position.second+i));
        if(checkMove(m_position.first-i, m_position.second-i)) moves.emplace_back(std::make_pair(m_position.first-i, m_position.second-i));
    }
    return moves;
}

//////////////////////////////////////////////////////////////////
// PAWN
//////////////////////////////////////////////////////////////////

Pawn::Pawn() : AbstractPiece(PAWN_NBR)
{}

Pawn::Pawn(int t_x, int t_y, bool t_color) : AbstractPiece(t_x, t_y, t_color, PAWN_NBR)
{}

Pawn::~Pawn() = default;

bool Pawn::checkMove(int t_x, int t_y) {
    // Check if a pawn has already moved or not
	bool res;
	if (this->getColor()) res = whitePawnMove(t_x, t_y);
	else res = blackPawnMove(t_x, t_y);
	return res;
}

bool Pawn::whitePawnMove(int t_x, int t_y) {
    // because not the same direction move
	if(not hasMoved() && (m_position.first == 6 && t_x == 4 && m_position.second == t_y))
		return true;
	else
		return (m_position.first-t_x == 1 && (m_position.second == t_y || ((m_position.second+1) == t_y) || ((m_position.second-1) == t_y)));
}

bool Pawn::blackPawnMove(int t_x, int t_y) {
    // because not the same direction move
	if(not hasMoved() && (m_position.first == 1 && t_x == 3 && m_position.second == t_y))
		return true;
	else
		return (t_x-m_position.first == 1 && (m_position.second == t_y || ((m_position.second+1) == t_y) || ((m_position.second-1) == t_y)));
}

bool Pawn::checkSpecialMove() {
	return false;
}

std::string Pawn::getType() const {
    return PAWN;
}

std::vector<std::pair<int, int>> Pawn::getAllMoves() {
    // Return as list of tuple all the positions that can be reached
    std::vector<std::pair<int, int>> moves;
    for (int i = 1; i <= 2; ++i) {
        if(checkMove(m_position.first+i, m_position.second)) moves.emplace_back(std::make_pair(m_position.first+i, m_position.second));
        if(checkMove(m_position.first-i, m_position.second)) moves.emplace_back(std::make_pair(m_position.first-i, m_position.second));
    }
    if(checkMove(m_position.first-1, m_position.second-1)) moves.emplace_back(std::make_pair(m_position.first-1, m_position.second-1));
    if(checkMove(m_position.first-1, m_position.second+1)) moves.emplace_back(std::make_pair(m_position.first-1, m_position.second+1));
    if(checkMove(m_position.first+1, m_position.second-1)) moves.emplace_back(std::make_pair(m_position.first+1, m_position.second-1));
    if(checkMove(m_position.first+1, m_position.second+1)) moves.emplace_back(std::make_pair(m_position.first+1, m_position.second+1));
    return moves;
}
