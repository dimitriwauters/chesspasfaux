// INFO-F209 - Groupe 5 - Gamemodes.cpp

#include "Gamemodes.hpp"

//////////////////////////////////////////////////////////////
// DEFAULT
//////////////////////////////////////////////////////////////

template <typename B>
GameDefault<B>::GameDefault(Player* white_player, Player* black_player, int type): AbstractGame<B>(white_player, black_player, DEFAULT_GAMEMODE, type)
{}

template <typename B>
GameDefault<B>::~GameDefault() = default;

//////////////////////////////////////////////////////////////
// DARK
//////////////////////////////////////////////////////////////

//Board

BoardDark::BoardDark() : Board()
{}

BoardDark::~BoardDark() = default;

//Game

template <typename B>
GameDark<B>::GameDark(Player* white_player, Player* black_player, int type): AbstractGame<B>(white_player, black_player, DARK_GAMEMODE, type)
{}

template <typename B>
void GameDark<B>::rearrangeBoard()
{}

template <typename B>
GameDark<B>::~GameDark() = default;

template <typename B>
void GameDark<B>::checkWinConditions(bool playerTurn) {
    // check is the winning conditions have been reached
    if(this->isInStalemate(!playerTurn))
		this->running_game = false;
    if(this->getCheck(!playerTurn))
        this->setWinner(playerTurn);
}

template <typename B>
bool GameDark<B>::willBeInCheck(std::pair<int, int> src_pos, std::pair<int, int> dest_pos, bool color) {
	if(src_pos.first == 9 && dest_pos.first == 9 && color) return FALSE;	//No warning
    return FALSE;
}

template <typename B>
unsigned GameDark<B>::canCastle(std::pair<int, int> src_pos, std::pair<int, int> dest_pos, bool playerTurn) {
    // is castle available
    unsigned castling_type = this->whichCastling(src_pos, dest_pos);
    if(playerTurn) return castling_type;	//No warning
    return castling_type;
}

template <typename B>
void GameDark<B>::capturePiece(std::pair<int, int> dest_pos) {
    if (this->board->getTypePiece(dest_pos) == KING)
        this->setCheck(!this->turn, true);
    AbstractGame<B>::capturePiece(dest_pos);
}

#ifdef BOARDCLIENT_HPP
template <>
void GameDark<BoardClient<BoardDark>>::rearrangeBoard()
{
    // update the visible board in console
    int i, j;
    std::vector<std::pair<int, int>> squares_to_show = this->getAllMovesAllPieces(this->my_player->getColor());
    for (i = 0; i < MAX_COLS; i++)
    {
        for (j = 0; j < MAX_ROWS; j++)
        {
            if(std::find(squares_to_show.begin(), squares_to_show.end(), std::make_pair(i, j)) == squares_to_show.end())
            {
                int max_x, max_y;
                getmaxyx(this->board->getWindow(std::make_pair(i, j)), max_x, max_y);
                mvwprintw(this->board->getWindow(std::make_pair(i, j)), max_x/2-1, max_y/2-3, "   ?    ");
                wbkgd(this->board->getWindow(std::make_pair(i, j)), COLOR_PAIR(5));
            }
        }
    }
}
#endif

#ifdef BOARDCLIENTGUI_HPP
template <>
void GameDark<BoardClientGUI<BoardDark>>::rearrangeBoard()
{
    // update the visible board in gui
    int i, j;
    std::vector<std::pair<int, int>> squares_to_show = this->getAllMovesAllPieces(this->my_player->getColor());
    for (i = 0; i < MAX_COLS; i++)
    {
        for (j = 0; j < MAX_ROWS; j++)
        {
            if(std::find(squares_to_show.begin(), squares_to_show.end(), std::make_pair(i, j)) == squares_to_show.end())
            {
                this->board->getSquare(std::make_pair(i, j))->setPiece(PIECE_HIDDEN, WHITE);
            }
            else {
                if(this->board->getSquare(std::make_pair(i, j))->getPiece().first == PIECE_HIDDEN) {
                    if(!this->board->isEmpty(std::make_pair(i, j))) {
                        this->board->getSquare(std::make_pair(i, j))->setPiece(this->board->getTypePieceNbr(std::make_pair(i, j)), this->board->getColorPiece(std::make_pair(i, j)));
                    }
                    else this->board->getSquare(std::make_pair(i, j))->setPiece(NO_DEFINED, NO_DEFINED);
                }
            }
        }
    }
    this->board->refreshDisplay();
}
#endif

//////////////////////////////////////////////////////////////
// DICE
//////////////////////////////////////////////////////////////

// Board

BoardDice::BoardDice() : Board()
{}

BoardDice::~BoardDice() = default;

// Game

template <typename B>
GameDice<B>::GameDice(Player* white_player, Player* black_player, int type): AbstractGame<B>(white_player, black_player, DICE_GAMEMODE, type) {
	srand((int)time(0));
}

template <typename B>
GameDice<B>::~GameDice() = default;

template <typename B>
void GameDice<B>::rollTheDices() {
	dices = std::make_pair(rand() % 6 + 1, rand() % 6 + 1);
}

template <typename B>
std::pair<int, int> GameDice<B>::getDices() {
	return dices;
}

template <typename B>
void GameDice<B>::setDices(std::pair<int, int> new_dices) {
	dices.first = new_dices.first;
	dices.second = new_dices.second;
}

template <typename B>
unsigned GameDice<B>::checkMove(std::pair<int, int> src, std::pair<int, int> dest, bool playerTurn) {
    // check all the moves that can be done with the result of the dice given in parameter
	std::pair<int, int> dice = getDices();
	if(dice.first == dice.second)
		return AbstractGame<B>::checkMove(src, dest, playerTurn);
	else if((dice.first == 1 || dice.second == 1) && (AbstractGame<B>::board->getTypePiece(src) == PAWN))
		return AbstractGame<B>::checkMove(src, dest, playerTurn);
	else if((dice.first == 2 || dice.second == 2) && (AbstractGame<B>::board->getTypePiece(src) == KNIGHT))
		return AbstractGame<B>::checkMove(src, dest, playerTurn);
	else if((dice.first == 3 || dice.second == 3) && (AbstractGame<B>::board->getTypePiece(src) == BISHOP))
		return AbstractGame<B>::checkMove(src, dest, playerTurn);
	else if((dice.first == 4 || dice.second == 4) && (AbstractGame<B>::board->getTypePiece(src) == ROOK))
		return AbstractGame<B>::checkMove(src, dest, playerTurn);
	else if((dice.first == 5 || dice.second == 5) && (AbstractGame<B>::board->getTypePiece(src) == QUEEN))
		return AbstractGame<B>::checkMove(src, dest, playerTurn);
	else if((dice.first == 6 || dice.second == 6) && (AbstractGame<B>::board->getTypePiece(src) == KING))
		return AbstractGame<B>::checkMove(src, dest, playerTurn);
	else
		return FALSE;
}

template <typename B>
void GameDice<B>::checkWinConditions(bool playerTurn) {
    if(!this->isUnderAttack(this->getKingPosition(playerTurn), !playerTurn)) // Is my king in check ?
        this->setCheck(playerTurn, false);

    if(this->isUnderAttack(this->getKingPosition(!playerTurn), playerTurn)) // If opposite king check
        this->setCheck(!playerTurn, true); //Opposite king, check

    if(this->getCheck(!playerTurn)) {
        if(this->isInCheckMate(!playerTurn)) {
            this->setWinner(playerTurn);
        }
    }
}

template <typename B>
void GameDice<B>::capturePiece(std::pair<int, int> dest_pos) {
    if (this->board->getTypePiece(dest_pos) == KING) {
        this->setCheck(!this->turn, true);
    }
    AbstractGame<B>::capturePiece(dest_pos);
}

//////////////////////////////////////////////////////////////
// HORDE
//////////////////////////////////////////////////////////////

// Board

BoardHorde::BoardHorde() : Board()
{}

BoardHorde::~BoardHorde() = default;

void BoardHorde::placePieces(bool color) {
    // because there are 2 pawns on the 5th line and a gap on the first
    Board::placePieces(!color);
    std::pair<int, int> pawn_to_remove1 = std::make_pair(HORDE_FIRST_ROW, 3);
    std::pair<int, int> pawn_to_remove2 = std::make_pair(HORDE_FIRST_ROW, 4);
    std::pair<int, int> pawn_to_add1 = std::make_pair(HORDE_LAST_ROW, 3);
    std::pair<int, int> pawn_to_add2 = std::make_pair(HORDE_LAST_ROW, 4);
    for (int i = HORDE_FIRST_ROW; i < HORDE_LAST_ROW; i++) {
        for (int j = 0; j < MAX_COLS; j++) {
            addPiece(PAWN_NBR, std::make_pair(i, j), color);
        }
    }
    removePiece(pawn_to_remove1);
    removePiece(pawn_to_remove2);
    addPiece(PAWN_NBR, pawn_to_add1, color);
    addPiece(PAWN_NBR, pawn_to_add2, color);
}

// Game

template <typename B>
GameHorde<B>::GameHorde(Player* white_player, Player* black_player, int type): AbstractGame<B>(white_player, black_player, HORDE_GAMEMODE, type)
{
}

template <typename B>
GameHorde<B>::~GameHorde() = default;

template <typename B>
void GameHorde<B>::setPiecesOnBoard() {
    // place the piece on board since the starting set is different
    if (this->pieces_beginning.size() == 0) // New game
    {
		this->board->placePieces(BLACK);
		this->setKingPosition(WHITE, std::make_pair(MAX_ROWS-1, 4));
	}
    else // Paused game
    {
        for (int i = 0; i < static_cast<int>(this->pieces_beginning.size()); ++i)
        {
            std::string piece = this->pieces_beginning.at(i);
            int x_pos = std::stoi(piece.substr(0, 1));
            int y_pos = std::stoi(piece.substr(1, 1));
            int piece_type = std::stoi(piece.substr(2, 1));
            bool piece_color = static_cast<bool>(std::stoi(piece.substr(3, 1)));
            this->board->addPiece(piece_type, std::make_pair(x_pos, y_pos), piece_color);
            if (piece_type == KING_NBR) this->setKingPosition(piece_color, std::make_pair(x_pos, y_pos));
        }
    }
}

template <typename B>
void GameHorde<B>::capturePiece(std::pair<int, int> dest_pos) {
    if ((this->board->getColorPiece(dest_pos) == BLACK) && this->turn != BLACK)
        nb_black_pawns--;
    AbstractGame<B>::capturePiece(dest_pos);
}

template <typename B>
void GameHorde<B>::checkWinConditions(bool playerTurn) {
	if(!this->getCheck(!playerTurn)) {
		if (this->isInStalemate(!playerTurn)) {
			this->running_game = false;
		}
	}
    if (this->turn == WHITE) {
        if (nb_black_pawns == 0) {
			this->setWinner(WHITE);
		}
    }
    if(this->isUnderAttack(this->getKingPosition(WHITE), BLACK)) // If opposite king check
    {
        this->setCheck(WHITE, true); // Opposite king, check
	}
    if(this->getCheck(WHITE)) {
        if(this->isInCheckMate(WHITE)) {
			this->setWinner(BLACK);
		}
    }
}

template <typename B>
bool GameHorde<B>::isInStalemate(bool player_color)
// Check if there is a stalemate situation for the given player color (black/white).
{
    if (this->getCheck(player_color)) return false;
    int i, j, nb_left_pieces = 0, nb_blocked_pieces = 0;
    std::pair<int, int> my_piece;
    for (i = 0; i < MAX_COLS; i++) {
        for (j = 0; j < MAX_ROWS; j++) {
            my_piece = std::make_pair(i, j);
            if (!this->board->isEmpty(my_piece)) { // If there is a piece on this square
                if (this->board->getColorPiece(my_piece) == player_color) {
                    // ...and if the piece belongs to the player whose color is given
                    nb_left_pieces++;
                    std::vector<std::pair<int, int>> moves = \
                    this->getAllMoves(my_piece, player_color);
                    if (moves.empty()) nb_blocked_pieces++;
                }
            }
        }
    }
    if (nb_left_pieces == 0) return false;
    // Has the player no legal move at all?
    return (nb_blocked_pieces == nb_left_pieces);
}
