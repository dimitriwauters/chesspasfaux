// INFO-F209 - Groupe 5 - Board.cpp
// Represent the chessboard and its management of pieces.

#include "Board.hpp"

////////////////////////////////////////////////////////////////////////////////
// INITIALISATION
////////////////////////////////////////////////////////////////////////////////

Board::Board()
{
    for (int i = 0; i < MAX_ROWS; i++)
    {
        for (int j = 0; j < MAX_COLS; j++)
        {
            board_mat[i][j] = nullptr;
        }
    }
}

Board::~Board() = default;

////////////////////////////////////////////////////////////////////////////////
// ADD AND REMOVE PIECES
////////////////////////////////////////////////////////////////////////////////

void Board::placePieces(bool color)
// Place pieces on the chessboard (following the traditional chess set of pieces).
{
    unsigned row, pawn_row;
    row = (color == WHITE) ? MAX_ROWS-1 : 0;
    pawn_row = (color == WHITE) ? MAX_ROWS-2 : MAX_ROWS-7;
    for(int i = 0; i < (NBR_TYPES_PIECES/2); i++) // (Bishop, Rook, Knight) x2
    {
        addPiece(i, std::pair<int, int>(row, i), color);
        addPiece(i, std::pair<int, int>(row, MAX_COLS-i-1), color);
    }

    for (int i = 0; i < MAX_RANGE; i++) // Pawn x8
    {
        addPiece(PAWN_NBR, std::pair<int, int>(pawn_row, i), color);
    }
    addPiece(QUEEN_NBR, std::pair<int, int>(row, 3), color); // Queen x1
    addPiece(KING_NBR, std::pair<int, int>(row, 4), color); // King x1
}

bool Board::addPiece(int piece_nbr, std::pair<int, int> coord, bool color)
// Add a piece (of color player_color) in board_mat[coord.first][coord.second].
{
    bool res = false;
    if (isEmpty(coord))
    {
        switch(piece_nbr)
        {
            case ROOK_NBR:
            {
                board_mat[coord.first][coord.second] = new Rook(coord.first, coord.second, color);
                break;
            }
            case BISHOP_NBR:
            {
                board_mat[coord.first][coord.second] = new Bishop(coord.first, coord.second, color);
                break;
            }
            case QUEEN_NBR:
            {
                board_mat[coord.first][coord.second] = new Queen(coord.first, coord.second, color);
                break;
            }
            case KING_NBR:
            {
                board_mat[coord.first][coord.second] = new King(coord.first, coord.second, color);
                break;
            }
            case KNIGHT_NBR:
            {
                board_mat[coord.first][coord.second] = new Knight(coord.first, coord.second, color);
                break;
            }
            case PAWN_NBR:
            {
                board_mat[coord.first][coord.second] = new Pawn(coord.first, coord.second, color);
                break;
            }
        }
        res = true;
    }
    return res;
}

void Board::removeAllPieces()
// Delete each piece of the board.
{
    for (int i = 0; i < MAX_ROWS; i++)
    {
        for (int j = 0; j < MAX_COLS; j++)
        {
            removePiece(std::pair<int, int>(i, j));
        }
    }
}

bool Board::removePiece(std::pair<int, int> coord)
// Remove a piece from the board at the position coord and return the result.
{
    bool res = false;
    if (!isEmpty(coord))
    {
        delete board_mat[coord.first][coord.second];
        board_mat[coord.first][coord.second] = nullptr;
        res = true;
    }
    return res;
}

////////////////////////////////////////////////////////////////////////////////
// INFORMATION ABOUT PIECES (GETTERS)
////////////////////////////////////////////////////////////////////////////////

std::string Board::getTypePiece(std::pair<int, int> coord) const
// Return the type (as a str) of a piece located at coord.
{
    std::string res = NO_PIECE;
    if (board_mat[coord.first][coord.second])
    {
        res = board_mat[coord.first][coord.second]->getType();
    }
    return res;
}

unsigned Board::getTypePieceNbr(std::pair<int, int> coord) const
// Return the type (as a number, unsigned int) of a piece located at coord.
{
    unsigned res = NO_DEFINED;
    if (board_mat[coord.first][coord.second])
    {
        res = board_mat[coord.first][coord.second]->getTypeNbr();
    }
    return res;
}

bool Board::getColorPiece(std::pair<int, int> coord) const
// Return the color of a piece located at coord.
{
    return board_mat[coord.first][coord.second]->getColor();
}

bool Board::hasMoved(std::pair<int, int> coord) const
// Check if a piece has already moved and return the result.
{
    return board_mat[coord.first][coord.second]->hasMoved();
}

std::vector<std::pair<int, int>> Board::getAllMoves(std::pair<int, int> coord)
// Return all possible moves of a particular piece from coord.
{
    if (!isEmpty(coord)) return board_mat[coord.first][coord.second]->getAllMoves();
    return {};
}

////////////////////////////////////////////////////////////////////////////////
// MOVES
////////////////////////////////////////////////////////////////////////////////

void Board::setFirstMovePiece(std::pair<int, int> coord)
// Define the first move of a piece located at coord.
{
  if (!board_mat[coord.first][coord.second]->hasMoved())
  {
      board_mat[coord.first][coord.second]->firstMove();
  }
}

void Board::setNewPosition(std::pair<int, int> src_pos, std::pair<int, int> dest_pos)
// Define a new position of a piece in the matrix.
{
    board_mat[dest_pos.first][dest_pos.second] = board_mat[src_pos.first][src_pos.second];
    board_mat[src_pos.first][src_pos.second]->move(dest_pos.first, dest_pos.second);
    board_mat[src_pos.first][src_pos.second] = nullptr;
}

unsigned Board::whichMove(std::pair<int, int> src_pos, std::pair<int, int> dest_pos)
// Check and return if a move is horizontal, vertical or diagonal.
{
    unsigned move_type = 0;
    if ((src_pos.first == dest_pos.first) && (src_pos.second != dest_pos.second))
    {
        move_type = HORIZONTAL;
    }
    else if ((src_pos.second == dest_pos.second) && (src_pos.first != dest_pos.first))
    {
        move_type = VERTICAL;
    }
    else if (abs(src_pos.first-dest_pos.first) == abs(src_pos.second-dest_pos.second))
    {
        move_type = DIAGONAL;
    }
    return move_type;
}

void Board::doVirtualMove(bool status)
// Define if it is a real move or a virtual move (in case to get a king out of a check).
{
    virtual_move = status;
}

bool Board::isVirtualMove()
// Return if a move is real (that is, the board has to be updated) or not.
{
    return virtual_move;
}

// Moves validity

bool Board::isInRange(std::pair<int, int> coord)
// Check if coord is in the range of the chessboard. Return the result.
{
    return (coord.first >= 0 && coord.first < MAX_ROWS) && (coord.second >= 0 && \
      coord.second < MAX_COLS);
}

bool Board::isLegalMovePiece(std::pair<int, int> src_pos, std::pair<int, int> dest_pos)
// Check if a move to dest_pos corresponds to the particular moves of a piece located
// at src_pos. Return the result.
{
    return board_mat[src_pos.first][src_pos.second]->checkMove(dest_pos.first, dest_pos.second);
}

bool Board::noPiecesBetween(std::pair<int, int> src_pos, std::pair<int, int> dest_pos)
// Check if there are not pieces in the range between src_pos and dest_pos.
// Return the result.
{
    int val_x = src_pos.first-dest_pos.first;
    int val_y = src_pos.second-dest_pos.second;
    int abs_val_x = abs(val_x);
    int abs_val_y = abs(val_y);
    unsigned direction = whichMove(src_pos, dest_pos);
    switch (direction)
    {
        case HORIZONTAL:
        {
            if (abs_val_y == 1) return true;
            else if (val_y < -1) return rangeEmpty(NO_DEFINED, src_pos.second+1, \
                dest_pos.second, src_pos.first, NO_DEFINED, NO_DEFINED, true); // To right
            return rangeEmpty(NO_DEFINED, src_pos.second-1, dest_pos.second, \
                src_pos.first, NO_DEFINED, NO_DEFINED, false);	// To left
        }
        case VERTICAL:
        {
            if (abs_val_x == 1) return true;
            else if (val_x < -1) return rangeEmpty(NO_DEFINED, src_pos.first+1, \
                dest_pos.first, NO_DEFINED, src_pos.second, NO_DEFINED, true); // Down
            return rangeEmpty(NO_DEFINED, src_pos.first-1, dest_pos.first, \
                NO_DEFINED, src_pos.second, NO_DEFINED, false); // Up
        }
        case DIAGONAL:
        {
            if (abs_val_y == 1) return true;
            else if (val_y > 0)  // Descending move
            {
                // To left
                if (val_x > 0) return noDiagonalJump(src_pos, abs_val_y, -1, -1);
                else return noDiagonalJump(src_pos, abs_val_y, 1, -1);
            }
            else // Ascending move
            {
                // To left
                if (val_x > 0) return noDiagonalJump(src_pos, abs_val_y, -1, 1);
                else return noDiagonalJump(src_pos, abs_val_y, 1, 1);
            }
        }
    }
    return true;
}

bool Board::rangeEmpty(int val, int start, int stop, int x, int y, int z, bool dir)
// Check if a range of squares is empty. Return the result.
{
    std::pair<int, int> cell;
    if (val == NO_DEFINED)
    {
		if (dir)
		{
            // Positive move (++)
			for (int i = start; i < stop; i++)
			{
				if (z == NO_DEFINED) // Check for horizontal & vertical moves
				{
					if (x == NO_DEFINED) cell = std::make_pair(i, y);
					else if (y == NO_DEFINED) cell = std::make_pair(x, i);
					else cell = std::make_pair(x, y);
				}
				else if (z < 0) cell = std::make_pair(x, y-i); // Check for castling (queenside)
				else cell = std::make_pair(x, y+i); // Castling (kingside)
                if (!isEmpty(cell)) return false;
			}
		}
		else
		{
            // Negative move (--)
			for (int i = start; i > stop; i--)
			{
				if (z == NO_DEFINED) // Check for horizontal & vertical moves
				{
					if (x == NO_DEFINED) cell = std::make_pair(i, y);
					else if (y == NO_DEFINED) cell = std::make_pair(x, i);
					else cell = std::make_pair(x, y);
				}
				else if (z < 0) cell = std::make_pair(x, y-i); // Check for castling (queenside)
				else cell = std::make_pair(x, y+i); // Castling (kingside)
                if (!isEmpty(cell)) return false;
			}
		}
    }
    else // Diagonal moves
    {
        for (int i = start; i > stop; i--)
        {
            if (x == NO_DEFINED) cell = std::make_pair(i, y);
            else if (y == NO_DEFINED) cell = std::make_pair(x, i);
            else cell = std::make_pair(x, y);
            if (!isEmpty(cell)) return false;
        }
    }
    return true;
}

bool Board::isEmpty(std::pair<int, int> coord)
// Check if a square (in board_mat[coord.first][coord.second]) is empty.
// Return the result.
{
    return board_mat[coord.first][coord.second] == nullptr;
}

bool Board::noDiagonalJump(std::pair<int, int> coord, int abs_val_y, int x, int y)
// Check if a piece (especially the bishop) do not jump diagonally any piece.
// Return the result.
{
    bool res = true;
    std::pair<int, int> cell;
    for (int i = 1; i < abs_val_y; i++)
    {
        cell = std::make_pair(coord.first+(i*x), coord.second+(i*y));
        if (!isEmpty(cell)) res = false;
    }
    return res;
}

// Special moves

bool Board::isSpecialMove(std::pair<int, int> coord)
// Check and return if there is a special move.
{
	return board_mat[coord.first][coord.second]->checkSpecialMove();
}
