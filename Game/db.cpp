// INFO-F209 - Groupe 5 - db.cpp
// Create a database and set some requests (SQLite3).

#include "db.hpp"

////////////////////////////////////////////////////////////////////////////////
// GENERAL
////////////////////////////////////////////////////////////////////////////////

int initializeDB()
// Create database (if it does not exist) with its tables: Players, Friends, Histo_match
// and Saved_game.
{
	sqlite3 *db;
	int exit = 0;
	std::string sql;
	char* err_message;
	// Opening database
	exit = sqlite3_open(DB_NAME, &db);
	if (exit)
	{
        std::cerr << DB_OPENING_ERROR << sqlite3_errmsg(db) << std::endl;
        return (-1);
    }
    else std::cout << DB_OPENING_SUCCESS << std::endl;
	// Creating tables: Players, Friends, Histo_match, Saved_game
	// Players (id, pseudo, password, games_won, games_lost, games_drawn, elo):
	// store the user's stats
	sql = "CREATE TABLE Players(id INTEGER PRIMARY KEY AUTOINCREMENT, \
								pseudo VARCHAR(255) NOT NULL, \
								password VARCHAR(255) NOT NULL, \
								games_won INTEGER DEFAULT 0, \
								games_lost INTEGER DEFAULT 0, \
								games_drawn INTEGER DEFAULT 0, \
								elo INTEGER DEFAULT 1000);";
	exit = sqlite3_exec(db, sql.c_str(), 0, 0, &err_message);
	if (exit != SQLITE_OK)
	{
        std::cerr << DB_CREATING_TABLE_ERROR << std::endl;
        sqlite3_free(err_message);
    }
    else std::cout << DB_CREATING_TABLE_SUCCESS << std::endl;
	// Friends (id, friend1, friend2, status): store if 2 users are being friends
	// or are friends
	sql = "CREATE TABLE Friends(id INTEGER PRIMARY KEY AUTOINCREMENT, \
								friend1 INTEGER, \
								friend2 INTEGER, \
								status BOOLEAN DEFAULT 0)";
	exit = sqlite3_exec(db, sql.c_str(), 0, 0, &err_message);
	if (exit != SQLITE_OK)
	{
        std::cerr << DB_CREATING_TABLE_ERROR << std::endl;
        sqlite3_free(err_message);
    }
    else std::cout << DB_CREATING_TABLE_SUCCESS << std::endl;
	// Histo_match (id, player1, player2, winner, ended)
	sql = "CREATE TABLE Histo_match(id INTEGER PRIMARY KEY AUTOINCREMENT, \
									player1 INTEGER, \
									player2 INTEGER, \
									winner INTEGER, \
									ended BOOLEAN DEFAULT 0, \
									date INTEGER, \
									game_mode INTEGER)";
	exit = sqlite3_exec(db, sql.c_str(), 0, 0, &err_message);
	if (exit != SQLITE_OK)
	{
        std::cerr << DB_CREATING_TABLE_ERROR << std::endl;
        sqlite3_free(err_message);
    }
    else std::cout << DB_CREATING_TABLE_SUCCESS << std::endl;
	// Saved_game (id, game_id, x_pos, y_pos, piece_type, color_piece): store the
	// position of pieces when a game is paused
	sql = "CREATE TABLE Saved_game(id INTEGER PRIMARY KEY AUTOINCREMENT, \
									game_id INTEGER, \
									x_pos INTEGER, \
									y_pos INTEGER, \
									piece_type INTEGER, \
									color_piece INTEGER)";
	exit = sqlite3_exec(db, sql.c_str(), 0, 0, &err_message);
	if (exit != SQLITE_OK)
	{
        std::cerr << DB_CREATING_TABLE_ERROR << std::endl;
        sqlite3_free(err_message);
    }
    else std::cout << DB_CREATING_TABLE_SUCCESS << std::endl;
	sqlite3_close(db);
	return 0;
}

void selectDB(std::string* data, std::string field, std::string table, std::string where, \
	std::string where_res)
// Store in data the selected value.
{
	sqlite3 *db;
	sqlite3_stmt *res;
	int rc = sqlite3_open(DB_NAME, &db);
	std::string sql = "SELECT " + field + " FROM " + table + " WHERE " + where + " = \'" +\
	where_res + "\';";
	rc = sqlite3_prepare_v2(db, sql.c_str(), static_cast<int>(sql.size()+1), &res, nullptr);
	rc = sqlite3_step(res);
	if (rc == SQLITE_ROW) *data = std::string(reinterpret_cast<const char*>(sqlite3_column_text(res, 0)));
	sqlite3_finalize(res);
	sqlite3_close(db);
}

void selectDBLimit(std::vector<std::pair<std::string, int>>* data, std::string field, \
	std::string table, std::string order, int limit)
// Store in data the selected value (with a restriction limit).
{
    sqlite3 *db;
    sqlite3_stmt *res;
    int rc = sqlite3_open(DB_NAME, &db);
    std::string sql = "SELECT " + field + " FROM " + table + " ORDER BY " + order + " DESC LIMIT " + \
	std::to_string(limit) + ";";
	rc = sqlite3_prepare_v2(db, sql.c_str(), static_cast<int>(sql.size()+1), &res, nullptr);
    while( true)
	{
		rc = sqlite3_step(res);
		if (rc == SQLITE_ROW) data->emplace_back(std::make_pair(std::string(\
			reinterpret_cast<const char*>(sqlite3_column_text(res, 0))), sqlite3_column_int(res, 1)));
		else break;
    }
    sqlite3_finalize(res);
    sqlite3_close(db);
}

void countDB(std::string* data, std::string table, std::string where, std::string where_res)
// Store in data the number of repetitions of the searched value.
{
	sqlite3 *db;
	sqlite3_stmt *res;
	int rc = sqlite3_open(DB_NAME, &db);
	std::string sql = "SELECT COUNT(" + where + ") FROM " + table + " WHERE " + where + " = \'" + where_res + "\';";
	rc = sqlite3_prepare_v2(db, sql.c_str(), static_cast<int>(sql.size()+1), &res, nullptr);
	rc = sqlite3_step(res);
    if (rc == SQLITE_ROW) *data = std::string(reinterpret_cast<const char*>(\
		sqlite3_column_text(res, 0)));
	sqlite3_finalize(res);
	sqlite3_close(db);
}

void updateDB(std::string field, std::string new_val, std::string table, std::string where, std::string where_res)
// Update the value stored at the given field and replacing it by new_val.
{
	sqlite3 *db;
	char* err_msg;
	sqlite3_open(DB_NAME, &db);
	std::string sql = "UPDATE " + table + " set " + field + " = \'" + new_val + "\' \
						WHERE " + where + " = \'" + where_res + "\';";
	sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &err_msg);
	sqlite3_close(db);
}

void updateDBInc(std::string field, std::string table, std::string where, std::string where_res, int n)
// Update the value stored at the given field and replacing it by new_val (with an increment of n).
{
	sqlite3 *db;
	char *err_msg = 0;
	sqlite3_open(DB_NAME, &db);
	std::string sql = "UPDATE " + table + " set " + field + " = (" + field + " + " + std::to_string(n) + ") \
					WHERE " + where + " = \'" + where_res + "\';";
	sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &err_msg);
	sqlite3_close(db);
}

////////////////////////////////////////////////////////////////////////////////
// PLAYERS & FRIENDS
////////////////////////////////////////////////////////////////////////////////

void insertPlayerDB(std::string pseudo, std::string password)
// Insert a new player to the database.
{
	sqlite3 *db;
	char *err_msg = 0;
	sqlite3_open(DB_NAME, &db);
	std::string sql = "INSERT INTO Players(pseudo, password) VALUES (\'" + pseudo + "\', \'" + password + "\');";
	sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &err_msg);
	sqlite3_close(db);
}

void insertFriendsDB(std::string friend1, std::string friend2)
// Insert a new friendship (requested or confirmed) in the database.
{
	sqlite3 *db;
	char *err_msg = 0;
	sqlite3_open(DB_NAME, &db);
	std::string sql = "INSERT INTO Friends(friend1, friend2) VALUES (" + friend1 + ", " + friend2 + ");";
	sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &err_msg);
	sqlite3_close(db);
}

void getFriendsDB(std::string* data, std::string player_id, std::string status)
// Insert in data the ids of the players with whom player_id is friend (with a '$' separator).
{
	sqlite3 *db;
	sqlite3_stmt *res;
	int rc = sqlite3_open(DB_NAME, &db);
	std::string sql;
	sql = "SELECT friend1 FROM Friends WHERE (friend2=='" + player_id + "' AND status ='" + status + "');";
	rc = sqlite3_prepare_v2(db, sql.c_str(), static_cast<int>(sql.size()+1), &res, nullptr);
	rc = sqlite3_step(res);
	*data = "";
	if (rc == SQLITE_ROW)
	{
		while(sqlite3_column_text(res, 0))
		{
			*data += std::string(reinterpret_cast<const char*>(sqlite3_column_text(res, 0)));
			rc = sqlite3_step(res);
			if (sqlite3_column_text(res, 0)) *data += "$";
		}
	}
	if (status == "1") // If we are looking for all friends (and not only the requests)
	{
		sql = "SELECT friend2 FROM Friends WHERE (friend1='" + player_id + "' AND status ='" + status + "');";
		rc = sqlite3_prepare_v2(db, sql.c_str(), static_cast<int>(sql.size()+1), &res, nullptr);
		rc = sqlite3_step(res);
		if (rc == SQLITE_ROW)
		{
			if (*data != "") { *data += "$"; }
			while(sqlite3_column_text(res, 0))
			{
				*data += std::string(reinterpret_cast<const char*>(sqlite3_column_text(res, 0)));
				rc = sqlite3_step(res);
				if (sqlite3_column_text(res, 0)) *data += "$";
			}
		}
	}
	sqlite3_finalize(res);
	sqlite3_close(db);
}

void deleteFriendsDB(std::string friend1, std::string friend2)
// Remove a friendship from the database.
{
	sqlite3 *db;
	char *err_msg = 0;
	sqlite3_open(DB_NAME, &db);
	std::string sql = "DELETE FROM Friends WHERE ((friend1 = \'" + friend1 + "\' \
						AND friend2=\'" + friend2 + "\') OR (friend1 = \'" + friend2 + "\' \
						AND friend2 = \'" + friend1 + "\'));";
	sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &err_msg);
	sqlite3_close(db);
}

void acceptFriendsDB(std::string friend1, std::string friend2)
// Update a friendship's status (from requested to confirmed).
{
	sqlite3 *db;
	char *err_msg = 0;
	sqlite3_open(DB_NAME, &db);
	std::string sql = "UPDATE Friends set status=1 WHERE (friend1 = \'" + friend1 + "\' \
						AND friend2=\'" + friend2 + "\');";
	sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &err_msg);
	sqlite3_close(db);
}

////////////////////////////////////////////////////////////////////////////////
// MATCHES
////////////////////////////////////////////////////////////////////////////////

unsigned insertMatchDB(std::string player1, std::string player2, std::string winner, bool ended, \
	int game_mode)
{
    return insertMatchDB(player1, player2, winner, ended, game_mode, time(0));
}

unsigned insertMatchDB(std::string player1, std::string player2, std::string winner, bool ended, \
	int game_mode, time_t date)
// Insert the details of a match in the database (date included).
{
	sqlite3 *db;
    sqlite3_stmt *res;
	char *err_msg = 0;
	int rc;
	unsigned ID = 0;
	sqlite3_open(DB_NAME, &db);
	std::string sql = "INSERT INTO Histo_match (player1, player2, winner, ended, date, game_mode) \
						VALUES('" + player1 + "', '" + player2 + "', '" + winner + "', '" + \
						std::to_string(ended) + "', '" + std::to_string(date) + "', '" + \
						std::to_string(game_mode) + "');";
	sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &err_msg);
    sql = "SELECT id FROM Histo_match WHERE (player1='" + player1 + "' AND player2='" + \
			player2 + "' AND winner='" + winner + "' AND ended=" + std::to_string(ended) + " \
			AND date='" + std::to_string(date) + "' AND game_mode=" + std::to_string(game_mode) + ");";
    sqlite3_prepare_v2(db, sql.c_str(), static_cast<int>(sql.size()+1), &res, nullptr);
    rc = sqlite3_step(res);
    if (rc == SQLITE_ROW)ID = static_cast<unsigned>(sqlite3_column_int(res, 0));
    sqlite3_finalize(res);
	sqlite3_close(db);
	return ID;
}

void getMatchDB(std::vector<std::vector<std::string>>* data, std::string pseudo)
// Load the information about a match.
{
    sqlite3 *db;
    sqlite3_stmt *res;
    int rc = sqlite3_open(DB_NAME, &db);
    for (int i = 0; i < 2; ++i)
	{
        std::string sql;
        if (i == 0) sql = "SELECT id, player2, date FROM Histo_match WHERE (player1='" + pseudo + "' AND ended=0);";
        else if (i == 1) sql = "SELECT id, player1, date FROM Histo_match WHERE (player2='" + pseudo + "' AND ended=0);";
        sqlite3_prepare_v2(db, sql.c_str(), static_cast<int>(sql.size()+1), &res, nullptr);
        while (true)
		{
            rc = sqlite3_step(res);
            std::vector<std::string> request;
            if (rc == SQLITE_ROW)
			{
                std::string first = reinterpret_cast<const char*>(sqlite3_column_text(res, 1));
                auto time = static_cast<time_t>(sqlite3_column_int(res, 2));
                std::string second = std::ctime(&time);
                std::string third = std::to_string(sqlite3_column_int(res, 0));
                std::vector<std::string> temp = {first, second, third};
                data->emplace_back(temp);
            }
            else break;
        }
    }
    sqlite3_finalize(res);
    sqlite3_close(db);
}

void getMatchDB(std::pair<std::string, std::string>* data, std::string game_id, int* game_mode)
{
    sqlite3 *db;
    sqlite3_stmt *res;
    int rc = sqlite3_open(DB_NAME, &db);
    std::string sql = "SELECT player1, player2, game_mode FROM Histo_match WHERE id = " + game_id + ";";
    rc = sqlite3_prepare_v2(db, sql.c_str(), static_cast<int>(sql.size()+1), &res, nullptr);
    rc = sqlite3_step(res);
    if (rc == SQLITE_ROW)
	{
        data->first = reinterpret_cast<const char*>(sqlite3_column_text(res, 0));
        data->second = reinterpret_cast<const char*>(sqlite3_column_text(res, 1));
        *game_mode = sqlite3_column_int(res, 2);
    }
    sqlite3_finalize(res);
    sqlite3_close(db);
}

void deleteMatchDB(std::string game_id)
// Remove a match from the database.
{
    sqlite3 *db;
    char *err_msg = 0;
    sqlite3_open(DB_NAME, &db);
    std::string sql = "DELETE FROM Histo_match WHERE (id = '" + game_id + "');";
    sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &err_msg);
    sqlite3_close(db);
}

////////////////////////////////////////////////////////////////////////////////
// SAVED GAMES
////////////////////////////////////////////////////////////////////////////////

void insertSavedGameDB(unsigned game_id, int x_pos, int y_pos, int piece_type, bool color_piece)
// Insert a paused game in the database.
{
	sqlite3 *db;
	char *err_msg = 0;
	sqlite3_open(DB_NAME, &db);
	std::string sql = "INSERT INTO Saved_game(game_id, x_pos, y_pos, piece_type, color_piece) \
						VALUES('" + std::to_string(game_id) + "', '" + std::to_string(x_pos) + \
						"', '" + std::to_string(y_pos) + "', '" + std::to_string(piece_type) + \
						"', '" + std::to_string(color_piece) + "');";
	sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &err_msg);
	sqlite3_close(db);
}

void getSavedGameDB(std::vector<std::string>* data, std::string game_id)
// Get a paused game from the database.
{
    sqlite3 *db;
    sqlite3_stmt *res;
    int rc = sqlite3_open(DB_NAME, &db);
    std::string sql;
    sql = "SELECT * FROM Saved_game WHERE game_id=" + game_id + ";";
    sqlite3_prepare_v2(db, sql.c_str(), static_cast<int>(sql.size()+1), &res, nullptr);
    while (true)
	{
        rc = sqlite3_step(res);
        if (rc == SQLITE_ROW)
		{
            int x_pos = sqlite3_column_int(res, 2);
            int y_pos = sqlite3_column_int(res, 3);
            int piece_type = sqlite3_column_int(res, 4);
            int piece_color = sqlite3_column_int(res, 5);
            std::string result = std::to_string(x_pos) + std::to_string(y_pos) + std::to_string(piece_type) + std::to_string(piece_color);
            data->emplace_back(result);
        }
        else break;
    }
    sqlite3_finalize(res);
    sqlite3_close(db);
}

void deleteSavedGameDB(std::string game_id)
// Remove a saved game from the database.
{
    sqlite3 *db;
    char *zErrMsg = 0;
    sqlite3_open(DB_NAME, &db);
    std::string sql = "DELETE FROM Saved_game WHERE (game_id = '" + game_id + "');";
    sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &zErrMsg);
    sqlite3_close(db);
}
