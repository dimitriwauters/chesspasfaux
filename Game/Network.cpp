// INFO-F209 - Groupe 5 - network.cpp

#include "Network.hpp"

////////////////////////////////////////////////////////////////////
// SERVER
////////////////////////////////////////////////////////////////////

void initialiseSocket(SockaddrIn* server_addr, uint16_t port) {
    server_addr->sin_family = AF_INET;
    server_addr->sin_port = htons(port);
    memset(&(server_addr->sin_zero), '\0', 8);
}

void setServer(SockaddrIn* server_addr, uint16_t port) {
    server_addr->sin_addr.s_addr = INADDR_ANY;
    initialiseSocket(server_addr, port);
}

void setHost(char* hostname, SockaddrIn* server_addr, uint16_t port) {
    struct hostent *he;
    if ((he=gethostbyname(hostname)) == nullptr) {
        perror("Client: gethostbyname");
        exit(1);
    }
    server_addr->sin_addr = *((struct in_addr*)he->h_addr);
    initialiseSocket(server_addr, port);
}

void binding(int* socketfd, SockaddrIn* server_addr) {
    if(bind(*socketfd, (Sockaddr*)server_addr, sizeof(Sockaddr)) == -1){
        perror("bind");
        exit(1);
    }
}

void listening(int* socketfd) {
    if(listen(*socketfd, BACKLOG) == -1) { perror("listen"); exit(1); }
}

void accepting(int* socketfd, SockaddrIn* their_add, int* new_fd) {
    socklen_t sin_size = sizeof(SockaddrIn);
    *new_fd = accept(*socketfd, (struct sockaddr *)their_add, &sin_size);
    if(*new_fd == -1) { perror("accept"); exit(1); }
}

void connecting(int* socketfd, SockaddrIn* server_addr) {
    if(connect(*socketfd, (struct sockaddr *)server_addr, sizeof(struct sockaddr)) == -1) {
        perror("Client: connect");
        exit(1);
    }
}

int i_number(int position, char* message) {
    int number = message[position] - '0';	//Position of the number we want
    return number;
}

////////////////////////////////////////////////////////////////////
// SENDING
////////////////////////////////////////////////////////////////////

bool sending_T(int socketfd, char* message) {
    size_t size = strlen(message);
    ssize_t first, second;
    first = send(socketfd, &size, sizeof(size), 0); //Send size
    if(first == -1) { perror("send"); exit(1); }
    else if (first == 0) { return false; }
    second = send(socketfd, message, size, 0); //Send message
    if(second == -1) { perror("send"); exit(1); }
    else if (second == 0) { return false; }
    return true;
}

bool sending(int socketfd, std::string message) {
    size_t size = message.length();
    ssize_t first, second;
    first = write(socketfd, &size, sizeof(size)); //Send size
    if(first == -1) { perror("send"); exit(1); }
    else if (first == 0) { return false; }
    second = write(socketfd, &message[0], size); //Send message
    if(second == -1) { perror("send"); exit(1); }
    else if (second == 0) { return false; }
    return true;
}

bool sending(int socketfd, int number) {
    ssize_t first;
    first = write(socketfd, &number, sizeof(int)); //Send number
    if(first == -1) { perror("send"); exit(1); }
    else if (first == 0) { return false; }
    return true;
}

char* prepForSending(int opcode, std::string message) {
    message = std::to_string(opcode) + message;
    return const_cast<char*>(message.c_str());
}

////////////////////////////////////////////////////////////////////
// RECEIVING
////////////////////////////////////////////////////////////////////

bool receiving_T(int socketfd, char message[MAXBUFSIZE]) {
    size_t size;
    ssize_t first, second;
    first = recv(socketfd, &size, sizeof(size), 0); //Send size
    if(first == -1) { std::string error = "recv (" + std::to_string(socketfd) + ") "; perror(error.c_str()); exit(1); }
    else if (first == 0) { return false; }
    second = recv(socketfd, message, size, 0); //Send message
    if(second == -1) { std::string error = "recv (" + std::to_string(socketfd) + ") "; perror(error.c_str()); exit(1); }
    else if (second == 0) { return false; }
    if(strlen(message) > size)
        message[strlen(message) - (strlen(message) - size)] = '\0';
    return true;
}

bool receiving(int socketfd, char message[MAXBUFSIZE]) {
    size_t size;
    ssize_t first, second;
    first = read(socketfd, &size, sizeof(size)); //Receive size
    if(first == -1) { std::string error = "recv (" + std::to_string(socketfd) + ") "; perror(error.c_str()); exit(1); }
    else if (first == 0) { return false; }
    second = read(socketfd, message, size); //Reveive message
    if(second == -1) { std::string error = "recv (" + std::to_string(socketfd) + ") "; perror(error.c_str()); exit(1); }
    else if (second == 0) { return false; }
    if(message) {
        if(strlen(message) > size) {
            message[strlen(message) - (strlen(message) - size)] = '\0';
        }
    }
    return true;
}

bool receiving(int socketfd, std::string* message) {
    size_t size;
    ssize_t first, second;
    char buffer[MAXBUFSIZE];
    first = read(socketfd, &size, sizeof(size)); //Receive size
    if(first == -1) { std::string error = "recv (" + std::to_string(socketfd) + ") "; perror(error.c_str()); exit(1); }
    else if (first == 0) { return false; }
    second = read(socketfd, buffer, size); //Receive message
    if(second == -1) { std::string error = "recv (" + std::to_string(socketfd) + ") "; perror(error.c_str()); exit(1); }
    else if (second == 0) { return false; }
    *message = buffer;
    if(!message->empty()) {
        if(message->length() > size) {
            *message = message->substr(0, size);
        }
    }
    return true;
}

bool receiving(int socketfd, int* number) {
    ssize_t first;
    first = read(socketfd, number, sizeof(int)); //Receive number
    if(first == -1) { std::string error = "recv (" + std::to_string(socketfd) + ") "; perror(error.c_str()); exit(1); }
    else if (first == 0) { return false; }
    return true;
}

void closeSocket(int socketfd)
{
    close(socketfd);
}
