// INFO-F209 - Groupe 5 - Player.hpp

#include "const.hpp"

#ifndef PLAYER_HPP
#define PLAYER_HPP

class Player {
	private:
		bool m_color;
		bool winner = false;
		std::string m_username;
		int m_socket = 0;
		int _chatSocket = 0;
		Player* inChat = nullptr;
		unsigned elo = 0;
		std::vector<std::string> friendsUsernames;
		std::vector<std::string> requestFriend;
	public:
		Player();
		explicit Player(int);
		Player(int, int);
		Player(std::string, bool, int);
		Player(bool, std::string);
		~Player();
			//Attributes setters & getters
		void setColor(bool);
		bool getColor() const;
		void setUsername(std::string);
		std::string getUsername() const;
		void setSocket(int);
		int getSocket() const;
		bool isWinner() const;
		void hasWon();
		unsigned getELO() const;
		void setELO(unsigned);
		void resetGamePlayer();
			//Friends
		std::vector<std::string>& getFriendsUsernames();
		void pushInFriendsUsernames(char*);
		void delFriend(char*);
		std::vector<std::string>& getRequestFriend();
		void pushRequestFriend(std::string);
		void delRequestFriend(char*);
			//Chat
		Player* getInChat();
		void setInChat(Player*);
		int getChatSocket() const;
		void setChatSocket(int);
};

#endif
