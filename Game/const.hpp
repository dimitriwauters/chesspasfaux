// INFO-F209 - Groupe 5 - const.hpp
// Contain the constant values of the program and the libraries often used.

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

////////////////////////////////////////////////////////////////////////////////
// GENERAL
////////////////////////////////////////////////////////////////////////////////

#define GAME_NAME "Ch'esspasfaux"
#define GAME_NAME_MAJ "CH'ESSPASFAUX"
#define DASHES "-----"
#define YES "oui"
#define NO "non"
#define YES_NO " [" YES " / " NO "]"
#define IS " est "
#define AND " et "
#define BY " par "
#define YOU_HAVE "Vous avez "
#define YOU_PLAY "Vous jouez les "
#define WHITE_P "BLANCS"
#define BLACK_P "NOIRS"
#define SEND "Envoyer"
#define QUIT_LOWERCASE "quitter"
#define QUIT "Quitter"
#define CANCEL "Annuler"
#define VALIDATE "Valider"
#define COME_BACK "Revenir"
#define FATAL_ERROR "ERREUR FATALE !"

// Errors
#define ERROR_TRY_AGAIN " Veuillez réessayer."
#define ERROR_INPUT "Saisie incorrecte." ERROR_TRY_AGAIN
#define ERROR_TITLE "Une erreur s'est produite" // GUI only
#define PLAYER_NOT_FOUND "Cet utilisateur est introuvable." ERROR_TRY_AGAIN

////////////////////////////////////////////////////////////////////////////////
// NETWORK, CLIENT-SERVER & DATABASE
////////////////////////////////////////////////////////////////////////////////

#define PORT_GAME 5555
#define PORT_CHAT 5556
#define BACKLOG 20
#define MAXBUFSIZE 1024
#define MSG_SEPARATOR "$"

// Server
#define SERVER_SHUTDOWN "Shutdown du serveur en cours..."
#define SERVER_RECEIVED_INFO "Information reçue : " // Information
#define SERVER_SUBSCRIPTION " s'est inscrit(e)."
#define SERVER_CONNECTION " est connecté(e)."
#define SERVER_DECONNECTION " s'est déconnecté(e)."
#define SERVER_JOIN_CHAT " a joint le chat."
#define SERVER_LEAVE_CHAT " a quitté le chat."
#define SERVER_LAUNCH_GAME "Lancement d'une partie avec "
#define SERVER_DEFAULT_GAMEMODE "default" // Game
#define SERVER_DARK_GAMEMODE "dark"
#define SERVER_DICE_GAMEMODE "dice"
#define SERVER_HORDE_GAMEMODE "horde"
#define SERVER_DEFAULT_MODE "normal"
#define SERVER_REALTIME_MODE "realtime"

// Client
#define CLIENT_SHUTDOWN "Shutdown du client en cours..."

// Database
#define DB_NAME "chesspasfaux.db"
#define DB_OPENING_SUCCESS "Ouverture de la base de données réalisée avec succès !"
#define DB_OPENING_ERROR "Erreur d'ouverture de la base de données"
#define DB_CREATING_TABLE_ERROR "Erreur de création de table"
#define DB_CREATING_TABLE_SUCCESS "Table créée avec succès"
#define DB_TABLE_PLAYER "Players"
#define DB_PSEUDO_FIELD "pseudo"
#define DB_ID_FIELD "id"
#define DB_ELO_FIELD "elo"
#define DB_PASSWORD_FIELD "password"
#define DB_GAMES_WON_FIELD "games_won"
#define DB_GAMES_LOST_FIELD "games_lost"
#define DB_GAMES_DRAWN_FIELD "games_drawn"

////////////////////////////////////////////////////////////////////////////////
// MENUS
////////////////////////////////////////////////////////////////////////////////

#define MENU_GAME_NAME DASHES " " GAME_NAME_MAJ " " DASHES "\n"
#define MENU_SELECT_NUMBER "Veuillez entrer un nombre parmi ceux proposés :\n"
#define MENU_WRITE_QUIT "Tapez 'quitter' afin de revenir au menu\n"
#define MENU_PRESS_ENTER "Pressez ENTER pour continuer..."

// First window
#define FIRST_WELCOME "Bienvenue sur " GAME_NAME " !"
#define FIRST_DESCRIPTION "» Jouer à différents modes de jeu d'échecs parmi \
Horde Chess, Dice Chess et Dark Chess\n \
» Préenregistrer ses mouvements\n \
» Possibilité de jouer sans tour (temps réel) au jeu d'échecs\n \
» Trouver des amis\n \
» Comparer son niveau à celui des autres grâce au classement ELO\n \
» Discuter avec ses amis, y compris en jeu\n \
» Reprendre une partie non achevée\n \
» ... et tout cela, dans l'esprit de la série télévisée Kaamelott !\n\n"
#define FIRST_SIGN_UP "Créer un compte"
#define FIRST_SIGN_IN "Se connecter"

// Sign up
#define SIGN_UP "S'inscrire"
#define SIGN_UP_TITLE "Inscription :"
#define SIGN_UP_PSEUDO "Pseudonyme"
#define SIGN_UP_PASSWORD "Mot de passe"
#define SIGN_UP_PASSWORD2 "Veuillez répéter le mot de passe"
#define SIGN_UP_SUCCESS_TITLE "Compte créé avec succès" // GUI only
#define SIGN_UP_SUCCESS "Votre compte a été créé avec succès ! Vous pouvez désormais vous connecter."
#define SIGN_UP_PSEUDO_ERROR "Ce pseudonyme est déjà pris. Veuillez en choisir un autre."
#define SIGN_UP_PASSWORD_ERROR "Les saisies de mots de passe ne correspondent pas." ERROR_TRY_AGAIN

// Login/Sign in
#define SIGN_IN "Se connecter"
#define SIGN_IN_TITLE "Connexion :"
#define SIGN_IN_PSEUDO "Pseudonyme"
#define SIGN_IN_PASSWORD "Mot de passe"
#define SIGN_IN_SUCCESS "Vous êtes maintenant connecté(e)."
#define SIGN_IN_ERROR "Pseudonyme et/ou mot de passe incorrect(s)." ERROR_TRY_AGAIN
#define SIGN_IN_ALREADY_CONNECTED "Ce compte est déjà connecté."

// Main menu
#define MAIN_NEW_GAME "Lancer une partie"
#define MAIN_SAVED_GAME "Reprendre une partie"
#define MAIN_STATS "Consulter les statistiques"
#define MAIN_FRIENDS "Gérer sa liste d'amis"
#define MAIN_CHAT "Ouvrir le chat"
#define MAIN_HELP "Aide"
#define MAIN_CREDITS "Crédits"

////////////////////////////////////////////////////////////////////////////////
// LAUNCH A GAME
////////////////////////////////////////////////////////////////////////////////

// Gamemodes
#define DEFAULT_GAMEMODE 0
#define HORDE_GAMEMODE 1
#define DARK_GAMEMODE 2
#define DICE_GAMEMODE 3
#define DEFAULT_GAMEMODE_STR "Défaut"
#define HORDE_GAMEMODE_STR "Horde Chess"
#define DICE_GAMEMODE_STR "Dice Chess"
#define DARK_GAMEMODE_STR "Dark Chess"
#define DEFAULT_GAMEMODE_DESCR "Jouez aux échecs traditionnels dans l'ambiance de Kaamelott."
#define HORDE_GAMEMODE_DESCR "Une horde de 32 pions contre un set classique. \
Est-ce qu'une fois encore le nombre \nfera la différence ?"
#define DICE_GAMEMODE_DESCR "Seules certaines pièces peuvent être déplacées\
 lors de votre tour... Saurez-vous aligner \nchance et stratégie ?"
#define DARK_GAMEMODE_DESCR "Impossible de voir les pièces de votre adversaire \
 avant de pouvoir les prendre... Cette \npartie s'annonce aussi sombre que surprenante."

// Modes
#define REALTIME_OFF 0 // Default
#define REALTIME_ON 1
#define REALTIME_OFF_STR "Défaut"
#define REALTIME_ON_STR "Temps réel"
#define REALTIME_OFF_DESCR "Jeu de mode classique."
#define REALTIME_ON_DESCR "Ici, pas de tour. Soyez ingénieux le plus rapidemment possible !"

////////////////////////////////////////////////////////////////////////////////
// MATCHMAKING
////////////////////////////////////////////////////////////////////////////////

#define MATCHMAKING_WAITING "En attente d'un adversaire, veuillez patienter..."
#define PAUSED_MATCHMAKING_WAITING "En attente de "

#define QUOTES_NBR 103

////////////////////////////////////////////////////////////////////////////////
// IN GAME
////////////////////////////////////////////////////////////////////////////////

// General variables
#define MAX_RANGE 8
#define NBR_TYPES_PIECES 6
#define MAX_ROWS 8
#define MAX_COLS 8
#define MAX_TURNS 20
#define ALL_CELLS MAX_ROWS*MAX_COLS
#define NO_DEFINED 999
#define PIECE_HIDDEN 998
#define BLACK 0
#define WHITE 1

// Pieces
#define ROOK_NBR 0
#define KNIGHT_NBR 1
#define BISHOP_NBR 2
#define KING_NBR 3
#define QUEEN_NBR 4
#define PAWN_NBR 5
#define NO_PIECE "NULL"
#define BISHOP "Fou"
#define KING "Roi"
#define KNIGHT " Cavalier"
#define PAWN "Pion"
#define QUEEN "Reine"
#define ROOK "Tour"
#define A_BISHOP "un fou"
#define YOUR_KING " votre roi"
#define A_KNIGHT "un cavalier"
#define A_PAWN "un pion"
#define YOUR_QUEEN "votre reine"
#define A_ROOK "une tour"

// Moves directions
#define HORIZONTAL 1
#define VERTICAL 2
#define DIAGONAL 3

// Check moves
#define MOVE_NOT_ALLOWED 0
#define MOVE_ALLOWED 1
#define MOVE_EN_PASSANT 2
#define MOVE_CASTLING_LONG 3
#define MOVE_CASTLING_SHORT 4
#define MOVE_PROMOTION 5

// Columns
#define COL_A "A"
#define COL_B "B"
#define COL_C "C"
#define COL_D "D"
#define COL_E "E"
#define COL_F "F"
#define COL_G "G"
#define COL_H "H"

// Timer
#define TIMER_BLITZ 600.0
#define TIMER_NORMAL 5400.0

// Gamemodes specific variables
#define HORDE_FIRST_ROW 0
#define HORDE_LAST_ROW 4
#define HORDE_NB_PAWNS MAX_RANGE*HORDE_LAST_ROW
#define DARK_SQUARE "?"

// Modes specific variables
#define REALTIME_BLOCKEDTIME 4 // (s)
#define REALTIME_DELAYTIME 1000 // (ms)
#define REALTIME_CANT_SAVE "Vous ne pouvez pas sauvegarder ce type de partie !"
#define REALTIME_WAITING "En attente du choix de l'adversaire..."

// Buttons (GUI only)
#define OPEN_CHAT "Ouvrir le chat"
#define SAVE_GAME "Sauvegarder"
#define RESIGN "Abandonner"
#define GAME_AGAINST "Adversaire :\n"

// Terminal
#define TERMINAL_PLAY_VS "Vous jouez contre " // General
#define TERMINAL_MOVE_FROM " de "
#define TERMINAL_MOVE_TO " vers "
#define TERMINAL_WRITE ">>> "
#define TERMINAL_CONTINUE "Ok, continuons."
#define TERMINAL_EXIT "Cliquez sur la console pour quitter le jeu"
#define TERMINAL_DRAWN_MATCH "Match nul !"
#define TERMINAL_GAME_OVER DASHES " GAME OVER " DASHES
#define TERMINAL_WINNER "Le gagnant est "
#define TERMINAL_CONGRATULATIONS ", bravo !"
#define TERMINAL_ASK_ENDGAME "Rien de neuf depuis longtemps ici... Match nul ?" YES_NO
#define TERMINAL_ASK_QUIT "Quitter la partie (vous allez perdre) ?" YES_NO
#define TERMINAL_QUIT "quitter"
#define TERMINAL_QUIT_NULL "quit_null"
#define TERMINAL_ASK_SAVE "Continuer la partie plus tard ?" YES_NO
#define TERMINAL_SAVE "sauvegarder"
#define TERMINAL_SAVE_OTHER "sauvegarder_autre"
#define TERMINAL_SAVE_SAVED "Jeu mis en pause ! Vous pourrez le poursuivre plus tard."
#define TERMINAL_SAVE_NOT_SAVED "Votre adversaire ne souhaite pas mettre le jeu en pause.\nContinuez ou quittez le jeu."
#define TERMINAL_PROMOTION "Promotion !" // Special moves
#define TERMINAL_PROMOTION_CHOICE "Choisissez la " QUEEN ", la " ROOK ", le " KNIGHT " ou le " BISHOP"."
#define TERMINAL_PROMOTION_WRITE_CHOICE "Tapez votre choix ci-dessous parmi : '" QUEEN "', '" ROOK "', '" KNIGHT "', '" BISHOP "'.\nAttention, sensible a la casse !"
#define TERMINAL_PROMOTION_ASK_QUEEN "Choisir la " QUEEN " ?" YES_NO
#define TERMINAL_PROMOTION_ASK_KNIGHT "Choisir le " KNIGHT " ?" YES_NO
#define TERMINAL_PROMOTION_ASK_ROOK "Choisir la " ROOK " ?" YES_NO
#define TERMINAL_PROMOTION_ASK_BISHOP "Choisir le " BISHOP " ?" YES_NO
#define TERMINAL_CHOOSE_PIECE "Ok, que souhaitez-vous choisir ?"
#define TERMINAL_PREMOVE_ADDED "Premove retenu, vous pouvez en jouer " // Premoves
#define TERMINAL_PREMOVE_MORE " de plus"
#define TERMINAL_PREMOVE_NO_MORE "Vous ne pouvez plus ajouter de premoves !"
#define TERMINAL_PREMOVE_DO "Premove fait !"
#define TERMINAL_PREMOVE_NOT_ALLOWED "Premove interdit ! Vos premoves ne sont plus retenus."
#define TERMINAL_DICECHESS_ROLL "Lancement des des" // Gamemodes
#define TERMINAL_DICECHESS_RESULT "Vous obtenez : "
#define TERMINAL_DICECHESS_ALLOWED_MOVES "Vous pouvez jouer avec "
#define TERMINAL_DICECHESS_DOUBLES "Vous pouvez jouer avec tout, veinard !"
#define TERMINAL_DICECHESS_PASSTURN "Aucun mouvement possible : vous passez votre tour."
#define TERMINAL_DICECHESS_REROLL "Aucun mouvement possible : on relance les des !"

////////////////////////////////////////////////////////////////////////////////
// SAVED GAMES
////////////////////////////////////////////////////////////////////////////////

#define SAVED_GAMES_NB " partie(s) en pause."
#define SAVED_GAMES_SELECT "Veuillez sélectionner une partie :"
#define SAVED_GAMES_LAUNCH "Voulez-vous continuer une partie ?\n \
Si oui, entrez le numéro correspondant à la partie.\n \
Sinon, pressez 0 pour retourner au menu."

////////////////////////////////////////////////////////////////////////////////
// STATS
////////////////////////////////////////////////////////////////////////////////

#define STATS_PERSO "Votre score ELO"
#define STATS_FRIENDS "Classement de vos amis"
#define STATS_GENERAL "Classement général"
#define STATS_ELO_OF "Le classement ELO de " // console only
#define STATS_GIVE_USERNAME "Veuillez saisir le nom du joueur :" // console only
#define STATS_TABLE_NBR "n°" // GUI only
#define STATS_TABLE_NAME "Nom" // GUI only
#define STATS_TABLE_SCORE "Score" // GUI only

////////////////////////////////////////////////////////////////////////////////
// FRIENDS
////////////////////////////////////////////////////////////////////////////////

// Add a friend
#define FRIENDS_ADD "Ajouter un(e) ami(e)"
#define FRIENDS_ADD_USERNAME "Veuillez entrer le nom de l'ami(e) à ajouter :"
#define FRIENDS_ADD_SUCCESS_TITLE " ajouté(e) avec succès" // GUI only
#define FRIENDS_ADD_SUCCESS " a été ajouté(e) avec succès à votre liste d'amis."
#define FRIENDS_ADDED "Ami(e) ajouté(e)"

// Remove a friend
#define FRIENDS_REMOVE "Supprimer un(e) ami(e)"
#define FRIENDS_REMOVE_USERNAME "Veuillez entrer le nom de l'ami(e) à supprimer :"
#define FRIENDS_REMOVE_SUCCESS_TITLE " supprimé(e)" // GUI only
#define FRIENDS_REMOVE_SUCCESS " a été supprimé(e) de votre liste d'amis."
#define FRIENDS_SELECT_REMOVE "Veuillez au préalable sélectionner un ami à supprimer."
#define FRIENDS_REMOVED "Ami(e) supprimé(e)"

// Friends list
#define FRIENDS_SHOW_FRIENDS_LIST "Consulter sa liste d'amis" // console only
#define FRIENDS_FRIENDS_LIST_TITLE "Liste d'amis"
#define FRIENDS_EMPTY_FRIENDS_LIST "Votre liste d'amis est vide."
#define FRIENDS_STATUS_FRIENDS "1"

// Requests
#define FRIENDS_SHOW_REQUESTS "Voir les requêtes" // console only
#define FRIENDS_REQUESTS_LIST_TITLE "Requêtes"
#define FRIENDS_EMPTY_REQUESTS_LIST "Aucune demande d'ami."
#define FRIENDS_SENT_REQUEST_TITLE "Demande d'ami envoyée"
#define FRIENDS_SENT_REQUEST "La demande d'ami a bien été envoyée à "
#define FRIENDS_REQUEST_REMOVED_TITLE "Requête supprimée" // GUI only
#define FRIENDS_REQUEST_REMOVED "La requête a bien été supprimée."
#define FRIENDS_REQUEST_V "V" // GUI only
#define FRIENDS_REQUEST_X "X" // GUI only
#define FRIENDS_STATUS_REQUEST "0"

////////////////////////////////////////////////////////////////////////////////
// CHAT
////////////////////////////////////////////////////////////////////////////////

#define CHAT_START_CHAT "Discussion avec "
#define CHAT_RECEIVER_AVAILABLE "true"
#define CHAT_RECEIVER_BUSY "in_chat"
#define CHAT_RECEIVER_DISCONNECTED "disconnected"

// Console
#define WHO_IS_CONNECTED 1
#define SEND_MSG 2
#define CHAT_COMMANDS "Sélectionnez :\n\n1. Pour savoir qui est connecté(e).\n2. \
Pour envoyer un message.\nCtrl+C ou le bouton X pour fermer la fenêtre.\n"
#define CHAT_LOGIN "Connexion..."
#define CHAT_CONNECTED_AS "Connecté(e) en tant que "
#define CHAT_RECEIVER_USERNAME "Écrivez le nom du destinataire :"
#define CHAT_ALREADY_IN_CHAT "Désolé, cet utilisateur parle déjà à quelqu'un"
#define CHAT_ERROR "Erreur : cet utilisateur est soit introuvable, soit n'est pas \
connecté au chat."
#define CHAT_NOBODY_ONLINE "Personne n'est en ligne, désolé !"
#define CHAT_DISCONNECTED " est déconnecté(e)."

// GUI
#define CHAT_CONNECTED_USERS "Amis en ligne"
#define CHAT_CLICK "Cliquez sur un ami"

////////////////////////////////////////////////////////////////////////////////
// HELP
////////////////////////////////////////////////////////////////////////////////

#define HELP_CHESS_RULES "Règles du jeu d'échecs"
#define HELP_GAMEMODES "Modes de jeu"
#define HELP_CONTINUE "================================\nPoursuivre la lecture (pressez n'importe quelle touche)"
#define HELP_CHESS_RULES_1 "================================\n\
Jeu d'échecs\n\
================================\n\
Le jeu d’échecs traditionnel est un jeu de plateau en tour par tour opposant deux joueurs. Chaque \
joueur possède 16 pièces disposées sur un plateau de 64 cases appelé échiquier. L’un possède des pièces \
blanches, et commence toujours la partie, l’autre possède des pièces noires. Il existe 6 types de pièces, \
caractérisées par un déplacement spécifique : le roi (1 pièce par joueur), la reine (1 pièce par joueur), \
le fou (2 pièces par joueur), le cavalier (2 pièces par joueur), la tour (2 pièces par joueur) et le pion (8 \
pièces par joueur).\n\
Lors de chaque tour, un joueur (sélectionné alternativement) doit déplacer une de ses pièces sur \
l’échiquier. Si la pièce déplacée par le joueur se trouve sur la même case qu’une pièce du joueur \
adverse, alors celle-ci est capturée et est retirée de l’échiquier. Le but du jeu est de capturer le roi de \
l’adversaire sans que son propre roi ne soit menacé et sans issue possible (c’est-à-dire en échec et mat). \
Pour cela, le jeu se déroule en tour par tour jusqu’à ce qu’il y ait un gagnant ou que la partie soit \
nulle, un tour de jeu correspondant au déplacement d’une seule des pièces restantes au joueur."
#define HELP_CHESS_RULES_2 "================================\n\
Règles du jeu d'échecs\n================================\n\
Échiquier\n================================\n\
Un échiquier ou plateau de jeu est constitué de 8 colonnes, nommées de a à h, ainsi que de 8 lignes, \
numérotées de 1 jusque 8. Chaque case alterne une couleur claire et une couleur foncée. De facto, les \
diagonales sont toutes soit complètement claires, soit complètement foncées.\
\n================================\nPièces\n\
================================\n\
Les pièces du joueur blanc sont placées respectivement de gauche à droite sur la première ligne de \
l’échiquier : Une tour, un cavalier, un fou, sa reine, son roi, un fou, un cavalier et une tour. Sur la \
deuxième ligne de l’échiquier sont placés les 8 pions. Du côté du joueur noir, ses pièces sont placées de la \
même manière que son adversaire à la seule différence que le roi et la reine ont leurs places échangées. \
Sur l’échiquier les deux rois se trouvent donc sur la même colonne «e » et la colonne «d» pour les \
reines"
#define HELP_CHESS_RULES_3 "================================\nTypes\n================================\n\
Ce qui différencie les pièces les unes des autres est leur faculté à avoir chacune un déplacement qui \
leur est propre. Nous les définirons donc par les règles de déplacement qui les caractérisent.\
\n      Pion\n\
Le pion ne peut avancer que vers l’avant, d’une case à la fois. À l’exception de son premier mouvement, \
où il peut être déplacé soit d’une case, soit de deux. Voir règle « prise en passant ». Contrairement \
à toutes les autres pièces, le pion ne prend pas une pièce dans le sens de son déplacement. En effet, \
quand le pion prend une pièce adverse, il le fait en diagonale, et non en ligne droite.\
\n      Tour\n\
La tour ne bouge qu’en suivant les lignes et colonnes du plateau. Elle peut être déplacée d’autant \
de cases que le joueur souhaite.\
\n      Cavalier\n\
Le cavalier possède le déplacement le plus particulier, c’est le seul qui puisse « sauter » au dessus \
des autres pièces. Il peut se déplacer de 3 cases mais uniquement en forme de « L » : de deux cases \
dans une direction verticale ou horizontale, ensuite d’une seule case dans l’autre.\
\n      Fou\n\
Le fou se meut uniquement en diagonale, il reste donc toujours sur la même couleur de case du \
plateau. À l’instar de la tour, il peut être avancé d’autant de cases que l’on veut."
#define HELP_CHESS_RULES_4 "================================\
\n      Reine\n\
La reine cumule les facultés de déplacement de la tour et du fou. Elle peut se déplacer d’autant de \
cases souhaitées aussi bien en diagonale qu’en ligne droite.\
\n      Roi\n\
Le roi ne peut se déplacer que d’une case à la fois, et ce dans tous les sens. Il possède néanmoins un \
déplacement spécial, nommé le roque.\n\
================================\nCoups spéciaux\n================================\n\
Le roque\n================================\n\
Le roque consiste à faire avancer exceptionnellement le roi de deux cases vers la tour et de passer \
cette dernière de l’autre côté du roi.\
Il existe deux roques différents : le petit roque et le grand roque. Le petit roque se fait du côté de la \
tour la plus proche et le grand, du côté de la tour la plus éloignée du roi.\
Mais on ne peut pas roquer dans 4 conditions :\
si le roi ou la tour a déjà bougé :\n\
- s’il y a une pièce sur la case d’arrivée du roi ou de la tour ;\n\
- s’il y a une pièce sur une case de passage du roi ou de la tour ;\n\
- si le roi passe ou arrive sur une case contrôlée par une pièce adverse."
#define HELP_CHESS_RULES_5 "\
================================\nLa promotion\n================================\
\nLa promotion désigne un échange d’un pion contre, au choix, une dame, une tour, un cavalier ou un \
fou. Pour cela, il faut amener le pion sur la rangée no8 pour les blancs et sur la rangée no1 pour les \
noirs.\
\n================================\nLa prise en passant\n================================\n\
La prise en passant est un coup de pion. Quand un pion est sur la 5e rangée pour les blancs ou sur \
la 4e rangée pour les noirs, et qu’un pion adverse n’ayant pas encore bougé avance de deux cases pour \
se mettre à côté du pion allié, il peut exécuter une prise en passant : il peut prendre le pion adverse \
comme s’il n’avait avancé que d’une case.\
Attention : Un pion ne peut faire la prise qu’en passant immédiatement après l’avancée de deux \
cases du pion adverse. S’il attend un tour, il ne pourra plus la faire."
#define HELP_CHESS_RULES_6 "================================\n\
Condition de victoire\n================================\n\
Un joueur gagne la partie s’il capture le roi de son adversaire. Tout d’abord, un joueur est en « échec \
» si son roi est menacé d’être pris par une pièce adverse. Le joueur en échec est obligé de bouger son \
roi ou l’une de ses autres pièces afin d’empêcher son roi d’être pris. Si le joueur ne peut pas empêcher \
son roi d’être pris, on dit alors qu’il est « échec et mat » et son adversaire gagne la partie. Un joueur \
peut également gagner la partie si son adversaire abandonne.\
\n================================\nPartie nulle\n================================\n\
Dans plusieurs cas une partie peut être nulle et aucun des deux joueurs ne gagne la partie :\n\
- Si l’un des deux joueurs ne peut faire de déplacement valide, on dit alors qu’il y a « pat »;\n\
- S’il est impossible de faire un échec et mat à l’un ou l’autre des joueurs (par manque de pièces);\n\
- S’il y a répétition de coups potentiellement infinie et qu’aucun échec et mat n’est fait."
#define HELP_CHESS_RULES_7 "================================\n\
Premoves\n================================\n\
Lorsque ce n'est pas votre tour, il est possible d'enregistrer des mouvements à l'avance, appelés \
« premoves ». Le coup sera joué automatiquement dès que votre tour commence et ainsi vous gagnerez du temps.\
Vous pouvez enregistrer jusqu'à 5 premoves qui seront joué dans l'ordre enregistré.\
Cependant, un premove ne sera pas effectué si le coup enregistré n'est plus valide, et tous les premoves \
enregistrés seront annulés."

#define HELP_GAMEMODES_1 "================================\n\
Modes de jeu\n================================\n\
Le programme offrira la possibilité de jouer à 4 types de jeu d’échecs différents : le jeu d’échecs \
traditionnel, Dark Chess, Dice Chess et Horde Chess. Ces trois derniers types de jeu se basent en grande\
partie sur le fonctionnement du jeu d’échecs traditionnel, que nous allons rappeler ici brièvement."
#define HELP_GAMEMODES_2 "================================\n\
Jeu d'échecs traditionnel\n================================\n\
Le jeu d’échecs traditionnel est un jeu de plateau en tour par tour opposant deux joueurs. Chaque\
joueur possède 16 pièces disposées sur un plateau de 64 cases appelé échiquier. L’un possède des pièces\
blanches, et commence toujours la partie, l’autre possède des pièces noires. Il existe 6 types de pièces,\
caractérisées par un déplacement spécifique : le roi (1 pièce par joueur), la reine (1 pièce par joueur),\
le fou (2 pièces par joueur), le cavalier (2 pièces par joueur), la tour (2 pièces par joueur) et le pion (8 pièces par joueur).\
Lors de chaque tour, un joueur (sélectionné alternativement) doit déplacer une de ses pièces sur\
l’échiquier. Si la pièce déplacée par le joueur se trouve sur la même case qu’une pièce du joueur\
adverse, alors celle-ci est capturée et est retirée de l’échiquier. Le but du jeu est de capturer le roi de\
l’adversaire sans que son propre roi ne soit menacé et sans issue possible (c’est-à-dire en échec et mat).\
Pour cela, le jeu se déroule en tour par tour jusqu’à ce qu’il y ait un gagnant ou que la partie soit\
nulle, un tour de jeu correspondant au déplacement d’une seule des pièces restantes au joueur.\
Une description plus détaillée du jeu d’échecs et de ses règles se trouve dans la rubrique éponyme."
#define HELP_GAMEMODES_3 "================================\n\
Horde Chess\n================================\n\
Dans ce type de jeu, la distribution et le positionnement des pièces diffèrent : le joueur noir commence\
la partie avec 32 pions tandis que le joueur blanc possède un ensemble « classique » de pièces (1 roi, 1 reine,\
2 cavaliers, etc.). Le but du joueur blanc est de capturer l’ensemble des pions adverses, l’objectif\
du joueur noir étant tout simplement de mettre en échec et mat son adversaire.\
Le positionnement des pièces du joueur ayant le set habituel est le même que celui du mode traditionnel. \
Les 32 pions adverses sont répartis sur les 4 lignes opposées à l’exception des pions des 2\
colonnes « d » et « e » qui sont avancés d’une case.\
Hormis ces particularités, Horde Chess se joue comme un jeu d’échecs traditionnel : les règles normales s’appliquent."
#define HELP_GAMEMODES_4 "================================\n\
Dark Chess\n================================\n\
Dans la variante de jeu nommée Dark Chess, chaque joueur ne peut voir que les cases, et donc les\
pièces qui s’y trouvent, qui sont à portée des siennes. Cela signifie que seules les cases où ses pièces\
peuvent se déplacer lui sont connues. Le joueur n’a donc qu’une vue partielle de l’échiquier et du\
positionnement des pièces adverses. De facto, les règles quant à la mise en échec ne s’appliquent pas :\
la capture du roi est possible, et c’est celle-ci qui conditionne la victoire de la partie.\
Hormis ces deux particularités, Dark Chess se joue comme un jeu d’échecs traditionnel : les règles\
normales s’appliquent."
#define HELP_GAMEMODES_5 "================================\n\
Dice Chess\n================================\n\
Dice Chess se joue à l’aide de dés. Aux 6 faces du dé correspondent les 6 types de pièces :\n\
  1       2        3      4      5      6\n\
Pion   Cavalier   Fou   Tour   Reine   Roi\n\
Au début de son tour, le joueur lance deux dés. Le résultat des dés indique quelles pièces (et\
uniquement celles-là) peuvent être déplacées durant le tour : le joueur ne peut déplacer une autre\
pièce que celles indiquées par le résultat du lancer de dés. Si les deux dés tombent sur la même valeur,\
alors n’importe quelle pièce peut être déplacée. Si aucun coup valide ne peut être joué, le joueur doit\
passer son tour.\
Hormis cette particularité, Dice Chess se joue comme un jeu d’échecs traditionnel : les règles normales\
s’appliquent."

////////////////////////////////////////////////////////////////////////////////
// CREDITS
////////////////////////////////////////////////////////////////////////////////

#define CREDITS "Crédits"
