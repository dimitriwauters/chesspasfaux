// INFO-F209 - Groupe 5 - Game.hpp

#ifndef ABSTRACTGAME_HPP
#define ABSTRACTGAME_HPP

#include "const.hpp"
#include "Player.hpp"

template <typename B>
class AbstractGame
{
protected:
    B* board;
    Player* player[2];
    Player* my_player;
    Player* other_player;
    int turn = WHITE;
    bool running_game = true;
    int no_captured_pieces = 0;
    std::pair<int, int> king_position[2];
    bool check[2] = {false, false};
    std::pair<int, int> last_move;
    bool finished = false;
    std::vector<std::string> pieces_beginning;
    int gamemode;
    int type;

	AbstractGame();
    AbstractGame(Player*, Player*, int, int);
    virtual ~AbstractGame() = 0;
    virtual void createBoard() = 0;
    virtual void setPiecesOnBoard();
    virtual void run() = 0;
    virtual void finishGame() = 0;
    // Rules & win conditions
    void swapTurn();
    bool notOver();
    void gameFinished();
    void setWinner(bool);
    Player* getWinner();
    virtual void checkWinConditions(bool);
    // Moves
    virtual bool movePiece(std::pair<int, int>, std::pair<int, int>, bool);
    virtual unsigned checkMove(std::pair<int, int>, std::pair<int, int>, bool);
    unsigned isLegalMoveAllowed(std::pair<int, int>, std::pair<int, int>, bool);
    std::vector<std::pair<int, int>> getAllMoves(std::pair<int, int>, bool);
    std::vector<std::pair<int, int>> getAllMovesAllPieces(bool);
    virtual bool movePiece(std::pair<int, int>, std::pair<int, int>, unsigned);
    // Special moves
    virtual unsigned enPassant(std::pair<int, int>, std::pair<int, int>);
    void setLastMove(std::pair<int, int>);
    bool compareLastMove(std::pair<int, int>);
    virtual unsigned canCastle(std::pair<int, int>, std::pair<int, int>, bool);
    unsigned whichCastling(std::pair<int, int>, std::pair<int, int>);
    void setCastlingForRook(std::pair<int, int>, std::pair<int, int>);
    void promote(std::pair<int, int>, bool, int);
    // Capture
    virtual void capturePiece(std::pair<int, int>);
    bool canCapture(std::pair<int, int>, std::pair<int, int>);
    virtual bool isUnderAttack(std::pair<int, int>, bool);
    // Stalemate, check & checkmate
    virtual bool isInStalemate(bool);
    std::pair<int, int> getKingPosition(bool) const;
    void setKingPosition(bool, std::pair<int, int>);
    void setCheck(bool, bool);
    bool getCheck(bool) const;
    virtual bool willBeInCheck(std::pair<int, int>, std::pair<int, int>, bool);
    bool isInCheckMate(bool);
    // Conversions: tuple <-> str, int <-> str
	std::string getChessColumn(int);
	std::string getChessRank(int);
    std::string pairToString(std::pair<int, int>, std::pair<int, int>);
    std::pair<std::pair<int, int>, std::pair<int, int>> stringToPair(char*);
	//Override
    virtual void rearrangeBoard();
    virtual void rollTheDices();
    virtual std::pair<int, int> getDices();
    virtual void setDices(std::pair<int, int>);
public:
    bool canDeleteGame();
	std::pair<Player*, Player*> getPlayers();
};

#include "Game.cpp"

#endif
