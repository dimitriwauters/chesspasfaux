// INFO-F209 - Groupe 5 - Pieces.hpp

#include "const.hpp"

#ifndef ABSTRACTPIECE
#define ABSTRACTPIECE

class AbstractPiece
{
protected:
    std::pair <int, int> m_position;
    bool m_color;
    int m_type;
    bool m_moved;
    AbstractPiece();
    AbstractPiece(int, int, bool);
    AbstractPiece(int);
    AbstractPiece(int, int, bool, int);
public:
	virtual ~AbstractPiece() = 0;
	virtual std::string getType() const = 0;
	virtual bool checkMove(int, int) = 0;
    virtual bool checkSpecialMove() = 0;
    virtual std::vector<std::pair<int, int>> getAllMoves() = 0;
    void setColor(bool);
    bool getColor() const;
    void move(int, int);
	std::pair <int, int> getPosition() const;
    void firstMove();
    bool hasMoved();
    int getTypeNbr() const;
    void printPosition() const;
};

#endif

//////////////////////////////////////////////////////////////////
// ROOK
//////////////////////////////////////////////////////////////////

#ifndef ROOK_HPP
#define ROOK_HPP

class Rook : public AbstractPiece
{
public:
    Rook();
    Rook(int, int, bool);
    ~Rook();
    bool checkMove(int, int);
    bool checkSpecialMove();
    std::string getType() const override;
    std::vector<std::pair<int, int>> getAllMoves() override;
};

#endif

//////////////////////////////////////////////////////////////////
// KNIGHT
//////////////////////////////////////////////////////////////////

#ifndef KNIGHT_HPP
#define KNIGHT_HPP

class Knight : public AbstractPiece
{
public:
    Knight();
    Knight(int, int, bool);
    ~Knight();
    bool checkMove(int, int);
    bool checkSpecialMove();
    std::string getType() const override;
    std::vector<std::pair<int, int>> getAllMoves() override;
};

#endif

//////////////////////////////////////////////////////////////////
// BISHOP
//////////////////////////////////////////////////////////////////

#ifndef BISHOP_HPP
#define BISHOP_HPP

class Bishop : public AbstractPiece
{
public:
    Bishop();
    Bishop(int, int, bool);
    ~Bishop();
    bool checkMove(int, int);
    bool checkSpecialMove();
    std::string getType() const override;
    std::vector<std::pair<int, int>> getAllMoves() override;
};

#endif

//////////////////////////////////////////////////////////////////
// KING
//////////////////////////////////////////////////////////////////

#ifndef KING_HPP
#define KING_HPP

class King : public AbstractPiece
{
public:
    King();
    King(int, int, bool);
    ~King();
    bool checkMove(int, int);
    bool checkSpecialMove();
    std::string getType() const override;
    std::vector<std::pair<int, int>> getAllMoves() override;
};

#endif

//////////////////////////////////////////////////////////////////
// QUEEN
//////////////////////////////////////////////////////////////////

#ifndef QUEEN_HPP
#define QUEEN_HPP

class Queen : public AbstractPiece
{
public:
    Queen();
    Queen(int, int, bool);
    ~Queen();
    bool checkMove(int, int);
    bool checkSpecialMove();
    std::string getType() const override;
    std::vector<std::pair<int, int>> getAllMoves() override;
};

#endif

//////////////////////////////////////////////////////////////////
// PAWN
//////////////////////////////////////////////////////////////////

#ifndef PAWN_HPP
#define PAWN_HPP

class Pawn : public AbstractPiece
{
public:
    Pawn();
    Pawn(int, int, bool);
    ~Pawn();
    bool checkMove(int, int);
    bool whitePawnMove(int, int);
    bool blackPawnMove(int, int);
    bool checkSpecialMove();
    std::string getType() const override;
    std::vector<std::pair<int, int>> getAllMoves() override;
};

#endif
