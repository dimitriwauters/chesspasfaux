QT += widgets \
      gui \
      multimedia \
      core

SOURCES += \
    ClientGUI.cpp \
    ConnectionWindow.cpp \
    MenuWindow.cpp \
    LaunchGameWindow.cpp \
    SavedGamesWindow.cpp \
    StatsWindow.cpp \
    FriendsWindow.cpp \
    ChatWindow.cpp \
    HelpWindow.cpp \
    GameWindow.cpp \
    CreditsWindow.cpp \
    ChessSquare.cpp \
    ../Network.cpp \
    ../Player.cpp \
    ../Board.cpp \
    ../Pieces.cpp \
    TerminalGUI.cpp \
    TimerGUI.cpp \

HEADERS += \
    ClientGUI.hpp \
    ConnectionWindow.hpp \
    MenuWindow.hpp \
    LaunchGameWindow.hpp \
    SavedGamesWindow.hpp \
    StatsWindow.hpp \
    FriendsWindow.hpp \
    ChatWindow.hpp \
    HelpWindow.hpp \
    GameWindow.hpp \
    CreditsWindow.hpp \
    ChessSquare.hpp \
    ../Network.hpp \
    ../Player.hpp \
    ../Board.hpp \
    ../Pieces.hpp \
    TerminalGUI.hpp \
    TimerGUI.hpp \
