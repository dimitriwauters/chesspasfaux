// INFO-F209 - Groupe 5 - CreditsWindow.hpp

#include "MenuWindow.hpp"

#ifndef CREDITSWINDOW_HPP
#define CREDITSWINDOW_HPP

class CreditsWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QTextBrowser* text;
        QLabel *credits_title;
    public:
        CreditsWindow(Player*, QPoint);
    public slots:
        void comeBack() override;
};

#endif
