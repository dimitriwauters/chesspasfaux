// INFO-F209 - Groupe 5 - TerminalGUI.cpp

#include "TerminalGUI.hpp"

TerminalGUI::TerminalGUI(Player* me, Player* other, int server, QVBoxLayout *v_layout_left_sidepane): QWidget(), serverfd(server), me(me), other(other), layout_sidepane(v_layout_left_sidepane)
{
    QFontDatabase::addApplicationFont("resources/fonts/Cinzel.otf");
    menu_font = QFont("Cinzel",16,QFont::Normal);
    user_input = new SendBox(202, 70);
    user_input_content = TERMINAL_WRITE;
    user_input->setPlaceholderText(user_input_content);
    send_msg_btn = new QPushButton(SEND, this);
    send_msg_btn->setMinimumHeight(40);
    send_msg_btn->setFont(menu_font);
    send_msg_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    terminal_container = new QTextEdit(this);
    terminal_container->setReadOnly(true);
    terminal_container->setMaximumSize(202, 470);
    terminal_container->setStyleSheet("QScrollBar:vertical {"
                                    "background:transparent;"
                                    "width:10px;    "
                                    "margin: 0px 0px 0px 0px;"
                                    "}"
                                    "QScrollBar::handle:vertical {"
                                    "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                    "stop: 0 rgb(48,30,30), stop: 0.5 rgb(48,30,30), stop:1 rgb(48,30,30));"
                                    "min-height: 0px;"
                                    "}"
                                    "QScrollBar::add-line:vertical {"
                                    "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                    "stop: 0 rgb(48,30,30), stop: 0.5 rgb(48,30,30),  stop:1 rgb(48,30,30));"
                                    "height: 0px;"
                                    "subcontrol-position: bottom;"
                                    "subcontrol-origin: margin;"
                                    "}"
                                    "QScrollBar::sub-line:vertical {"
                                    "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                    "stop: 0  rgb(48,30,30), stop: 0.5 rgb(48,30,30),  stop:1 rgb(48,30,30));"
                                    "height: 0 px;"
                                    "subcontrol-position: top;"
                                    "subcontrol-origin: margin;"
                                    "}");
    //terminal_container->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    layout_sidepane->addWidget(terminal_container);
    layout_sidepane->addWidget(user_input);
    layout_sidepane->addWidget(send_msg_btn);
    QObject::connect(send_msg_btn, SIGNAL(clicked()), this, SLOT(writeTerminal()));
    QObject::connect(user_input, SIGNAL(enterPressed()), this, SLOT(writeTerminal()));
    QObject::connect(this, SIGNAL(writeSignal(QString)), this, SLOT(writeText(QString)));
}

TerminalGUI::~TerminalGUI() = default;

void TerminalGUI::setPrefixCommand(std::string command) {
    prefix_command = QString::fromStdString(command);
}

/*
unsigned TerminalGUI::getMaxLines() {
    unsigned result;
    if(terminal_text.size() > static_cast<unsigned>(size.second-4)) result = static_cast<unsigned>(size.second-4);
    else result = static_cast<unsigned>(terminal_text.size());
    return result;
}*/

void TerminalGUI::write(std::string text) {
    emit TerminalGUI::writeSignal(QString::fromStdString(text));
}

void TerminalGUI::writeTerminal()
{
    interpretCommand(user_input->toPlainText());
    user_input->clear();
}

void TerminalGUI::writeText(QString text) {
    terminal_container->clear();
    terminal_text.emplace_back(text);
	terminal_content = "";
	for (int i = static_cast<int>(terminal_text.size())-1; i >= 0; --i)
	{
		terminal_content = terminal_content + terminal_text[i] + "\n";
	}
	terminal_container->setText(terminal_content);
}

/////////////////////////////////////////////////////////////////
// COMMAND
/////////////////////////////////////////////////////////////////

bool TerminalGUI::interpretCommand(QString command) {
    bool result = false;
    if(command.length() > 0) {
        if (prefix_command.length() > 0) {
			doCommand(command);
			prefix_command = "";
		}
        else getCommand(command);
    }
    command = "";
    return result;
}

void TerminalGUI::doCommand(QString command) {
	if (prefix_command == TERMINAL_QUIT) {
		if (command == YES) signal = 9;
		else { write(TERMINAL_CONTINUE); signal = -1; }
	}
	else if (prefix_command == TERMINAL_QUIT_NULL) {
		if (command == YES) signal = 8;
		else { write(TERMINAL_CONTINUE); signal = -2; }
	}
	else if (prefix_command == TERMINAL_SAVE) {
		if (command == YES) signal = 14;
		else { write(TERMINAL_CONTINUE); signal = -1; }
	}
	else if (prefix_command == TERMINAL_SAVE_OTHER) {
		if (command == YES) signal = 15;
		else { write(TERMINAL_CONTINUE); signal = 16; }
	}
	else if (prefix_command == QUEEN) {
		if (command == YES) {signal = 10;}
		else { write(TERMINAL_CHOOSE_PIECE); signal = -3; }
	}
	else if (prefix_command == KNIGHT) {
		if (command == YES) signal = 11;
		else { write(TERMINAL_CHOOSE_PIECE); signal = -3; }
	}
	else if (prefix_command == ROOK) {
		if (command == YES) signal = 12;
		else { write(TERMINAL_CHOOSE_PIECE); signal = -3; }
	}
	else if (prefix_command == BISHOP) {
		if (command == YES) signal = 13;
		else { write(TERMINAL_CHOOSE_PIECE); signal = -3; }
	}
}

void TerminalGUI::getCommand(QString command) {
   if (command == TERMINAL_QUIT) {
		write(TERMINAL_ASK_QUIT);
		prefix_command = TERMINAL_QUIT;
	}
	else if (command == TERMINAL_SAVE) {
		write(TERMINAL_ASK_SAVE);
		prefix_command = TERMINAL_SAVE;
	}
	else if(command == QUEEN) {
		write(TERMINAL_PROMOTION_ASK_QUEEN);
		prefix_command = QUEEN;
	}
	else if(command == KNIGHT) {
		write(TERMINAL_PROMOTION_ASK_KNIGHT);
		prefix_command = KNIGHT;
	}
	else if(command == ROOK) {
		write(TERMINAL_PROMOTION_ASK_ROOK);
		prefix_command = ROOK;
	}
	else if(command == BISHOP) {
		write(TERMINAL_PROMOTION_ASK_BISHOP);
		prefix_command = BISHOP;
	}
	else {
		std::string message = "[" + me->getUsername() + "] : " + command.toStdString();
		write(message);
		sending_T(serverfd, prepForSending(330, message));
	}
}

int TerminalGUI::getSignal() {
    int status = signal;
    signal = 0;
    return status;
}
