#include "TimerGUI.hpp"

TimerGUI::TimerGUI(Player* white, Player* black, float mode, int* turn, QGridLayout* layout) : QWidget()
{
    timer_white = new QLCDNumber(this);
    timer_black = new QLCDNumber(this);
    timer_white->setMinimumSize(QSize(80, 50));
    timer_black->setMinimumSize(QSize(80, 50));
    timer_white->setMaximumSize(QSize(80, 50));
    timer_black->setMaximumSize(QSize(80, 50));
    timer_white->setFrameShape(QFrame::NoFrame);
    timer_black->setFrameShape(QFrame::NoFrame);
    white_block = new QLabel();
    white_block->setStyleSheet("background-color: white;");
    white_block->setMinimumSize(80, 10);
    white_block->setMaximumSize(80, 10);
    black_block = new QLabel();
    black_block->setStyleSheet("background-color: black;");
    black_block->setMinimumSize(80, 10);
    black_block->setMaximumSize(80, 10);
    layout->addWidget(timer_white, 1, 0, 1, 1, Qt::AlignCenter);
    layout->addWidget(timer_black, 1, 1, 1, 1, Qt::AlignCenter);
    layout->addWidget(white_block, 2, 0, 1, 1, Qt::AlignCenter);
    layout->addWidget(black_block, 2, 1, 1, 1, Qt::AlignCenter);
    time[WHITE] = mode;
    time[BLACK] = mode;
    players[WHITE] = white;
    players[BLACK] = black;
    game_turn = turn;
    std::thread t1(&TimerGUI::timeOut, this);
    t1.detach();
}

void TimerGUI::timeOut()
{
    while(!over) {
        while(*game_turn == color) { // Tant que le tour n'a pas changé
            usleep(100000); // 100 millisecondes
            time[color] -= 0.1;
            if(time[color] <= 0) players[!color]->hasWon();
        }
        time[color] += 5.0;
        color = !color;
    }
}

void TimerGUI::refreshTimer()
{
    std::stringstream second;
    std::stringstream minute;
    minute << std::setw(2) << std::setfill('0') << std::to_string(static_cast<int>(std::floor(time[WHITE]/60)));
    second << std::setw(2) << std::setfill('0') << std::to_string(static_cast<int>(time[WHITE]) % 60);
    std::string time_white = minute.str() + ":" + second.str();
    second.str(std::string());
    minute.str(std::string());
    minute << std::setw(2) << std::setfill('0') << std::to_string(static_cast<int>(std::floor(time[BLACK]/60)));
    second << std::setw(2) << std::setfill('0') << std::to_string(static_cast<int>(time[BLACK]) % 60);
    std::string time_black = minute.str() + ":" + second.str();

    timer_white->display(QString::fromStdString(time_white));
    timer_black->display(QString::fromStdString(time_black));
}

TimerGUI::~TimerGUI() = default;
