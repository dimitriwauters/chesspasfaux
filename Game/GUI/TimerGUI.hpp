#ifndef TIMERGUI_HPP
#define TIMERGUI_HPP

#include "../Player.hpp"
#include "../const.hpp"
#include "../Network.hpp"
#include "MenuWindow.hpp"
#include <sstream>
#include <iomanip>

class TimerGUI : public QWidget {
private:
    double time[2];
    Player* players[2];
    bool color = WHITE;
    int* game_turn;
    QLCDNumber* timer_white;
    QLCDNumber* timer_black;
    QLabel *white_block;
    QLabel *black_block;
    bool over = false;
public:
    TimerGUI(Player*, Player*, float, int*, QGridLayout*);
    ~TimerGUI();
    void timeOut();
    void refreshTimer();
};

#endif //TIMERGUI_HPP
