// INFO-F209 - Groupe 5 - GameWindow.hpp

#include "MenuWindow.hpp"
#include "BoardClientGUI.hpp"
#include "GameClientGUI.hpp"
#include "TerminalGUI.hpp"
#include "../const.hpp"

#ifndef GAMEWINDOW_HPP
#define GAMEWINDOW_HPP

class GameWindow : public Window
{
        Q_OBJECT
    private:
        QPushButton *quit_btn;
        QPushButton *open_chat_btn;
        QPushButton *save_game_btn;
        QHBoxLayout *h_layout;
        QVBoxLayout *v_layout_left_sidepane;
        QVBoxLayout *v_layout_game;
        QVBoxLayout *v_layout_right;
        QGridLayout *g_layout_infos_game;
        QGridLayout *g_layout_chessboard;
        QLabel *advertising;
        QLabel *my_color;
        QLabel *opponent_name;
        //QSpacerItem *v_spacer;
        //QSpacerItem *h_spacer;
        //std::vector<std::string> pieces;
    public:
        GameWindow(Player*, Player*, Player*, int, int, int, std::vector<std::string>, QPoint);
        void playStartSound();
        void mousePressEvent(QMouseEvent*) override;
        template <typename G>
        bool serverSwitch(int, int, std::string, G*, bool);
        template <typename G>
        void serverInfos(G*);
        void gamemodeChoice(unsigned, std::string, Player*, Player*, int, QGridLayout*, QVBoxLayout*, QGridLayout*, std::vector<std::string>);
public slots:
        void openChat();
        void resign();
        void saveGame();
        void comeBackMainMenu();
        void repaintGame();
signals:
        void gameEnded();
        void repaintGameSIG();
};

#endif

#ifndef ADVERTISING_HPP
#define ADVERTISING_HPP

class Advertising : public QLabel
{
    Q_OBJECT
protected:
    void enterEvent(QEvent *) override;
public:
    Advertising(QWidget*);
};

#endif
