// INFO-F209 - Groupe 5 - SavedGamesWindow.cpp

#include "SavedGamesWindow.hpp"
#include "LaunchGameWindow.hpp"

SavedGamesWindow::SavedGamesWindow(Player* player, QPoint current_pos) : MenuWindow(player)
{
    move(current_pos);
    setFixedSize(608, 340);
    close_window_btn->setGeometry(580, 15, 14, 14);
    setBackground("resources/bg_actions_window.jpg");
    v_layout->setContentsMargins(100, 140, 100, 30);
    choose_match_label = new QLabel(this);
    choose_match_label->setStyleSheet("font: 12pt \"Georgia\"; color:#dbd8ca;");
    choose_match_combobox = new QComboBox(this);
    // Styling combobox
    choose_match_combobox->setStyleSheet(
                    "QComboBox {"
                    "font: 12pt \"Georgia\";"
                    "color:#dbd8ca;"
                    "border: 1px solid #301e1e;"
                    "border-radius: 7px;"
                    "background-color: #140a0a;"
                    "padding: 5px;"
                    "}"

                    "QComboBox:on {"
                    "background-color: #301e1e;"
                    "}"

                    "QComboBox::drop-down {"
                    "border: none;"
                    "background-color: #301e1e;"
                     "}"

                    "QComboBox::down-arrow {"
                        "image: url(resources/down-arrow.png);"
                   "}"

                    "QComboBox::down-arrow:on {"
                    "border: none;"
                    "background-color: #301e1e;"
                     "}"

                    "QComboBox QListView {"
                    "background-color: #140a0a;"
                    "selection-background-color: #301e1e;"
                    "}"
                );
    v_spacer = new QSpacerItem(15, 30, QSizePolicy::Expanding, QSizePolicy::Minimum);
    v_layout->addWidget(choose_match_label);
    v_layout->addWidget(choose_match_combobox);
    v_layout->addItem(v_spacer);
    // Adding matches
    matches_list = getPausedMatches();
    QString nb_paused_matches = QString::fromStdString(std::to_string(matches_list.size()));
    choose_match_label->setText(nb_paused_matches + SAVED_GAMES_NB + SAVED_GAMES_SELECT);
    insertPausedMatches();
    setNavButtons();
}

std::vector<std::vector<std::string>> SavedGamesWindow::getPausedMatches()
{
    std::string result;
    std::vector<std::vector<std::string>> data;
    sending_T(player->getSocket(), prepForSending(270, ""));
    if (receiving(player->getSocket(), &result))
    {
        unsigned long pos;
        while(!result.empty())
        {
            pos = result.find(MSG_SEPARATOR);
            std::string temp = result.substr(0, pos);
            result = result.substr(pos + 1, result.size());
            pos = temp.find('+');
            std::string other_player = temp.substr(0, pos);
            temp = temp.substr(pos + 1, temp.size());
            pos = temp.find('+');
            std::string date = temp.substr(0, pos);
            std::string game_id = temp.substr(pos + 1, temp.size());
            std::vector<std::string> vector = {other_player, date, game_id};
            data.emplace_back(vector);
        }
    }
    return data;
}

void SavedGamesWindow::insertPausedMatches()
{
    if (!matches_list.empty())
    {
        for (int i = 0; i < static_cast<int>(matches_list.size()); ++i)
        {
            choose_match_combobox->insertItems(i, QStringList("Partie contre " + QString::fromStdString(matches_list.at(i).at(0)) + " (" + QString::fromStdString(matches_list.at(i).at(1)) + ")"));
        }
    }

}

void SavedGamesWindow::setNavButtons()
{
    const QSize NAV_BUTTON_BACK = QSize(105, 42);
    confirm_btn = new QPushButton(VALIDATE, this);
    confirm_btn->setFont(menu_font);
    confirm_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;}  QPushButton:hover {border: 1px solid #441409;}");
    confirm_btn->setMinimumSize(NAV_BUTTON_BACK);
    confirm_btn->setCursor(Qt::PointingHandCursor); // Set pointing cursor when the button is hovered
    back_btn = new QPushButton(COME_BACK, this);
    back_btn->setFont(menu_font);
    back_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;}  QPushButton:hover {border: 1px solid #441409;}");
    back_btn->setMinimumSize(NAV_BUTTON_BACK);
    back_btn->setCursor(Qt::PointingHandCursor); // Set pointing cursor when the button is hovered
    h_spacer_nav_buttons = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
    h_layout_nav_buttons = new QHBoxLayout;
    h_layout_nav_buttons->addItem(h_spacer_nav_buttons);
    h_layout_nav_buttons->addWidget(confirm_btn);
    h_layout_nav_buttons->addWidget(back_btn);
    v_layout->addLayout(h_layout_nav_buttons);
    QObject::connect(confirm_btn, SIGNAL(clicked()), this, SLOT(launchSavedGame()));
    QObject::connect(back_btn, SIGNAL(clicked()), this, SLOT(comeBack()));
}

void SavedGamesWindow::comeBack()
{
    QPoint current_pos = pos();
    MenuWindow::comeBack();
    MainMenuWindow *main_menu_window = new MainMenuWindow(this->player, current_pos, false);
    main_menu_window->show();
}

void SavedGamesWindow::launchSavedGame()
{
    auto user_choice = static_cast<unsigned>(std::stoi(matches_list.at(choose_match_combobox->currentIndex()).at(2)));
    sending_T(player->getSocket(), prepForSending(271, std::to_string(user_choice)));
    std::string other_player_name = matches_list.at(choose_match_combobox->currentIndex()).at(0);
    QPoint current_pos = pos();
    closeWindow();
    auto paused_matchmaking_window = new PausedMatchmakingWindow(this->player, 0, REALTIME_OFF, other_player_name, current_pos);
    paused_matchmaking_window->show();
}
