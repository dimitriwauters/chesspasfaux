#ifndef GAMECLIENTGUI_HPP
#define GAMECLIENTGUI_HPP

#include "BoardClientGUI.hpp"
#include "TerminalGUI.hpp"
#include "TimerGUI.hpp"
#include <ctime>

template <typename B, typename C>
class GameClientGUI : public B {
protected:
    int serverfd;
    QVBoxLayout* left_sidepane_layout = nullptr;
    QGridLayout* chessboard_layout = nullptr;
    QGridLayout* infos_game_layout = nullptr;
    QLabel* player_turn = nullptr;
    bool game_freeze = false;
    int promotionPiece = 4;
    bool canPromote = false;
    TerminalGUI* terminal;
    TimerGUI* timer;
    std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> preMoves;
    bool diceTurn[2] = {true, true};
    int gamemode;
    std::pair<int, int> last_coord = std::make_pair(9, 9);
    bool premoveDone = false;
public:
    GameClientGUI(Player*, Player*, int, int, int, int, QGridLayout*, QVBoxLayout*, QGridLayout*, std::vector<std::string>);
    ~GameClientGUI();
    void createBoard() override;
    virtual bool itsMyTurn();
    void run() override;
    void finishGame() override;
    virtual void moveSelectedPiece(ChessSquare*, ChessSquare*);
    virtual void clickDetected(ChessSquare*);
    void messageFromServer(int, std::string);
    void messageMove(std::string);
    void messagePromotion(std::string);
    void messageSave(std::string);
    void showKingInCheck();
    virtual void showBlockedPieces();
    virtual void askEndGame();
    void doPromotion(std::pair<int, int>);
    void setPromotionPiece(int);
    int getPromotionPiece();
    void setCanPromote(bool);
    bool getCanPromote();
    virtual void checkPromotion(std::pair<int, int>);
		//Dice
	void diceChessCheck(std::pair<int, int>);
	void displayDicePieces(int, int);
		//Premoves
    void addPreMove(std::pair<std::pair<int, int>, std::pair<int, int>>);
    void cancelPreMoves();
    std::pair<std::pair<int, int>, std::pair<int, int>> getPreMove();
    bool isPreMovesEmpty();
    void doPreMove();
    void interpretSignalTerminal();
    void displayTurn();
    void saveGame();
    void resignGame();
    void playMoveSound();
};

#include "GameClientGUI.cpp"

#endif //GAMECLIENTGUI_HPP
