// INFO-F209 - Groupe 5 - StatsWindow.hpp

#include "MenuWindow.hpp"

#ifndef STATSWINDOW_HPP
#define STATSWINDOW_HPP

class StatsWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QLabel *my_score;
        QHBoxLayout *h_layout;
        QTabWidget *tab_stats_container;
        QTableWidget *tab_general;
        QTableWidget *tab_friends;
        QSpacerItem *v_spacer;
    public:
        StatsWindow(Player*, QPoint);
    public slots:
        void comeBack() override;
};

#endif
