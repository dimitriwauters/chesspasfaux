// INFO-F209 - Groupe 5 - LaunchGameWindow.cpp

#include "LaunchGameWindow.hpp"
#include "GameWindow.hpp"

LaunchGameWindow::LaunchGameWindow(Player* player, QPoint current_pos) : MenuWindow(player)
{
    move(current_pos);
    setBackground("resources/bg_menu_with_description.jpg");
    v_layout->setContentsMargins(50, 150, 50, 25);
    description = new QLabel(this);
    description->setMinimumSize(QSize(0, 200));
    description->setStyleSheet("font: 12pt \"Georgia\"; color:#dbd8ca;");
}

void LaunchGameWindow::showDescription(QString text)
{
    description->setText(text);
}

GamemodesWindow::GamemodesWindow(Player* player, QPoint current_pos) : LaunchGameWindow(player, current_pos)
{
    move(current_pos);
    // GAMEMODES BUTTONS
    default_game_btn = new QPushButton("", this);
    horde_game_btn = new QPushButton("", this);
    dark_game_btn = new QPushButton("", this);
    dice_game_btn = new QPushButton("", this);
    // Show description
    default_game_btn->installEventFilter(this);
    horde_game_btn->installEventFilter(this);
    dark_game_btn->installEventFilter(this);
    dice_game_btn->installEventFilter(this);
    // Creating a grid layout and adding buttons to this layout
    g_layout = new QGridLayout;
    g_layout->setSpacing(40);
    g_layout->addWidget(default_game_btn, 1, 0, 1, 1);
    g_layout->addWidget(horde_game_btn, 1, 1, 1, 1);
    g_layout->addWidget(dark_game_btn, 2, 0, 1, 1);
    g_layout->addWidget(dice_game_btn, 2, 1, 1, 1);
    v_layout->addLayout(g_layout);
    // Defining the style of buttons
    default_game_btn->setStyleSheet("QPushButton {background-image: url(resources/default_game_btn.png); background-repeat:no-repeat; border:none;} QPushButton:hover {background-image: url(resources/default_game_btn_hover.png);  background-repeat:no-repeat; border:none;}");
    horde_game_btn->setStyleSheet("QPushButton {background-image: url(resources/horde_game_btn.png);  background-repeat:no-repeat; border:none;} QPushButton:hover {background-image: url(resources/horde_game_btn_hover.png);  background-repeat:no-repeat; border:none;}");
    dice_game_btn->setStyleSheet("QPushButton {background-image: url(resources/dice_game_btn.png);  background-repeat:no-repeat; border:none;} QPushButton:hover {background-image: url(resources/dice_game_btn_hover.png);  background-repeat:no-repeat; border:none;}");
    dark_game_btn->setStyleSheet("QPushButton {background-image: url(resources/dark_game_btn.png);  background-repeat:no-repeat; border:none;} QPushButton:hover {background-image: url(resources/dark_game_btn_hover.png);  background-repeat:no-repeat; border:none;}");
    const QSize GAMEMODE_BUTTON_SIZE = QSize(345, 150);
    default_game_btn->setMinimumSize(GAMEMODE_BUTTON_SIZE); // Set buttons size
    horde_game_btn->setMinimumSize(GAMEMODE_BUTTON_SIZE);
    dice_game_btn->setMinimumSize(GAMEMODE_BUTTON_SIZE);
    dark_game_btn->setMinimumSize(GAMEMODE_BUTTON_SIZE);
    // Functions (signals and slots)
    QObject::connect(default_game_btn, SIGNAL(clicked()), this, SLOT(selectDefaultGame()));
    QObject::connect(horde_game_btn, SIGNAL(clicked()), this, SLOT(selectHordeGame()));
    QObject::connect(dice_game_btn, SIGNAL(clicked()), this, SLOT(selectDiceGame()));
    QObject::connect(dark_game_btn, SIGNAL(clicked()), this, SLOT(selectDarkGame()));
    // DESCRIPTION AND NAV BUTTONS
    v_layout->addWidget(description);
    setNavButtons();
}

bool GamemodesWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::Enter)
    {
        if (obj == (QObject*) default_game_btn) showDescription(DEFAULT_GAMEMODE_DESCR);
        else if (obj == (QObject*) horde_game_btn) showDescription(HORDE_GAMEMODE_DESCR);
        else if (obj == (QObject*) dice_game_btn) showDescription(DICE_GAMEMODE_DESCR);
        else if (obj == (QObject*) dark_game_btn) showDescription(DARK_GAMEMODE_DESCR);
    }
    else if (event->type() == QEvent::Leave) showDescription("");
    return QWidget::eventFilter(obj, event);
}

void GamemodesWindow::comeBack()
{
    QPoint current_pos = pos();
    MenuWindow::comeBack();
    MainMenuWindow *main_menu_window = new MainMenuWindow(this->player, current_pos, false);
    main_menu_window->show();
}

void GamemodesWindow::selectDefaultGame()
{
    int gamemode_selected = DEFAULT_GAMEMODE;
    createModeWindow(gamemode_selected);
}

void GamemodesWindow::selectHordeGame()
{
    int gamemode_selected = HORDE_GAMEMODE;
    createModeWindow(gamemode_selected);
}

void GamemodesWindow::selectDiceGame()
{
    int gamemode_selected = DICE_GAMEMODE;
    createModeWindow(gamemode_selected);
}

void GamemodesWindow::selectDarkGame()
{
    int gamemode_selected = DARK_GAMEMODE;
    createModeWindow(gamemode_selected);
}

void GamemodesWindow::createModeWindow(int gamemode)
{
    QPoint current_pos = pos();
    closeWindow();
    ModesWindow *modes_window = new ModesWindow(gamemode, this->player, current_pos);
    modes_window->show();
}

ModesWindow::ModesWindow(int gamemode, Player* player, QPoint current_pos) : LaunchGameWindow(player, current_pos)
{
    move(current_pos);
    chosen_gamemode = gamemode;
    // Default and real time modes buttons
    default_mode_btn = new QPushButton(REALTIME_OFF_STR, this);
    real_time_mode_btn = new QPushButton(REALTIME_ON_STR, this);
    // Show description
    default_mode_btn->installEventFilter(this);
    real_time_mode_btn->installEventFilter(this);
    // Adding the buttons to an horizontal layout
    h_layout = new QHBoxLayout;
    h_layout->addWidget(default_mode_btn);
    h_layout->addWidget(real_time_mode_btn);
    v_layout->addLayout(h_layout);
    // Defining the style of buttons
    setStyleSheet("QPushButton {background-image: url(resources/select_mode_bg.png); background-repeat:no-repeat; border:none;color: #ece6ac;} QPushButton:hover {background-image: url(resources/select_mode_bg_hover.png);  background-repeat:no-repeat; border:none;color: #f0eed8;}");
    default_mode_btn->setFont(menu_font);
    real_time_mode_btn->setFont(menu_font);
    const QSize GAMEMODE_BUTTON_SIZE = QSize(341, 244);
    default_mode_btn->setMinimumSize(GAMEMODE_BUTTON_SIZE); // Set buttons size
    real_time_mode_btn->setMinimumSize(GAMEMODE_BUTTON_SIZE);
    default_mode_btn->setCursor(Qt::PointingHandCursor);
    real_time_mode_btn->setCursor(Qt::PointingHandCursor);
    // Functions (signals and slots)
    QObject::connect(default_mode_btn, SIGNAL(clicked()), this, SLOT(launchDefaultGame()));
    QObject::connect(real_time_mode_btn, SIGNAL(clicked()), this, SLOT(launchRealTimeGame()));
    // Description and nav buttons
    v_layout->addWidget(description);
    setNavButtons();
 }

bool ModesWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::Enter)
    {
        if (obj == (QObject*) default_mode_btn) showDescription(REALTIME_OFF_DESCR);
        else if (obj == (QObject*) real_time_mode_btn) showDescription(REALTIME_ON_DESCR);
    }
    else if (event->type() == QEvent::Leave) showDescription("");
    return QWidget::eventFilter(obj, event);
}

void ModesWindow::comeBack()
{
    QPoint current_pos = pos();
    MenuWindow::comeBack();
    GamemodesWindow *gamemodes_window = new GamemodesWindow(this->player, current_pos);
    gamemodes_window->show();
}

void ModesWindow::launchGameSound()
{
    QSound::play("resources/sounds/intro_kaamelott.wav");
}

void ModesWindow::launchDefaultGame()
{
    launchGameSound();
    QPoint current_pos = pos();
    closeWindow();
    //int mode = REALTIME_OFF;
    //MatchmakingWindow *matchmaking_window = new MatchmakingWindow(this->socketfd, this->clientName);
    MatchmakingWindow *matchmaking_window = new MatchmakingWindow(this->player, chosen_gamemode, REALTIME_OFF, {}, current_pos);
    matchmaking_window->show();
}

void ModesWindow::launchRealTimeGame()
{
    launchGameSound();
    QPoint current_pos = pos();
    closeWindow();
    //int mode = REALTIME_ON;
    //MatchmakingWindow *matchmaking_window = new MatchmakingWindow(this->socketfd, this->clientName);
    MatchmakingWindow *matchmaking_window = new MatchmakingWindow(this->player, chosen_gamemode, REALTIME_ON, {}, current_pos);
    matchmaking_window->show();
}

MatchmakingWindow::MatchmakingWindow(Player* player, int gamemode, int type, std::vector<std::string> pieces_pos, QPoint current_pos) : Window(player), gamemode(gamemode), type(type), pieces(std::move(pieces_pos))
{
    move(current_pos);
    setFixedSize(800, 314);
    close_window_btn->setGeometry(773, 12, 14, 14);
    setBackground("resources/bg_matchmaking.jpg");
    title = new QLabel(this);
    title->setText(MATCHMAKING_WAITING);
    title->setFont(menu_font);
    title->setStyleSheet("color:#ece6ac;");
    title->setAlignment(Qt::AlignCenter);
    quote = new QLabel(this);
    std::string quoteString = quoteFinder();
    unsigned lenQuote = strlen(quoteString.c_str());
    unsigned height = (270-(35*(lenQuote/90)));
    if(lenQuote > 90) quoteString = separateString(quoteString, lenQuote);
    QString quoteQString = QString::fromUtf8(quoteString.c_str());
    quote->setText(quoteQString);
    quote->setStyleSheet("font: 12pt \"Georgia\"; font-style: italic; color:#dbd8ca;");
    v_spacer = new QSpacerItem(0, height-20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    v_layout = new QVBoxLayout();
    v_layout->setContentsMargins(15, 15, 15, 20);
    setLayout(v_layout);
    v_layout->addWidget(title);
    v_layout->addItem(v_spacer);
    v_layout->addWidget(quote);
    QObject::connect(this, SIGNAL(ready()), this, SLOT(startGame()));
}

void MatchmakingWindow::closeWindow()
{
    Window::closeWindow();
}

void MatchmakingWindow::mouseDoubleClickEvent(QMouseEvent* e)
{
    if (e->button() == Qt::LeftButton)
    {
        closeWindow();
    }
}

void MatchmakingWindow::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);
    usleep(50000);
    std::thread start(&MatchmakingWindow::getInfosGame, this);
    start.detach();
}

void MatchmakingWindow::getInfosGame()
{
    if(type) sending_T(player->getSocket(), prepForSending(231+gamemode, "realtime"));
    else sending_T(player->getSocket(), prepForSending(231+gamemode, "default"));
    receiving(player->getSocket(), &team);
    receiving(player->getSocket(), player_name);
    receiving(player->getSocket(), &ELO);
    emit ready();
}

void MatchmakingWindow::startGame()
{
    QPoint current_pos = pos();
    player->setColor(static_cast<bool>(team));
    auto opponent = new Player(!static_cast<bool>(team), player_name);
    opponent->setELO(ELO);
    std::cout << "Gamemode : " << gamemode << ", type : " << type << ", pieces.size() :  " << pieces.size() << std::endl;
    if(team) { auto game_window = new GameWindow(this->player, this->player, opponent, gamemode, type, team, pieces, current_pos); game_window->show(); }
    else { auto game_window = new GameWindow(this->player, opponent, this->player, gamemode, type, team, pieces, current_pos); game_window->show(); }
    Window::closeWindow();
}

std::string MatchmakingWindow::quoteFinder()
{
    std::ifstream file("resources/text/quotes.txt");
    if(file) {
        srand((int)time(0));
        std::string quote;
        int lineCounter = 0, lineNumber = rand() % 103;
        while(lineCounter != lineNumber) {
            getline(file, quote);
            lineCounter++;
        }
        file.close();
        return quote;
    }
    else
        return "C'est pas faux !";
}

std::string MatchmakingWindow::separateString(std::string quoteString, unsigned lenQuote) {
    unsigned sizeLeft = lenQuote, sizePart = 90, sizeTodo = 0;
    std::string res = "";
    while(sizeLeft >= 90) {
        while(quoteString[(sizeTodo+sizePart)] != ' ')
            sizePart--;
        for(unsigned i = sizeTodo; i < (sizeTodo+sizePart); i++) {
            res += quoteString[i];
        }
        res += "\n";
        sizeLeft -= sizePart;
        sizeTodo += sizePart;
        sizePart = 90;
    }
    for(unsigned i = sizeTodo; i < (sizeTodo+sizeLeft); i++)
        res += quoteString[i];
    return res;
}

PausedMatchmakingWindow::PausedMatchmakingWindow(Player* player, int gamemode, int type, std::string name, QPoint current_pos) : MatchmakingWindow(player, gamemode, type, {}, current_pos), opponent_name(std::move(name))
{
    QString label = QString::fromStdString(PAUSED_MATCHMAKING_WAITING+opponent_name);
    title->setText(label);
}

void PausedMatchmakingWindow::getInfosGame()
{
    char message[MAXBUFSIZE];
    if (receiving(player->getSocket(), message))
    {
        std::string buffer = message;
        unsigned long position;
        while(!buffer.empty())
        {
            position = buffer.find(MSG_SEPARATOR);
            pieces.emplace_back(buffer.substr(0, position));
            buffer = buffer.substr(position + 1, buffer.size());
        }
    }
    else emit serverDisconnected();
    receiving(player->getSocket(), &gamemode);
    receiving(player->getSocket(), &team);
    receiving(player->getSocket(), player_name);
    receiving(player->getSocket(), &ELO);
    if (player_name == opponent_name) emit ready();
}
