// INFO-F209 - Groupe 5 - Terminal.hpp

#ifndef TERMINALGUI_HPP
#define TERMINALGUI_HPP

#include <cstring>
#include "../Player.hpp"
#include "../const.hpp"
#include "../Network.hpp"
#include "MenuWindow.hpp"

class TerminalGUI : public QWidget {
    Q_OBJECT
private:
	int serverfd;
    Player* me;
    Player* other;
    QVBoxLayout *layout_sidepane;
    SendBox *user_input;
    QString user_input_content;
    QString prefix_command = "";
    QPushButton *send_msg_btn;
    QFont menu_font;
    std::vector<QString> terminal_text;
    int signal = 0;
    //int max_message_length = 100;
    QTextEdit *terminal_container;
    QString terminal_content = "";
public:
    TerminalGUI(Player*, Player*, int, QVBoxLayout*);
    ~TerminalGUI();
    void write(std::string);
		//Command
    bool interpretCommand(QString);
    void doCommand(QString);
    void getCommand(QString);
    void setPrefixCommand(std::string);
    int getSignal();
public slots:
    void writeText(QString);
    void writeTerminal();
signals:
    void writeSignal(QString);
};

#endif
