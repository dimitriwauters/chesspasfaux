#ifndef CHESSSQUARE_HPP
#define CHESSSQUARE_HPP

#include "MenuWindow.hpp"

class ChessSquare: public QWidget
{
    Q_OBJECT
private:
    QLabel* picture;
    QLabel* text;
    int color;
    int row;
    int col;
    int piece = NO_DEFINED;
    int piece_color = NO_DEFINED;
    QFont pieces_font;
public:
    ChessSquare(int, int, int);
    ~ChessSquare();
    void setPiece(int, int);
    std::pair<int, int> getPiece();
    std::pair<int, int> getCoord();
    void display();
    void setBackground(bool, QString="");
    void mouseMoveEvent(QMouseEvent *) override;
};

#endif
