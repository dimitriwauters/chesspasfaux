// INFO-F209 - Groupe 5 - SavedGamesWindow.hpp

#include "MenuWindow.hpp"

#ifndef SAVEDGAMESWINDOW_HPP
#define SAVEDGAMESWINDOW_HPP

class SavedGamesWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QLabel *choose_match_label;
        QComboBox *choose_match_combobox;
        QSpacerItem *v_spacer;
        QPushButton *confirm_btn;
        std::vector<std::vector<std::string>> matches_list;
    public:
        SavedGamesWindow(Player*, QPoint);
        std::vector<std::vector<std::string>> getPausedMatches();
        void insertPausedMatches();
        void setNavButtons() override;
    public slots:
        void comeBack() override;
        void launchSavedGame();
};

#endif
