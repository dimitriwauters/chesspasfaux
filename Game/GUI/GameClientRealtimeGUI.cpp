#include "GameClientRealtimeGUI.hpp"

template <typename B>
GameClientRealtimeGUI<B>::GameClientRealtimeGUI(Player* white_player, Player* black_player, int socket, int start, int type, int mode, QGridLayout* chessboard, QVBoxLayout* left_sidepane, QGridLayout* bottom_sidepane) : B(white_player, black_player, socket, start, type, mode, chessboard, left_sidepane, bottom_sidepane, {})
{
    std::thread timer(&GameClientRealtimeGUI<B>::timer, this);
    timer.detach();
}


template <typename B>
GameClientRealtimeGUI<B>::~GameClientRealtimeGUI() = default;

template <typename B>
void GameClientRealtimeGUI<B>::moveSelectedPiece(ChessSquare* src, ChessSquare* dest) {
    bool found = false;
    for(unsigned i = 0; i < blocked_pieces.size(); ++i) {
        if(blocked_pieces.at(i).first == src->getCoord()) { found = true; break; }
    }
    if(!found) {
        B::moveSelectedPiece(src, dest);
    }
    else this->board->refreshDisplay();
}

template <typename B>
void GameClientRealtimeGUI<B>::clickDetected(ChessSquare* square)
{
    bool found = false;
    for(unsigned i = 0; i < blocked_pieces.size(); ++i) {
        if(blocked_pieces.at(i).first == square->getCoord()) { found = true; break; }
    }
    if(!found) {
        B::clickDetected(square);
    }
}

template <typename B>
bool GameClientRealtimeGUI<B>::itsMyTurn() {
    return true;
}

template <typename B>
void GameClientRealtimeGUI<B>::checkPromotion(std::pair<int, int> coord __attribute__((unused))) {}

template <typename B>
bool GameClientRealtimeGUI<B>::willBeInCheck(std::pair<int, int> src __attribute__((unused)), std::pair<int, int> dest __attribute__((unused)), bool playerTurn __attribute__((unused)))
{
    return false;
}

template <typename B>
bool GameClientRealtimeGUI<B>::isUnderAttack(std::pair<int, int> current_pos __attribute__((unused)), bool color __attribute__((unused)))
{
    return false;
}

template <typename B>
void GameClientRealtimeGUI<B>::checkWinConditions(bool playerTurn) {
    if(this->isInStalemate(!playerTurn)) this->running_game = false;
    if(this->isInStalemate(playerTurn)) this->running_game = false;
    if(this->getKingPosition(!playerTurn).first == -1) this->setWinner(playerTurn);
    if(this->getKingPosition(playerTurn).first == -1) this->setWinner(!playerTurn);
}

template <typename B>
bool GameClientRealtimeGUI<B>::movePiece(std::pair<int, int> src_pos, std::pair<int, int> dest_pos, unsigned canMove) {
    if(canMove != MOVE_NOT_ALLOWED) {
        if(this->board->getColorPiece(src_pos) == this->my_player->getColor()) blocked_pieces.emplace_back(std::make_pair(dest_pos, current_tick));
        last_moved_piece = current_tick;
        return B::movePiece(src_pos, dest_pos, canMove);
    }
    return false;
}

template <typename B>
void GameClientRealtimeGUI<B>::showBlockedPieces() {
    for(unsigned i = 0; i < blocked_pieces.size(); ++i) {
        this->board->getSquare(blocked_pieces.at(i).first)->setBackground(true, "resources/check_square.png");
    }
}

template <typename B>
unsigned GameClientRealtimeGUI<B>::enPassant(std::pair<int, int> src_pos, std::pair<int, int> dest_pos) {
    bool found = false;
    unsigned passant = B::enPassant(src_pos, dest_pos);
    if(passant) {
        std::pair<int, int> cell;
        cell = std::make_pair(src_pos.first, dest_pos.second);
        for(unsigned i = 0; i < blocked_pieces.size(); ++i) {
            if(blocked_pieces.at(i).first == cell) { found = true; break; }
        }
        if(!found) { passant = 0; }
    }
    return passant;
}

template <typename B>
void GameClientRealtimeGUI<B>::askEndGame() {}

template <typename B>
void GameClientRealtimeGUI<B>::timer() {
    while(this->notOver()) {
        current_tick++;
        if(last_moved_piece + (300 / (REALTIME_DELAYTIME/1000)) <= current_tick) {
            if(this->my_player->getColor() == WHITE) {
                this->running_game = FALSE;
                sending_T(this->serverfd, prepForSending(120, "drawn"));
            }
        }
        for(int i = static_cast<int>(blocked_pieces.size())-1; i >= 0; --i) {
            if(blocked_pieces.at(i).second + (REALTIME_BLOCKEDTIME / (REALTIME_DELAYTIME/1000)) <= current_tick) {
                this->board->getSquare(blocked_pieces.at(i).first)->setBackground(false);
                blocked_pieces.erase(blocked_pieces.begin() + i);
            }
        }
        usleep(REALTIME_DELAYTIME*1000);
    }
}
