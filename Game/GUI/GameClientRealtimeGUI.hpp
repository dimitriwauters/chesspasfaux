#ifndef GAMECLIENTREALTIMEGUI_HPP
#define GAMECLIENTREALTIMEGUI_HPP

#include "MenuWindow.hpp"
#include "ChessSquare.hpp"
#include "../Console/GameClientRealtime.hpp"

template <typename B>
class GameClientRealtimeGUI : public B {
private:
    unsigned current_tick = 0;
    unsigned last_moved_piece = 0;
    std::vector<std::pair<std::pair<int, int>, unsigned>> blocked_pieces;
public:
    GameClientRealtimeGUI(Player*, Player*, int, int, int, int, QGridLayout*, QVBoxLayout*, QGridLayout*);
    ~GameClientRealtimeGUI();
    void moveSelectedPiece(ChessSquare*, ChessSquare*) override;
    void clickDetected(ChessSquare*) override;
    void checkPromotion(std::pair<int, int>) override;
    bool willBeInCheck(std::pair<int, int>, std::pair<int, int>, bool) override;
    bool isUnderAttack(std::pair<int, int>, bool) override;
    void checkWinConditions(bool) override;
    bool movePiece(std::pair<int, int>, std::pair<int, int>, unsigned) override;
    void showBlockedPieces() override;
    unsigned enPassant(std::pair<int, int> src_pos, std::pair<int, int> dest_pos) override;
    bool itsMyTurn() override;
    void askEndGame() override;
    void timer();
};

#include "GameClientRealtimeGUI.cpp"

#endif //GAMECLIENTREALTIMEGUI_HPP
