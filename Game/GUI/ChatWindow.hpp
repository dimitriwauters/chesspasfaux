// INFO-F209 - Groupe 5 - ChatWindow.hpp

#include "MenuWindow.hpp"
#include <cctype>

#ifndef CHATWINDOW_HPP
#define CHATWINDOW_HPP

class ChatWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QGridLayout *g_layout;
        QTextEdit *transcript_container;
        QString transcript[100];
        SendBox *text_to_send;
        QPushButton *send_msg_btn;
        QLabel *description;
        QLabel *discuss_with_title;
        QLabel *connected_friends_title;
        QTableWidget *connected_friends_tab;
        std::string inChatWith = "no";
        std::vector<std::string> friends;
    public:
        ChatWindow(Player*);
        void sendMessageFriends();
    public slots:
        void send();
        void receivedMessage(QString, QString);
        void changeTranscript();
        void refreshFriends(QString);
        QString extractName(QString);
};

#endif
