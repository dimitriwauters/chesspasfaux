// INFO-F209 - Groupe 5 - GameWindow.cpp

#include "GameWindow.hpp"
#include "ChatWindow.hpp"
#include "../Gamemodes.hpp"
#include "GameClientRealtimeGUI.hpp"

typedef GameClientGUI<GameDefault<BoardClientGUI<Board>>, BoardClientGUI<Board>> gameDefault;
typedef GameClientGUI<GameDark<BoardClientGUI<BoardDark>>, BoardClientGUI<BoardDark>> gameDark;
typedef GameClientGUI<GameDice<BoardClientGUI<BoardDice>>, BoardClientGUI<BoardDice>> gameDice;
typedef GameClientGUI<GameHorde<BoardClientGUI<BoardHorde>>, BoardClientGUI<BoardHorde>> gameHorde;
typedef GameClientRealtimeGUI<GameClientGUI<GameDefault<BoardClientGUI<Board>>, BoardClientGUI<Board>>> gameDefaultRealtime;
typedef GameClientRealtimeGUI<GameClientGUI<GameDark<BoardClientGUI<BoardDark>>, BoardClientGUI<BoardDark>>> gameDarkRealtime;
typedef GameClientRealtimeGUI<GameClientGUI<GameDice<BoardClientGUI<BoardDice>>, BoardClientGUI<BoardDice>>> gameDiceRealtime;
typedef GameClientRealtimeGUI<GameClientGUI<GameHorde<BoardClientGUI<BoardHorde>>, BoardClientGUI<BoardHorde>>> gameHordeRealtime;

gameDefault* game_default_normal = nullptr;
gameDark* game_dark_normal = nullptr;
gameDice* game_dice_normal = nullptr;
gameHorde* game_horde_normal = nullptr;
gameDefaultRealtime* game_default_realtime = nullptr;
gameDarkRealtime* game_dark_realtime = nullptr;
gameDiceRealtime* game_dice_realtime = nullptr;
gameHordeRealtime* game_horde_realtime = nullptr;

GameWindow::GameWindow(Player* my_player, Player* white_player, Player* black_player, int gamemode, int type, int team, std::vector<std::string> pieces, QPoint current_pos) : Window(my_player)
{
    move(current_pos);
    Player* other_player;
    if(white_player == my_player) other_player = black_player;
    else other_player = white_player;
    setFixedSize(980, 780);
    setBackground("resources/bg_ingame.jpg");
    setStyleSheet("QTextEdit {font: 10pt \"Georgia\";"
                            "text-align:justify;"
                            "color:#dbd8ca;"
                            "background-color: #140a0a;"
                            "border:1px solid #301e1e;"
                            "padding: 5px;}");
    close_window_btn->setGeometry(950, 15, 14, 14);
    playStartSound();
    // Creating layouts
    h_layout = new QHBoxLayout(); // Container (= main layout)
    h_layout->setContentsMargins(0, 20, 0, 20);
    setLayout(h_layout);
    v_layout_left_sidepane = new QVBoxLayout();
    v_layout_game = new QVBoxLayout();
    v_layout_right = new QVBoxLayout();
    v_layout_right->setContentsMargins(0,0,0,20);
    g_layout_chessboard = new QGridLayout();
    g_layout_infos_game = new QGridLayout();
    g_layout_infos_game->setContentsMargins(0,0,0,30);
    h_layout->addLayout(v_layout_left_sidepane);
    h_layout->addLayout(v_layout_game);
    h_layout->addLayout(v_layout_right);
    // Left sidepane
    open_chat_btn = new QPushButton(OPEN_CHAT, this);
    save_game_btn = new QPushButton(SAVE_GAME, this);
    quit_btn = new QPushButton(RESIGN, this);
    open_chat_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    save_game_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    quit_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    open_chat_btn->setFont(menu_font);
    save_game_btn->setFont(menu_font);
    quit_btn->setFont(menu_font);
    const QSize BUTTON_SIZE = QSize(202, 42);
    open_chat_btn->setMaximumSize(BUTTON_SIZE);
    save_game_btn->setMaximumSize(BUTTON_SIZE);
    quit_btn->setMaximumSize(BUTTON_SIZE);
    QObject::connect(open_chat_btn, SIGNAL(clicked()), this, SLOT(openChat()));
    QObject::connect(save_game_btn, SIGNAL(clicked()), this, SLOT(saveGame()));
    QObject::connect(quit_btn, SIGNAL(clicked()), this, SLOT(resign()));
    v_layout_left_sidepane->addWidget(open_chat_btn);
    v_layout_left_sidepane->addWidget(save_game_btn);
    v_layout_left_sidepane->addWidget(quit_btn);
    // Chess
    g_layout_chessboard->setSpacing(0);
    g_layout_chessboard->setContentsMargins(10,135,0,0);
    v_layout_game->addLayout(g_layout_chessboard);
    // Infos about the game (timer, turn)
    auto infos_game = new QWidget();
    infos_game->setMinimumSize(455, 134);
    infos_game->setMaximumSize(455, 134);
    infos_game->setLayout(g_layout_infos_game);
    v_layout_game->addWidget(infos_game);
    // Right sidepane
    opponent_name = new QLabel(); // Information about the opponent's player
    opponent_name->setFont(menu_font);
    opponent_name->setMinimumSize(172, 80);
    opponent_name->setMaximumSize(172, 80);
    QString text_displayed = GAME_AGAINST+QString::fromStdString(other_player->getUsername()) + "\n(ELO : "+ QString::fromStdString(std::to_string(other_player->getELO())) + ")";
    opponent_name->setText(text_displayed);
    opponent_name->setStyleSheet("font-size: 20px; color:#dbd8ca;");
    opponent_name->setAlignment(Qt::AlignHCenter);
    v_layout_right->addWidget(opponent_name);
    my_color = new QLabel(); // Playing with black/white pieces
    QString color;
    if (my_player->getColor()) color = "BLANCS";
    else color = "NOIRS";
    my_color->setTextFormat(Qt::RichText);
    my_color->setText("Vous êtes les <strong>" +color+"</strong>");
    my_color->setStyleSheet("font: 12pt \"Georgia\";"
                            "text-align:justify;"
                            "color:#dbd8ca;"
                            "padding-bottom: 15px;");
    my_color->setAlignment(Qt::AlignCenter);
    v_layout_right->addWidget(my_color);
    advertising = new Advertising(this); // Advertising
    v_layout_right->addWidget(advertising);
    QObject::connect(this, SIGNAL(repaintGameSIG()), this, SLOT(repaintGame()));
    if(type) gamemodeChoice(gamemode+1, "realtime", white_player, black_player, team, g_layout_chessboard, v_layout_left_sidepane, g_layout_infos_game, pieces);
    else gamemodeChoice(gamemode+1, "default", white_player, black_player, team, g_layout_chessboard, v_layout_left_sidepane, g_layout_infos_game, pieces);
    QObject::connect(this, SIGNAL(gameEnded()), this, SLOT(comeBackMainMenu()));
};

void GameWindow::openChat()
{
    QPoint current_pos = pos();
    foreach (QWidget *widget, QApplication::topLevelWidgets()) {
        if(widget->objectName() == "chat_window") {
            widget->move(current_pos);
            widget->show();
        }
    }
}

void GameWindow::saveGame()
{
    if(game_default_normal) game_default_normal->saveGame();
    else if(game_dice_normal) game_dice_normal->saveGame();
    else if(game_horde_normal) game_horde_normal->saveGame();
    else if(game_dark_normal) game_dark_normal->saveGame();

    else if(game_default_realtime) game_default_realtime->saveGame();
    else if(game_dice_realtime) game_dice_realtime->saveGame();
    else if(game_horde_realtime) game_horde_realtime->saveGame();
    else if(game_dark_realtime) game_dark_realtime->saveGame();
}


void GameWindow::resign()
{
    if(game_default_normal) game_default_normal->resignGame();
    else if(game_dice_normal) game_dice_normal->resignGame();
    else if(game_horde_normal) game_horde_normal->resignGame();
    else if(game_dark_normal) game_dark_normal->resignGame();

    else if(game_default_realtime) game_default_realtime->resignGame();
    else if(game_dice_realtime) game_dice_realtime->resignGame();
    else if(game_horde_realtime) game_horde_realtime->resignGame();
    else if(game_dark_realtime) game_dark_realtime->resignGame();
}

void GameWindow::playStartSound()
{
    QSound::play("resources/sounds/en_garde_ma_mignonne.wav");
}

void GameWindow::mousePressEvent(QMouseEvent* event)
{
    //std::cout << "début fct" << std::endl;
    if(childAt(event->pos())) {
        //std::cout << "if childAt" << std::endl;
        auto square = dynamic_cast<ChessSquare*>(childAt(event->pos())->parent());
        if(square) {
            //std::cout << "if square" << std::endl;
            if(game_default_normal) game_default_normal->clickDetected(square);
            else if(game_dice_normal) game_dice_normal->clickDetected(square);
            else if(game_horde_normal) game_horde_normal->clickDetected(square);
            else if(game_dark_normal) game_dark_normal->clickDetected(square);

            else if(game_default_realtime) game_default_realtime->clickDetected(square);
            else if(game_dice_realtime) game_dice_realtime->clickDetected(square);
            else if(game_horde_realtime) game_horde_realtime->clickDetected(square);
            else if(game_dark_realtime) game_dark_realtime->clickDetected(square);
        }
    }
    else Window::mousePressEvent(event);
}

template <typename G>
void GameWindow::serverInfos(G* game) {
    std::string message;
    char buffer[MAXBUFSIZE];
    int opcode_first, opcode_second;
    bool running = true;
    while(running && player->getSocket() != 0) {
        memset(buffer, '\0', MAXBUFSIZE);
        if(receiving_T(player->getSocket(), buffer)) {
            std::cout << "--- " << buffer << std::endl;
            message = buffer;
            opcode_first = std::stoi(message.substr(0, 1));
            opcode_second = std::stoi(message.substr(1, 1));
            message = message.substr(3, message.size());
            running = serverSwitch(opcode_first, opcode_second, message, game, running);
        }
        else { emit serverDisconnected(); sleep(2); break; } // Serveur déconnecté
    }
    if(player->getSocket() != 0) { sleep(2); emit gameEnded(); }
}

template <typename G>
bool GameWindow::serverSwitch(int opcode_first, int opcode_second, std::string message, G* game, bool running) {
    switch (opcode_first) {
        case 1: { // Jeu
            switch (opcode_second) {
                case 1: { // Déplacement
                    game->messageFromServer(1, message);
                    break;
                }
                case 2: { // Abandon
                    game->messageFromServer(2, message);
                    break;
                }
                case 3: { // Gagnant
                    game->messageFromServer(3, message);
                    break;
                }
                case 4: { // Changement de pièce (Promotion)
                    game->messageFromServer(4, message);
                    break;
                }
                case 5: { // SwapTurn
                    game->messageFromServer(5, message);
                    break;
                }
                case 6: { // Arrêt du thread
                    running = false;
                    break;
                }
                case 7: { // Roll the dices
                    game->messageFromServer(7, message);
                    break;
                }
                case 8: { // Save game
                    game->messageFromServer(8, message);
                    break;
                }
            }
            break;
        }
        case 3: { // CHAT
            switch (opcode_second) {
                case 3: { // Message
                    game->messageFromServer(33, message);
                    break;
                }
            }
            break;
        }
    }
    return running;
}

void GameWindow::gamemodeChoice(unsigned choice, std::string gamemode, Player* white_player, Player* black_player, int team, QGridLayout* chessboard, QVBoxLayout *sidepane_layout, QGridLayout *g_layout_infos_game, std::vector<std::string> pieces)
{
    switch(choice){
        case 1: { // Default
            if(gamemode == "realtime") {
                auto client = new gameDefaultRealtime(white_player, black_player, player->getSocket(), team, REALTIME_ON, DEFAULT_GAMEMODE, chessboard, sidepane_layout, g_layout_infos_game);
                game_default_realtime = client;
                std::thread server(&GameWindow::serverInfos<gameDefaultRealtime>, this, client);
                server.detach();
            }
            else {
                auto client = new gameDefault(white_player, black_player, player->getSocket(), team, REALTIME_OFF, DEFAULT_GAMEMODE, chessboard, sidepane_layout, g_layout_infos_game, pieces);
                game_default_normal = client;
                std::thread server(&GameWindow::serverInfos<gameDefault>, this, client);
                server.detach();
            }
            break;
        }
        case 2: { // Horde
            if(gamemode == "realtime") {
                auto client = new gameHordeRealtime(white_player, black_player, player->getSocket(), team, REALTIME_ON, HORDE_GAMEMODE, chessboard, sidepane_layout, g_layout_infos_game);
                game_horde_realtime = client;
                std::thread server(&GameWindow::serverInfos<gameHordeRealtime>, this, client);
                server.detach();
            }
            else {
                auto client = new gameHorde(white_player, black_player, player->getSocket(), team, REALTIME_OFF, HORDE_GAMEMODE, chessboard, sidepane_layout, g_layout_infos_game, pieces);
                game_horde_normal = client;
                std::thread server(&GameWindow::serverInfos<gameHorde>, this, client);
                server.detach();
            }
            break;
        }
        case 3: { // Dark
            if(gamemode == "realtime") {
                auto client = new gameDarkRealtime(white_player, black_player, player->getSocket(), team, REALTIME_ON, DARK_GAMEMODE, chessboard, sidepane_layout, g_layout_infos_game);
                game_dark_realtime = client;
                std::thread server(&GameWindow::serverInfos<gameDarkRealtime>, this, client);
                server.detach();
            }
            else {
                auto client = new gameDark(white_player, black_player, player->getSocket(), team, REALTIME_OFF, DARK_GAMEMODE, chessboard, sidepane_layout, g_layout_infos_game, pieces);
                game_dark_normal = client;
                std::thread server(&GameWindow::serverInfos<gameDark>, this, client);
                server.detach();
            }
            break;
        }
        case 4: { // Dice
            if(gamemode == "realtime") {
                auto client = new gameDiceRealtime(white_player, black_player, player->getSocket(), team, REALTIME_ON, DICE_GAMEMODE, chessboard, sidepane_layout, g_layout_infos_game);
                game_dice_realtime = client;
                std::thread server(&GameWindow::serverInfos<gameDiceRealtime>, this, client);
                server.detach();
            }
            else {
                auto client = new gameDice(white_player, black_player, player->getSocket(), team, REALTIME_OFF, DICE_GAMEMODE, chessboard, sidepane_layout, g_layout_infos_game, pieces);
                game_dice_normal = client;
                std::thread server(&GameWindow::serverInfos<gameDice>, this, client);
                server.detach();
            }
            break;
        }
    }
}

void GameWindow::comeBackMainMenu()
{
    NotificationWindow *msg = new NotificationWindow;
    msg->setText("La partie est terminée, merci d'avoir joué. Vous pouvez bouger cette fenêtre pour regarder le jeu et ensuite appuyer sur 'Ok' pour retourner au menu.");
    msg->setWindowTitle("Partie terminée");
    msg->exec();
    QPoint current_pos = pos();
    closeWindow();
    auto main_window = new MainMenuWindow(this->player, current_pos, true);
    main_window->show();
}

void GameWindow::repaintGame()
{
    repaint();
}

Advertising::Advertising(QWidget* parent) : QLabel(parent)
{
    setMinimumSize(172, 599);
    setMaximumSize(172, 599);
    QMovie *gif_animation = new QMovie("resources/pub1.gif");
    setMovie(gif_animation);
    gif_animation->start();
}

void Advertising::enterEvent(QEvent *ev)
{
    char tmp;
    if (ev) tmp = 'a'; // Avoiding compiler warning
   QSound::play("resources/sounds/cest_qui_tous_ces_cons.wav");
}
