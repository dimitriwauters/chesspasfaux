// INFO-F209 - Groupe 5 - HelpWindow.hpp

#include "MenuWindow.hpp"

#ifndef HELPWINDOW_HPP
#define HELPWINDOW_HPP

class HelpWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QPushButton *chess_rules_btn;
        QPushButton *modes_rules_btn;
        QHBoxLayout *h_layout;
        QSpacerItem *v_spacer;
    public:
        HelpWindow(Player*, QPoint);
    public slots:
        void comeBack() override;
        void displayChessRules();
        void displayGamemodesRules();
};

#endif

////////////////////////////////////////////////////////////////////////////////
// RULES EXPLANATIONS WINDOWS
////////////////////////////////////////////////////////////////////////////////

#ifndef RULESWINDOW_HPP
#define RULESWINDOW_HPP

class RulesWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QTextEdit *text;
    public:
        RulesWindow(Player*, QString, QPoint);
    public slots:
        void comeBack() override;
};

#endif
