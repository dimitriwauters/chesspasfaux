// INFO-F209 - Groupe 5 - MenuWindow.hpp

#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFont>
#include <QFontDatabase>
#include <QLabel>
#include <QStyle>
#include <QDesktopWidget>
#include <QtMultimedia/QSound>
#include <QMouseEvent>
#include <QLabel>
#include <QSpacerItem>
#include <QMessageBox>
#include <QComboBox>
#include <QTableWidget>
#include <QHeaderView>
#include <QtWidgets>
#include <QTextEdit>
#include <iostream>
#include <string>
#include <fstream>
#include <QMouseEvent>
#include <QTimer>
#include <unistd.h>
#include <iostream>
#include <thread>
#include "../Network.hpp"
#include "../const.hpp"
#include "../Player.hpp"

////////////////////////////////////////////////////////////////////////////////
// GENERAL
////////////////////////////////////////////////////////////////////////////////

#ifndef WINDOW_HPP
#define WINDOW_HPP

class Window : public QWidget
{
        Q_OBJECT
    private:
        QPoint drag_position;
    protected:
        QPalette p;
        QFont menu_font;
        QPushButton *close_window_btn;
        Player* player = nullptr;
    public:
        Window(Player*, QWidget *parent = nullptr);
        void mouseMoveEvent(QMouseEvent *) override;
        void mousePressEvent(QMouseEvent *) override;
        void setBackground(QString);
   public slots:
        virtual void closeWindow();
        void closeApplication();
   signals:
        void serverDisconnected();
};

#endif

#ifndef MENUWINDOW_HPP
#define MENUWINDOW_HPP

class MenuWindow : public Window
{
        Q_OBJECT
    protected:
        QVBoxLayout *v_layout;
        QHBoxLayout *h_layout_nav_buttons;
        QPushButton *back_btn;
        QPushButton *quit_btn;
        QSpacerItem *h_spacer_nav_buttons;
    public:
        MenuWindow(Player*);
        virtual void setNavButtons();
        void chatThread();
    public slots:
        virtual void comeBack();
    signals:
        void receivedChat(QString, QString);
        void refreshFriends(QString);
};

#endif

#ifndef NOTIFICATIONWINDOW_HPP
#define NOTIFICATIONWINDOW_HPP

class NotificationWindow : public QMessageBox
{
    public:
        NotificationWindow();
};

#endif

#ifndef ADVWINDOW_HPP
#define ADVWINDOW_HPP

class AdvWindow : public Window
{
        Q_OBJECT
    protected:
		QVBoxLayout *v_layout;
        void mousePressEvent(QMouseEvent *) override;
    public:
        AdvWindow(Player*);
};

#endif

#ifndef SENDBOX_HPP
#define SENDBOX_HPP

class SendBox : public QTextEdit
{
        Q_OBJECT
    public:
        SendBox(int, int);
        void keyPressEvent(QKeyEvent *);
    signals:
        void enterPressed();
};

#endif

////////////////////////////////////////////////////////////////////////////////
// MAIN MENU
////////////////////////////////////////////////////////////////////////////////

#ifndef MAINMENUWINDOW_HPP
#define MAINMENUWINDOW_HPP

class MainMenuWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QPushButton *launch_game_btn;
        QPushButton *saved_game_btn;
        QPushButton *show_stats_btn;
        QPushButton *manage_friends_list_btn;
        QPushButton *open_chat_btn;
        QPushButton *help_btn;
        QPushButton *credits_btn;
        QPushButton *exit_btn;
        AdvWindow * advertising_tops;
    public:
        MainMenuWindow(Player*, QPoint, bool);
        void onButtonPressed();
        void playSoundMenu();
        void launchAdvertisingWindow();
    public slots:
        void launchGame();
        void launchSavedGame();
        void showStats();
        void manageFriendsList();
        void openChat();
        void getHelp();
        void showCredits();
};

#endif
