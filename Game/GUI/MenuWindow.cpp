// INFO-F209 - Groupe 5 - MenuWindow.cpp

#include "MenuWindow.hpp"
#include "ChatWindow.hpp"
#include "FriendsWindow.hpp"
#include "HelpWindow.hpp"
#include "SavedGamesWindow.hpp"
#include "StatsWindow.hpp"
#include "LaunchGameWindow.hpp"
#include "CreditsWindow.hpp"

////////////////////////////////////////////////////////////////////////////////
// GENERAL
////////////////////////////////////////////////////////////////////////////////

Window::Window(Player* player, QWidget *parent) : QWidget(parent, Qt::FramelessWindowHint | Qt::WindowSystemMenuHint), player(player)
{
    // Centering the window
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, this->size(), qApp->desktop()->availableGeometry()));
    // Adding a special font
    QFontDatabase::addApplicationFont("resources/fonts/Cinzel.otf");
    menu_font = QFont("Cinzel",16,QFont::Normal);
    // Set a custom cursor
    qApp->setOverrideCursor(QCursor(QPixmap("resources/cursor.png")));
    // Set the close button on the right upper corner
    close_window_btn = new QPushButton("", this);
    close_window_btn->setStyleSheet("background-image: url(resources/close.png); background-repeat:no-repeat; border:none;");
    QObject::connect(close_window_btn, SIGNAL(clicked()), this, SLOT(closeWindow()));
    QObject::connect(this, SIGNAL(serverDisconnected()), this, SLOT(closeApplication()));
}

void Window::setBackground(QString path_bg)
{
    p.setBrush(QPalette::Window, QBrush(QPixmap(path_bg)));
    this->setPalette(p);
}

void Window::closeWindow()
{
    this->close();
}

void Window::closeApplication()
{
    qDebug() << "END APP";
    if (player->getSocket()) {
        closeSocket(player->getSocket());
        player->setSocket(0);
    }
    if(player->getChatSocket()) {
        closeSocket(player->getChatSocket());
        player->setChatSocket(0);
    }
    NotificationWindow *msg = new NotificationWindow;
    msg->setText("La connexion avec le serveur a été perdue, l'application va se fermer.");
    msg->setWindowTitle("Connexion perdue");
    msg->exec();
    qApp->exit(1);
}

// Frameless window

void Window::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton)
    {
        move(event->globalPos() - drag_position);
        event->accept();
    }
}

void Window::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        drag_position = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
}

NotificationWindow::NotificationWindow() : QMessageBox()
{
    setStyleSheet("background-color: #140a0a; font: 12pt \"Georgia\"; color:#dbd8ca; padding: 5px; QPushButton{border: 1px solid #3a2929;}");
    setStandardButtons(QMessageBox::Ok);
}

MenuWindow::MenuWindow(Player* player) : Window(player)
{
    setFixedSize(800, 600);
    close_window_btn->setGeometry(770, 15, 14, 14);
    setBackground("resources/bg_menu_logo_only.jpg");
    v_layout = new QVBoxLayout(); // Container
    v_layout->setContentsMargins(100, 135, 100, 95);
    setLayout(v_layout);
}

void MenuWindow::setNavButtons()
{
    const QSize NAV_BUTTON_BACK = QSize(105, 42);
    back_btn = new QPushButton(COME_BACK, this);
    back_btn->setFont(menu_font);
    back_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;}  QPushButton:hover {border: 1px solid #441409;}");
    back_btn->setMinimumSize(NAV_BUTTON_BACK);
    h_spacer_nav_buttons = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
    h_layout_nav_buttons = new QHBoxLayout;
    h_layout_nav_buttons->addItem(h_spacer_nav_buttons);
    h_layout_nav_buttons->addWidget(back_btn);
    v_layout->addLayout(h_layout_nav_buttons);
    QObject::connect(back_btn, SIGNAL(clicked()), this, SLOT(comeBack()));
}

void MenuWindow::comeBack()
{
    closeWindow();
}

void MenuWindow::chatThread()
{
    std::string message;
    char buffer[MAXBUFSIZE];
    int opcode_first, opcode_second;
    while (this->player->getChatSocket() != 0) {
        memset(buffer, '\0', MAXBUFSIZE);
        usleep(50000);
        if (receiving_T(this->player->getChatSocket(), buffer)) {
            message = buffer;
            std::cout << "CHAT : " << message << std::endl;
            opcode_first = std::stoi(message.substr(0, 1));
            opcode_second = std::stoi(message.substr(1, 1));
            message = message.substr(3, message.size());
            switch (opcode_first) {
                case 3: { // CHAT
                    switch(opcode_second) {
                        case 2: {
                            emit MenuWindow::refreshFriends(QString::fromStdString(message));
                            break;
                        }
                        case 5: {
                            foreach (QWidget *widget, QApplication::topLevelWidgets()) {
                                if(widget->objectName() == "chat_window") {
                                    if(widget->isHidden()) { widget->show(); }


                                    long unsigned int pos = message.find(MSG_SEPARATOR);
                                    QString username = QString::fromStdString(message.substr(0, pos));
                                    emit MenuWindow::receivedChat(username, QString::fromStdString(message.substr(pos+1, message.size())));
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
        else { emit serverDisconnected(); break; }
    }
}

SendBox::SendBox(int width, int height): QTextEdit()
{
    setMinimumSize(width, height);
    setMaximumSize(width, height);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void SendBox::keyPressEvent(QKeyEvent *event)
{
    if (event->key()==Qt::Key_Return)
    {
        emit enterPressed();
        clear();
    }
    else
    {
       QTextEdit::keyPressEvent( event );
    }
}

////////////////////////////////////////////////////////////////////////////////
// MAIN MENU WINDOW
////////////////////////////////////////////////////////////////////////////////

MainMenuWindow::MainMenuWindow(Player* player, QPoint current_pos, bool first_time) : MenuWindow(player)
{
    move(current_pos);
    setBackground("resources/bg_menu_copyright.jpg");
    // BUTTONS
    // Creating buttons
    launch_game_btn = new QPushButton(MAIN_NEW_GAME, this);
    saved_game_btn = new QPushButton(MAIN_SAVED_GAME, this);
    show_stats_btn = new QPushButton(MAIN_STATS, this);
    manage_friends_list_btn = new QPushButton(MAIN_FRIENDS, this);
    open_chat_btn = new QPushButton(MAIN_CHAT, this);
    help_btn = new QPushButton(MAIN_HELP, this);
    credits_btn = new QPushButton(MAIN_CREDITS, this);
    exit_btn = new QPushButton(QUIT, this);
    // Adding buttons to the container's layout
    v_layout->addWidget(launch_game_btn);
    v_layout->addWidget(saved_game_btn);
    v_layout->addWidget(show_stats_btn);
    v_layout->addWidget(manage_friends_list_btn);
    v_layout->addWidget(open_chat_btn);
    v_layout->addWidget(help_btn);
    v_layout->addWidget(credits_btn);
    v_layout->addWidget(exit_btn);
    // Defining the style of buttons
    setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    launch_game_btn->setFont(menu_font);
    saved_game_btn->setFont(menu_font);
    show_stats_btn->setFont(menu_font);
    manage_friends_list_btn->setFont(menu_font);
    open_chat_btn->setFont(menu_font);
    help_btn->setFont(menu_font);
    credits_btn->setFont(menu_font);
    exit_btn->setFont(menu_font);
    const QSize BUTTON_SIZE = QSize(200, 42);
    launch_game_btn->setMinimumSize(BUTTON_SIZE);
    saved_game_btn->setMinimumSize(BUTTON_SIZE);
    show_stats_btn->setMinimumSize(BUTTON_SIZE);
    manage_friends_list_btn->setMinimumSize(BUTTON_SIZE);
    open_chat_btn->setMinimumSize(BUTTON_SIZE);
    help_btn->setMinimumSize(BUTTON_SIZE);
    credits_btn->setMinimumSize(BUTTON_SIZE);
    exit_btn->setMinimumSize(BUTTON_SIZE);
    // Functions (signals and slots)
    QObject::connect(launch_game_btn, SIGNAL(clicked()), this, SLOT(launchGame()));
    QObject::connect(saved_game_btn, SIGNAL(clicked()), this, SLOT(launchSavedGame()));
    QObject::connect(show_stats_btn, SIGNAL(clicked()), this, SLOT(showStats()));
    QObject::connect(manage_friends_list_btn, SIGNAL(clicked()), this, SLOT(manageFriendsList()));
    QObject::connect(open_chat_btn, SIGNAL(clicked()), this, SLOT(openChat()));
    QObject::connect(help_btn, SIGNAL(clicked()), this, SLOT(getHelp()));
    QObject::connect(credits_btn, SIGNAL(clicked()), this, SLOT(showCredits()));
    QObject::connect(exit_btn, SIGNAL(clicked()), qApp, SLOT(quit()));
    // Advertising window
    if (first_time)
    {
        advertising_tops = new AdvWindow(player);
        QTimer::singleShot(800, advertising_tops, SLOT(show()));
        QTimer::singleShot(11000, advertising_tops, SLOT(close()));
    }
}

void MainMenuWindow::playSoundMenu()
{
    QSound::play("resources/sounds/cest_pas_faux.wav");
}

void MainMenuWindow::onButtonPressed()
{
    playSoundMenu();
    closeWindow();
}

// Functions

void MainMenuWindow::launchGame()
{
    QPoint current_pos = pos();
    onButtonPressed();
    GamemodesWindow *gamemodes_window = new GamemodesWindow(this->player, current_pos);
    gamemodes_window->show();
}

void MainMenuWindow::launchSavedGame()
{
    QPoint current_pos = pos();
    onButtonPressed();
    SavedGamesWindow *saved_games_window = new SavedGamesWindow(this->player, current_pos);
    saved_games_window->show();
}

void MainMenuWindow::showStats()
{
    QPoint current_pos = pos();
    onButtonPressed();
    StatsWindow *stats_window = new StatsWindow(this->player, current_pos);
    stats_window->show();
}

void MainMenuWindow::manageFriendsList()
{
    QPoint current_pos = pos();
    onButtonPressed();
    FriendsListWindow *friends_window = new FriendsListWindow(this->player, current_pos);
    friends_window->show();
}

void MainMenuWindow::openChat()
{
    QPoint current_pos = pos();
    playSoundMenu();
    foreach (QWidget *widget, QApplication::topLevelWidgets()) {
        if(widget->objectName() == "chat_window") {
            widget->move(current_pos);
            widget->show();
        }
    }
}

void MainMenuWindow::getHelp()
{
    QPoint current_pos = pos();
    onButtonPressed();
    HelpWindow *help_window = new HelpWindow(this->player, current_pos);
    help_window->show();
}

void MainMenuWindow::showCredits()
{
    QPoint current_pos = pos();
    onButtonPressed();
    CreditsWindow *credits_window = new CreditsWindow(this->player, current_pos);
    credits_window->show();
}

////////////////////////////////////////////////////////////////////////////////
// ADVERTISING WINDOW
////////////////////////////////////////////////////////////////////////////////

AdvWindow::AdvWindow(Player* player) : Window(player)
{
    setFixedSize(400, 400);
    close_window_btn->hide();
    setBackground("resources/adv_tops.png");
    setWindowModality(Qt::ApplicationModal);
}

void AdvWindow::mousePressEvent(QMouseEvent * event)
{
   QSound::play("resources/sounds/le_gras.wav");
   Window::mousePressEvent(event);
}
