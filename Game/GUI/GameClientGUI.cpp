#include "GameClientGUI.hpp"

template <typename B, typename C>
GameClientGUI<B, C>::GameClientGUI(Player* white_player, Player* black_player, int socket, int start, int type, int mode, QGridLayout* chessboard, \
QVBoxLayout* sidepane, QGridLayout* chessboard_bottom, std::vector<std::string> pieces) : B(white_player, black_player, type), serverfd(socket), \
left_sidepane_layout(sidepane), chessboard_layout(chessboard), infos_game_layout(chessboard_bottom), gamemode(mode)
{
    if(!pieces.empty()) {
        this->pieces_beginning = pieces;
    }
    this->my_player = this->player[start];
	this->other_player = this->player[!start];
	createBoard();
	usleep(500);	//Wait for gameServer to launch (dice mode) / microseconds
	std::thread run_game(&GameClientGUI<B, C>::run, this);
	run_game.detach();
	sleep(1);
}

template <typename B, typename C>
GameClientGUI<B, C>::~GameClientGUI() = default;

template <typename B, typename C>
void GameClientGUI<B, C>::createBoard()
{
    this->board = new C(chessboard_layout);
    B::setPiecesOnBoard();
    this->rearrangeBoard();
    timer = new TimerGUI(this->player[WHITE], this->player[BLACK], TIMER_BLITZ, &this->turn, infos_game_layout);
    terminal = new TerminalGUI(this->my_player, this->other_player, this->serverfd, left_sidepane_layout);
    player_turn = new QLabel();
    QFontDatabase::addApplicationFont("resources/fonts/Cinzel.otf");
    QFont menu_font = QFont("Cinzel",16,QFont::Normal);
    player_turn->setFont(menu_font);
    player_turn->setAlignment(Qt::AlignCenter);
    player_turn->setStyleSheet("QLabel {color:#dbd8ca; padding-top:20px;}");
    infos_game_layout->addWidget(player_turn, 3, 0, 1, 2, Qt::AlignCenter);
    srand((int)time(0));
}

template <typename B, typename C>
bool GameClientGUI<B, C>::itsMyTurn() {
    return (this->my_player->getColor() == this->turn);
}

template <typename B, typename C>
void GameClientGUI<B, C>::run()
{
    std::pair<int, int> dice;
    while (this->notOver()) {

        if(this->gamemode == DICE_GAMEMODE) diceChessCheck(dice);
		if(itsMyTurn() && !isPreMovesEmpty() && !premoveDone) doPreMove();	//PreMoves

			//Post
        timer->refreshTimer();
        displayTurn();
        askEndGame();
        if(last_coord.first != 9) doPromotion(last_coord);
        interpretSignalTerminal();
        usleep(10000);
    }
    finishGame();
}

template <typename B, typename C>
void GameClientGUI<B, C>::finishGame()
{
    terminal->write(DASHES);
    if(this->no_captured_pieces >= MAX_TURNS || (this->isInStalemate(this->turn) && \
    (!this->getCheck(this->turn) || !this->getCheck(!this->turn))) || !this->running_game) {
        terminal->write(TERMINAL_DRAWN_MATCH);
    }
    else this->terminal->write(TERMINAL_WINNER + this->getWinner()->getUsername() + TERMINAL_CONGRATULATIONS);
    terminal->write(TERMINAL_GAME_OVER);

    this->my_player->resetGamePlayer();
	this->other_player->resetGamePlayer();

    //this->board->removeAllPieces();
    delete this->board;
    this->gameFinished();
}

template <typename B, typename C>
void GameClientGUI<B, C>::moveSelectedPiece(ChessSquare* src, ChessSquare* dest)
{
    unsigned canMove = this->checkMove(src->getCoord(), dest->getCoord(), this->turn);
    if(canMove)
    {
        if(itsMyTurn())	{
            sending_T(serverfd, prepForSending(110, this->pairToString(src->getCoord(), dest->getCoord())));
            playMoveSound();
            if((this->gamemode == DICE_GAMEMODE) && (this->type == REALTIME_ON)) diceTurn[this->turn] = true;
        }
        else //Add preMove
            addPreMove(std::make_pair(src->getCoord(), dest->getCoord()));
    }
    else this->board->refreshDisplay();
}

template <typename B, typename C>
void GameClientGUI<B, C>::playMoveSound()
{
        QSound::play("resources/sounds/move.wav");
}

template <typename B, typename C>
void GameClientGUI<B, C>::clickDetected(ChessSquare* square)
{
    if(!game_freeze) {
        std::pair<int, int> piece = square->getPiece();
        if(!this->board->getSelectedSquare() && (piece.second == this->my_player->getColor())) {
            square->setBackground(true, "resources/selected_square.png");
            this->board->setSelectedSquare(square);
            std::vector<std::pair<int, int>> moves = this->getAllMoves(square->getCoord(), this->turn);
            if(!moves.empty()) {
                for(unsigned i = 0; i < moves.size(); ++i) {
                    ChessSquare* square_move = this->board->getSquare(moves.at(i));
                    if(square_move) {
                        square_move->setBackground(true, "resources/square_move.png");
                    }
                }
            }
        }
        else if (this->board->getSelectedSquare()) {
            moveSelectedPiece(this->board->getSelectedSquare(), square);
            this->board->setSelectedSquare(nullptr);
            last_coord.first = square->getCoord().first;
            last_coord.second = square->getCoord().second;
        }
    }
}

//////////////////////////////////////////
// SERVER
//////////////////////////////////////////

template <typename B, typename C>
void GameClientGUI<B, C>::messageFromServer(int opcode, std::string buffer)
{
    switch(opcode) {
        case 1: { // Move
            messageMove(buffer);
            break;
        }
        case 2: { // Give up
            if(buffer == "drawn") { this->running_game = false; }
            else { this->my_player->hasWon(); }
            break;
        }
        case 3: { // Set winner
            int winner = std::stoi(buffer.substr(0, 1));
            this->setWinner(winner);
            break;
        }
        case 4: { // Promotion
            messagePromotion(buffer);
            break;
        }
        case 5: { // SwapTurn
			this->swapTurn();
			if(this->gamemode == DICE_GAMEMODE) diceTurn[this->turn] = true;
            break;
        }
        case 7: { // Set the dices
            std::pair<int, int> new_dices;
            new_dices.first = std::stoi(buffer.substr(0, 1));
            new_dices.second = std::stoi(buffer.substr(1, 1));
            this->setDices(new_dices);
            break;
        }
        case 8: { // Save game
            messageSave(buffer);
            break;
        }
        case 33: {
            terminal->write(buffer);
            break;
        }
    }
}

template <typename B, typename C>
void GameClientGUI<B, C>::messageMove(std::string buffer)
{
    std::pair<std::pair<int, int>, std::pair<int, int>> fromBuffer = this->stringToPair(const_cast<char*>(buffer.c_str()));
    std::pair<int, int> src_pos = fromBuffer.first;
    std::pair<int, int> dest_pos = fromBuffer.second;
    unsigned canMove = this->checkMove(src_pos, dest_pos, this->turn);
    if(canMove){
        bool colorPiece = this->board->getColorPiece(src_pos);
        this->movePiece(src_pos, dest_pos, canMove);
        //emit move();
        //Terminal
        if(this->gamemode != DARK_GAMEMODE){
            terminal->write("* " + this->board->getTypePiece(dest_pos) + TERMINAL_MOVE_FROM + this->getChessColumn(src_pos.second) + this->getChessRank(src_pos.first) + TERMINAL_MOVE_TO +\
			this->getChessColumn(dest_pos.second) + this->getChessRank(dest_pos.first) + BY + this->player[colorPiece]->getUsername());
        }
        else terminal->write("* " DARK_SQUARE TERMINAL_MOVE_FROM DARK_SQUARE TERMINAL_MOVE_TO DARK_SQUARE BY +this->player[colorPiece]->getUsername());
        //Special moves
        if (canMove == MOVE_ALLOWED || canMove == MOVE_EN_PASSANT) {
            std::pair<int, int> cell = std::make_pair(src_pos.first, dest_pos.second);
            if (this->compareLastMove(cell) && (this->board->getTypePiece(cell) == PAWN) &&
                ((this->board->getTypePiece(dest_pos)) == PAWN) && (dest_pos.first == 6 || dest_pos.first == 1))
                this->board->removePiece(cell);	//En passant
            this->setLastMove(dest_pos);
        }
        else if(canMove == MOVE_PROMOTION) checkPromotion(dest_pos);
        if(!game_freeze){
            this->checkWinConditions(this->turn);
            if(this->type == REALTIME_OFF)
				this->swapTurn();
        }
        this->rearrangeBoard();
        showKingInCheck();
        showBlockedPieces();
        if((this->gamemode == DICE_GAMEMODE) && (this->type == REALTIME_OFF)) diceTurn[this->turn] = true;
    }
    premoveDone = false;
}

template <typename B, typename C>
void GameClientGUI<B, C>::messagePromotion(std::string buffer)
{
    std::string coord = buffer.substr(0, 4);
    int piece = std::stoi(buffer.substr(4, 1));
    std::pair<std::pair<int, int>, std::pair<int, int>> fromBuffer = this->stringToPair(const_cast<char*>(coord.c_str()));
    std::pair<int, int> dest_pos = fromBuffer.second;
    if (this->board->getColorPiece(dest_pos) == WHITE && dest_pos.first == 0) this->promote(dest_pos, this->board->getColorPiece(dest_pos), piece);
    else if (this->board->getColorPiece(dest_pos) == BLACK && dest_pos.first == MAX_ROWS-1) this->promote(dest_pos, this->board->getColorPiece(dest_pos), piece);
    B::checkWinConditions(this->turn);
    game_freeze = false;
    if(this->type == REALTIME_OFF) {
		this->swapTurn();
		if(this->gamemode == DICE_GAMEMODE) diceTurn[this->turn] = true;
	}
}

template <typename B, typename C>
void GameClientGUI<B, C>::messageSave(std::string buffer)
{
    if(buffer.empty()) {
        game_freeze = true;
        terminal->write(TERMINAL_ASK_SAVE);
        terminal->setPrefixCommand(TERMINAL_SAVE_OTHER);
    }
    else if (buffer == "true") {
        terminal->write(TERMINAL_SAVE_SAVED);
        this->running_game = false;
    }
    else if (buffer == "false") {
        terminal->write(TERMINAL_SAVE_NOT_SAVED);
        game_freeze = false;
    }
}

//////////////////////////////////////////
// OTHER
//////////////////////////////////////////

template <typename B, typename C>
void GameClientGUI<B, C>::showKingInCheck() {
    if(this->getCheck(this->turn)) {
        this->board->getSquare(this->getKingPosition(this->turn))->setBackground(true, "resources/check_square.png");
    }
}

template <typename B, typename C>
void GameClientGUI<B, C>::showBlockedPieces() {}

template <typename B, typename C>
void GameClientGUI<B, C>::askEndGame()
{
    if (this->no_captured_pieces >= MAX_TURNS && !game_freeze)
    {
        terminal->write(TERMINAL_ASK_ENDGAME);
        terminal->setPrefixCommand("quit_null");
        game_freeze = true;
    }
}

template <typename B, typename C>
void GameClientGUI<B, C>::displayTurn()
{
    QString playername, color;
    if(itsMyTurn()) {
        player_turn->setText("À vous de jouer !");
    }
    else
    {
        player_turn->setText("");
    }
}

//////////////////////////////////////////
// PROMOTION
//////////////////////////////////////////

template <typename B, typename C>
void GameClientGUI<B, C>::doPromotion(std::pair<int, int> coord)
{
    if((this->board->getTypePiece(coord) == PAWN) && (coord.first == 0 || coord.first == 7) && !game_freeze && getCanPromote())
    {
        int piece = getPromotionPiece();
        sending_T(serverfd, prepForSending(140, this->pairToString(coord, coord) + std::to_string(piece)));
        setCanPromote(false);
    }
}

template <typename B, typename C>
void GameClientGUI<B, C>::setPromotionPiece(int piece)
{
    promotionPiece = piece;
}

template <typename B, typename C>
int GameClientGUI<B, C>::getPromotionPiece()
{
    return promotionPiece;
}

template <typename B, typename C>
void GameClientGUI<B, C>::setCanPromote(bool can)
{
    canPromote = can;
}

template <typename B, typename C>
bool GameClientGUI<B, C>::getCanPromote()
{
    return canPromote;
}

template <typename B, typename C>
void GameClientGUI<B, C>::checkPromotion(std::pair<int, int> coord)
{
    game_freeze = true;
    if((this->board->getTypePiece(coord) == PAWN) && (coord.first == 0 || coord.first == 7) && itsMyTurn()) {
        std::pair<int, int> king = this->getKingPosition(!this->turn);
        if((king.first == coord.first && king.second == coord.second) && this->gamemode == DARK_GAMEMODE) {
            sending_T(serverfd, prepForSending(130, std::to_string(this->turn)));
            this->setWinner(this->turn);
        }
        else {
            terminal->write(TERMINAL_PROMOTION_WRITE_CHOICE);
            terminal->write(TERMINAL_PROMOTION_CHOICE);
            terminal->write(TERMINAL_PROMOTION);
            setCanPromote(true);
        }
    }
}

//////////////////////////////////////////
// DICE CHESS
//////////////////////////////////////////

template <typename B, typename C>
void GameClientGUI<B, C>::diceChessCheck(std::pair<int, int> dice)
{
	if(itsMyTurn() && diceTurn[this->turn]) {
		terminal->write(TERMINAL_DICECHESS_ROLL);
		sending_T(serverfd, prepForSending(170, "")); //Server RollTheDices
		do{
			usleep(500000);	//Wait for server to set dices.
			dice = this->getDices();
		}while(dice.first == 0 && dice.second == 0);
		terminal->write(TERMINAL_DICECHESS_RESULT + std::to_string(dice.first) + AND + std::to_string(dice.second));
        displayDicePieces(dice.first, dice.second);
		diceTurn[this->turn] = false;
		if(this->isInStalemate(this->turn)) { //No moves allowed
			diceTurn[this->turn] = true;
			if(this->type == REALTIME_OFF) {
				terminal->write(TERMINAL_DICECHESS_PASSTURN);
				sending_T(serverfd, prepForSending(150, "")); //Server SwapTurn
				this->swapTurn();
			}
			else
				terminal->write(TERMINAL_DICECHESS_REROLL);
		}
	}
}

template <typename B, typename C>
void GameClientGUI<B, C>::displayDicePieces(int first, int second)
{
	std::string diceOne, diceTwo;
	if(first == second)
		terminal->write(TERMINAL_DICECHESS_DOUBLES);
	else {
		switch(first) {
			case 1: {diceOne = A_PAWN; break;}
			case 2: {diceOne = A_KNIGHT; break;}
			case 3: {diceOne = A_BISHOP; break;}
			case 4: {diceOne = A_ROOK; break;}
			case 5: {diceOne = YOUR_QUEEN; break;}
			case 6: {diceOne = YOUR_KING; break;}
		}
		switch(second) {
			case 1: {diceTwo = A_PAWN; break;}
			case 2: {diceTwo = A_KNIGHT; break;}
			case 3: {diceTwo = A_BISHOP; break;}
			case 4: {diceTwo = A_ROOK; break;}
			case 5: {diceTwo = YOUR_QUEEN; break;}
			case 6: {diceTwo = YOUR_KING; break;}
		}
		terminal->write(TERMINAL_DICECHESS_ALLOWED_MOVES + diceOne + " ou " + diceTwo);
	}
}

//////////////////////////////////////////
// PREMOVES
//////////////////////////////////////////

template <typename B, typename C>
void GameClientGUI<B, C>::addPreMove(std::pair<std::pair<int, int>, std::pair<int, int>> newMove)
{
    if(preMoves.size() < 5) {
        preMoves.insert(preMoves.begin(), newMove);
        terminal->write(TERMINAL_PREMOVE_ADDED + std::to_string((5-preMoves.size())) + TERMINAL_PREMOVE_MORE);
    }
    else {
        terminal->write(TERMINAL_PREMOVE_NO_MORE);
    }
    this->board->refreshDisplay();
}

template <typename B, typename C>
void GameClientGUI<B, C>::cancelPreMoves()
{
    preMoves.clear();
}

template <typename B, typename C>
std::pair<std::pair<int, int>, std::pair<int, int>> GameClientGUI<B, C>::getPreMove()
{
    std::pair<std::pair<int, int>, std::pair<int, int>> move = preMoves.back();
    preMoves.pop_back();
    return move;
}

template <typename B, typename C>
bool GameClientGUI<B, C>::isPreMovesEmpty()
{
    return preMoves.empty();
}

template <typename B, typename C>
void GameClientGUI<B, C>::doPreMove()
{
    std::pair<std::pair<int, int>, std::pair<int, int>> move = getPreMove();
    unsigned canMove = this->checkMove(move.first, move.second, this->turn);
    if(canMove) {
        terminal->write(TERMINAL_PREMOVE_DO);
        sending_T(serverfd, prepForSending(110, this->pairToString(move.first, move.second)));
    }
    else {
        terminal->write(TERMINAL_PREMOVE_NOT_ALLOWED);
        cancelPreMoves();
    }
    premoveDone = true;
}

//////////////////////////////////////////
// SIGNALS
//////////////////////////////////////////

template <typename B, typename C>
void GameClientGUI<B, C>::interpretSignalTerminal() {
    int signal = terminal->getSignal();
    if(signal != 0) {
        if (signal == -2) { game_freeze = false; this->no_captured_pieces = 0; }
        else if (signal == -1) game_freeze = false;
        else if (signal == 8) { this->running_game = false; sending_T(serverfd, prepForSending(120, "drawn")); }// Match nul
        else if (signal == 9) { this->other_player->hasWon(); sending_T(serverfd, prepForSending(120, "")); }  // Abandon
        else if (signal == 10) { setPromotionPiece(QUEEN_NBR); game_freeze = false;}
        else if (signal == 11) { setPromotionPiece(KNIGHT_NBR); game_freeze = false;}
        else if (signal == 12) { setPromotionPiece(ROOK_NBR); game_freeze = false;}
        else if (signal == 13) { setPromotionPiece(BISHOP_NBR); game_freeze = false;}
        else if (signal == 14) {
            if(this->type == REALTIME_OFF) {
                terminal->write(REALTIME_WAITING);
                game_freeze = true;
                sending_T(serverfd, prepForSending(180, "ask"));
            }
            else {
                terminal->write(REALTIME_CANT_SAVE);
            }
        }
        else if (signal == 15) { sending_T(serverfd, prepForSending(180, "true")); this->running_game = false; }
        else if (signal == 16) { sending_T(serverfd, prepForSending(180, "false")); game_freeze = false; }
    }
}

template <typename B, typename C>
void GameClientGUI<B, C>::saveGame()
{
    terminal->getCommand("sauvegarder");
}

template <typename B, typename C>
void GameClientGUI<B, C>::resignGame()
{
    terminal->getCommand("quitter");
}
