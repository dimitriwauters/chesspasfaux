// INFO-F209 - Groupe 5 - HelpWindow.cpp

#include "HelpWindow.hpp"

HelpWindow::HelpWindow(Player* player, QPoint current_pos) : MenuWindow(player)
{
    move(current_pos);
    v_layout->setContentsMargins(50, 180, 50, 50);
    chess_rules_btn = new QPushButton(HELP_CHESS_RULES, this);
    modes_rules_btn = new QPushButton(HELP_GAMEMODES, this);
    h_layout = new QHBoxLayout;
    h_layout->addWidget(chess_rules_btn);
    h_layout->addWidget(modes_rules_btn);
    h_layout->setSpacing(20);
    v_layout->addLayout(h_layout);
    setStyleSheet("QPushButton {background-image: url(resources/select_mode_bg.png); background-repeat:no-repeat; border:none;color: #ece6ac;} QPushButton:hover {background-image: url(resources/select_mode_bg_hover.png);  background-repeat:no-repeat; border:none;color: #f0eed8;}");
    chess_rules_btn->setFont(menu_font);
    modes_rules_btn->setFont(menu_font);
    const QSize GAMEMODE_BUTTON_SIZE = QSize(341, 244);
    chess_rules_btn->setMinimumSize(GAMEMODE_BUTTON_SIZE); // Set buttons size
    modes_rules_btn->setMinimumSize(GAMEMODE_BUTTON_SIZE);
    // Functions (signals and slots)
    QObject::connect(chess_rules_btn, SIGNAL(clicked()), this, SLOT(displayChessRules()));
    QObject::connect(modes_rules_btn, SIGNAL(pressed()), this, SLOT(displayGamemodesRules()));
    v_spacer = new QSpacerItem(0, 300, QSizePolicy::Expanding, QSizePolicy::Minimum);
    v_layout->addItem(v_spacer);
    setNavButtons();
}

void HelpWindow::comeBack()
{
    QPoint current_pos = pos();
    this->close();
    MainMenuWindow *main_menu_window = new MainMenuWindow(this->player, current_pos, false);
    main_menu_window->show();
}

void HelpWindow::displayChessRules()
{
    QString rules =
            "<h1>Jeu d'échecs</h1>"
            "<p>Le jeu d’échecs traditionnel est un jeu de plateau en tour par tour opposant deux joueurs. Chaque "
            "joueur possède 16 pièces disposées sur un plateau de 64 cases appelé échiquier. L’un possède des pièces "
            "blanches, et commence toujours la partie, l’autre possède des pièces noires. Il existe 6 types de pièces, "
            "caractérisées par un déplacement spécifique : le roi (1 pièce par joueur), la reine (1 pièce par joueur), "
            "le fou (2 pièces par joueur), le cavalier (2 pièces par joueur), la tour (2 pièces par joueur) et le pion (8 "
            "pièces par joueur).</p>"
            "<p>Lors de chaque tour, un joueur (sélectionné alternativement) doit déplacer une de ses pièces sur "
            "l’échiquier. Si la pièce déplacée par le joueur se trouve sur la même case qu’une pièce du joueur "
            "adverse, alors celle-ci est capturée et est retirée de l’échiquier. Le but du jeu est de capturer le roi de "
            "l’adversaire sans que son propre roi ne soit menacé et sans issue possible (c’est-à-dire en échec et mat). "
            "Pour cela, le jeu se déroule en tour par tour jusqu’à ce qu’il y ait un gagnant ou que la partie soit "
            "nulle, un tour de jeu correspondant au déplacement d’une seule des pièces restantes au joueur.</p>"


            "<h2>Règles du jeu d'échecs</h2>"
            "<h3>Échiquier</h3>"

            "<p>Un échiquier ou plateau de jeu est constitué de 8 colonnes, nommées de a à h, ainsi que de 8 lignes, "
            "numérotées de 1 jusque 8. Chaque case alterne une couleur claire et une couleur foncée. De facto, les "
            "diagonales sont toutes soit complètement claires, soit complètement foncées.</p>"

            "<h3>Pièces</h3>"

            "<h4>Placement initial</h4>"

            "<p>Les pièces du joueur blanc sont placées respectivement de gauche à droite sur la première ligne de "
            "l’échiquier : Une tour, un cavalier, un fou, sa reine, son roi, un fou, un cavalier et une tour. Sur la "
            "deuxième ligne de l’échiquier sont placés les 8 pions. Du côté du joueur noir, ses pièces sont placées de la "
            "même manière que son adversaire à la seule différence que le roi et la reine ont leurs places échangées. "
            "Sur l’échiquier les deux rois se trouvent donc sur la même colonne « e » et la colonne « d » pour les "
            "reines.</p>"

            "<h4>Types</h4>"
            "<p>Ce qui différencie les pièces les unes des autres est leur faculté à avoir chacune un déplacement qui "
            "leur est propre. Nous les définirons donc par les règles de déplacement qui les caractérisent.</p>"

            "<h5>Pion</h5>"
            "<p>Le pion ne peut avancer que vers l’avant, d’une case à la fois. À l’exception de son premier mouvement, "
            "où il peut être déplacé soit d’une case, soit de deux. Voir règle « prise en passant ». Contrairement "
            "à toutes les autres pièces, le pion ne prend pas une pièce dans le sens de son déplacement. En effet, "
            "quand le pion prend une pièce adverse, il le fait en diagonale, et non en ligne droite.</p>"
            "<h5>Tour</h5>"
            "<p>La tour ne bouge qu’en suivant les lignes et colonnes du plateau. Elle peut être déplacée d’autant "
            "de cases que le joueur souhaite.</p>"
            "<h5>Cavalier</h5>"
            "<p>Le cavalier possède le déplacement le plus particulier, c’est le seul qui puisse « sauter » au dessus "
            "des autres pièces. Il peut se déplacer de 3 cases mais uniquement en forme de « L » : de deux cases "
            "dans une direction verticale ou horizontale, ensuite d’une seule case dans l’autre.</p>"

            "<h5>Fou</h5>"
            "<p>Le fou se meut uniquement en diagonale, il reste donc toujours sur la même couleur de case du "
            "plateau. À l’instar de la tour, il peut être avancé d’autant de cases que l’on veut.</p>"
            "<h5>Reine</h5>"
            "<p>La reine cumule les facultés de déplacement de la tour et du fou. Elle peut se déplacer d’autant de "
            "cases souhaitées aussi bien en diagonale qu’en ligne droite.</p>"
            "<h5>Roi</h5>"
            "<p>Le roi ne peut se déplacer que d’une case à la fois, et ce dans tous les sens. Il possède néanmoins un "
            "déplacement spécial, nommé le roque.</p>"

            "<h3>Coups spéciaux</h3>"
            "<h4>Le roque</h4>"
            "<p>Le roque consiste à faire avancer exceptionnellement le roi de deux cases vers la tour et de passer "
            "cette dernière de l’autre côté du roi.</p>"
            "<p>Il existe deux roques différents : le petit roque et le grand roque. Le petit roque se fait du côté de la "
            "tour la plus proche et le grand, du côté de la tour la plus éloignée du roi.</p>"
            "<p>Mais on ne peut pas roquer dans 4 conditions :</p>"
            "<ol><li>si le roi ou la tour a déjà bougé ;</li>"
                "<li>s’il y a une pièce sur la case d’arrivée du roi ou de la tour ;</li>"
                "<li>s’il y a une pièce sur une case de passage du roi ou de la tour ;</li>"
                "<li>si le roi passe ou arrive sur une case contrôlée par une pièce adverse</li></ol>"
            "<h4>La promotion</h4>"
            "<p>La promotion désigne un échange d’un pion contre, au choix, une dame, une tour, un cavalier ou un "
            "fou. Pour cela, il faut amener le pion sur la rangée no8 pour les blancs et sur la rangée no1 pour les "
            "noirs.</p>"
            "<h4>La prise en passant</h4>"
            "<p>La prise en passant est un coup de pion. Quand un pion est sur la 5e rangée pour les blancs ou sur "
            "la 4e rangée pour les noirs, et qu’un pion adverse n’ayant pas encore bougé avance de deux cases pour "
            "se mettre à côté du pion allié, il peut exécuter une prise en passant : il peut prendre le pion adverse "
            "comme s’il n’avait avancé que d’une case.</p>"
            "<p>Attention : Un pion ne peut faire la prise qu’en passant immédiatement après l’avancée de deux "
            "cases du pion adverse. S’il attend un tour, il ne pourra plus la faire.</p>"

            "<h3>Condition de victoire</h3>"

            "<p>Un joueur gagne la partie s’il capture le roi de son adversaire. Tout d’abord, un joueur est en « échec "
            "» si son roi est menacé d’être pris par une pièce adverse. Le joueur en échec est obligé de bouger son "
            "roi ou l’une de ses autres pièces afin d’empêcher son roi d’être pris. Si le joueur ne peut pas empêcher "
            "son roi d’être pris, on dit alors qu’il est « échec et mat » et son adversaire gagne la partie. Un joueur "
            "peut également gagner la partie si son adversaire abandonne.</p>"

            "<h4>Partie nulle</h4>"
            "<p>Dans plusieurs cas une partie peut être nulle et aucun des deux joueurs ne gagne la partie :</p>"
            "<ul>"
            "<li>Si l’un des deux joueurs ne peut faire de déplacement valide, on dit alors qu’il y a « pat » ;</li>"
            "<li>S’il est impossible de faire un échec et mat à l’un ou l’autre des joueurs (par manque de pièces) ;</li>"
            "<li>S’il y a répétition de coups potentiellement infinie et qu’aucun échec et mat n’est fait</li>"
            "</ul>"

            "<h3>Premoves</h3>"
            "<p>Lorsque ce n'est pas votre tour, il est possible d'enregistrer des mouvements à l'avance, appelés "
            "« premoves ». Le coup sera joué automatiquement dès que votre tour commence et ainsi vous gagnerez du temps. "
            "Vous pouvez enregistrer jusqu'à 5 premoves qui seront joué dans l'ordre enregistré. "
            "Cependant, un premove ne sera pas effectué si le coup enregistré n'est plus valide, et tous les premoves "
            " enregistrés seront annulés."
            "</ul>";

    QPoint current_pos = pos();
    this->close();
    RulesWindow *chess_rules_window = new RulesWindow(this->player, rules, current_pos);
    chess_rules_window->show();
}

void HelpWindow::displayGamemodesRules()
{
    QString rules =
            "<h1>Modes de jeu</h1>"
            "<p>Le programme offrira la possibilité de jouer à 4 types de jeu d’échecs différents : le jeu d’échecs "
            "traditionnel, Dark Chess, Dice Chess et Horde Chess. Ces trois derniers types de jeu se basent en grande "
            "partie sur le fonctionnement du jeu d’échecs traditionnel, que nous allons rappeler ici brièvement.</p>"

            "<h2>Jeu d'échecs traditionnel</h2>"

            "<p>Le jeu d’échecs traditionnel est un jeu de plateau en tour par tour opposant deux joueurs. Chaque "
            "joueur possède 16 pièces disposées sur un plateau de 64 cases appelé échiquier. L’un possède des pièces "
            "blanches, et commence toujours la partie, l’autre possède des pièces noires. Il existe 6 types de pièces, "
            "caractérisées par un déplacement spécifique : le roi (1 pièce par joueur), la reine (1 pièce par joueur), "
            "le fou (2 pièces par joueur), le cavalier (2 pièces par joueur), la tour (2 pièces par joueur) et le pion (8 "
            "pièces par joueur).</p>"
            "<p>Lors de chaque tour, un joueur (sélectionné alternativement) doit déplacer une de ses pièces sur "
            "l’échiquier. Si la pièce déplacée par le joueur se trouve sur la même case qu’une pièce du joueur "
            "adverse, alors celle-ci est capturée et est retirée de l’échiquier. Le but du jeu est de capturer le roi de "
            "l’adversaire sans que son propre roi ne soit menacé et sans issue possible (c’est-à-dire en échec et mat). "
            "Pour cela, le jeu se déroule en tour par tour jusqu’à ce qu’il y ait un gagnant ou que la partie soit "
            "nulle, un tour de jeu correspondant au déplacement d’une seule des pièces restantes au joueur.</p>"
            "<p>Une description plus détaillée du jeu d’échecs et de ses règles se trouve dans la rubrique éponyme.</p>"

            "<h2>Horde Chess</h2>"

            "<p>Dans ce type de jeu, la distribution et le positionnement des pièces diffèrent : le joueur noir commence "
            "la partie avec 32 pions tandis que le joueur blanc possède un ensemble « classique » de pièces (1 roi, 1 "
            "reine, 2 cavaliers, etc.). Le but du joueur blanc est de capturer l’ensemble des pions adverses, l’objectif "
            "du joueur noir étant tout simplement de mettre en échec et mat son adversaire.</p>"
            "<p>Le positionnement des pièces du joueur ayant le set habituel est le même que celui du mode traditionnel. Les 32 pions adverses sont répartis sur les 4 lignes opposées à l’exception des pions des 2 "
            "colonnes « d » et « e » qui sont avancés d’une case.</p>"
            "<p>Hormis ces particularités, Horde Chess se joue comme un jeu d’échecs traditionnel : les règles normales s’appliquent.</p>"

            "<h2>Dark Chess</h2>"

            "<p>Dans la variante de jeu nommée Dark Chess, chaque joueur ne peut voir que les cases, et donc les "
            "pièces qui s’y trouvent, qui sont à portée des siennes. Cela signifie que seules les cases où ses pièces "
            "peuvent se déplacer lui sont connues. Le joueur n’a donc qu’une vue partielle de l’échiquier et du "
            "positionnement des pièces adverses. De facto, les règles quant à la mise en échec ne s’appliquent pas : "
            "la capture du roi est possible, et c’est celle-ci qui conditionne la victoire de la partie.</p>"
            "<p>Hormis ces deux particularités, Dark Chess se joue comme un jeu d’échecs traditionnel : les règles "
            "normales s’appliquent.</p>"

            "<h2>Dice Chess</h2>"

            "<p>Dice Chess se joue à l’aide de dés. Aux 6 faces du dé correspondent les 6 types de pièces :</p>"
            "<table>"
              "<tr>"
                "<th>1</th>"
                "<th>2</th>"
                "<th>3</th>"
                "<th>4</th>"
                "<th>5</th>"
                "<th>6</th>"
              "</tr>"
              "<tr>"
                "<td>Pion</td>"
                "<td>Cavalier</td>"
                "<td>Fou</td>"
                "<td>Tour</td>"
                "<td>Reine</td>"
                "<td>Roi</td>"
              "</tr>"
            "</table>"

            "<p>Au début de son tour, le joueur lance deux dés. Le résultat des dés indique quelles pièces (et "
            "uniquement celles-là) peuvent être déplacées durant le tour : le joueur ne peut déplacer une autre "
            "pièce que celles indiquées par le résultat du lancer de dés. Si les deux dés tombent sur la même valeur, "
            "alors n’importe quelle pièce peut être déplacée. Si aucun coup valide ne peut être joué, le joueur doit "
            "passer son tour.</p>"
            "<p>Hormis cette particularité, Dice Chess se joue comme un jeu d’échecs traditionnel : les règles normales "
            "s’appliquent.</p>";

    QPoint current_pos = pos();
    this->close();
    RulesWindow *gamemodes_rules_window = new RulesWindow(this->player, rules, current_pos);
    gamemodes_rules_window->show();
}

RulesWindow::RulesWindow(Player* player, QString text_to_display, QPoint current_pos) :  MenuWindow(player)
{
    move(current_pos);
    v_layout->setContentsMargins(30, 130, 30, 30);
    setStyleSheet("QScrollBar:vertical {"
    "background:transparent;"
    "width:10px;    "
    "margin: 0px 0px 0px 0px;"
    "}"
    "QScrollBar::handle:vertical {"
    "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
    "stop: 0 rgb(15,8,8), stop: 0.5 rgb(15,8,8), stop:1 rgb(15,8,8));"
    "min-height: 0px;"
    "}"
    "QScrollBar::add-line:vertical {"
    "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
    "stop: 0 rgb(15,8,8), stop: 0.5 rgb(15,8,8),  stop:1 rgb(15,8,8));"
    "height: 0px;"
    "subcontrol-position: bottom;"
    "subcontrol-origin: margin;"
    "}"
    "QScrollBar::sub-line:vertical {"
    "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
    "stop: 0  rgb(15,8,8), stop: 0.5 rgb(15,8,8),  stop:1 rgb(15,8,8));"
    "height: 0 px;"
    "subcontrol-position: top;"
    "subcontrol-origin: margin;"
    "}");
    text = new QTextEdit(this);
    text->setStyleSheet("font: 12pt \"Georgia\";"
                        "text-align:justify;"
                       "color:#dbd8ca;"
                       "background-color: transparent;"
                       "padding: 5px;");
    text->setReadOnly(true);
    text->setHtml(text_to_display);
    v_layout->addWidget(text);
    setNavButtons();
}

void RulesWindow::comeBack()
{
    QPoint current_pos = pos();
    this->close();
    HelpWindow *help_window = new HelpWindow(this->player, current_pos);
    help_window->show();
}
