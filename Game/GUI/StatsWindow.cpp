// INFO-F209 - Groupe 5 - StatsWindow.cpp

#include "StatsWindow.hpp"

StatsWindow::StatsWindow(Player* player, QPoint current_pos) : MenuWindow(player)
{
    move(current_pos);
    char buffer[MAXBUFSIZE];
    v_layout->setContentsMargins(30, 130, 30, 20);
    v_layout->setSpacing(15);
    h_layout = new QHBoxLayout();
    // My score
    my_score = new QLabel(this);
    sending_T(this->player->getSocket(), prepForSending(240, ""));
    if(receiving_T(this->player->getSocket(), buffer)) {
        std::cout << buffer << std::endl;
        std::string message = buffer;
        message = message.substr(3, message.size());
        unsigned long pos;
        pos = message.find(MSG_SEPARATOR);
        std::string elo = message.substr(pos+1, message.size());
        my_score->setText(QString(STATS_PERSO " : ").append(elo.c_str()));
    }
    my_score->setFont(menu_font);
    my_score->setStyleSheet("color:#ece6ac;");
    // Tab Widget
    setStyleSheet("QTabBar::tab:selected {color:white;}"
                  "QTableWidget {background: transparent; color:#dbd8ca;}"
                  "QHeaderView::section {background-color: #301e1e;}"
                  "QHeaderView {background:transparent;}"
                  "QTableCornerButton::section {background: transparent;}"
                  "QTableView {selection-background-color: #301e1e;}"
                  "QScrollBar:vertical {"
                  "background:transparent;"
                  "width:10px;    "
                  "margin: 0px 0px 0px 0px;"
                  "}"
                  "QScrollBar::handle:vertical {"
                  "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                  "stop: 0 rgb(48,30,30), stop: 0.5 rgb(48,30,30), stop:1 rgb(48,30,30));"
                  "min-height: 0px;"
                  "}"
                  "QScrollBar::add-line:vertical {"
                  "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                  "stop: 0 rgb(48,30,30), stop: 0.5 rgb(48,30,30),  stop:1 rgb(48,30,30));"
                  "height: 0px;"
                  "subcontrol-position: bottom;"
                  "subcontrol-origin: margin;"
                  "}"
                  "QScrollBar::sub-line:vertical {"
                  "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                  "stop: 0  rgb(48,30,30), stop: 0.5 rgb(48,30,30),  stop:1 rgb(48,30,30));"
                  "height: 0 px;"
                  "subcontrol-position: top;"
                  "subcontrol-origin: margin;"
                  "}");
    tab_stats_container = new QTabWidget(this);
    tab_stats_container->setMinimumSize(400, 360);
    tab_stats_container->setStyleSheet("background-color: #140a0a; color:#dbd8ca; font: 12pt \"Georgia\";");
    // Friends stats
    tab_friends = new QTableWidget(this);
    tab_friends->setColumnCount(3);
    tab_friends->setRowCount(10);
    tab_friends->setHorizontalHeaderLabels(QStringList() << "n°" << "Nom" << "Score");
    tab_friends->setColumnWidth(0, 105);
    tab_friends->setColumnWidth(1, 451);
    tab_friends->setColumnWidth(2, 165);
    tab_friends->verticalHeader()->setVisible(false); // Hide vertical header (line counter)
    tab_friends->setShowGrid(false);
    tab_friends->setEditTriggers(QAbstractItemView::NoEditTriggers); // Disable editing
    tab_friends->setSelectionMode(QAbstractItemView::NoSelection);

    sending_T(this->player->getSocket(), prepForSending(280, ""));
    memset(buffer, '\0', MAXBUFSIZE);
    if(receiving_T(this->player->getSocket(), buffer))
    {
        std::string message = buffer;
        message = message.substr(3, message.size());
        unsigned long pos;
        int count = 1;
        std::vector<std::pair<std::string, std::string>> points_elo;
        while(!message.empty()) {
            pos = message.find(MSG_SEPARATOR);
            points_elo.push_back(std::pair<std::string, std::string>({message.substr(0, pos), ""}));
            message = message.substr(pos + 1, message.size());
            pos = message.find(MSG_SEPARATOR);
            points_elo[count-1].second=message.substr(0, pos);
            message = message.substr(pos + 1, message.size());
            count++;
        }
        for(int i = 0; i<count-1; ++i){
            tab_friends->setRowCount(count);
            QTableWidgetItem *item_nbr = new QTableWidgetItem(QString::number(i+1));
            QTableWidgetItem *item_name = new QTableWidgetItem(points_elo[i].first.c_str());
            QTableWidgetItem *item_score = new QTableWidgetItem(points_elo[i].second.c_str());
            item_nbr->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
            item_name->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
            item_score->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
            tab_friends->setItem(i, 0, item_nbr);
            tab_friends->setItem(i, 1, item_name);
            tab_friends->setItem(i, 2, item_score);
        }
    }


    // General stats
    tab_general = new QTableWidget(this);
    tab_general->setRowCount(10);
    tab_general->setColumnCount(3);
    tab_general->setHorizontalHeaderLabels(QStringList() << STATS_TABLE_NBR << STATS_TABLE_NAME << STATS_TABLE_SCORE);
    tab_general->setColumnWidth(0, 105);
    tab_general->setColumnWidth(1, 451);
    tab_general->setColumnWidth(2, 165);
    tab_general->verticalHeader()->setVisible(false);
    tab_general->setShowGrid(false);
    tab_general->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tab_general->setSelectionMode(QAbstractItemView::NoSelection);
    sending_T(this->player->getSocket(), prepForSending(250, "10"));
    memset(buffer, '\0', MAXBUFSIZE);
    if(receiving_T(this->player->getSocket(), buffer))
    {
        std::string message = buffer;
        message = message.substr(3, message.size());
        unsigned long pos;
        int count = 1;
        while(!message.empty()) {
            pos = message.find(MSG_SEPARATOR);
            QTableWidgetItem *item_nbr = new QTableWidgetItem(QString::number(count));
            QTableWidgetItem *item_name = new QTableWidgetItem(message.substr(0, pos).c_str());
            message = message.substr(pos + 1, message.size());
            pos = message.find(MSG_SEPARATOR);
            QTableWidgetItem *item_score = new QTableWidgetItem(message.substr(0, pos).c_str());
            message = message.substr(pos + 1, message.size());
            item_nbr->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
            item_name->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
            item_score->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
            tab_general->setItem(count-1, 0, item_nbr);
            tab_general->setItem(count-1, 1, item_name);
            tab_general->setItem(count-1, 2, item_score);
            count++;
        }
    }
    // Adding widgets to the container tab_stats_container
    tab_stats_container->addTab(tab_friends, QString());
    tab_stats_container->addTab(tab_general, QString());
    tab_stats_container->setCurrentIndex(0);
    tab_stats_container->setTabText(tab_stats_container->indexOf(tab_friends), STATS_FRIENDS);
    tab_stats_container->setTabText(tab_stats_container->indexOf(tab_general), STATS_GENERAL);
    // Adding widgets to layout
    v_layout->addWidget(my_score);
    v_layout->addWidget(tab_stats_container);
    setNavButtons();
}

void StatsWindow::comeBack()
{
    QPoint current_pos = pos();
    MenuWindow::comeBack();
    MainMenuWindow *main_menu_window = new MainMenuWindow(this->player, current_pos, false);
    main_menu_window->show();
}
