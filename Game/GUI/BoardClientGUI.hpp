#ifndef BOARDCLIENTGUI_HPP
#define BOARDCLIENTGUI_HPP

#include "ChessSquare.hpp"

template <typename B>
class BoardClientGUI : public B {
private:
    ChessSquare* square[MAX_COLS][MAX_ROWS] = {{ nullptr }};
    ChessSquare* selectedSquare = nullptr;
    QGridLayout* chessboard;
public:
    BoardClientGUI(QGridLayout*);
    ~BoardClientGUI();
    ChessSquare* getSquare(std::pair<int, int>);
    void setSelectedSquare(ChessSquare*);
    ChessSquare* getSelectedSquare();
    void refreshDisplay();
    bool addPiece(int, std::pair<int, int>, bool) override;
    bool removePiece(std::pair<int, int>) override;
    void setNewPosition(std::pair<int, int> src_pos, std::pair<int, int> dest_pos) override;
};

#include "BoardClientGUI.cpp"

#endif //BOARDCLIENTGUI_HPP
