// INFO-F209 - Groupe 5 - FriendsWindow.hpp

#include "MenuWindow.hpp"

#ifndef FRIENDSLISTWINDOW_HPP
#define FRIENDSLISTWINDOW_HPP

////////////////////////////////////////////////////////////////////////////////
// MANAGE FRIENDS LIST
////////////////////////////////////////////////////////////////////////////////

class FriendsListWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QGridLayout *g_layout;
        QLabel *friends_list_title;
        QLabel *requests_list_title;
        QPushButton *add_friend_btn;
        QPushButton *remove_friend_btn;
        QPushButton *deleteFriend_btn;
        QPushButton *confirm_btn;
        QPushButton *discard_btn;
        QTableWidget *friends_list_tab;
        QTableWidget *requests_list_tab;
        std::string pseudoSelected = "no";
    public:
        FriendsListWindow(Player*, QPoint);
    public slots:
        void comeBack() override;
        void addFriend();
        void removeFriend();
        void SetFriendsList();
        void SetFriendsRequestList();
        void changeSelectedPseudo();
        void removeFriendSelected();
};

#endif

////////////////////////////////////////////////////////////////////////////////
// ADD A FRIEND/DELETE A FRIEND'S WINDOWS
////////////////////////////////////////////////////////////////////////////////

#ifndef ACTIONSFRIENDSWINDOW_HPP
#define ACTIONSFRIENDSWINDOW_HPP

class ActionsFriendsWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QLabel *type_name_label;
        QLineEdit *type_name_input;
        QPushButton *ok_btn;
        QPushButton *cancel_btn;
        bool add_friend;
    public:
        ActionsFriendsWindow(Player*, bool);
        void setNavButtons() override;
        void setLabel(QString);
        void showSuccessInput(NotificationWindow *, std::string);
        void showErrorInput(NotificationWindow *, std::string);
        bool eventFilter(QObject*, QEvent*) override;
    public slots:
        void addFriend();
        void removeFriend();
    signals:
        void refreshFriendsList();
};

#endif

#ifndef REQUESTBUTTON_HPP
#define REQUESTBUTTON_HPP

class RequestButton : public QPushButton
{
        Q_OBJECT
    private:
        std::string _name;
        int _socketfd;
    public:
        RequestButton(QString, FriendsListWindow*, std::string, int);
    public slots:
        void acceptRequest();
        void removeRequest();
        //void removeFriend();
    signals:
        void refreshFriendsList();
        void refreshRequestsList();
};

#endif
