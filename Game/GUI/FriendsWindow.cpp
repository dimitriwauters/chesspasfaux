// INFO-F209 - Groupe 5 - FriendsWindow.cpp

#include "FriendsWindow.hpp"

FriendsListWindow::FriendsListWindow(Player* player, QPoint current_pos) : MenuWindow(player)
{
    move(current_pos);
    setBackground("resources/bg_manage_friends.jpg");
    v_layout->setContentsMargins(80, 150, 80, 30);
    setStyleSheet("QTabBar::tab:selected {color:white;}"
                  "QTableWidget {background: transparent; color:#dbd8ca; font: 12pt \"Georgia\";}"
                  "QHeaderView::section {background-color: #301e1e;}"
                  "QHeaderView {background:transparent;}"
                  "QTableCornerButton::section {background: transparent;}"
                  "QTableView {selection-background-color: #301e1e;}");
    // FRIENDS LIST
    friends_list_title = new QLabel(this);
    friends_list_title->setText(FRIENDS_FRIENDS_LIST_TITLE);
    friends_list_title->setFont(menu_font);
    friends_list_title->setStyleSheet("color:#ece6ac;");
    friends_list_tab = new QTableWidget(this);
    friends_list_tab->setRowCount(1);
    friends_list_tab->setColumnCount(1);
    friends_list_tab->horizontalHeader()->setVisible(false);
    friends_list_tab->verticalHeader()->setVisible(false);
    friends_list_tab->setShowGrid(false);
    friends_list_tab->setEditTriggers(QAbstractItemView::NoEditTriggers);
    friends_list_tab->setSelectionBehavior(QAbstractItemView::SelectItems);
    friends_list_tab->setSelectionMode(QAbstractItemView::SingleSelection);
    friends_list_tab->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    QObject::connect(friends_list_tab, SIGNAL(itemSelectionChanged()), this, SLOT(changeSelectedPseudo()));
    friends_list_tab->setColumnWidth(0, 275);
    SetFriendsList();
    // REQUESTS LIST
    requests_list_title = new QLabel(this);
    requests_list_title->setText(FRIENDS_REQUESTS_LIST_TITLE);
    requests_list_title->setFont(menu_font);
    requests_list_title->setStyleSheet("color:#ece6ac;");
    requests_list_tab = new QTableWidget(this);
    requests_list_tab->setRowCount(1);
    requests_list_tab->setColumnCount(3);
    requests_list_tab->setColumnWidth(0, 250);
    requests_list_tab->setColumnWidth(1, 25);
    requests_list_tab->setColumnWidth(2, 25);
    requests_list_tab->horizontalHeader()->setVisible(false);
    requests_list_tab->verticalHeader()->setVisible(false);
    requests_list_tab->setShowGrid(false);
    requests_list_tab->setEditTriggers(QAbstractItemView::NoEditTriggers);
    requests_list_tab->setSelectionMode(QAbstractItemView::NoSelection);
    requests_list_tab->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    SetFriendsRequestList();

    // ADD AND REMOVE FRIENDS
    add_friend_btn = new QPushButton(FRIENDS_ADD, this);
    remove_friend_btn = new QPushButton(FRIENDS_REMOVE, this);
    add_friend_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    remove_friend_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    add_friend_btn->setFont(menu_font);
    remove_friend_btn->setFont(menu_font);
    const QSize BUTTON_SIZE = QSize(150, 42);
    add_friend_btn->setMinimumSize(BUTTON_SIZE);
    remove_friend_btn->setMinimumSize(BUTTON_SIZE);
    QObject::connect(add_friend_btn, SIGNAL(clicked()), this, SLOT(addFriend()));
    //QObject::connect(remove_friend_btn, SIGNAL(clicked()), this, SLOT(removeFriend()));
    QObject::connect(remove_friend_btn, SIGNAL(clicked()), this, SLOT(removeFriendSelected()));
    // ADDING WIDGETS TO THE LAYOUTS
    g_layout = new QGridLayout();
    g_layout->addWidget(friends_list_title, 0, 0, 1, 1);
    g_layout->addWidget(friends_list_tab, 1, 0, 1, 1);
    g_layout->addWidget(requests_list_title, 0, 1, 1, 1);
    g_layout->addWidget(requests_list_tab, 1, 1, 1, 1);
    g_layout->addWidget(add_friend_btn, 2, 0, 1, 2);
    g_layout->addWidget(remove_friend_btn, 3, 0, 1, 2);
    v_layout->addLayout(g_layout);
    setNavButtons();
}

void FriendsListWindow::SetFriendsList(){
    friends_list_tab->clear();
    //RECEIVING FRIEND LIST
    char buffer[MAXBUFSIZE];
    sending_T(this->player->getSocket(), prepForSending(264, ""));
    memset(buffer, '\0', MAXBUFSIZE);
    int count = 0;
    if(receiving_T(this->player->getSocket(), buffer))
    {
        std::vector<std::string> names;
        std::string message = buffer;
        message = message.substr(3, message.size());
        unsigned long pos;
        while(!message.empty()) {
            pos = message.find(MSG_SEPARATOR);
            names.push_back(message.substr(0, pos));
            count++;
            message = message.substr(pos+ 1, message.size());
        }
    if(count==0)
        {
            friends_list_tab->setItem(0, 0, new QTableWidgetItem(FRIENDS_EMPTY_FRIENDS_LIST));
        }
    else{
        friends_list_tab->setRowCount(count);
        for(int i = 0; i<count; ++i){
            friends_list_tab->setItem(i, 0, new QTableWidgetItem(QString::fromStdString(names[i])));
        }
    }
    }
}

void FriendsListWindow::SetFriendsRequestList(){
    requests_list_tab->clear();
    //RECEIVING FRIEND REQUEST LIST
    char buffer[MAXBUFSIZE];
    sending_T(this->player->getSocket(), prepForSending(265, ""));
    memset(buffer, '\0', MAXBUFSIZE);
    int count = 0;
    if(receiving_T(this->player->getSocket(), buffer))
    {
        std::string message = buffer;
        message = message.substr(3, message.size());
        unsigned long pos;
        std::vector<std::string> names;
        while(!message.empty()) {
            pos = message.find(MSG_SEPARATOR);
            names.push_back(message.substr(0, pos));
            count++;
            message = message.substr(pos+ 1, message.size());
        }
		if(count==0){
            requests_list_tab->setItem(0, 0, new QTableWidgetItem(FRIENDS_EMPTY_REQUESTS_LIST));
		}
        else{
            requests_list_tab->setRowCount(count);
            for(int i = 0; i<count; ++i){
                std::cout<<names[i]<<std::endl;
                requests_list_tab->setItem(i, 0, new QTableWidgetItem(QString::fromStdString(names[i])));
                confirm_btn = new RequestButton(FRIENDS_REQUEST_V, this, names[i], this->player->getSocket());
                discard_btn = new RequestButton(FRIENDS_REQUEST_X, this, names[i], this->player->getSocket());
                confirm_btn->setFont(menu_font);
                discard_btn->setFont(menu_font);
                confirm_btn->setStyleSheet("QPushButton {background-image:url(resources/confirm_request.png); background-repeat:repeat-x; color: #9ebc5e;border:none; font: 12pt \"Georgia\"; border-radius: 3px; margin: 3px;} QPushButton:hover{background-image:url(resources/confirm_request_hover.png); background-repeat:repeat-x; color: white;}");
                discard_btn->setStyleSheet("QPushButton {background-image:url(resources/discard_request.png); background-repeat:repeat-x; color: #b25858;border:none; font: 12pt \"Georgia\"; border-radius: 3px; margin: 3px;} QPushButton:hover{background-image:url(resources/discard_request_hover.png); background-repeat:repeat-x; color: white;}");
                requests_list_tab->setCellWidget(i,1,(QWidget*)confirm_btn);
                requests_list_tab->setCellWidget(i,2,(QWidget*)discard_btn);
                QObject::connect(confirm_btn, SIGNAL(clicked()), confirm_btn, SLOT(acceptRequest()));
                QObject::connect(confirm_btn, SIGNAL(refreshFriendsList()), this, SLOT(SetFriendsList()));
                QObject::connect(confirm_btn, SIGNAL(refreshRequestsList()), this, SLOT(SetFriendsRequestList()));
                QObject::connect(discard_btn, SIGNAL(clicked()), discard_btn, SLOT(removeRequest()));
                QObject::connect(discard_btn, SIGNAL(refreshRequestsList()), this, SLOT(SetFriendsRequestList()));
            }
        }
	}
}

void FriendsListWindow::comeBack()
{
    QPoint current_pos = pos();
    MenuWindow::comeBack();
    MainMenuWindow *main_menu_window = new MainMenuWindow(this->player, current_pos, false);
    main_menu_window->show();
}

void FriendsListWindow::addFriend()
{
    ActionsFriendsWindow *add_friend_window = new ActionsFriendsWindow(this->player, true);
    add_friend_window->show();
    add_friend_window->setLabel(FRIENDS_ADD_USERNAME);
}

void FriendsListWindow::removeFriend()
{
    ActionsFriendsWindow *remove_friend_window = new ActionsFriendsWindow(this->player, false);
    remove_friend_window->show();
    remove_friend_window->setLabel(FRIENDS_REMOVE_USERNAME);
    QObject::connect(remove_friend_window, SIGNAL(refreshFriendsList()), this, SLOT(SetFriendsList()));
}

void FriendsListWindow::removeFriendSelected(){
    if(pseudoSelected != "no" && pseudoSelected != "Votre liste d'amis est vide."){
        sending_T(this->player->getSocket(), prepForSending(262, pseudoSelected));
        char buffer[MAXBUFSIZE];
        std::string message;
        if(receiving_T(this->player->getSocket(), buffer)) {
            message = buffer;
            message = message.substr(3, message.size());
            if(message.compare(FRIENDS_REMOVED) == 0){
                NotificationWindow *msg = new NotificationWindow();
                QString message = QString::fromStdString(pseudoSelected+FRIENDS_REMOVE_SUCCESS);
                QString title = QString::fromStdString(pseudoSelected+FRIENDS_REMOVE_SUCCESS_TITLE);
                msg->setText(message);
                msg->setWindowTitle(title);
                msg->exec();
                SetFriendsList();

            }
        }
    }
    else {
    NotificationWindow *msg = new NotificationWindow();
    msg->setText(FRIENDS_SELECT_REMOVE);
    msg->setWindowTitle(ERROR_TITLE);
    msg->exec();
    }
}

void FriendsListWindow::changeSelectedPseudo(){
    if(friends_list_tab->selectedItems().at(0))
        pseudoSelected = friends_list_tab->selectedItems().at(0)->text().toStdString();
    else
        pseudoSelected = "no";
}

RequestButton::RequestButton(QString symbol, FriendsListWindow* win, std::string name, int socket) : QPushButton(symbol, win)
{
    _name=name;
    _socketfd=socket;
}

void RequestButton::acceptRequest()
{
    sending_T(_socketfd, prepForSending(263, this->_name));
    char buffer[MAXBUFSIZE];
    std::string message;
    if(receiving_T(_socketfd, buffer)) {
        message = buffer;
        message = message.substr(3, message.size());
        NotificationWindow *msg = new NotificationWindow();
        if(message.compare(FRIENDS_ADDED) == 0){
            msg->setText(QString::fromStdString(this->_name+FRIENDS_ADD_SUCCESS));
            msg->setWindowTitle(QString::fromStdString(this->_name+FRIENDS_ADD_SUCCESS_TITLE));
            emit refreshFriendsList();
            emit refreshRequestsList();
        }
        else{
            msg->setText(PLAYER_NOT_FOUND);
            msg->setWindowTitle(ERROR_TITLE);
        }
    msg->exec();
    }
}

void RequestButton::removeRequest()
{
    sending_T(_socketfd, prepForSending(266, this->_name));
    NotificationWindow *msg = new NotificationWindow();
    msg->setText(FRIENDS_REQUEST_REMOVED);
    msg->setWindowTitle(FRIENDS_REQUEST_REMOVED_TITLE);
    emit refreshRequestsList();
    msg->exec();
}

////////////////////////////////////////////////////////////////////////////////
// ADD A FRIEND/DELETE A FRIEND'S WINDOWS
////////////////////////////////////////////////////////////////////////////////

ActionsFriendsWindow::ActionsFriendsWindow(Player* player, bool add_remove) : MenuWindow(player), add_friend(add_remove)
{
    setFixedSize(490, 214);
    close_window_btn->setGeometry(490, 15, 14, 14);
    setBackground("resources/bg_actions_window_without_logo.jpg");
    v_layout->setContentsMargins(80, 30, 80, 30);
    type_name_label = new QLabel(this);
    type_name_label->setStyleSheet("font: 12pt \"Georgia\"; color:#dbd8ca;");
    type_name_input = new QLineEdit();
    type_name_input->setStyleSheet("font: 12pt \"Georgia\";"
                                   "color:#dbd8ca;"
                                   "border: 1px solid #301e1e;"
                                   "border-radius: 7px;"
                                   "background-color: #140a0a;"
                                   "padding: 5px;"
                                   "margin-bottom: 20px;");
    v_layout->addWidget(type_name_label);
    v_layout->addWidget(type_name_input);
    // Catching Enter key
    installEventFilter(this);
    setNavButtons();
}

void ActionsFriendsWindow::setLabel(QString text)
{
    type_name_label->setText(text);
}

void ActionsFriendsWindow::setNavButtons()
{
    const QSize NAV_BUTTON_BACK = QSize(105, 42);
    ok_btn = new QPushButton(VALIDATE, this);
    ok_btn->setFont(menu_font);
    ok_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;}  QPushButton:hover {border: 1px solid #441409;}");
    ok_btn->setMinimumSize(NAV_BUTTON_BACK);
    cancel_btn = new QPushButton(CANCEL, this);
    cancel_btn->setFont(menu_font);
    cancel_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;}  QPushButton:hover {border: 1px solid #441409;}");
    cancel_btn->setMinimumSize(NAV_BUTTON_BACK);
    h_spacer_nav_buttons = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
    h_layout_nav_buttons = new QHBoxLayout;
    h_layout_nav_buttons->addItem(h_spacer_nav_buttons);
    h_layout_nav_buttons->addWidget(ok_btn);
    h_layout_nav_buttons->addWidget(cancel_btn);
    v_layout->addLayout(h_layout_nav_buttons);
    if (add_friend) QObject::connect(ok_btn, SIGNAL(clicked()), this, SLOT(addFriend()));
    else QObject::connect(ok_btn, SIGNAL(clicked()), this, SLOT(removeFriend()));
    QObject::connect(cancel_btn, SIGNAL(clicked()), this, SLOT(close()));
}

void ActionsFriendsWindow::addFriend() {
	char buffer[MAXBUFSIZE];
	std::string message;
	std::string friendToAdd = type_name_input->text().toStdString();
	sending_T(this->player->getSocket(), prepForSending(261, friendToAdd));
	if(receiving_T(this->player->getSocket(), buffer)) {
		message = buffer;
		message = message.substr(3, message.size());
		closeWindow();
		NotificationWindow *msg = new NotificationWindow();
		if(message.compare(FRIENDS_SENT_REQUEST_TITLE) == 0)
			showSuccessInput(msg, friendToAdd);
		else
			showErrorInput(msg, friendToAdd);
	}
}

void ActionsFriendsWindow::removeFriend() {
	char buffer[MAXBUFSIZE];
    std::string message;
    std::string friendToRemove = type_name_input->text().toStdString();
	sending_T(this->player->getSocket(), prepForSending(262, friendToRemove));
	if(receiving_T(this->player->getSocket(), buffer)) {
		message = buffer;
		message = message.substr(3, message.size());
		closeWindow();
		NotificationWindow *msg = new NotificationWindow();
		if(message.compare(FRIENDS_REMOVED) == 0){
			showSuccessInput(msg, friendToRemove);
            emit refreshFriendsList();
    }
		else
			showErrorInput(msg, friendToRemove);
	}
}

void ActionsFriendsWindow::showSuccessInput(NotificationWindow *msg, std::string name)
{
    if(add_friend)
    {

        QString message = QString::fromStdString(FRIENDS_SENT_REQUEST+name);
        QString title = QString::fromStdString(FRIENDS_SENT_REQUEST_TITLE);
        msg->setText(message);
        msg->setWindowTitle(title);
    }
    else
    {
		QString message = QString::fromStdString(name+FRIENDS_REMOVE_SUCCESS);
        QString title = QString::fromStdString(name+FRIENDS_REMOVE_SUCCESS_TITLE);
        msg->setText(message);
        msg->setWindowTitle(title);
    }
    msg->exec();
}

void ActionsFriendsWindow::showErrorInput(NotificationWindow *msg, std::string name)
{
    QSound::play("resources/sounds/pas_foutu_de_savoir_son_nom.wav");
    msg->setText(PLAYER_NOT_FOUND);
    msg->setWindowTitle(ERROR_TITLE);
    msg->exec();
    ActionsFriendsWindow *popup_window;
    if (add_friend)
    {
        popup_window = new ActionsFriendsWindow(this->player, true);
        popup_window->setLabel(FRIENDS_ADD_USERNAME);
    }
    else
    {
        popup_window = new ActionsFriendsWindow(this->player, false);
        popup_window->setLabel(FRIENDS_REMOVE_USERNAME);
    }
    popup_window->show();
}

bool ActionsFriendsWindow::eventFilter(QObject* obj, QEvent* event) // Catching Enter key
{
    if (event->type()==QEvent::KeyPress)
    {
        auto key = dynamic_cast<QKeyEvent*>(event);
        if ((key->key()==Qt::Key_Enter) || (key->key()==Qt::Key_Return))
        {
            if (add_friend) addFriend();
            else removeFriend();
        }
    }
    else return QObject::eventFilter(obj, event);
}
