// INFO-F209 - Groupe 5 - ConnectionWindow.hpp

#include "MenuWindow.hpp"
#include "ChatWindow.hpp"

////////////////////////////////////////////////////////////////////////////////
// FIRST WINDOW
////////////////////////////////////////////////////////////////////////////////

#ifndef FIRSTWINDOW_HPP
#define FIRSTWINDOW_HPP

class FirstWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QPushButton *login_btn;
        QPushButton *sign_up_btn;
        QLabel *description;
    public:
        FirstWindow(Player*);
        FirstWindow(Player*, QPoint);
    public slots:
        void openSignUpWindow();
        void openLoginWindow();
};

#endif

////////////////////////////////////////////////////////////////////////////////
// SIGN UP WINDOW
////////////////////////////////////////////////////////////////////////////////

#ifndef SIGNLOGLINE_HPP
#define SIGNLOGLINE_HPP

class SignLogLine : public QLineEdit
{
    protected:
        void focusInEvent(QFocusEvent *) override;
    public:
        SignLogLine();
};

#endif

#ifndef SIGNUPWINDOW_HPP
#define SIGNUPWINDOW_HPP

class SignUpWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QFormLayout *sign_up_form;
        SignLogLine *pseudo;
        SignLogLine *password;
        SignLogLine *repeat_password;
        QLabel *pseudo_label;
        QLabel *password_label;
        QLabel *repeat_password_label;
        QLabel *sign_up_title;
        QPushButton *sign_up_btn;
        QPushButton *back_btn;
    public:
        SignUpWindow(Player*, QPoint);
        void showSuccessInput(NotificationWindow *);
        void showErrorInput(NotificationWindow *, QString);
        bool eventFilter(QObject*, QEvent*) override;
        void setNavButtons() override;
    public slots:
        void register_user();
        void comeBack() override;
};

#endif

////////////////////////////////////////////////////////////////////////////////
// LOG IN WINDOW
////////////////////////////////////////////////////////////////////////////////

#ifndef LOGINWINDOW_HPP
#define LOGINWINDOW_HPP

class LoginWindow : public MenuWindow
{
        Q_OBJECT
    private:
        QFormLayout *login_form;
        SignLogLine *pseudo;
        SignLogLine *password;
        QLabel *pseudo_label;
        QLabel *password_label;
        QLabel *login_title;
        QPushButton *login_btn;
        QPushButton *back_btn;
    public:
        LoginWindow(Player*, QPoint);
        bool eventFilter(QObject*, QEvent*) override;
        void setNavButtons() override;
    public slots:
        void login_user();
        void comeBack() override;
};

#endif
