// INFO-F209 - Groupe 5 - CreditsWindow.cpp

#include "CreditsWindow.hpp"

CreditsWindow::CreditsWindow(Player* player, QPoint current_pos) :  MenuWindow(player)
{
    move(current_pos);
    v_layout->setContentsMargins(30, 130, 30, 30);
    credits_title = new QLabel(this);
    credits_title->setText(CREDITS);
    credits_title->setFont(menu_font);
    credits_title->setStyleSheet("color:#ece6ac;");
    text = new QTextBrowser(this);
    text->setStyleSheet("font: 12pt \"Georgia\";"
                           "text-align:justify;"
                          "color:#dbd8ca;"
                          "background-color: transparent;"
                          "padding: 5px;");
    text->setHtml("<ul><li>Le curseur provient d'<a href='http://www.rw-designer.com/cursor-detail/76012'><span style='color:#a2a099;'>ici</span></a></li>"
                     "<li>Les polices de caractère utilisées sont Cinzel, Chess Alpha et Pieces of Eight (toutes téléchargeables sur <a href='http://dafont.com'><span style='color:#a2a099;'>dafont</span></a>)</li>"
                     "<li>Les icônes des boutons des modes de jeu proviennent de <a href='http://www.flaticon.com'><span style='color:#a2a099;'>Flaticon</span></a></li>"
                    "<li>La photo de notre groupe a été tirée par Ilias Larin</li>"
                    "<li>Les sons de Kaamelott ont été trouvés sur <a href='http://kaamelott-soundboard.2ec0b4.fr'><span style='color:#a2a099;'>Kaamelott Soundboard</span></a></li>"
                    "<li>Le son de déplacement de pièces est tiré de la collection <a href='https://opengameart.org/content/50-rpg-sound-effects'><span style='color:#a2a099;'>50 RPG sound effects</span></a> disponible sur OpenGameArt</li>"
                    "<p>Tout le reste a été réalisé par nos soins. :-)</p>"
                         "<li></li></ul>");
    text->setOpenExternalLinks( true );
    v_layout->addWidget(credits_title);
    v_layout->addWidget(text);
    setNavButtons();
}

void CreditsWindow::comeBack()
{
    QPoint current_pos = pos();
    MenuWindow::comeBack();
    MainMenuWindow *main_menu_window = new MainMenuWindow(this->player, current_pos, false);
    main_menu_window->show();
}
