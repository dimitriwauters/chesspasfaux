// INFO-F209 - Groupe 5 - LaunchGameWindow.hpp

#include "MenuWindow.hpp"

////////////////////////////////////////////////////////////////////////////////
// LAUNCH A GAME
////////////////////////////////////////////////////////////////////////////////

#ifndef LAUNCHGAMEWINDOW_HPP
#define LAUNCHGAMEWINDOW_HPP

class LaunchGameWindow : public MenuWindow
{
    protected:
        QLabel *description;
    public:
        LaunchGameWindow(Player*, QPoint);
        void showDescription(QString);
};

#endif

////////////////////////////////////////////////////////////////////////////////
// GAMEMODES WINDOW
////////////////////////////////////////////////////////////////////////////////

#ifndef GAMEMODESWINDOW_HPP
#define GAMEMODESWINDOW_HPP

class GamemodesWindow : public LaunchGameWindow
{
        Q_OBJECT
    private:
        QPushButton *default_game_btn;
        QPushButton *horde_game_btn;
        QPushButton *dark_game_btn;
        QPushButton *dice_game_btn;
        QGridLayout *g_layout;
    public:
        GamemodesWindow(Player*, QPoint);
        void createModeWindow(int);
        bool eventFilter(QObject*, QEvent*) override;
    public slots:
        void comeBack() override;
        void selectDefaultGame();
        void selectDiceGame();
        void selectHordeGame();
        void selectDarkGame();
};

#endif

////////////////////////////////////////////////////////////////////////////////
// MODES WINDOW
////////////////////////////////////////////////////////////////////////////////

#ifndef MODESWINDOW_HPP
#define MODESWINDOW_HPP

class ModesWindow : public LaunchGameWindow
{
        Q_OBJECT
    private:
        int chosen_gamemode;
        QPushButton *default_mode_btn;
        QPushButton *real_time_mode_btn;
        QHBoxLayout *h_layout;
    public:
        ModesWindow(int, Player*, QPoint);
        void launchGameSound();
        bool eventFilter(QObject*, QEvent*) override;
    public slots:
        void comeBack() override;
        void launchDefaultGame();
        void launchRealTimeGame();
};

#endif

////////////////////////////////////////////////////////////////////////////////
// MATCHMAKING
////////////////////////////////////////////////////////////////////////////////

#ifndef MATCHMAKINGWINDOW_HPP
#define MATCHMAKINGWINDOW_HPP

class MatchmakingWindow : public Window
{
        Q_OBJECT
    protected:
        QLabel *quote;
        QLabel *title;
        QSpacerItem *v_spacer;
        QVBoxLayout *v_layout;
        std::string quoteFinder();
        std::string separateString(std::string, unsigned);
        int gamemode;
        int type;
        int team = 0;
        char player_name[256] = "Unknown";
        int ELO = 0;
        std::vector<std::string> pieces;
    public:
        MatchmakingWindow(Player*, int, int, std::vector<std::string>, QPoint);
        void showEvent(QShowEvent*) override;
        virtual void getInfosGame();
        void mouseDoubleClickEvent(QMouseEvent* e) override;
    public slots:
        virtual void startGame();
        void closeWindow() override;
    signals:
        void ready();
};

#endif

#ifndef PAUSEDMATCHMAKINGWINDOW_HPP
#define PAUSEDMATCHMAKINGWINDOW_HPP

class PausedMatchmakingWindow : public MatchmakingWindow
{
        Q_OBJECT
    protected:
        std::string opponent_name;
    public:
        PausedMatchmakingWindow(Player*, int, int, std::string, QPoint);
        void getInfosGame() override;
    /*public slots:
        void startGame() override;*/
};

#endif
