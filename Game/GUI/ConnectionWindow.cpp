// INFO-F209 - Groupe 5 - ConnectionWindow.cpp

#include "ConnectionWindow.hpp"

SignLogLine::SignLogLine() : QLineEdit ()
{
    setStyleSheet("font: 11pt \"Georgia\";"
                "text-align:justify;"
                "color:#dbd8ca;"
                "background-color: #251C1C;"
                "padding: 3px;");
}

void SignLogLine::focusInEvent(QFocusEvent *e)
{
    QLineEdit::focusInEvent(e);
    clear();
}

////////////////////////////////////////////////////////////////////////////////
// FIRST WINDOW
////////////////////////////////////////////////////////////////////////////////

FirstWindow::FirstWindow(Player* player) : MenuWindow(player)
{
    setBackground("resources/bg_menu_copyright.jpg");
    // Description
    description = new QLabel(this);
    description->setText("<p>» Jouer à <strong>différents modes de jeu d'échecs</strong> parmi Horde Chess, Dice<br /> Chess et Dark Chess</p>"
                        "<p>» Préenregistrer ses mouvements</p>"
                         "<p>» Possibilité de jouer <strong>sans tour (temps réel)</strong> au jeu d'échecs</p>"
                         "<p>» Trouver des <strong>amis</strong></p>"
            "<p>» Comparer son niveau à celui des autres grâce au <strong>classement ELO</strong></p>"
            "<p>» <strong>Discuter avec ses amis</strong>, y compris en jeu</p>"
            "<p>» <strong>Reprendre une partie</strong> non achevée</p>"
            "<p>» ... et tout cela, dans l'esprit de la série télévisée <strong>Kaamelott !</strong></p>");
    description->setStyleSheet("font: 12pt \"Georgia\"; color:#dbd8ca; margin-bottom:10px;");
    // Buttons
    sign_up_btn = new QPushButton(FIRST_SIGN_UP, this);
    login_btn = new QPushButton(FIRST_SIGN_IN, this);
    // Buttons's style
    setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    sign_up_btn->setFont(menu_font);
    login_btn->setFont(menu_font);
    const QSize BUTTON_SIZE = QSize(400, 42);
    sign_up_btn->setMinimumSize(BUTTON_SIZE);
    login_btn->setMinimumSize(BUTTON_SIZE);
    // Functions (signals and slots)
    QObject::connect(sign_up_btn, SIGNAL(clicked()), this, SLOT(openSignUpWindow()));
    QObject::connect(login_btn, SIGNAL(clicked()), this, SLOT(openLoginWindow()));
    // Adding widgets to the layout
    v_layout->addWidget(description);
    v_layout->addWidget(sign_up_btn);
    v_layout->addWidget(login_btn);
    v_layout->setAlignment(sign_up_btn, Qt::AlignHCenter);
    v_layout->setAlignment(login_btn, Qt::AlignHCenter);
}

FirstWindow::FirstWindow(Player* player, QPoint current_pos) : MenuWindow(player)
{
    move(current_pos);
    setBackground("resources/bg_menu_copyright.jpg");
    // Description
    description = new QLabel(this);
    description->setText("<p>» Jouer à <strong>différents modes de jeu d'échecs</strong> parmi Horde Chess, Dice<br /> Chess et Dark Chess</p>"
                         "<p>» Possibilité de jouer <strong>sans tour (temps réel)</strong> au jeu d'échecs</p>"
                         "<p>» Trouver des <strong>amis</strong></p>"
            "<p>» Comparer son niveau à celui des autres grâce au <strong>classement ELO</strong></p>"
            "<p>» <strong>Discuter avec ses amis</strong>, y compris en jeu</p>"
            "<p>» <strong>Reprendre une partie</strong> non achevée</p>"
            "<p>» ... et tout cela, dans l'esprit de la série télévisée <strong>Kaamelott !</strong></p>");
    description->setStyleSheet("font: 12pt \"Georgia\"; color:#dbd8ca; margin-bottom:30px; margin-right: 5px;");
    // Buttons
    sign_up_btn = new QPushButton(FIRST_SIGN_UP, this);
    login_btn = new QPushButton(FIRST_SIGN_IN, this);
    // Buttons's style
    setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    sign_up_btn->setFont(menu_font);
    login_btn->setFont(menu_font);
    const QSize BUTTON_SIZE = QSize(400, 42);
    sign_up_btn->setMinimumSize(BUTTON_SIZE);
    login_btn->setMinimumSize(BUTTON_SIZE);
    // Functions (signals and slots)
    QObject::connect(sign_up_btn, SIGNAL(clicked()), this, SLOT(openSignUpWindow()));
    QObject::connect(login_btn, SIGNAL(clicked()), this, SLOT(openLoginWindow()));
    // Adding widgets to the layout
    v_layout->addWidget(description);
    v_layout->addWidget(sign_up_btn);
    v_layout->addWidget(login_btn);
    v_layout->setAlignment(sign_up_btn, Qt::AlignHCenter);
    v_layout->setAlignment(login_btn, Qt::AlignHCenter);
}

void FirstWindow::openSignUpWindow()
{
    QPoint current_pos = pos();
    closeWindow();
    SignUpWindow *sign_up_window = new SignUpWindow(this->player, current_pos);
    sign_up_window->show();
}

void FirstWindow::openLoginWindow()
{
    QPoint current_pos = pos();
    closeWindow();
    LoginWindow *login_window = new LoginWindow(this->player, current_pos);
    login_window->show();
}

////////////////////////////////////////////////////////////////////////////////
// SIGN UP WINDOW
////////////////////////////////////////////////////////////////////////////////

SignUpWindow::SignUpWindow(Player* player, QPoint current_pos) : MenuWindow(player)
{
    move(current_pos);
    setBackground("resources/bg_login_signup.jpg");
    v_layout->setContentsMargins(140, 170, 140, 180);
    v_layout->setSpacing(35);
    sign_up_title = new QLabel(this);
    sign_up_title->setText(SIGN_UP_TITLE);
    sign_up_title->setFont(menu_font);
    sign_up_title->setStyleSheet("color:#ece6ac;");
    // Fields
    pseudo = new SignLogLine;
    password = new SignLogLine;
    password->setEchoMode(QLineEdit::Password);
    repeat_password = new SignLogLine;
    repeat_password->setEchoMode(QLineEdit::Password);
    pseudo_label = new QLabel(this);
    pseudo_label->setText(SIGN_UP_PSEUDO);
    pseudo_label->setFont(menu_font);
    pseudo_label->setStyleSheet("color:#ece6ac;");
    password_label = new QLabel(this);
    password_label->setText(SIGN_UP_PASSWORD);
    password_label->setFont(menu_font);
    password_label->setStyleSheet("color:#ece6ac;");
    repeat_password_label = new QLabel(this);
    repeat_password_label->setText(SIGN_UP_PASSWORD2);
    repeat_password_label->setStyleSheet("font: 10pt \"Georgia\"; color:#dbd8ca;");
    // Sign up form
    sign_up_form = new QFormLayout;
    sign_up_form->addRow(pseudo_label, pseudo);
    sign_up_form->addRow(password_label, password);
    sign_up_form->addRow(repeat_password_label, repeat_password);
    sign_up_form->setSpacing(10);
    // Catching Enter key
    installEventFilter(this);
    // Adding widgets to the layout
    v_layout->addWidget(sign_up_title);
    v_layout->addLayout(sign_up_form);
    // Buttons
    setNavButtons();
    // Focus on the first field
    pseudo->setFocusPolicy(Qt::StrongFocus);
    pseudo->setFocus();
}

void SignUpWindow::setNavButtons()
{
    sign_up_btn = new QPushButton(SIGN_UP, this);
    back_btn = new QPushButton(CANCEL, this);
    sign_up_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    back_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    sign_up_btn->setFont(menu_font);
    back_btn->setFont(menu_font);
    const QSize BUTTON_SIZE = QSize(250, 42);
    sign_up_btn->setMinimumSize(BUTTON_SIZE);
    back_btn->setMinimumSize(BUTTON_SIZE);
    h_layout_nav_buttons = new QHBoxLayout;
    h_layout_nav_buttons->setSpacing(15);
    h_layout_nav_buttons->addWidget(sign_up_btn);
    h_layout_nav_buttons->addWidget(back_btn);
    v_layout->addLayout(h_layout_nav_buttons);
    QObject::connect(sign_up_btn, SIGNAL(clicked()), this, SLOT(register_user()));
    QObject::connect(back_btn, SIGNAL(clicked()), this, SLOT(comeBack()));
}

void SignUpWindow::register_user()
{
    if(!pseudo->text().isEmpty() && !password->text().isEmpty() && !repeat_password->text().isEmpty())
    {
        if(repeat_password->text().toStdString() == password->text().toStdString()) {
            char buffer[MAXBUFSIZE];
            std::string sender = pseudo->text().toStdString();
            sender += MSG_SEPARATOR;
            sender += password->text().toStdString();
            sending_T(this->player->getSocket(), prepForSending(210, sender));
            memset(buffer, '\0', MAXBUFSIZE);
            if(receiving_T(this->player->getSocket(), buffer)) {
                std::string message = buffer;
                message = message.substr(3, message.size());
                if(message == "true") {
                    NotificationWindow *msg = new NotificationWindow;
                    showSuccessInput(msg);
                }
                else {
                    NotificationWindow *msg = new NotificationWindow;
                    showErrorInput(msg, SIGN_UP_PSEUDO_ERROR);
                }
            }
            else emit serverDisconnected();
        }
        else {
            NotificationWindow *msg = new NotificationWindow;
            showErrorInput(msg, SIGN_UP_PASSWORD_ERROR);
        }
    }
}

void SignUpWindow::comeBack()
{
    QPoint current_pos = pos();
    closeWindow();
    FirstWindow *window = new FirstWindow(this->player, current_pos);
    window->show();
}

void SignUpWindow::showSuccessInput(NotificationWindow *msg)
{
    msg->setText(SIGN_UP_SUCCESS);
    msg->setWindowTitle(SIGN_UP_SUCCESS_TITLE);
    msg->exec();
    QPoint current_pos = pos();
    closeWindow();
    LoginWindow *login_window = new LoginWindow(this->player, current_pos);
    login_window->show();
}

void SignUpWindow::showErrorInput(NotificationWindow *msg, QString cause)
{
    msg->setText(cause);
    msg->setWindowTitle(ERROR_TITLE);
    msg->exec();
}

bool SignUpWindow::eventFilter(QObject* obj, QEvent* event) // Catching Enter key
{
    if (event->type()==QEvent::KeyPress)
    {
        auto key = dynamic_cast<QKeyEvent*>(event);
        if ((key->key()==Qt::Key_Enter) || (key->key()==Qt::Key_Return))
        {
            register_user();
        }
    }
    else QObject::eventFilter(obj, event);
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// LOG IN WINDOW
////////////////////////////////////////////////////////////////////////////////

LoginWindow::LoginWindow(Player* player, QPoint current_pos) : MenuWindow(player)
{
    move(current_pos);
    setBackground("resources/bg_login_signup.jpg");
    v_layout->setContentsMargins(140, 170, 140, 200);
    v_layout->setSpacing(50);
    login_title = new QLabel(this);
    login_title->setText(SIGN_IN_TITLE);
    login_title->setFont(menu_font);
    login_title->setStyleSheet("color:#ece6ac;");
    // Fields
    pseudo = new SignLogLine;
    password = new SignLogLine;
    password->setEchoMode(QLineEdit::Password);
    pseudo_label = new QLabel(this);
    pseudo_label->setText(SIGN_IN_PSEUDO);
    pseudo_label->setFont(menu_font);
    pseudo_label->setStyleSheet("color:#ece6ac;");
    password_label = new QLabel(this);
    password_label->setText(SIGN_IN_PASSWORD);
    password_label->setFont(menu_font);
    password_label->setStyleSheet("color:#ece6ac;");
    // Log in form
    login_form = new QFormLayout;
    login_form->addRow(pseudo_label, pseudo);
    login_form->addRow(password_label, password);
    login_form->setSpacing(10);
    // Catching Enter key
    installEventFilter(this);
    // Adding widgets to the layout
    v_layout->addWidget(login_title);
    v_layout->addLayout(login_form);
    // Buttons
    setNavButtons();
    // Focus on the first field
    pseudo->setFocusPolicy(Qt::StrongFocus);
    pseudo->setFocus();
}

void LoginWindow::setNavButtons()
{
    // Button
    login_btn = new QPushButton(SIGN_IN, this);
    back_btn = new QPushButton(CANCEL, this);
    login_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    back_btn->setStyleSheet("QPushButton {background-image: url(resources/btn.png); background-repeat:repeat-x; color: #ece6ac; border: none;} QPushButton:hover {background-image: url(resources/btn_hover.png); border: none;}");
    login_btn->setFont(menu_font);
    back_btn->setFont(menu_font);
    const QSize BUTTON_SIZE = QSize(250, 42);
    login_btn->setMinimumSize(BUTTON_SIZE);
    back_btn->setMinimumSize(BUTTON_SIZE);
    h_layout_nav_buttons = new QHBoxLayout;
    h_layout_nav_buttons->setSpacing(15);
    h_layout_nav_buttons->addWidget(login_btn);
    h_layout_nav_buttons->addWidget(back_btn);
    v_layout->addLayout(h_layout_nav_buttons);
    QObject::connect(login_btn, SIGNAL(clicked()), this, SLOT(login_user()));
    QObject::connect(back_btn, SIGNAL(clicked()), this, SLOT(comeBack()));
}

void LoginWindow::login_user()
{
    if(!pseudo->text().isEmpty() && !password->text().isEmpty()) {
        char buffer[MAXBUFSIZE] = "";
        std::string sender = pseudo->text().toStdString();
        sender += MSG_SEPARATOR;
        sender += password->text().toStdString();
        sending_T(this->player->getSocket(), prepForSending(220, sender));
        if(receiving_T(this->player->getSocket(), buffer)) {
            std::string message = buffer;
            message = message.substr(3, message.size());
            if(message == "true") {
                sleep(1); // Avoiding send pipe error
                QPoint current_pos = pos();
                closeWindow();
                this->player->setUsername(pseudo->text().toStdString());
                sending(this->player->getChatSocket(), this->player->getUsername());
                if(!(receiving_T(this->player->getChatSocket(), buffer))) {emit serverDisconnected();}
                std::thread handle_thread(&MenuWindow::chatThread, this);
                handle_thread.detach();
                auto chat_window = new ChatWindow(this->player);
                chat_window->show();
                chat_window->close();
                QObject::connect(this, SIGNAL(receivedChat(QString, QString)), chat_window, SLOT(receivedMessage(QString, QString)));
                QObject::connect(this, SIGNAL(refreshFriends(QString)), chat_window, SLOT(refreshFriends(QString)));
                MainMenuWindow *main_menu_window = new MainMenuWindow(this->player, current_pos, true);
                main_menu_window->show();
            }
            else if (message == "connected") {
                NotificationWindow *msg = new NotificationWindow;
                msg->setText(SIGN_IN_ALREADY_CONNECTED);
                msg->setWindowTitle(ERROR_TITLE);
                msg->exec();
            }
            else {
                NotificationWindow *msg = new NotificationWindow;
                msg->setText(SIGN_IN_ERROR);
                msg->setWindowTitle(ERROR_TITLE);
                msg->exec();
            }
        }
        else emit serverDisconnected();
    }
}

void LoginWindow::comeBack()
{
    QPoint current_pos = pos();
    closeWindow();
    FirstWindow *window = new FirstWindow(this->player, current_pos);
    window->show();
}

bool LoginWindow::eventFilter(QObject* obj, QEvent* event) // Catching Enter key
{
    if (event->type()==QEvent::KeyPress)
    {
        auto key = dynamic_cast<QKeyEvent*>(event);
        if ((key->key()==Qt::Key_Enter) || (key->key()==Qt::Key_Return))
        {
            login_user();
        }
    }
    return QObject::eventFilter(obj, event);
}
