// INFO-F209 - Groupe 5 - ChatWindow.cpp

#include "ChatWindow.hpp"

ChatWindow::ChatWindow(Player* player) : MenuWindow(player)
{
    setObjectName("chat_window");
    setFixedSize(572, 600);
    close_window_btn->setGeometry(545, 12, 14, 14);
    setBackground("resources/bg_chat.jpg");
    v_layout->setContentsMargins(20, 17, 20, 20);
    setStyleSheet("QTextEdit {font: 12pt \"Georgia\";"
                  "text-align:justify;"
                  "color:#dbd8ca;"
                  "background-color: #140a0a;"
                  "border:1px solid #301e1e;"
                  "padding: 5px;}"
                "QScrollBar:vertical {"
                "background:transparent;"
                "width:10px;    "
                "margin: 0px 0px 0px 0px;"
                "}"
                "QScrollBar::handle:vertical {"
                "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                "stop: 0 rgb(48,30,30), stop: 0.5 rgb(48,30,30), stop:1 rgb(48,30,30));"
                "min-height: 0px;"
                "}"
                "QScrollBar::add-line:vertical {"
                "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                "stop: 0 rgb(48,30,30), stop: 0.5 rgb(48,30,30),  stop:1 rgb(48,30,30));"
                "height: 0px;"
                "subcontrol-position: bottom;"
                "subcontrol-origin: margin;"
                "}"
                "QScrollBar::sub-line:vertical {"
                "background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                "stop: 0  rgb(48,30,30), stop: 0.5 rgb(48,30,30),  stop:1 rgb(48,30,30));"
                "height: 0 px;"
                "subcontrol-position: top;"
                "subcontrol-origin: margin;"
                "}");
    // DISCUSSION PANEL
    discuss_with_title = new QLabel(this);
    discuss_with_title->setText(CHAT_CLICK);
    discuss_with_title->setFont(menu_font);
    discuss_with_title->setStyleSheet("color:#ece6ac;");
    transcript_container = new QTextEdit(this);
    transcript_container->setReadOnly(true);
    transcript_container->setMinimumSize(350, 400);
    transcript_container->setHtml(transcript[0]);
    text_to_send = new SendBox(350, 80);
    send_msg_btn = new QPushButton(SEND, this);
    send_msg_btn->setFont(menu_font);
    send_msg_btn->setMinimumHeight(80);
    send_msg_btn->setStyleSheet("QPushButton {color:#dbd8ca; background-color: #301e1e; padding: 5px;} QPushButton:hover {background-color: #464240;}");
    // CONNECTED FRIENDS LIST
    connected_friends_title = new QLabel(this);
    connected_friends_title->setText(CHAT_CONNECTED_USERS);
    connected_friends_title->setFont(menu_font);
    connected_friends_title->setStyleSheet("color:#ece6ac;");
    connected_friends_tab = new QTableWidget(this);
    connected_friends_tab->setColumnCount(1);
    connected_friends_tab->setRowCount(0);
    connected_friends_tab->horizontalHeader()->setVisible(false);
    connected_friends_tab->verticalHeader()->setVisible(false);
    connected_friends_tab->setShowGrid(false);
    connected_friends_tab->setEditTriggers(QAbstractItemView::NoEditTriggers);
    connected_friends_tab->setSelectionBehavior(QAbstractItemView::SelectItems);
    connected_friends_tab->setSelectionMode(QAbstractItemView::SingleSelection);
    connected_friends_tab->setColumnWidth(0, 150);
    QObject::connect(connected_friends_tab, SIGNAL(itemSelectionChanged()), this, SLOT(changeTranscript()));
    QObject::connect(send_msg_btn, SIGNAL(clicked()), this, SLOT(send()));
    QObject::connect(text_to_send, SIGNAL(enterPressed()), this, SLOT(send()));
    connected_friends_tab->setStyleSheet(
            "QTabBar::tab:selected {color:white;}"
            "QTableWidget {background: transparent; color:#dbd8ca; font: 12pt \"Georgia\";}"
            "QHeaderView::section {background-color: #301e1e;}"
            "QHeaderView {background:transparent;}"
            "QTableCornerButton::section {background: transparent;}"
            "QTableView {selection-background-color: #301e1e;}"
    );
    std::thread refresh(&ChatWindow::sendMessageFriends, this);
    refresh.detach();
    // ADDING WIDGETS TO THE LAYOUTS
    g_layout = new QGridLayout();
    g_layout->addWidget(discuss_with_title, 0, 0, 1, 1);
    g_layout->addWidget(transcript_container, 1, 0, 1, 1);
    g_layout->addWidget(text_to_send, 2, 0, 1, 1);
    g_layout->addWidget(send_msg_btn, 2, 1, 1, 2);
    g_layout->addWidget(connected_friends_title, 0, 2, 1, 1);
    g_layout->addWidget(connected_friends_tab, 1, 2, 1, 1);
    v_layout->addLayout(g_layout);
}

void ChatWindow::changeTranscript()
{
    if(connected_friends_tab->selectedItems().at(0)) {
        inChatWith = extractName(connected_friends_tab->selectedItems().at(0)->text()).toStdString();
        discuss_with_title->setText("Discussion avec " + QString::fromStdString(inChatWith));
        connected_friends_tab->selectedItems().at(0)->setText(QString::fromStdString(inChatWith));
        for(unsigned long i = 0; i < friends.size(); ++i) {
            if(friends.at(i) == inChatWith) {
                transcript_container->setHtml(transcript[i+1]);
                break;
            }
        }

    }
}

void ChatWindow::send()
{
    // récupérer le contenu d'un QTextEdit via toPlainText() (renvoie un QString)
    if(inChatWith != "no" && !text_to_send->toPlainText().isEmpty()) {
        connected_friends_tab->selectedItems().at(0)->setText(QString::fromStdString(inChatWith));
        std::string send_message = inChatWith + MSG_SEPARATOR + text_to_send->toPlainText().toStdString();
        sending_T(this->player->getChatSocket(), prepForSending(350, send_message));
        for(unsigned long i = 0; i < friends.size(); ++i) {
            if(friends.at(i) == inChatWith) {
                transcript[i+1] = transcript[i+1] + "<br>" + "[" + QString::fromStdString(this->player->getUsername()) + "] " + text_to_send->toPlainText();
                transcript_container->setHtml(transcript[i+1]);
                break;
            }
        }
        QSound::play("resources/sounds/message_out.wav");
    }
    text_to_send->clear();
}

void ChatWindow::receivedMessage(QString who, QString message)
{
    int count = 0;
    unsigned long pos = 0;
    auto tab = connected_friends_tab->findItems(who, Qt::MatchStartsWith);
    pos = tab.at(0)->text().toStdString().find(' ');
    if(pos != std::string::npos) {
        if(extractName(tab.at(0)->text()) == who) {
            count = std::stoi(tab.at(0)->text().toStdString().substr(pos+2, 1));
        }
    }
    count++;
    tab.at(0)->setText(who + " (" + QString::number(count) + ")");
    for(unsigned long i = 0; i < friends.size(); ++i) {
        if(friends.at(i) == who.toStdString()) {
            transcript[i+1] = transcript[i+1] + "<br>" + "[" + who + "] " + message;
            if(QString::fromStdString(inChatWith) == who) transcript_container->setHtml(transcript[i+1]);
            break;
        }
    }
    transcript_container->verticalScrollBar()->setValue(transcript_container->verticalScrollBar()->maximum());
    QSound::play("resources/sounds/message_in.wav");
}

void ChatWindow::refreshFriends(QString msg)
{
    std::string message = msg.toStdString();
    QString friend_name;
    unsigned long pos;
    int count = 0;
    friends.clear();
    while(!message.empty()) {
        pos = message.find(MSG_SEPARATOR);
        friend_name = QString::fromStdString(message.substr(0, pos));
        friends.emplace_back(message.substr(0, pos));
        if(connected_friends_tab->item(count, 0)) {
            if(extractName(connected_friends_tab->item(count, 0)->text()) != friend_name) {
                transcript[count+1] = "";
                connected_friends_tab->setItem(count, 0, new QTableWidgetItem(friend_name));
            }
        }
        else connected_friends_tab->setItem(count, 0, new QTableWidgetItem(friend_name));
        message = message.substr(pos+ 1, message.size());
        count++;
    }
    auto search = std::find(friends.begin(), friends.end(), discuss_with_title->text().toStdString().substr(16, discuss_with_title->text().toStdString().size()));
    if(search == friends.end()) {
        discuss_with_title->setText(CHAT_CLICK);
        transcript_container->setHtml(transcript[0]);
        inChatWith = "no";
    }
    connected_friends_tab->setRowCount(count);
}

void ChatWindow::sendMessageFriends()
{
    while(this->player->getChatSocket() != 0) {
        sending_T(this->player->getChatSocket(), prepForSending(320, ""));
        sleep(2);
    }
}

QString ChatWindow::extractName(QString name)
{
    std::string string = name.toStdString();
    unsigned long pos = 0;
    pos = string.find(' ');
    if(pos != std::string::npos) {
        return QString::fromStdString(string.substr(0, pos));
    }
    return name;
}
