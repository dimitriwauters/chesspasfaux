#include <sstream>
#include <iomanip>
#include "ChessSquare.hpp"

ChessSquare::ChessSquare(int x, int y, int color) : QWidget(), color(color), row(x), col(y)
{
    picture = new QLabel(this);
    setBackground(false);
    text = new QLabel(this);
    text->setText("");
    picture->setMinimumSize(52, 52);
    picture->setMaximumSize(52, 52);
    text->setMinimumSize(52, 52);
    text->setMaximumSize(52, 52);

    QFontDatabase::addApplicationFont("resources/fonts/Alpha.ttf");
    pieces_font = QFont("Chess Alpha",30,QFont::Normal);
    text->setFont(pieces_font);
    text->setAlignment(Qt::AlignCenter);
}

ChessSquare::~ChessSquare() = default;

void ChessSquare::setPiece(int piece_nbr, int color)
{
    piece = piece_nbr;
    piece_color = color;
}

std::pair<int, int> ChessSquare::getPiece()
{
    return {piece, piece_color};
}

std::pair<int, int> ChessSquare::getCoord()
{
    return std::make_pair(row, col);
}

void ChessSquare::display()
{
    QString old_text = "";
    if(text->size().isEmpty()) old_text = text->text();
    if (piece != NO_DEFINED)
    {
        switch (piece)
        {
            case PAWN_NBR:
                text->setText("o");
                break;
            case ROOK_NBR:
                text->setText("t");
                break;
            case KING_NBR:
                text->setText("l");
                break;
            case QUEEN_NBR:
                text->setText("w");
                break;
            case KNIGHT_NBR:
                text->setText("j");
                break;
            case BISHOP_NBR:
                text->setText("n");
                break;
            case PIECE_HIDDEN:
                text->setText("?");
                break;
        }
    }
    else {
        text->setText("");
    }
    std::string color_str;
    if(piece_color != NO_DEFINED && old_text != text->text()) {
        QPalette palette = text->palette();
        if(piece_color) palette.setColor(QPalette::WindowText, QColor(255, 255, 255));
        else palette.setColor(QPalette::WindowText, QColor(35, 35, 35));
        text->setPalette(palette);
    }
    setBackground(false);
}

void ChessSquare::setBackground(bool with_color, QString bg)
{
    picture->clear();
    if(!with_color) {
        if(color) picture->setPixmap(QPixmap("resources/dark_square.jpg"));
        else picture->setPixmap(QPixmap("resources/light_square.jpg"));
    }
    else {
        QPalette palette = text->palette();
        if (text->text() == "?") bg = "resources/dark_chess_square.png";
        palette.setBrush(QPalette::Window, QBrush(QPixmap(bg)));
        //int opacity = static_cast<int>(a*255);
        //palette.setColor(QPalette::Window, QColor(r, g, b, opacity));
        picture->setAutoFillBackground(true);
        picture->setPalette(palette);
    }
}

void ChessSquare::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton)
    {
        event->accept();
    }
}
