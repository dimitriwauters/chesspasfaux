// INFO-F209 - Groupe 5 - clientGUI.cpp

#include "clientGUI.hpp"

int main(int argc, char *argv[])
{
    // Establishing client-server connection
    char* hostname;
    SockaddrIn server_addr;
    SockaddrIn chat_addr;
    hostname = argv[argc-1];
    setHost(hostname, &server_addr, PORT_GAME);
    if ((socketfd = socket(PF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("Client: socket");
        exit(1);
    }
    connecting(&socketfd, &server_addr);
    setHost(hostname, &chat_addr, PORT_CHAT);
    if ((chatfd = socket(PF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("Client: socket");
        exit(1);
    }
    connecting(&chatfd, &chat_addr);
    std::string gamemode = "unknown";
    std::vector<std::string> pieces;
    Player* white_player = new Player(socketfd, chatfd);
    // GUI (using Qt)
    QApplication app(argc, argv);
    app.setWindowIcon(QIcon("resources/icon.png"));
    FirstWindow *window = new FirstWindow(white_player);
    window->show();
    app.exec();
}
