#include "BoardClientGUI.hpp"

template <typename B>
BoardClientGUI<B>::BoardClientGUI(QGridLayout* chessboard) : B(), chessboard(chessboard)
{
    for (int i = 0; i < MAX_COLS; i++)
    {
        for (int j = 0; j < MAX_ROWS; j++)
        {
            square[i][j] = new ChessSquare(i, j, (i+j)%2);
            square[i][j]->setMinimumSize(52, 52);
            square[i][j]->setMaximumSize(52, 52);
            chessboard->addWidget(square[i][j], i, j, 1, 1);
            square[i][j]->display();
        }
    }
}

template <typename B>
BoardClientGUI<B>::~BoardClientGUI() = default;

template <typename B>
ChessSquare* BoardClientGUI<B>::getSquare(std::pair<int, int> coord)
{
    if(this->isInRange(coord)) {
        return square[coord.first][coord.second];
    }
    return nullptr;
}

template <typename B>
void BoardClientGUI<B>::setSelectedSquare(ChessSquare* square)
{
    selectedSquare = square;
}

template <typename B>
ChessSquare* BoardClientGUI<B>::getSelectedSquare()
{
    return selectedSquare;
}

template <typename B>
void BoardClientGUI<B>::refreshDisplay()
{
    for (int i = 0; i < MAX_COLS; i++) {
        for (int j = 0; j < MAX_ROWS; j++) {
            square[i][j]->display();
        }
    }
}

template <typename B>
bool BoardClientGUI<B>::addPiece(int piece, std::pair<int, int> coord, bool color)
{
    if(B::addPiece(piece, coord, color)) {
        square[coord.first][coord.second]->setPiece(piece, color);
        square[coord.first][coord.second]->display();
        return true;
    }
    return false;
}

template <typename B>
bool BoardClientGUI<B>::removePiece(std::pair<int, int> coord) {
    if(B::removePiece(coord)) {
        square[coord.first][coord.second]->setPiece(NO_DEFINED, NO_DEFINED);
        square[coord.first][coord.second]->display();
        return true;
    }
    return false;
}

template <typename B>
void BoardClientGUI<B>::setNewPosition(std::pair<int, int> src_pos, std::pair<int, int> dest_pos) {
    B::setNewPosition(src_pos, dest_pos);
    if(!this->isVirtualMove()) {
        std::pair<int, int> piece = square[src_pos.first][src_pos.second]->getPiece();
        square[dest_pos.first][dest_pos.second]->setPiece(piece.first, piece.second);
        square[src_pos.first][src_pos.second]->setPiece(NO_DEFINED, NO_DEFINED);
        refreshDisplay();
    }
}
