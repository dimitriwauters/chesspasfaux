// INFO-F209 - Groupe 5 - Player.cpp

#include "Player.hpp"

Player::Player() 
{}

Player::Player(int socket) : m_socket(socket) 
{}

Player::Player(int socket, int chat) : m_socket(socket), _chatSocket(chat)
{}

Player::Player(std::string username, bool t_color, int socket) : m_color(t_color), m_username(std::move(username)), m_socket(socket) 
{}

Player::Player(bool t_color, std::string username) : m_color(t_color), m_username(std::move(username))
{}

Player::~Player() = default;

//////////////////////////////////////////////////////////////////
// ATTRIBUTES SETTERS & GETTERS
//////////////////////////////////////////////////////////////////

void Player::setColor(bool color) {
// Set color of the player.
    m_color = color;
}

bool Player::getColor() const {
// Get color of the player.
    return m_color;
}

void Player::setUsername(std::string username) {
// Set the pseudo of the player.
    m_username = std::move(username);
}

std::string Player::getUsername() const {
// Get the pseudo of the player.
    return m_username;
}

void Player::setSocket(int socket) {
// Set socket, to the server, of the player.
    m_socket = socket;
}

int Player::getSocket() const {
// Get socket, to the server, of the player.
    return m_socket;
}

bool Player::isWinner() const {
// Get player winner.
	return winner;
}

void Player::hasWon() {
// Set the player has winner.
	winner = true;
}

unsigned Player::getELO() const {
// Get ELO of the player.
    return elo;
}

void Player::setELO(unsigned points) {
// Set ELO of the player.
    elo = points;
}

void Player::resetGamePlayer() {
// Set the player has not winner.
    winner = false;
}

//////////////////////////////////////////////////////////////////
// FRIENDS
//////////////////////////////////////////////////////////////////

std::vector<std::string>& Player::getFriendsUsernames() {
// Get a vector with the pseudos of the friends.
	return friendsUsernames;
}

void Player::pushInFriendsUsernames(char* username){
// Add a friend's name.
    friendsUsernames.emplace_back(username);
}

void Player::delFriend(char* username) {
// Delete a friend's name.
    auto search = std::find(friendsUsernames.begin(), friendsUsernames.end(), username);
    if(search != friendsUsernames.end()) {
        friendsUsernames.erase(search);
    }
}

std::vector<std::string>& Player::getRequestFriend() {
// Return a vector with the friend's resquests.
    return requestFriend;
}

void Player::pushRequestFriend(std::string username) {
// Add a friend's request.
    requestFriend.emplace_back(username);
}

void Player::delRequestFriend(char* username) {
// Delete a friend's request.
    auto search = std::find(requestFriend.begin(), requestFriend.end(), username);
    if(search != requestFriend.end()) {
        requestFriend.erase(search);
    }
}

//////////////////////////////////////////////////////////////////
// CHAT
//////////////////////////////////////////////////////////////////

Player* Player::getInChat() {
// Get the friends connected in the chat.
    return inChat;
}

void Player::setInChat(Player* status) {
// Set the player connected to the chat.
    inChat = status;
}

int Player::getChatSocket() const {
// Get chat socket.
    return _chatSocket;
}

void Player::setChatSocket(int socket) {
// Set chat socket.
    _chatSocket = socket;
}
