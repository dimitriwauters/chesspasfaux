# Groupe 5

## Structure de l'arborescence

- Les fichiers relatifs au jeu sont dans 'Game'
  - Les fichiers relatifs à la console se trouvent dans "Console"
  - Les fichiers permettant de construire l'interface graphique se trouvent dans "GUI"
- Et les fichiers relatifs au SRD se trouvent dans le dossier ... SRD

## Dépendances

Il faut avoir installé les dépendances de Qt 5.9.5 (version installée au NO3) pour que l'interface graphique fonctionne : dans un terminal, tapez `sudo apt-get install build-essential libgl1-mesa-dev mesa-common-dev` ou `sudo apt-get install qtmultimedia5-dev` (au minimum).

## Lancement du jeu

 
- Afin de jouer à notre jeu, tapez les commandes suivantes dans des terminaux (au niveau du dossier 'Game') :
	- `make`
	- L'ordinateur faisant office de serveur : `./server`
	- À partir d'un autre terminal, le joueur pourra se connecter au serveur soit via `./console 127.0.0.1`, soit via `./gui 127.0.0.1`
	- Have fun!
- N.B.: L'adresse IP change si le serveur est lancé depuis un autre ordinateur connecté en réseau.

   